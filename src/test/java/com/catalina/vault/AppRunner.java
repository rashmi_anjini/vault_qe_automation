package com.catalina.vault;

import static org.junit.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.catalina.vault.pagefactory.BaseClass;
import com.catalina.vault.steps.SharedResource;
import com.catalina.vault.utils.FileUtils;
import com.catalina.vault.utils.TargetProcess;
import com.cucumber.listener.Reporter;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import com.itextpdf.text.log.SysoCounter;

import cucumber.api.CucumberOptions;
//import cucumber.api.junit.Cucumber;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(retryCount = 1)
@CucumberOptions(features = {

		 //"src//resource//feature//AudienceSegment.feature",
		"src/resource/feature/USADistributionCapping.feature",
//		"src/resource/feature/FranceRetailerOne.feature",
//		 "src/resource/feature/FranceRetailerTwo.feature",
//		// "src//resource//feature//France_AudienceSegment.feature",

		// "src/resource/feature/ItalyRetailerOne.feature",
		// "src/resource/feature/ItalyRetailerTwo.feature",
		// "src/resource/feature/ItalyRetailerThree.feature",
		// "src//resource//feature//Italy_Audience.feature",

		// "src/resource/feature/UKRetailer.feature",
		// "src/resource/feature/UKRetailerOne.feature",
		// "src/resource/feature/UKRetailerThree.feature",

		// "src/resource/feature/USAEORD.feature",
		// "src/resource/feature/USARetailer.feature",
		// "src/resource/feature/USAAdvertisement.feature",
		// "src/resource/feature/USAManufacturer.feature",
		// "src/resource/feature/USAManufacturerNewcases.feature",

		//"src/resource/feature/ProductCarried.feature"

}, glue = { "com.catalina.vault.steps" }, plugin = {
		"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" })

public class AppRunner {
	/**
	 * Rigorous Test :-)
	 */
	private static SharedResource sharedResource;
	private static FileUtils fileUtils = new FileUtils();
	private static final TargetProcess tp = new TargetProcess();
	public int testPlanRunID = 0;
	private static final int testPlanId = 42466;
	static String buildName = "DemoBuild";
	public static int buildId = 0;
	private static int propFileCounter = 1;

	// 3 region array is been stored in Array, random number ID 11 digit is been
	// mentioned and generated

	@BeforeClass
	// public static void createBuild() {
	// try {
	//
	// if (buildName == null) {
	// buildName = "DemoBuild";
	// }
	//
	// System.out.println(testPlanId + " " + buildName);
	// tp.getBuild(testPlanId, buildName);
	// buildId = tp.getBuildId();
	// System.out.println("Build Number:" + buildId);
	//
	// } catch (Exception e) {
	// System.out.println("Build is Not Created");
	// }
	//
	// }
	// @BeforeClass
	public static void createFile() throws IOException {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd").format(new Date());
		System.out.println(timeStamp);
		String fileName = "printedPromotions_" + timeStamp + "_" + String.valueOf(propFileCounter);

		// String path= System.getProperty()
		File file = new File("prp/Pause//" + fileName + ".properties");
		System.out.println(file.getAbsolutePath());

		// file.createNewFile();
		FileOutputStream fis = new FileOutputStream(file);
		Properties prop = new Properties();
		try {
			prop.store(fis, "Latest file created");
			System.out.println(fis);
			fis.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		SharedResource.setPropertyFilename(fileName);
		sharedResource = new SharedResource();
		propFileCounter++;
		/*
		 * created a method to create a folder along with 3 files, we have
		 * already files related Util class is there once done with this process
		 * we can please this method in FileUtils class
		 */
		fileUtils.createRegionsAudienceListFile();
		// createRegionsCouponCodeFile(); // we are calling this method -

	}

	@Test
	public void shouldAnswerWithTrue() {
		assertTrue(true);
	}

	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File("./src/resource/extent-config.xml"));
	}

}