package com.catalina.vault;


import static org.junit.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.catalina.vault.pagefactory.BaseClass;
import com.catalina.vault.steps.Campaign;
import com.catalina.vault.steps.SharedResource;
import com.catalina.vault.utils.FileUtils;
import com.catalina.vault.utils.PropertiesFile;
import com.catalina.vault.utils.TargetProcess;
import com.cucumber.listener.Reporter;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import com.itextpdf.text.log.SysoCounter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
//@ExtendedCucumberOptions(retryCount = 1)
@CucumberOptions(features = {
		
		
//		"src//resource//feature//FranceAudienceSegment.feature",
		"src/resource/feature/FranceRetailerOne.feature",	
		"src/resource/feature/FranceRetailerTwo.feature",
		"src//resource//feature//France_Audience.feature",

}, glue = { "com.catalina.vault.steps" }, plugin = {
		"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html","rerun:rerun/failed_scenarios.txt"  },tags={"@SmokeTest"})

public class FranceRunner {
	/**
	 * Rigorous Test :-)
	 */
	private static SharedResource sharedResource;
	private static FileUtils fileUtils = new FileUtils();
	private static final TargetProcess tp = new TargetProcess();
	public int testPlanRunID = 0;
	private static final int testPlanId = 42466;
	static String buildName = "DemoBuild";
	public static int buildId = 0;
	private static int propFileCounter = 1;
	private static PropertiesFile propertiesFile;
	
	// 3 region array is been stored in Array, random number ID 11 digit is been
	// mentioned and generated

	@BeforeClass
//	 public static void createBuild() {
//	 try {
//	
//	 if (buildName == null) {
//	 buildName = "DemoBuild";
//	 }
	//
	// System.out.println(testPlanId + " " + buildName);
	// tp.getBuild(testPlanId, buildName);
	// buildId = tp.getBuildId();
	// System.out.println("Build Number:" + buildId);
	//
	// } catch (Exception e) {
	// System.out.println("Build is Not Created");
	// }
	//
//		Handler globalExceptionHandler = new Handler();
//	    Thread.setDefaultUncaughtExceptionHandler(globalExceptionHandler);
//	 }
	// @BeforeClass
	public static void createFile() throws IOException {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd_HH.mm").format(new Date());
		System.out.println(timeStamp);
		
		String fileName = "printedPromotions_" + timeStamp ;

		// String path= System.getProperty()
		File file = new File("//test_storage1/spliceshare/VaultPromotionPause//" + fileName + ".properties");
		System.out.println(file.getAbsolutePath());

		// file.createNewFile();
		FileOutputStream fis = new FileOutputStream(file);
		Properties prop = new Properties();
		try {
			prop.store(fis, "Latest file created");
			System.out.println(fis);
			fis.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		SharedResource.setPropertyFilename(fileName);
		sharedResource = new SharedResource();
		propertiesFile = new PropertiesFile("environment");
		String key= "propFileCounter";
		propertiesFile.SetProperty(key, String.valueOf(propFileCounter+1));
		
		/*
		 * created a method to create a folder along with 3 files, we have
		 * already files related Util class is there once done with this process
		 * we can please this method in FileUtils class
		 */
		fileUtils.createRegionsAudienceListFile();
		// createRegionsCouponCodeFile(); // we are calling this method -

	}

	@Test
	public void shouldAnswerWithTrue() {
		
		assertTrue(true);
	}
	
	@AfterClass
	public static void writeExtentReport() {
		
		Reporter.loadXMLConfig(new File("./src/resource/extent-config.xml"));
	}

}

