 Feature: Create Promotion for ItalyRetailer Three
 Scenario Outline: ITA: Coupon - Dept. purchase | total units >=4 + total order | total spend 
   Given : Url of application Navigate to webpage
   Then : Fill credentails
   Then : Click on Login
   Then : Change region to "Italy"
	 Then : Search for the Campaign name
   When : Campagin created Then create Ad Group with "<TestCaseName>" 
	 Then : Create ad using Ad Galary with retail for Italy
   Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\Popcorn.jpg" 
   Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>" 
   Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
   Then : Publish "<TestCaseName>"
	 Then : Verify DMP Promotion on server "AZURE" and "ITA" 
    
    Examples: 
     | Condition | quantity |  TestCaseName                                          | Barcode     |	Conditionorder	|	quantityorder	|	departmentid	|
     | >=   |   4 |  Department purchase total units >=4 and total order-total spend | 85214796301 |	>=	|	1	|	4	|
      
      
 Scenario Outline: ITA: Coupon - Dept. purchase | total units -> qty + Dept. purchase | total units == 0
    Given : Url of application Navigate to webpage 
	  Then : Fill credentails 
	  Then : Click on Login 
		Then : Change region to "Italy"
		Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>" 
		Then : Create ad using Ad Galary with retail for Italy 
		Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\PotatoSamosa.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Add department id with secondrow "<Condition1>", "<quantity1>" and "<departmentid2>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

     Examples: 
     | Condition | quantity |  TestCaseName                                          | Barcode     |	Condition1	|	quantity1	|	departmentid |	departmentid2|
     | >=   |   1 |   Department purchase total units >=1 and total dept purchase ==0 | 85214796301 |	==	|	0	|	1|	7	| 
      
      
Scenario Outline: ITA: Coupon - Dept. purchase | total spend
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
	  Then : Click on Login 
		Then : Change region to "Italy"
		Then : Search for the Campaign name
 	  When : Campagin created Then create Ad Group with "<TestCaseName>" 
		Then : Create ad using Ad Galary with retail for Italy 
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\smiley2.jpg"
    Then : Add department id totalspend with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

    
    Examples: 
     | Condition | quantity | manualList | TestCaseName            | Barcode     |	
     | >=   |   2 |   834567031 | Department purchase total spend | 85214796301 |	
      
 

#Scenario Outline: ITA: Coupon - Audience segment | in + Custom parameters
    #Given : Url of application Navigate to webpage 
#		Then : Fill credentails 
#		Then : Click on Login 
#		Then : Change region to "Italy"
#		Then : Search for the Campaign name
  #	When : Campagin created Then create Ad Group with "<TestCaseName>" 
#		Then : Create ad using Ad Galary with retail for Italy 
#		Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\vegetablebasket.jpg"
    #Then : Add Audience Segment for retailer firstrow with "ODOULSITA" and "in"
    #Then : Add Custom Targeting with external id "Italy"
    #Then : Publish "<TestCaseName>"
    #Then : Verify DMP Promotion on server "AZURE" and "ITA"
#
   #
    #Examples: 
     #|  TestCaseName            | Barcode     |	
     #| Audience Segment and Custom Parameters | 85214796301 |	
     #
    

  Scenario Outline: ITA: Coupon - Catalina Redemption
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Italy"
		Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>" 
		Then : Create ad using Ad Galary with retail for Italy 
		Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\apple.jpg"
   Then : Add Catalina Redemption "Italy"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

   
    Examples: 
     |  TestCaseName            | Barcode     |	
     | Catalina Redemption | 85214796301 |	   
