Feature: Custom Parameter

@Regression
  Scenario Outline: Indesign1 Template
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "INDESIGN"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Indesign1 Template
    Then : Fill ad form for Indesign1 "Vault USA Template\59.jpg", "-1" and "5"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName       | Condition | quantity | Barcode                               |
      | Indesign1 Template | >=        |        1 | 85214796301 |

@Regression
  Scenario Outline: Indesign4 Template
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Indesign4 Template
    Then : Fill ad form for Indesign4 "-1" and "5"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName       | Condition | quantity | Barcode                               |
      | Indesign4 Template | >=        |        1 | 85214796301 |

@Regression
  Scenario Outline: Indesign2 Template
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Indesign2 Template
    Then : Fill ad form for Indesign2 "-1" and "5"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Audience Segment for with
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName       | Condition | quantity | Barcode                               |
      | Indesign2 Template | >=        |        1 | 85214796301 |
  
  @Regression   
  Scenario Outline: Custom Parameter with Item Purchase and Audience Segment
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login    
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Indesign3 Template
    Then : Fill ad form for Indesign3 Custom Param with "<country>","Vault USA Template\61.jpg", "-1" and "5"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Audience Segment for with
    Then : Add Custom Parameter
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName                                  | Condition | quantity | Barcode     | 
      | Indesign3 Template | >=        |        1 | 85214796301 | 