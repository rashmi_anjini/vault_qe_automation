Feature: Create Promotion for ItalyRetailer

  @SmokeTest @Regression
  Scenario Outline: ITA: Coupon - Item purchase | total units -> qty + Total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for Italy "RETAILER"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\Italyimage.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

    
    Examples: 
      | Condition | quantity | TestCaseName                  | Barcode     | Conditionorder | quantityorder | testCaseId | 
      | >=        |        1 | Item Purchase and total order | 85214796301 | >=             |             1 |      70693 |     

 
 @SmokeTest @Regression
  Scenario Outline: ITA: Coupon - Item purchase |total units -> qty with "Unlimited" coupon condition
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with unlimited "<Barcode>" and "Vault UI Template\Britannia.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 

   
    Examples: 
      | Condition | quantity | TestCaseName                                  | Barcode     | manualList | testCaseId | 
      | ==        |        1 | Item purchase with Unlimited coupon condition | 09841524701 |  834567003 |      70694 |     

 @Regression
  Scenario Outline: ITA: Coupon - Item purchase |total units -> qty + Payment method -> Check
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\Burgerimage.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Payment Method "<payment>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

  
    Examples: 
      | Condition | quantity | TestCaseName                           | Barcode     | manualList | payment | testCaseId |
      | =         |        1 | Item purchase and Payment method check | 85214796301 |  834567004 | Check   |      70695 |      

 @Regression
  Scenario Outline: ITA: Coupon - Item purchase| total units -> qty + User Identifier -> Loyality card
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\colddoffe.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add User Identifier
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"
    
 
    Examples: 
      | Condition | quantity | TestCaseName                                   | Barcode     | manualList | testCaseId | 
      | >=        |        1 | Item purchase and User identifier Loyalty card | 85214796301 |  834567008 |      70698 |      

  @Regression
  Scenario Outline: ITA: Coupon - Item purchase | total units == 1
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\cookies.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

   
    Examples: 
      | Condition | quantity | TestCaseName      | Barcode     | testCaseId | 
      | ==        |        1 | Item purchase ==1 | 85214796301 |      70699 |     

  	
  @Regression
  Scenario Outline: ITA: Coupon - Item purchase | total units between 2,00->3,00
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\crossents.jpg"
    Then : Add Targeting for retailer group with between "<Condition>" , "<quantity>" and "<quantity1>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue betweenQty "<testCaseId>"

   
    Examples: 
      | Condition | quantity | quantity1 | TestCaseName                              | Barcode     | testCaseId | 
      | between   |        2 |         3 | item purchase between units 2.00 and 3.00 | 85214796301 |      70701 |      
@Regression
  Scenario Outline: ITA: Coupon - Item purchase | total units between 15,00->20,00
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\cupcake.jpg"
    Then : Add Targeting for retailer group with between "<Condition>" , "<quantity>" and "<quantity1>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue betweenQty "<testCaseId>"

    
    Examples: 
      | Condition | quantity | quantity1 | TestCaseName                                | Barcode     | testCaseId | 
      | between   |       15 |        20 | item purchase between units 15.00 and 20.00 | 85214796301 |      70704 |      

  
  @Regression
  Scenario Outline: ITA: Coupon - Total order | total spend + Payment method | Foodstamp
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\donut image.jpg"
    Then : Add Targeting group with Total Order first "<Condition>" and "<value>"
    Then : Add Payment Method with Foodstamp
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

   
    Examples: 
      | TestCaseName                              | Barcode     | Condition | value | testCaseId | 
      | Total order  and payment method foodstamp | 85214796301 | >=        |     1 |      70715 |      
