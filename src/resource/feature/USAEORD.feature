Feature: EORD

@Regression
  Scenario Outline: EORD-NBIC distribution Rule
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "EORD"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form NBIC for USAretailer with "<Barcode>" and "Vault USA Template\33.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 

    Examples: 
      | TestCaseName |  Condition | quantity | Barcode     |testCaseId	|
      | NBIC         |  ==        |        1 | 85214796301 |68785|

  
@Regression
  Scenario Outline: EORD-Competitive Rule
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Enter EORD Competitve ad details name "<Barcode>" and "Vault USA Template\34.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 

    Examples: 
      | TestCaseName |  Condition | quantity | Barcode     |testCaseId	|
      | Competitive  |  ==        |        1 | 85214796301 |69077|

@SmokeTest @Regression
  Scenario Outline: EORD-CapPerSession distribution Rule
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form with CapsPerSession for USAretailer with "<Barcode>" and "Vault USA Template\35.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 

    Examples: 
      | TestCaseName  |  Condition | quantity | Barcode     |testCaseId	|
      | CapPerSession |==        |        1 | 85214796301 |68911|

@Regression
  Scenario Outline: EORD-Sensitive category distribution Rule
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Enter EORD Sensitive ad details name "<Barcode>" image "Vault USA Template\36.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

    Examples: 
      | TestCaseName | CampaignName      | Condition | quantity | Barcode     |testCaseId	|
      | Sensitive    | USA_EORD_Campaign | ==        |        1 | 85214796301 |68800|

@SmokeTest @Regression
  Scenario Outline: EORD-Unlimited distribution Rule
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Enter EORD Unlimited ad details name "<Barcode>" image "Vault USA Template\37.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 

    Examples: 
      | TestCaseName |  ExpirationDate | Condition | quantity | Barcode     |testCaseId	|
      | Unlimited distribution rule    |              17 | ==        |        1 | 85214796301 |68796|
  
  
@SmokeTest @Regression
  Scenario Outline: EORD-Promotion Allocation Rule
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Enter Retailer ad details name "<Barcode>" image "Vault USA Template\40.jpg" with rank "3" 
    Then : Add Targeting for itempurchase group with "<Condition>" and "1"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Go to created Ad Group
    Then : Create US ad using Ad Galary with retail
    Then : Enter Retailer ad details name "<Barcode>" image "Vault USA Template\41.jpg" with rank "2"
    Then : Add Targeting for itempurchase group same upc "<Condition>" and "2"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Go to created Ad Group
    Then : Create US ad using Ad Galary with retail
    Then : Enter Retailer ad details name "<Barcode>" image "Vault USA Template\42.jpg" with rank "1"
    Then : Add Targeting for itempurchase group same upc "<Condition>" and "3"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Send data to Kafka Queue "<testCaseId>" 

    Examples: 
      | Condition | TestCaseName  |     Barcode     |  testCaseId	|
      | >=        | Promotion_AllocationRank |    85214796301 | 68912|

@Regression
  Scenario Outline: EORD-Session Frequency cap distribution Rule
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
   	When : Campagin created Then create Ad Group with frequency cap "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Enter Retailer ad details name "<Barcode>" image "Vault USA Template\43.jpg" with rank "3"
    Then : Add Targeting for itempurchase group with "<Condition>" and "1"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Go to created Ad Group
    Then : Create US ad using Ad Galary with retail
    Then : Enter Retailer ad details name "<Barcode>" image "Vault USA Template\44.jpg" with rank "2"
    Then : Add Targeting for itempurchase group same upc "<Condition>" and "2"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Go to created Ad Group
    Then : Create US ad using Ad Galary with retail
    Then : Enter Retailer ad details name "<Barcode>" image "Vault USA Template\45.jpg" with rank "1"
    Then : Add Targeting for itempurchase group same upc "<Condition>" and "3"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
     Then : Send data to Kafka Queue "<testCaseId>" 

    Examples: 
      | Condition | TestCaseName  |   Barcode     | testCaseId	|
      | >=        |  Session Frequency cap distribution Rule |	85214796301 | 68921|
  
  @Regression    
  Scenario Outline: Simulation distribution Rule
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create Simulation campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "SIMULATION"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\62.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Simulation Promotion on server "AZURE" and "USA"
    Then : Clear cache
     Then : Send data to Kafka Queue "<testCaseId>"

    Examples: 
      | TestCaseName  |	Condition	|	quantity	|	Barcode	|testCaseId|
      | Simulation distribution Rule | >=	|	1	|    (8110)1007192113889131001203103201230	|70997|
      
  @Regression          
   Scenario Outline: EORD-High Risk distribution Rule
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "EORD"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form HighRisk for USAretailer with "<Barcode>" and "Vault USA Template\74.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

    Examples: 
      | TestCaseName |  Condition | quantity | Barcode     |testCaseId	|
      | High Risk         |  ==        |        1 | 85214796301 |89990|

  
      