Feature: Create Promotion for France  Retailer uses cases 2nd set

@Regression
  Scenario Outline: FR: Coupon - Total order | total spend + Dept. purchase -> total units
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\117.jpg" 
  	Then : Add Targeting group with Total Order first "<Condition>" and "<value>"
  	Then : Add department id with "<Condition>", "<quantity>" and "<departmentid>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
  	Then : Clear cache
  	Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"
  
  #	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |	TestCaseName |	Barcode	|	departmentid	|		value	|testCaseId 	| 
  		| >= | 1 |	Total order  and department purchase |	85214796301	|	1	| 1	|	70926 |
  
  @Regression
  Scenario Outline: FR: Coupon - Rolling day validation
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with Rolling Expirationdate "<Barcode>" and "Vault France Template\118.jpg"
  	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
  	Then : Clear cache
  	Then : Send data to Kafka Queue "<testCaseId>" 
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |	TestCaseName |	Barcode	|	testCaseId 	| 
  		| >= | 5 |	Rolling day Validation |	85214796301	| 70923 |
  
  @Regression
  Scenario Outline: FR: Coupon - Retargeting
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\119.jpg" 
  	Then : Add Targeting with Retargetting "France"
    Then : Add Targeting with Item Purchase with fixed period and Manual List 
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA"
  	Then : Clear cache
  	Then : Send data to Kafka Queue "<testCaseId>" 
  	
  	#put in same add group provide rank as well
  	Examples:
  		|	TestCaseName |	Barcode	|	testCaseId 	| 
  	 |	Creating Retailer for Retargeting  |	09841524701	|70945 |
  
  @Regression
  Scenario Outline: FR: Coupon - POS Redemption
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\120.jpg" 
  	Then : Add Targeting for retailer with POS Redemption "France"
    Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
  	
  	#put in same add group provide rank as well
  	Examples:
  		|	TestCaseName |	Barcode	|	
  	|	Creating Retailer for POS Redemption  |	85214796301	|
  
  @Regression
  Scenario Outline: FR: Coupon - Item purchase | total spend
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
 		Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\123.jpg"
  	Then : Add Targeting for retailer group with total spend "<Condition>" and "<quantity>" 
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
  	Then : Clear cache
  	Then : Send data to Kafka Queue "<testCaseId>" 
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |TestCaseName |	Barcode	| testCaseId 	| 
  		| >= | 5 | Item Purchase total spend	|	85214796301	|70922 |
  
  @Regression
  Scenario Outline: FRA: Coupon - Item purchase | total spend + Item purchase | total units -> qty
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\124.jpg" 
  	Then : Add Targeting for retailer group with total spend "<Condition>" and "<quantity>" 
  	Then : Add Targeting for retailer group with qty "<Condition1>" and "<quantity1>" 
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA"
  	Then : Clear cache 
  	Then : Send data to Kafka Queue with spendupc and unitsupc "<testCaseId>" 
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | 	TestCaseName |	Barcode	|	 Condition1	|	quantity1	|testCaseId 	| 
  		| >= | 5 | 	Item Purchase total spend and item purchase total units	|	85214796301	|	>=	|	1	|	70924	|
  
  @Regression
  Scenario Outline: FR: Coupon - Item purchase | total spend + Total order | total spend
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\125.jpg" 
  	Then : Add Targeting for retailer group with total spend "<Condition>" and "<quantity>"
  	Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA"
  	Then : Clear cache
  	Then : Send data to Kafka Queue with spendupc and unitsupc "<testCaseId>" 
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |TestCaseName |	Barcode	|	 Conditionorder	|	quantityorder	|testCaseId|
  		| >= | 3 | Item purchase total spend and total order	|	85214796301	|	>=	|	5	|70925|
  
  @Regression
  Scenario Outline: FR: Coupon - Dept. purchase | total units == 1
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\126.jpg" 
  	Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
  	Then : Clear cache
  	Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"
  
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | 	TestCaseName |	Barcode	|	departmentid	| testCaseId 	| 
  		| == | 1 | 	Department purchase total units ==1	|	85214796301	|	2	|70928|
  		
  		
  @Regression		
Scenario Outline: FR: Coupon - Dept. purchase | total units >= 4
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\127.jpg" 
  	Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
  	Then : Clear cache
  	Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | 	TestCaseName |	Barcode	|	departmentid	|testCaseId 	| 
  		| >= | 4 | 	Department purchase total units >=4	|	85214796301	|	4	|70930|
  		
  
 @Regression 		
Scenario Outline: FR: Coupon - Dept. purchase | total units between 2,00->3,00
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	 Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\129.jpg" 
    Then : Add department id firstrow between "<Condition>", "<quantity>", "<quantity2>" and "<departmentid>" 
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		Then : Clear cache
		Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"
    
    Examples: 
      | Condition | quantity | quantity2 | TestCaseName                                          | Barcode     |	departmentid	|testCaseId 	| 
      | between   |        2 |         3 |   Department purchase total units between 2,00 and 3,00 | 85214796301 |	3	|70929|
  
  @Regression    
 Scenario Outline: FR: Coupon - Dept. purchase | total units between 2,00 -> 3,00 + total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\130.jpg" 
    Then : Add department id firstrow between "<Condition>", "<quantity>", "<quantity2>" and "<departmentid>" 
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		Then : Clear cache
		Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"
    
    Examples: 
      | Condition | quantity | quantity2 |  TestCaseName                                          | Barcode     |	Conditionorder	|	quantityorder	|	departmentid	|testCaseId 	|
      | between   |        2 |         3 |  Department purchase total units between 2 and 3 and total order-total spend | 85214796301 |	>=	|	1	|	6	|70932|
   
   @Regression   
 Scenario Outline: FR: Coupon - Dept. purchase | total units ==1 + total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\131.jpg" 
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>" 
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		Then : Clear cache
		Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"
    
    Examples: 
      | Condition | quantity | TestCaseName                                          | Barcode     |	Conditionorder	|	quantityorder	|	departmentid	|testCaseId 	| 
      | ==   |   1 |   Department purchase total units ==1 and total order-total spend | 85214796301 |	>=	|	1	|	5	|70931|
      
@Regression      
Scenario Outline: FR: Coupon - Dept. purchase | total units >=4 + total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\132.jpg" 
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>" 
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		Then : Clear cache
		Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"
    
    Examples: 
      | Condition | quantity |  TestCaseName                                          | Barcode     |	Conditionorder	|	quantityorder	|	departmentid	|testCaseId 	| 
      | >=   |   4 |  Department purchase total units >=4 and total order-total spend | 85214796301 |	>=	|	1	|	4	|70933|


  @Regression    
Scenario Outline: FR: Coupon - Dept. purchase | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\134.jpg"
    Then : Add department id totalspend with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		Then : Clear cache
	 Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"
    
    Examples: 
      | Condition | quantity |  TestCaseName            | Barcode     |	testCaseId 	|
      | >=   |   2 |    Department purchase total spend | 85214796301 |	70936|
    
 
@Regression
  Scenario Outline: FR: Coupon - Catalina Redemption
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\141.jpg"
    Then : Add Catalina Redemption "France"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA"
		Then : Clear cache
		Then : Send data to Kafka Queue barcode "<testCaseId>"

		  Examples: 
     |  TestCaseName            | Barcode     |	testCaseId 	| 
     | Catalina Redemption | 85214796301 |	 70943|
 
 @Regression    
   Scenario Outline: FR: Coupon - Item purchase | total units -> qty + Item purchase | total units == 0 + Total order | total spend
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\121.jpg" 
  	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
  	Then : Add Targeting for retailer group with qty "<Condition1>" and "<quantity1>" 
  	Then : Add Targeting group with Total Order third row
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | 	TestCaseName |	Barcode	|	 Condition1 | quantity1 |	
  		| >= | 1 |	Item Purchase is >=1 and ==0 and total order |	85214796301	|	== | 0	|

@Regression  		
  Scenario Outline: FR: Coupon - Dept. purchase | total units -> qty + Dept. purchase | total units == 0
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\133.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Add department id with secondrow "<Condition1>", "<quantity1>" and "<departmentid2>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA" 
    
    Examples: 
      | Condition | quantity |  TestCaseName                                          | Barcode     |	Condition1	|	quantity1	|	departmentid |	departmentid2|
      | >=   |   1 |   Department purchase total units >=1 and total dept purchase ==0 | 85214796301 |	==	|	0	|	1|	7	|
   
   @Regression   
   Scenario Outline: FR: Coupon - Dept. purchase | total units == 0
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\128.jpg" 
  	Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
  	
  
  
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | 	TestCaseName |	Barcode	|	departmentid	|testCaseId 	| 
  		| == | 0 | 	Department purchase total units ==0	|	85214796301	|	8	|70934|
  
  @Regression		
 Scenario Outline: FR: Coupon - Item purchase | total units == 0 + Item purchase |total units -> qty
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\122.jpg"
  	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
  	Then : Add Targeting for retailer group with qty "<Condition1>" and "<quantity1>" 
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | TestCaseName |	Barcode	|	 Condition1 | quantity1 |	
  		| == | 0|Item Purchase is ==0 and >=1|	85214796301	|	>= | 1	|
  
  @Regression
  Scenario Outline: FR: Coupon - Item purchase | total units == 0 + Total order | total spend
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\102.jpg"
 	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
	Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
	
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | 	TestCaseName |	Barcode	|	Conditionorder	|	quantityorder	|testCaseId 	|
		| == | 0 | 	Item Purchase ==0 and total order |	85214796301	|	>=	|	10	|70919		|
		
	@Regression	
Scenario Outline: FR: Coupon - Item purchase | total units == 0
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\110.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA"
	
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	testCaseId 	| 
		| == | 0 | Item purchase ==0 |	85214796301	|	70917		|	
		
      
  

