Feature: Create Promotion for ItalyRetailer uses cases 2nd set

@Regression
  Scenario Outline: ITA: Coupon - Total order | total spend + Dept. purchase -> total units
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\ferrerorocher.jpg"
    Then : Add Targeting group with Total Order first "<Condition>" and "<quantity>"
    Then : Add department id with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"

    
    Examples: 
      | Condition | quantity | TestCaseName                         | Barcode     | departmentid | testCaseId |
      | >=        |        1 | Total order  and department purchase | 85214796301 |            1 |      70714 |   

@Regression
  Scenario Outline: ITA: Coupon - Item purchase | total units >= 4
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\dairymilk.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

    
    Examples: 
      | Condition | quantity | TestCaseName                  | Barcode     | testCaseId | 
      | >=        |        4 | Item purchase total units >=4 | 85214796301 |      70702 |    

@Regression
  Scenario Outline: ITA: Coupon - Item purchase | total units == 10
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\darkfantasy.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 
    
    
    Examples: 
      | Condition | quantity | TestCaseName                   | Barcode     | testCaseId | 
      | ==        |       10 | Item purchase total units ==10 | 85214796301 |      70703 |      

@Regression
  Scenario Outline: ITA: Coupon - Rolling day validation
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with rolling Expirationdate "<Barcode>" and "Vault UI Template\fries.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 

    
    Examples: 
      | Condition | quantity | TestCaseName           | Barcode     | testCaseId | 
      | >=        |        5 | Rolling day Validation | 85214796301 |      70711 |   
        
@Regression
  Scenario Outline: ITA: Coupon - Retargeting
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\fruitjuice.jpg"
    Then : Add Targeting with Retargetting "Italy"
    Then : Add Targeting with Item Purchase with fixed period and Manual List
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

   
    Examples: 
      | TestCaseName                      | Barcode     | testCaseId | 
      | Creating Retailer for Retargeting | 09841524701 |      70733 |     

@Regression
  Scenario Outline: ITA: Coupon - POS Redemption
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\gems.jpg"
    Then : Add Targeting for retailer with POS Redemption "Italy"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

    
    Examples: 
      | Condition | quantity | TestCaseName                         | Barcode     | testCaseId |
      | >=        |        5 | Creating Retailer for POS Redemption | 85214796301 |      70732 |    

 @Regression 
  Scenario Outline: ITA: Coupon - Item purchase | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\italy.jpg"
    Then : Add Targeting for retailer group with total spend "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

    
    Examples: 
      | Condition | quantity | TestCaseName              | Barcode     | testCaseId | 
      | >=        |        5 | Item Purchase total spend | 85214796301 |      70710 |  

@Regression
  Scenario Outline: ITA: Coupon - Item purchase | total spend + Item purchase | total units -> qty
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\Italyimage.jpg"
    Then : Add Targeting for retailer group with total spend "<Condition>" and "<quantity>"
    Then : Add Targeting for retailer group with qty "<Condition1>" and "<quantity1>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue with spendupc and unitsupc "<testCaseId>"

    
    Examples: 
      | Condition | quantity | TestCaseName                                            | Barcode     | Condition1 | quantity1 | testCaseId | 
      | >=        |        5 | Item Purchase total spend and item purchase total units | 85214796301 | >=         |         1 |      70712 |     

@Regression
  Scenario Outline: ITA: Coupon - Item purchase | total spend + Total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\juice image.jpg"
    Then : Add Targeting for retailer group with total spend "<Condition>" and "<quantity>"
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue with spendupc and unitsupc "<testCaseId>"

   
    Examples: 
      | Condition | quantity | TestCaseName                              | Barcode     | Conditionorder | quantityorder | testCaseId | 
      | >=        |        3 | Item purchase total spend and total order | 85214796301 | >=             |             5 |      70713 |      
  
  @Regression
  Scenario Outline: ITA: Coupon - Dept. purchase | total units == 1
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\kinderjoy.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"

    
    Examples: 
      | Condition | quantity | TestCaseName                        | Barcode     | departmentid | testCaseId | 
      | ==        |        1 | Department purchase total units ==1 | 85214796301 |            2 |      70716 |  

@Regression
  Scenario Outline: ITA: Coupon - Dept. purchase | total units >= 4
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\kitkat.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"

    
    Examples: 
      | Condition | quantity | TestCaseName                        | Barcode     | departmentid | testCaseId | 
      | >=        |        4 | Department purchase total units >=4 | 85214796301 |            4 |      70718 |  

 @Regression 
  Scenario Outline: ITA: Coupon - Dept. purchase | total units between 2,00->3,00
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\maggi.jpg"
    Then : Add department id firstrow between "<Condition>", "<quantity>", "<quantity2>" and "<departmentid>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>" 

    Examples: 
      | Condition | quantity | quantity2 | TestCaseName                                          | Barcode     | departmentid | testCaseId | 
      | between   |        2 |         3 | Department purchase total units between 2,00 and 3,00 | 85214796301 |            3 |      70717 |      

@Regression
  Scenario Outline: ITA: Coupon - Dept. purchase | total units between 2,00 -> 3,00 + total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\Mariegold.png"
    Then : Add department id firstrow between "<Condition>", "<quantity>", "<quantity2>" and "<departmentid>"
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>" 

    Examples: 
      | Condition | quantity | quantity2 | TestCaseName                                                                | Barcode     | Conditionorder | quantityorder | departmentid | testCaseId | 
      | between   |        2 |         3 | Department purchase total units between 2 and 3 and total order-total spend | 85214796301 | >=             |             1 |            6 |      70720 |    

@Regression
  Scenario Outline: ITA: Coupon - Dept. purchase | total units ==1 + total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\oreo.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>" 

    Examples: 
      | Condition | quantity | TestCaseName                                                    | Barcode     | Conditionorder | quantityorder | departmentid | testCaseId | 
      | ==        |        1 | Department purchase total units ==1 and total order-total spend | 85214796301 | >=             |             1 |            5 |      70719 |   

@Regression
  Scenario Outline: ITA: Coupon - Dept. purchase | total units >=4 + total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\Popcorn.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"

    Examples: 
      | Condition | quantity | TestCaseName                                                    | Barcode     | Conditionorder | quantityorder | departmentid | testCaseId | 
      | >=        |        4 | Department purchase total units >=4 and total order-total spend | 85214796301 | >=             |             1 |            4 |      70721 |  

  @Regression
  Scenario Outline: ITA: Coupon - Dept. purchase | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\smiley2.jpg"
    Then : Add department id totalspend with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Send data to Kafka Queue with min and max amt for dept "<testCaseId>"

    Examples: 
      | Condition | quantity | manualList | TestCaseName                    | Barcode     | testCaseId | 
      | >=        |        2 |  834567031 | Department purchase total spend | 85214796301 |      70724 |     

@Regression
  Scenario Outline: ITA: Coupon - Catalina Redemption
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\apple.jpg"
    Then : Add Catalina Redemption "Italy"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Send data to Kafka Queue barcode "<testCaseId>"

    Examples: 
      | TestCaseName        | Barcode     | testCaseId | 
      | Catalina Redemption | 85214796301 |      70731 | 
    
    @Regression  
    Scenario Outline: ITA: Coupon - Item purchase | total units == 0 + Total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\bread.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

   
    Examples: 
      | Condition | quantity | TestCaseName                      | Barcode     | Conditionorder | quantityorder | testCaseId | 
      | ==        |        0 | Item Purchase ==0 and total order | 85214796301 | >=             |             1 |      70707 |
   
   @Regression   
  Scenario Outline: ITA: Coupon - Item purchase | total units == 0
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\creamcookies.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

   
    Examples: 
      | Condition | quantity | TestCaseName      | Barcode     | testCaseId | 
      | ==        |        0 | Item purchase ==0 | 85214796301 |      70705 |    
   
   @Regression   
  Scenario Outline: ITA: Coupon - Item purchase | total units -> qty + Item purchase | total units == 0 + Total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\happysmiley.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Targeting for retailer group with qty "<Condition1>" and "<quantity1>"
    Then : Add Targeting group with Total Order third row
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

   
    Examples: 
      | Condition | quantity | TestCaseName                                 | Barcode     | Condition1 | quantity1 | testCaseId | 
      | >=        |        1 | Item Purchase is >=1 and ==0 and total order | 85214796301 | ==         |         0 |      70709 |     

@Regression
  Scenario Outline: ITA: Coupon - Item purchase | total units == 0 + Item purchase |total units -> qty
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\icecreamcone images.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Targeting for retailer group with qty "<Condition1>" and "<quantity1>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

   
    Examples: 
      | Condition | quantity | TestCaseName                 | Barcode     | Condition1 | quantity1 | testCaseId | 
      | ==        |        0 | Item Purchase is ==0 and >=1 | 85214796301 | >=         |         1 |      70706 |
    
    @Regression  
   Scenario Outline: ITA: Coupon - Dept. purchase | total units == 0
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\laysimg.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

    
    Examples: 
      | Condition | quantity | TestCaseName                        | Barcode     | departmentid | testCaseId | 
      | ==        |        0 | Department purchase total units ==0 | 85214796301 |            8 |      70722 |
   
  @Regression   
 Scenario Outline: ITA: Coupon - Dept. purchase | total units -> qty + Dept. purchase | total units == 0
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\PotatoSamosa.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Add department id with secondrow "<Condition1>", "<quantity1>" and "<departmentid2>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"

    Examples: 
      | Condition | quantity | TestCaseName                                                    | Barcode     | Condition1 | quantity1 | departmentid | departmentid2 | testCaseId | 
      | >=        |        1 | Department purchase total units >=1 and total dept purchase ==0 | 85214796301 | ==         |         0 |            1 |             7 |      70723 |     
   
        
    
       
   
