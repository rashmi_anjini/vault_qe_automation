 Feature: Create Promotion for UKRetailer 
 Scenario Outline: Using Retailer and with department purchase total units 1 and depart purchase 0
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\PotatoSamosa.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Add department id with secondrow "<Condition1>", "<quantity1>" and "<departmentid2>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
	
    #put in same add group provide rank as well
    Examples: 
      | Condition | quantity | manualList | TestCaseName                                          | Barcode     |	Condition1	|	quantity1	|	departmentid |	departmentid2|
      | >=   |   1 |   834560030 | Department purchase total units >=1 and total dept purchase ==0 | 85214796301 |	==	|	0	|	1|	7	|
      
      
Scenario Outline: Using Retailer and with department purchase total spend
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\smiley2.jpg"
    Then : Add department id totalspend with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
      | Condition | quantity | manualList | TestCaseName            | Barcode     |	
      | >=   |   2 |   834560031 | Department purchase total spend | 85214796301 |	
      
  Scenario Outline: Using Retailer and with  Audience Segment and Dept purchase targetting
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\smiley3.jpg"
    Then : Add Audience Segment for retailer firstrow with "Krishsainsbury" and "in"
    Then : Add department id with audienceSegment "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
      | Condition | quantity | manualList | TestCaseName            | Barcode     |	
      | >=   |   1 |   834560032 | Audience Segment and Dept purchase  | 85214796301 |	
      
      

 Scenario Outline: Using Retailer and with Audience Segment in targetting
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\spagehtii.jpg"
    Then : Add Audience Segment for retailer firstrow with "Krishsainsbury" and "in"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
      | TestCaseName            | Barcode     |	
      | Audience Segment in  | 85214796301 |	
          
 Scenario Outline: Using Retailer and with Audience Segment Not in  targetting
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\Sweets.jpg"
    Then : Add Audience Segment for retailer firstrow with "Krishsainsbury" and "not in"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
      | TestCaseName            | Barcode     |	
      | Audience Segment Not in  | 85214796301 |
      
Scenario Outline: Using Retailer and with  Audience Segment and Total order
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\Testing.jpg"
    Then : Add Audience Segment for retailer firstrow with "Krishsainsbury" and "in"
    Then : Add Targeting group with Total Order audiencesegment with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
      | Conditionorder | quantityorder | manualList | TestCaseName            | Barcode     |	
      | >=   |   3 |   834560033 | Audience Segment and Total order | 85214796301 |	
 
 Scenario Outline: Using Retailer and with  Audience Segment and Payment Method EFT targetting
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\tetly.jpeg"
    Then : Add Audience Segment for retailer firstrow with "Krishsainsbury" and "in"
    Then : Add Payment Method EFT
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
     |  TestCaseName            | Barcode     |	
     | Audience Segment and Add Payment Method EFT | 85214796301 |	
     

Scenario Outline: Using Retailer and with  Audience Segment and Custom Parameters targetting
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\vegetablebasket.jpg"
    Then : Add Audience Segment for retailer firstrow with "Krishsainsbury" and "in"
    Then : Add Custom Targeting with external id "BL_1924_8584"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
     |  TestCaseName            | Barcode     |	
     | Audience Segment and Custom Parameters | 85214796301 |	
     
    

  Scenario Outline: Using Retailer and with  Catalina Redemption
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\apple.jpg"
    Then : Add Catalina Redemption for Retailer
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
     |  TestCaseName            | Barcode     |	
     | Catalina Redemption | 85214796301 |	   
