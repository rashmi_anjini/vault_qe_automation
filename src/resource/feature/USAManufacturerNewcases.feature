Feature: Create Manufacturer Promotions for New functional cases

@Regression
  Scenario Outline: FSC - Unlimited
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "MANUFACTURER"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with unlimited "Vault USA Template\5.jpg"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add User Identifier
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
		Then : Send data to Kafka Queue with cid "<testCaseId>" 
    
    Examples: 
      | Condition | quantity | TestCaseName    | Barcode                               |testCaseId 	| 
      | ==        |        2 | FSC - Unlimited | (8110)1007192113889131001203103201230 |68042	|

@SmokeTest @Regression
  Scenario Outline: FSC - Limited
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
   	Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\6.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add User Identifier
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>" 

    
    Examples: 
      | Condition | quantity | TestCaseName  | Barcode                               |testCaseId 	| 
      | ==        |        1 | FSC - limited | (8110)1007192113889131001203103201230 |68043|

@Regression
  Scenario Outline: Coupon - Start date as current date
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with start date current date "Vault USA Template\7.jpg", "0" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

   
    Examples: 
      | Condition | quantity | TestCaseName                        | Barcode                               |testCaseId 	| 
      | >=        |        1 | Coupon - Start date as current date | (8110)1007192113889131001203103201230 | 68051	|	
  
  @Regression
  Scenario Outline: Coupon - Start date as current date - 1
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\8.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

    
    Examples: 
      | Condition | quantity | TestCaseName                          | Barcode                               |testCaseId 	| 
      | >=        |        1 | Coupon - Start date as current date-1 | (8110)1007192113889131001203103201230 |68052|

@Regression
  Scenario Outline: Coupon - Start date as current date + 1
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\9.jpg", "1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 
     
    
    Examples: 
      | Condition | quantity | TestCaseName                          | Barcode                               |testCaseId 	| 
      | >=        |        1 | Coupon - Start date as current date+1 | (8110)1007192113889131001203103201230 |68053	|

@Regression
  Scenario Outline: Coupon - Stop date as current date
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\10.jpg", "-5" and "0"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"
    
    Examples: 
      | Condition | quantity | TestCaseName                       | Barcode                               |testCaseId 	| 
      | >=        |        1 | Coupon - Stop date as current date | (8110)1007192113889131001203103201230 |68055|

@Regression
  Scenario Outline: Coupon - Stop date as current date - 1
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\11.jpg", "-5" and "-1"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 

   
    Examples: 
      | Condition | quantity | TestCaseName                         | Barcode                               |testCaseId 	| 
      | >=        |        1 | Coupon - Stop date as current date-1 | (8110)1007192113889131001203103201230 |68057|

@Regression
  Scenario Outline: Coupon - Stop date as current date + 1
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\12.jpg", "-5" and "1"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 
    
    Examples: 
      | Condition | quantity | TestCaseName                         | Barcode                               |testCaseId 	| 
      | >=        |        1 | Coupon - Stop date as current date+1 | (8110)1007192113889131001203103201230 |68058|	

@Regression
  Scenario Outline: Budget capping - Available
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with distribution cap "Vault USA Template\13.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    

   
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |testCaseId 	| 
      | >=        |        1 | Budget capping - Available | (8110)1007192113889131001203103201230 |68295|
   
   @Regression   
   Scenario Outline: Coupon - Check payment method
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\63.jpg", "-5" and "1"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting group with Total Order first "<Condition>" and "<value>"
    Then : Add Payment Method "<payment>" and "<Condition>" and "<value>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"
    
    Examples: 
      | Condition | value | TestCaseName         | Barcode                               | payment |testCaseId 	| 
      | >=        |     4 | Check payment method | (8110)1007192113889131001203103201230 | Check   |89994	|

@Regression
  Scenario Outline: Coupon - EFT payment method
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\64.jpg", "-5" and "1"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting group with Total Order first "<Condition>" and "<value>"
    Then : Add Payment Method EFT and "<Condition>" and "<value>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

    
    Examples: 
      | Condition | value | TestCaseName       | Barcode                               |testCaseId 	| 
      | >=        |     4 | EFT payment method | (8110)1007192113889131001203103201230 |89991	|

@Regression
  Scenario Outline: Coupon - Foodstamp payment method
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\65.jpg", "-5" and "1"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting group with Total Order first "<Condition>" and "<value>"
    Then : Add Payment Method with Foodstamp and "<Condition>" and "<value>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>" 

   
    Examples: 
      | Condition | value | TestCaseName             | Barcode                               |testCaseId 	| 
      | >=        |     4 | Foodstamp payment method | (8110)1007192113889131001203103201230 |89995	|

@Regression
  Scenario Outline: Coupon - Tender Charge
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\66.jpg", "-5" and "1"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting group with Total Order first "<Condition>" and "<value>"
    Then : Add Payment Method "<payment>" and "<Condition>" and "<value>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

   
    Examples: 
      | Condition | value | TestCaseName  | Barcode                               | payment |testCaseId 	| 
      | >=        |     4 | Tender Charge | (8110)1007192113889131001203103201230 | Charge  |89993	|

@Regression
  Scenario Outline: Coupon - Item Purchase | total spend
  Given : Url of application Navigate to webpage
  Then : Fill credentails
  Then : Click on Login
  Then : Change region to "USA"
  Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>"
  Then : Create ad using Ad Galary with manufacturer
  Then : Fill ad form for manufacturer with "Vault USA Template\67.jpg", "-5" and "1"
  Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  Then : Add Targeting for retailer group with total spend "<Condition>" and "<quantity>"
  Then : Publish "<TestCaseName>"
  Then : Verify DMP Promotion on server "AZURE" and "USA"
  Then : Clear cache
  Then : Send data to Kafka Queue "<testCaseId>" 
  
  
  Examples:
  | Condition | quantity | TestCaseName              | Barcode                               |testCaseId 	| 
  | >=        |        5 | Item Purchase-total spend | (8110)1007192113889131001203103201230 |89998	|
 
 @Regression
  Scenario Outline: Coupon - Total order | Units Purchased
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\68.jpg", "-5" and "1"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting group with Total Units Purchased first "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue "<testCaseId>"

    
    Examples: 
      | Condition | quantity | TestCaseName                 | Barcode                               |testCaseId 	|
      | >=        |        5 | Total order +Units Purchased | (8110)1007192113889131001203103201230 |89997	|
   
      
  