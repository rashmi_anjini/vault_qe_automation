Feature: Creating PINS list for USA

  Scenario Outline: Creating a new PINS List
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "<Region>"
    When : Click on PINS
    Then : Click on New PINs with "<Country>" and click on Save changes
    Then : Click on Import New PINs File and Upload file with counrty "<Country>"

    Examples: 
      | Country | Region |
     	| USA     | USA  |