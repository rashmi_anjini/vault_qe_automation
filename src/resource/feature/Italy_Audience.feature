Feature: Create Promotion for Italy Audience

@Regression
  Scenario Outline: ITA: Coupon - Item purchase |total units -> qty + Audience Segment -> in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\cakeimag.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Audience Segment for with
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"

   
    Examples: 
      | Condition | quantity | TestCaseName                       | Barcode     | manualList | testCaseId | 
      | >=        |        1 | Item purchase and Audience Segment | 85214796301 |  834567005 |      70696 |      

@SmokeTest @Regression
  Scenario Outline: ITA: Coupon - with Distribution and/or Frequency cap
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with freqcap "<Barcode>" and "Vault UI Template\dessert.png"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Audience Segment for with
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
     Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"

    Examples: 
      | Condition | quantity | manualList | TestCaseName                                | Barcode     | testCaseId | 
      | >=        |        1 |  834567014 | Freq cap and item purchase and Audience seg | 85214796301 |      70700 |  

@SmokeTest @Regression
  Scenario Outline: ITA: Coupon - Audience segment | in + Dept. purchase | total units -> qty
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\smiley3.jpg"
    Then : Add Audience Segment for retailer firstrow with "in"
    Then : Add department id with audienceSegment "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid min and max amt for dept "<testCaseId>"

    Examples: 
      | Condition | quantity | manualList | TestCaseName                       | Barcode     | testCaseId | 
      | >=        |        1 |  834567032 | Audience Segment and Dept purchase | 85214796301 |      70725 |     

@Regression
  Scenario Outline: ITA: Coupon - Audience segment | in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\spagehtii.jpg"
    Then : Add Audience Segment for retailer firstrow with "in"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
     Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"
    
    Examples: 
      | TestCaseName        | Barcode     | testCaseId | 
      | Audience Segment in | 85214796301 |      70728 |     

@Regression
  Scenario Outline: ITA: Coupon - Audience segment | not in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\Sweets.jpg"
    Then : Add Audience Segment for retailer firstrow with "not in"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
     Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>" 

    Examples: 
      | TestCaseName            | Barcode     | testCaseId | 
      | Audience Segment Not in | 85214796301 |      70730 |     

@Regression
  Scenario Outline: ITA: Coupon - Audience segment | in + Total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\Testing.jpg"
    Then : Add Audience Segment for retailer firstrow with "in"
    Then : Add Targeting group with Total Order audiencesegment with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
     Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"

    Examples: 
      | Conditionorder | quantityorder | TestCaseName                     | Barcode     | testCaseId | 
      | >=             |             3 | Audience Segment and Total order | 85214796301 |      70726 |    

@Regression
  Scenario Outline: ITA: Coupon - Audience segment | in + Payment method | EFT
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "Italy"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail for Italy
    Then : Fill ad form for Italyretailer with "<Barcode>" and "Vault UI Template\tetly.jpeg"
    Then : Add Audience Segment for retailer firstrow with "in"
    Then : Add Payment Method EFT second row
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "ITA"
     Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>" 

    Examples: 
      | TestCaseName                                | Barcode     | testCaseId | 
      | Audience Segment and Add Payment Method EFT | 85214796301 |      70727 |   
      
        
