Feature: Create Promotion for France Audience

@SmokeTest @Regression
  Scenario Outline: FR: Coupon - Audience segment | in + Dept. purchase | total units -> qty
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\135.jpg"
    Then : Add Audience Segment for retailer firstrow with "in"
    Then : Add department id with audienceSegment "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid min and max amt for dept "<testCaseId>"
    

    Examples: 
      | Condition | quantity | TestCaseName                       | Barcode     |testCaseId 	| 
      | >=        |        1 | Audience Segment and Dept purchase | 85214796301 |70937	|	

@Regression
  Scenario Outline: FR: Coupon - with Distribution and/or Frequency cap
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with frequency capping "<Barcode>" and "Vault France Template\115.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Audience Segment for with
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>" 

    
    Examples: 
      | Condition | quantity | TestCaseName                                | Barcode     |testCaseId 	|
      | >=        |        1 | Freq cap and item purchase and Audience seg | 85214796301 |70912	|	

@SmokeTest @Regression
  Scenario Outline: FR: Coupon - Audience segment | in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\136.jpg"
    Then : Add Audience Segment for retailer firstrow with "in"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"

    Examples: 
      | TestCaseName        | Barcode     |testCaseId 	| 
      | Audience Segment in | 85214796301 |70940	|	

@Regression
  Scenario Outline: FR: Coupon - Audience segment | not in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\119.jpg"
    Then : Add Audience Segment for retailer firstrow with "not in"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>" 

    Examples: 
      | TestCaseName            | Barcode     |testCaseId 	| 
      | Audience Segment Not in | 85214796301 |70942	|	

  @Regression
	Scenario Outline: FRA: Coupon-Audience segment | in + Total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\138.jpg"
    Then : Add Audience Segment for retailer firstrow with "in"
    Then : Add Targeting group with Total Order audiencesegment with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		Then : Clear cache
		Then : Send data to Kafka Queue with cid "<testCaseId>" 
		
    
    Examples: 
      | Conditionorder | quantityorder |  TestCaseName            | Barcode     |	testCaseId 	| 
      | >=   |   3 |    Audience Segment and Total order | 85214796301 |	70938	|	
 
 @Regression
  Scenario Outline: FR: Coupon - Audience segment | in + Payment method | EFT
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\120.jpg"
    Then : Add Audience Segment for retailer firstrow with "in"
    Then : Add Payment Method EFT second row
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"

    Examples: 
      | TestCaseName                                | Barcode     |testCaseId 	| 
      | Audience Segment and Add Payment Method EFT | 85214796301 |70939	|	

  
@Regression
  Scenario Outline: FR: Coupon - Item purchase |total units -> qty + Audience Segment -> in
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\105.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
	Then : Add Audience Segment for with  
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA"
	Then : Clear cache
	Then : Send data to Kafka Queue with cid "<testCaseId>" 
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	testCaseId 	| 
		| >= | 1 | Item purchase and Audience Segment |	85214796301	|	70908	|	
		
	