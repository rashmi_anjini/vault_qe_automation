 Feature: Create Promotion for Retailer 
 
 Scenario Outline: FR: Coupon - Dept. purchase | total units -> qty + Dept. purchase | total units == 0
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\133.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
    Then : Add department id with secondrow "<Condition1>", "<quantity1>" and "<departmentid2>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA" 
    
    Examples: 
      | Condition | quantity |  TestCaseName                                          | Barcode     |	Condition1	|	quantity1	|	departmentid |	departmentid2|
      | >=   |   1 |   Department purchase total units >=1 and total dept purchase ==0 | 85214796301 |	==	|	0	|	1|	7	|
      
      
Scenario Outline: FR: Coupon - Dept. purchase | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\134.jpg"
    Then : Add department id totalspend with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
    
    Examples: 
      | Condition | quantity |  TestCaseName            | Barcode     |	
      | >=   |   2 |    Department purchase total spend | 85214796301 |	
    
 Scenario Outline: FR: Coupon - Audience segment | in + Dept. purchase | total units -> qty
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\135.jpg"
    Then : Add Audience Segment for retailer firstrow with "ODOULSFRA" and "in"
    Then : Add department id with audienceSegment "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		
   
    Examples: 
      | Condition | quantity |  TestCaseName            | Barcode     |	
      | >=   |   1 |   Audience Segment and Dept purchase  | 85214796301 |	
      
      

 Scenario Outline: FR: Coupon - Audience segment | in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
 		 When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\136.jpg"
    Then : Add Audience Segment for retailer firstrow with "ODOULSFRA" and "in"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA"
		
   
    Examples: 
      | TestCaseName            | Barcode     |	
      | Audience Segment in  | 85214796301 |	
          
 Scenario Outline: FR: Coupon - Audience segment | not in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
 		 When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\137.jpg"
    Then : Add Audience Segment for retailer firstrow with "ODOULSFRA" and "not in"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		
    
    Examples: 
      | TestCaseName            | Barcode     |	
      | Audience Segment Not in  | 85214796301 |
      
Scenario Outline: FR: Coupon - Audience segment | in + Total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\138.jpg"
    Then : Add Audience Segment for retailer firstrow with "ODOULSFRA" and "in"
    Then : Add Targeting group with Total Order audiencesegment with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		
    
    Examples: 
      | Conditionorder | quantityorder |  TestCaseName            | Barcode     |	
      | >=   |   3 |    Audience Segment and Total order | 85214796301 |	
 
 Scenario Outline: FR: Coupon - Audience segment | in + Payment method | EFT
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\139.jpg"
    Then : Add Audience Segment for retailer firstrow with "ODOULSFRA" and "in"
    Then : Add Payment Method EFT
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		
    
    Examples: 
     |  TestCaseName            | Barcode     |	
     | Audience Segment and Add Payment Method EFT | 85214796301 |	
     

#Scenario Outline: FR: Coupon - Audience segment | in + Custom parameters
    #Given : Url of application Navigate to webpage
    #Then : Fill credentails
    #Then : Click on Login
    #Then : Change region to "France"
    #Then : Search for the Campaign name
  #	When : Campagin created Then create Ad Group with "<TestCaseName>"
    #Then : Create ad using Ad Galary with retail
    #Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\140.jpg"
    #Then : Add Audience Segment for retailer firstrow with "ODOULSFRA" and "in"
    #Then : Add Custom Targeting with external id "France"
    #Then : Publish "<TestCaseName>"
#		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
#		
    #put in same add group provide rank as well
    #Examples: 
     #|  TestCaseName            | Barcode     |	
     #| Audience Segment and Custom Parameters | 85214796301 |	
     #
    

  Scenario Outline: FR: Coupon - Catalina Redemption
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\141.jpg"
    Then : Add Catalina Redemption "France"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA"
		
    
    Examples: 
     |  TestCaseName            | Barcode     |	
     | Catalina Redemption | 85214796301 |	   
