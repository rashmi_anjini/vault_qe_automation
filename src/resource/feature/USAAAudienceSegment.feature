Feature: Creating Audience Segment list for USA

@SmokeTest @Regression
  Scenario Outline: Creating a new Audience List
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "<Region>"
    When : Click on Audience
    Then : Click on New Audience with "<Country>" and click on Save changes
    Then : Click on Static List
    Then : Click on Import New File and Upload file with counrty "<Country>"

    Examples: 
      | Country | Region |
     	| USA     | USA  |
  
@SmokeTest  @Regression   	
 Scenario Outline: Creating a new Audience List
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "<Region>"
    When : Click on Audience
    Then : Click on New Audience loyaltyRewards "<TestCase>" and click on Save changes
    Then : Click on Static List
    Then : Click on Import New File and Upload LoyaltyRewards file "<TestCase>"

    Examples: 
      | Country | Region |TestCase	|
     	| USA     | USA  |ItemUnits	|
     	| USA     | USA  |ItemSpend	|
     	| USA     | USA  |ManualUpc	|
