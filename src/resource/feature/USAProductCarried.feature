Feature: Product Carried

@Regression
  Scenario Outline: Product carried upc is not seen by the store in the specified product carried rolling days
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "Product_Carried"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer for LMC Product Carried with "Vault USA Template\71.jpg","<List>", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
  

    Examples: 
      | TestCaseName                                                                               | Condition | quantity | Barcode                               | List                   |
      | Product carried upc is not seen by the store in the specified product carried rolling days | >=        |        1 | (8110)1007192113889131001203103201230 | Robitussin - Trigger 2 |

  @Regression
  Scenario Outline: Republish with change in product carried upc
  Given : Url of application Navigate to webpage
  Then : Fill credentails
  Then : Click on Login
  Then : Change region to "USA"
  Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>"
  Then : Create ad using Ad Galary with manufacturer
  Then : Fill ad form for manufacturer for LMC Product Carried with "Vault USA Template\72.jpg","<List>", "-1" and "5"
  Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
  Then : Publish "<TestCaseName>"
  Then : Verify DMP Promotion on server "AZURE" and "USA"
 
  
  Examples:
  | TestCaseName                                  | Condition | quantity | Barcode                               | List      |
  | Republish with change in product carried upc    | >=        |        1 | (8110)1007192113889131001203103201230 | DryWall |
  
  
  @Regression
  Scenario Outline: Use a manual list for the product carried upc’s instead of lmc list
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer for Manual LMC Product Carried "USA" with "Vault USA Template\73.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with lmc list "<Condition>" and "<quantity>" and "<List>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName                                                       | Condition | quantity | Barcode                               | List    |
      | Use a manual list for the product carried upcs instead of lmc list | >=        |        1 | (8110)1007192113889131001203103201230 | Tequila-04-List |
