Feature: Creating Audience Segment list for France

@SmokeTest @Regression
  Scenario Outline: Creating a new Audience List
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "<Region>"
    When : Click on Audience
    Then : Click on New Audience with "<Country>" and click on Save changes
    Then : Click on Static List
    Then : Click on Import New File and Upload file with counrty "<Country>"
    

    Examples: 
      | Country | Region |
     	| FRA     | France  |
