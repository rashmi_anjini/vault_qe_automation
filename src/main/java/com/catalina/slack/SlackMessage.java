package com.catalina.slack;

import java.io.Serializable;

public class SlackMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String text;
	private String icon_emoji;
	private String channel;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIcon_emoji() {
		return icon_emoji;
	}

	public void setIcon_emoji(String icon_emoji) {
		this.icon_emoji = icon_emoji;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public SlackMessage(String username, String text, String icon_emoji, String channel) {
		super();
		this.username = username;
		this.text = text;
		this.icon_emoji = icon_emoji;
		this.channel = channel;
	}

	public SlackMessage() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "SlackMessage [username=" + username + ", text=" + text + ", icon_emoji=" + icon_emoji + ", channel="
				+ channel + "]";
	}

}