package com.catalina.slack;

import java.io.IOException;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SlackUtils {

	private static String slackWebhookUrl = "https://hooks.slack.com/services/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";

	public static void sendMessage(SlackMessage message) {

		CloseableHttpClient client = HttpClients.createDefault();

		HttpPost httpPost = new HttpPost(slackWebhookUrl);
		try {

			ObjectMapper objectMapper = new ObjectMapper();
			String json = objectMapper.writeValueAsString(message);
			StringEntity entity = new StringEntity(json);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
			client.execute(httpPost);
			client.close();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

}