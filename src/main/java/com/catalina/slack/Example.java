package com.catalina.slack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.catalina.vault.steps.Targeting;

public class Example {
	private static final Logger Log= LogManager.getLogger(Targeting.class);

	public static void slackMessage(String testRunMessage, String Country, String PromotionName, String BuildName) {

		SlackMessage slackMessage = new SlackMessage("Vault_TestRun", " "+BuildName+"_"+Country+ ": {" +PromotionName+" }: " +testRunMessage+"" , "", "qa-cellfire-automation");
		SlackUtils.sendMessage(slackMessage);
Log.info("slackMessage: "+slackMessage);
		System.out.println("////////////////////////////////////////////SUCCESSFULLY UPDATED IN SLACK/////////////////");
		Log.info("////////////////////////////////////////////SUCCESSFULLY UPDATED IN SLACK/////////////////");
	}

}