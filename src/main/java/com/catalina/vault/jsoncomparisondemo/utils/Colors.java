package com.catalina.vault.jsoncomparisondemo.utils;

/**
 * 
 * This class will help us load the respective reporting colors
 *
 */
public class Colors {

	String valueNotMatched;
	String elementNotFound;
	String ignoredKeywords;
	
	public Colors() {
		this.valueNotMatched = "red";
		this.elementNotFound = "blue";
		this.ignoredKeywords = "yellow";
	}
	public String getValueNotMatched() {
		return valueNotMatched;
	}
	public void setValueNotMatched(String valueNotMatched) {
		this.valueNotMatched = valueNotMatched;
	}
	public String getElementNotFound() {
		return elementNotFound;
	}
	public void setElementNotFound(String elementNotFound) {
		this.elementNotFound = elementNotFound;
	}
	public String getIgnoredKeywords() {
		return ignoredKeywords;
	}
	public void setIgnoredKeywords(String ignoredKeywords) {
		this.ignoredKeywords = ignoredKeywords;
	}
}
