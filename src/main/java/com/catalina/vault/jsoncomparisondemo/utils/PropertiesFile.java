package com.catalina.vault.jsoncomparisondemo.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFile {
	private Properties p;
	String keyWordsToBeIgnored;
	String sourcePath;
	String targetPath;
	String reportDestination;
	String reportName;
	String colorForValueNotMatch;
	String colorForElementNotFound;
	String colorForIgnoredKeyWords;
	String sourcePrefix;
	String targetPrefix;
	String mandatoryKeywords;
	String groupedReport;
	String workingOn;
	public PropertiesFile() {
		FileInputStream reader = null;
		p = new Properties();
		try {

			File file = new File("./prp/utils.properties");
			reader = new FileInputStream(file);
			p.load(reader);
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.keyWordsToBeIgnored = p.getProperty("keywordsToBEIgnored");
		this.sourcePath = p.getProperty("source-path");
		this.targetPath = p.getProperty("target-path");
		this.reportDestination = p.getProperty("report-destination-path");
		this.reportName = p.getProperty("report-name");
		this.colorForValueNotMatch = p.getProperty("value-not-matching-colour");
		this.colorForElementNotFound = p.getProperty("element-not-found-colour");
		this.colorForIgnoredKeyWords = p.getProperty("keywords-ignore-colour");
		this.sourcePrefix = p.getProperty("Source-Prefix");
		this.targetPrefix = p.getProperty("Target-Prefix");
		this.mandatoryKeywords = p.getProperty("Mandatory-Keywords");
		this.groupedReport = p.getProperty("Grouped_Report");
	}

	public String getGroupedReport() {
		return groupedReport;
	}

	public void setGroupedReport(String groupedReport) {
		this.groupedReport = groupedReport;
	}

	public String getMandatoryKeywords() {
		return mandatoryKeywords;
	}

	public void setMandatoryKeywords(String mandatoryKeywords) {
		this.mandatoryKeywords = mandatoryKeywords;
	}

	public String getSourcePrefix() {
		return sourcePrefix;
	}

	public void setSourcePrefix(String sourcePrefix) {
		this.sourcePrefix = sourcePrefix;
	}

	public String getTargetPrefix() {
		return targetPrefix;
	}

	public void setTargetPrefix(String targetPrefix) {
		this.targetPrefix = targetPrefix;
	}

	public String getKeyWordsToBeIgnored() {
		return keyWordsToBeIgnored;
	}

	public void setKeyWordsToBeIgnored(String keyWordsToBeIgnored) {
		this.keyWordsToBeIgnored = keyWordsToBeIgnored;
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getTargetPath() {
		return targetPath;
	}

	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}

	public String getReportDestination() {
		return reportDestination;
	}

	public void setReportDestination(String reportDestination) {
		this.reportDestination = reportDestination;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getColorForValueNotMatch() {
		return colorForValueNotMatch;
	}

	public void setColorForValueNotMatch(String colorForValueNotMatch) {
		this.colorForValueNotMatch = colorForValueNotMatch;
	}

	public String getColorForElementNotFound() {
		return colorForElementNotFound;
	}

	public void setColorForElementNotFound(String colorForElementNotFound) {
		this.colorForElementNotFound = colorForElementNotFound;
	}

	public String getColorForIgnoredKeyWords() {
		return colorForIgnoredKeyWords;
	}

	public void setColorForIgnoredKeyWords(String colorForIgnoredKeyWords) {
		this.colorForIgnoredKeyWords = colorForIgnoredKeyWords;
	}

	public String getWorkingOn() {
		return p.getProperty("WorkingOn");
	}

	public void setWorkingOn(String workingOn) {
		p.setProperty("WorkingOn", workingOn);
	}
	
	public String getProperty(String property) {
		return p.getProperty(property);
	}
}
