package com.catalina.vault.jsoncomparisondemo.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author automation
 *
 */
public class FileUtil {

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */
	public static List<File> getFilesInFolder(String folderPath, String fileNamePattern) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		for (File file : files) {
			if (file.getName().contains(fileNamePattern))
				list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */

	/**
	 * 
	 * @param folderPath
	 */
	public static List<File> getFilesInFolder(String folderPath) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		for (File file : files) {
			list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param sourceFolderPath
	 * @param targetFolderPath
	 */
	public static boolean moveFilesToFolder(String sourceFolderPath, String targetFolderPath) {
		File folder = new File(sourceFolderPath);
		boolean flag = true;
		File files[] = folder.listFiles();
		for (File file : files) {
			if (file.renameTo(new File(targetFolderPath + file.getName()))) {
				file.delete();
			} else {
				flag = false;
			}
		}
		return flag;
	}
	
	/**
	 * 
	 * @param sourceFolderPath
	 * @param targetFolderPath
	 */
	public static boolean moveFilesToFolder(String sourceFolderPath, String targetFolderPath,String fileName) {
		File folder = new File(sourceFolderPath);
		File files[] = folder.listFiles();
		for (File file : files) {
			if(file.getName().contains(fileName))
				if (file.renameTo(new File(targetFolderPath + file.getName()))) {
					file.delete();
					return true;
				}
		}
		return false;
	}

	/**
	 * 
	 * @param folderPath
	 * @return
	 */
	public static boolean deleteFilesInFolder(String folderPath) {
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		try {
			for (File file : files)
				file.delete();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}
	/**
	 * 
	 * @param file
	 * @return
	 */
	public static String fileTOString(File file) throws IOException {
		StringBuilder str = new StringBuilder();
		FileInputStream fin = new FileInputStream(file);
		int i = 0;
		while ((i = fin.read()) != -1) {
			str.append((char) i);
		}
		fin.close();
		fin = null;
		return str.toString();
	}
	
	

}
