package com.catalina.vault.jsoncomparisondemo.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.cucumber.listener.Reporter;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.Scenario;

/**
 * Hello world!
 *
 */
public class CompareUtil {
	ExtentTest test = null;
	StringBuilder firstFileComparisonResult = null;
	StringBuilder secondFileComparisonResult = null;
	HashSet<String> keyWordsToBEIgnored = null;
	HashSet<String> mandatoryKeywords = null;
	HashSet<String> keywordsNotMatched = null;
	HashMap<String, ArrayList<String>> valuesToBeIgnored = null;
	boolean isDebug;
	static String currentParameter;

	public CompareUtil() {
		initialization();
	}

	public CompareUtil(boolean isDebug) {
		this.isDebug = isDebug;
		initialization();

	}

	private void initialization() {
		keyWordsToBEIgnored = new HashSet<>();
		keywordsNotMatched = new HashSet<String>();
		mandatoryKeywords = new HashSet<>();
		valuesToBeIgnored = new HashMap<>();
		PropertiesFile pFile = new PropertiesFile();
		String keyWordsToIgnored = pFile.getKeyWordsToBeIgnored();
		String list[] = keyWordsToIgnored.split(",");
		String mandatoryKeyword = pFile.getMandatoryKeywords();
		String list2[] = mandatoryKeyword.split(",");
		//String alternate =pFile.getProperty("WorkingOn");
		//String find =  "VALUES-IGNORED";
		String list3[] = {};//pFile.getProperty(find).split("],");

		for (String s : list) {
			keyWordsToBEIgnored.add(s);
		}

		for (String s : list2) {
			mandatoryKeywords.add(s);
		}

		for (String s : list3) {
			s = s.replace("[", "");
			s = s.replace("]", "");
			System.out.println(s);
			String temp[] = s.split(":");
			ArrayList<String> templist = new ArrayList<>(Arrays.asList(temp[1].split(",")));
			System.out.println(Arrays.asList(temp[1]));
			valuesToBeIgnored.put(temp[0].toLowerCase(), templist);
		}
	}

	/**
	 * 
	 * @param fileContentString1
	 * @param fileContentString2
	 * @param test
	 * @param colors
	 * @param sourceFile
	 * @param targetFile
	 * @return
	 */
	public boolean doesMatch(String fileContentString1, String fileContentString2, Colors colors,
			String sourceFile, String targetFile) {
		//this.test = test;
		// parsing string to json element
		JsonParser parser = new JsonParser();

		if (isDebug)
			Reporter.addStepLog("Parsing string first file to json Object");
		// parsing first file
		JsonElement json1 = parser.parse(fileContentString1);

		if (isDebug)
			Reporter.addStepLog("Parsing string Second file to json Object");
		// parsing second file
		JsonElement json2 = parser.parse(fileContentString2);

		// calling method to compare these files
		firstFileComparisonResult = new StringBuilder();
		secondFileComparisonResult = new StringBuilder();
		boolean result = compareJsonFiles(json1, json2, colors);
		
		String log = "<table><tr><th style=width:50%>" + sourceFile + "</th><th style=width:50%>" + targetFile
				+ "</th></tr><tr><td style=width:50%><table>" + firstFileComparisonResult.toString()
				+ "</table></td><td style=width:50%><table>" + secondFileComparisonResult.toString() + "</table></td></tr></table>";
		
		Reporter.addStepLog("logging" + log);

		if (keywordsNotMatched.size() != 0) {
			StringBuilder temp = new StringBuilder();
			temp.append("<table><tr><th style=width:50%>Mandatory Keyword which failed</th><th style=width:50%>");
			for (String s : keywordsNotMatched) {
				temp.append(s + "<br>");
			}
			temp.append("</th></tr></table>");
			Reporter.addStepLog("temp string:"+ temp.toString());
		}
		return result;
	}

	/**
	 * Compares the source and the target JSON content based on the type of content
	 * -
	 * 
	 * @param sourceElement
	 * @param targetElement
	 * @param colors
	 * @return
	 */
	private boolean compareJsonFiles(JsonElement sourceElement, JsonElement targetElement, Colors colors) {
		boolean isEqual = true;
		// checking elements are null or not
		// if one of them is null it will crash code
		if (sourceElement != null && targetElement != null) {
			// Checking if element is a json object or not and both elements belong to same
			// category
			if (sourceElement.isJsonObject() && targetElement.isJsonObject()) {
				// Collecting all the elements in object1
				firstFileComparisonResult.append("<tr><td>{</td></tr>");
				secondFileComparisonResult.append("<tr><td>{</td></tr>");
				Set<java.util.Map.Entry<String, JsonElement>> sourceElementSet = ((JsonObject) sourceElement)
						.entrySet();
				// Collecting all the elements in object2
				Set<java.util.Map.Entry<String, JsonElement>> targetElementSet = ((JsonObject) targetElement)
						.entrySet();

				// Changing Element to Object for direct access
				JsonObject json1obj = (JsonObject) sourceElement;
				JsonObject json2obj = (JsonObject) targetElement;

				// Checking if both list are of same size as well as checking if both are not
				// not null
				if (sourceElementSet != null && targetElementSet != null
						&& (targetElementSet.size() == sourceElementSet.size())) {
					// Iterating over every element
					for (java.util.Map.Entry<String, JsonElement> en : sourceElementSet) {
						// calling recursive solution for child
						// even if one key is not available in second object that will send null from
						// here
						// in next level of recursion one object will be null another one is some value
						// which will return false
						if (keyWordsToBEIgnored.contains(en.getKey())) {
							firstFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\"><font color=\"" + colors.getIgnoredKeywords() + "\">"
									+ en.getKey() + "\":" + en.getValue() + "</font></div></td></tr>");
							secondFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\"><font color=\"" + colors.getIgnoredKeywords() + "\">"
									+ en.getKey() + "\":" + json2obj.get(en.getKey()) + "</font></div></td></tr>");
							continue;
						}
						if (json2obj.get(en.getKey()) == null) {

							firstFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\"><font color=\"" + colors.getElementNotFound() + "\">"
									+ en.getKey() + ":" + en.getValue() + "</font></div></td></tr>");
						} else {
							firstFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\">\"" + en.getKey() + "\":</div></td></tr>");
							secondFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\">\"" + en.getKey() + "\":</div></td></tr>");
						}
						currentParameter = en.getKey();
						boolean result = compareJsonFiles(en.getValue(), json2obj.get(en.getKey()), colors);
						if (result == false && mandatoryKeywords.contains(en.getKey()))
							keywordsNotMatched.add(en.getKey());
						isEqual = result && isEqual;
					}

				}
				// if size is not same
				else {
					if (sourceElementSet.size() != targetElementSet.size()) {
						for (java.util.Map.Entry<String, JsonElement> element : sourceElementSet) {
							if (json2obj.get(element.getKey()) == null) {

								firstFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\"><font color=\"" + colors.getElementNotFound()
										+ "\">" + element.getKey() + ":" + element.getValue() + "</font></div></td></tr>");
							} else {
								firstFileComparisonResult
										.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\">\"" + element.getKey() + "\":" + element.getValue()+"</div></td></tr>");
							}
						}

						for (java.util.Map.Entry<String, JsonElement> en : targetElementSet) {
							if (json1obj.get(en.getKey()) == null) {

								secondFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\"><font color=\"" + colors.getElementNotFound()
										+ "\">" + en.getKey() + ":" + en.getValue() + "</font></div></td></tr>");
							} else {
								secondFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\">\"" + en.getKey() + "\":" + en.getValue()+"</div></td></tr>");
							}
						}
					}
				//	isEqual = isEqual & false;
				}
				firstFileComparisonResult.append("<tr><td>}</td></tr>");
				secondFileComparisonResult.append("<tr><td>}</td></tr>");
			}
			// in case if both element is belongs to array category
			else if (sourceElement.isJsonArray() && targetElement.isJsonArray()) {
				// Converting element to array
				JsonArray arr1 = sourceElement.getAsJsonArray();
				// Converting element to array
				JsonArray arr2 = targetElement.getAsJsonArray();
				// comparing the size of both array if they are not same some values are missing
				// in one file
				if (arr1.size() != arr2.size()) {

					isEqual = isEqual & false;
				}
				// If sizes are same
				else {
					// iterating both arrays
					// comparing old result with new result
					// if any one element in array is not same overall result will be files are not
					// same
					for (int i = 0; i < arr1.size(); i++)
						isEqual = isEqual && compareJsonFiles(arr1.get(i), arr2.get(i), colors);
				}
			}
			// in case both elements are null
			else if (sourceElement.isJsonNull() && targetElement.isJsonNull()) {

				isEqual = isEqual & true;
			}
			// if the are leaves
			else if (sourceElement.isJsonPrimitive() && targetElement.isJsonPrimitive()) {
				// everything works in String in GSON
				// Comparing values in form of String
				if (sourceElement.equals(targetElement) || (valuesToBeIgnored.get(currentParameter) != null
						&& valuesToBeIgnored.get(currentParameter).contains(sourceElement.getAsString()))) {
					firstFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\">" + sourceElement+"</div></td></tr>");
					secondFileComparisonResult.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\">" + targetElement+"</div></td></tr>");
					isEqual = isEqual & true;
				}
				// if both values are not same
				else {
					firstFileComparisonResult
							.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\"><font color=\"" + colors.getValueNotMatched() + "\">" + sourceElement + "</font></div></td></tr>");
					secondFileComparisonResult
							.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\"><font color=\"" + colors.getValueNotMatched() + "\">" + targetElement + "</font></div></td></tr>");
					isEqual = isEqual & false;
				}

			}
			// if element not belong to any category mentioned above
			else {
				//Reporter.addStepLog("Not a json object");
				//System.out.print(sourceElement.getAsString() + " - " + targetElement.getAsString());
				isEqual = isEqual & false;
			}
		}
		// if both elements are null means they are equal
		else if (sourceElement == null && targetElement == null) {

			isEqual = isEqual & true;
		}
		// if no case matched till now they are not equal
		else {
			if (sourceElement == null) {
				secondFileComparisonResult
						.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\"><font color=\"" + colors.getElementNotFound() + "\">" + targetElement + "</font></div></td></tr>");
			} else {
				firstFileComparisonResult
						.append("<tr><td><div style=\"word-wrap:break-word;width:200px;white-space:normal\"><font color=\"" + colors.getElementNotFound() + "\">" + sourceElement + "</font></div></td></tr>");
			}
			isEqual = isEqual & false;
		}
		// sending result back
		return isEqual;
	}
}
