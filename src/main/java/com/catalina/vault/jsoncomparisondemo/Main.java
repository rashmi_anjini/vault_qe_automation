package com.catalina.vault.jsoncomparisondemo;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.List;

import org.junit.Assert;

import com.catalina.vault.jsoncomparisondemo.utils.Colors;
import com.catalina.vault.jsoncomparisondemo.utils.CompareUtil;
import com.catalina.vault.jsoncomparisondemo.utils.FileUtil;
import com.catalina.vault.jsoncomparisondemo.utils.PropertiesFile;
import com.catalina.vault.steps.Verification;
import com.catalina.vault.steps.Verification2;
import com.cucumber.listener.Reporter;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.Scenario;


public class Main {
	public static final String PASS_STATUS = "Passed";
	public static final String FAIL_STATUS = "Failed";
	public static final String COMPARE_STATUS = "true";

	private static PropertiesFile propertiesFile;
	private static ExtentReports report = null;
	

	public static void jsoncomparemain(Scenario scenario) {
		propertiesFile = new PropertiesFile();
		
//		String sourceFile = FileSystems.getDefault().getPath(propertiesFile.getSourcePath()).toAbsolutePath().toString() + "\\"+ scenario.getName()+ ".json";
//		System.out.println("sourceFiles" +sourceFile);
		
		
		//for (int i = 0; i < sourceFiles.size(); i++) {
		//	File sourceFile = sourceFiles.get(FileUtil.);
	//File targetFile = findTargetFile(targetFiles, sourceFile.getName());
			CompareUtil compareUtil = null;
			Colors colors = new Colors();
			colors.setValueNotMatched(propertiesFile.getColorForValueNotMatch());
			colors.setElementNotFound(propertiesFile.getColorForElementNotFound());
			colors.setIgnoredKeywords(propertiesFile.getColorForIgnoredKeyWords());

			//ExtentTest extentTest = null;
			//extentTest = report.startTest(sourceFile.getName());

//			compareUtil = new CompareUtil();
			boolean compareStatus = false;
//			try {
//				compareStatus = compareUtil.doesMatch(FileUtil.fileTOString(new File(sourceFile)),
//						Verification.jsonResponse, colors, scenario.getName(),scenario.getName());
//			} catch (Exception e) {
//				// Unable to convert File to String
//				System.out.println("I am here"+e);
//			}
			compareStatus = Verification2.verify_DMP_Promotion_on_server("BL_2730_4157", "USA", scenario.getName());
			// For the messageType get the testCaseId from"
			// TODO: Double testCaseID = ReadTestCaseId.getTestCaseId(messageType,
			// controllerType);
			if (compareStatus) {
					Reporter.addStepLog( "AFter compare:" +PASS_STATUS);
					
			} else {
				Reporter.addStepLog( FAIL_STATUS);
				
			  }
			//Assert.assertEquals(COMPARE_STATUS, compareStatus);
	        }
		}

		//report.flush();
	

	/*private static File findTargetFile(List<File> targetFiles, String name) {
		// TODO Auto-generated method stub
		for (File file : targetFiles) {
			if (name.equals(file.getName()))
				return file;
		}
		return null;
	}
}*/
