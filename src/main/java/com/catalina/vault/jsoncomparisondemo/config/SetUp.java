package com.catalina.vault.jsoncomparisondemo.config;

import org.testng.annotations.Test;

import com.catalina.vault.jsoncomparisondemo.utils.PropertiesFile;
import com.cucumber.listener.Reporter;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class SetUp {

	/**
	 * This is the JSON comparison configuration setup. This will represent all data in first log
	 */
	@Test
	public void setup() {
		PropertiesFile pFile = new PropertiesFile();
		String tableStart = "<table><tr><th><B>Property</B></th><th><B>Value</B></th></tr>";
		String tableEnd = "</table>";
		StringBuilder entries = new StringBuilder();
		//ExtentReports report = new ExtentReports(pFile.getReportDestination() + pFile.getReportName() + ".html");
		//ExtentTest test = report.startTest("SetUp");

		entries.append("<tr><td>Keywords To Be Ignored</td><td>" + pFile.getKeyWordsToBeIgnored() + "</td></tr>");
		entries.append("<tr><td>Mandatory Keywords</td><td>" + pFile.getMandatoryKeywords() + "</td></tr>");
		entries.append("<tr><td>Source Path</td><td>" + pFile.getSourcePath() + "</td></tr>");
		entries.append("<tr><td>Target Path</td><td>" + pFile.getTargetPath() + "</td></tr>");
		entries.append("<tr><td>Report Destination Path</td><td>" + pFile.getReportDestination() + "</td></tr>");
		entries.append("<tr><td>Report Name</td><td>" + pFile.getReportName() + "</td></tr>");
		entries.append("<tr><td>Color For Value Not Matched</td><td><font style='color:"
				+ pFile.getColorForValueNotMatch() + "'>" + pFile.getColorForValueNotMatch() + "</font></td></tr>");
		entries.append("<tr><td>Color For Value Not Found</td><td><font style='color:"
				+ pFile.getColorForElementNotFound() + "'>" + pFile.getColorForElementNotFound() + "</font></td></tr>");
		entries.append("<tr><td>Color for Keyword to Ignored</td><td><font style='color:"
				+ pFile.getColorForIgnoredKeyWords() + "'>" + pFile.getColorForIgnoredKeyWords() + "</font></td></tr>");
		Reporter.addStepLog("tablejson: "+ tableStart + entries.toString() + tableEnd);
		//report.endTest(test);
		//report.flush();
	}
}
