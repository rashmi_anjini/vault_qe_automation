package com.catalina.vault.entities;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Iterator;
import java.util.logging.Level;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.logging.log4j.core.util.Loader;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;

import com.catalina.vault.steps.SharedResource;



public class BLsToExcel {
	
	private static XSSFWorkbook workbook ;
	private static XSSFSheet sheet;
	 static XSSFRow Row ;
	private static XSSFCell Cell ;
	//private final SharedResource sharedResource;
	
	public BLsToExcel(WebDriver driver) {
		
	}

	public void setCellData(String Result,int RowNum,int ColNum,String Sheetname) throws Exception {
		File file = new File(System.getProperty("user.dir")
				+ "/Result.xlsx");
				
	     FileInputStream fis = new FileInputStream(file);
	     FileOutputStream fos = null;
	     XSSFWorkbook workbook = new XSSFWorkbook(fis);
	     XSSFSheet sheet = workbook.getSheet(Sheetname);
	     XSSFRow row = null;
	     XSSFCell cell = null;
        try
        {
         row = sheet.getRow(RowNum);
         if(row == null)
             row = sheet.createRow(RowNum);
  
         cell = row.getCell(ColNum);
         if(cell == null)
             cell = row.createCell(ColNum);
  
         cell.setCellValue(Result);
  
         fos = new FileOutputStream(file);
         workbook.write(fos);
         fos.close();

     }catch(Exception e)
        {
    	 System.out.println(e);
        }

	}
	
	public String getCellData(String Sheetname , int RowNum, int ColNum) throws IOException{
		File file = new File(System.getProperty("user.dir")
				+ "/Result.xlsx");
				
	     FileInputStream fis = new FileInputStream(file);
	     FileOutputStream fos = null;
	     XSSFWorkbook workbook = new XSSFWorkbook(fis);
	     XSSFSheet sheet = workbook.getSheet(Sheetname);
	     XSSFRow Row = sheet.getRow(RowNum);
	    			if(Row == null) {
	    				return null;
	    			}
	    			XSSFCell Cell = Row.getCell(ColNum);
	    			if(Cell == null) {
	    				return null;
	    			}
	    			String CellData = Cell.getStringCellValue();
	    			return Cell.getStringCellValue();
	    		}
	
	/**
	 * Update this method to take the columm number
	 * @param TestCaseName
	 * @param Sheetname
	 * @return
	 * @throws IOException
	 */
	public String getCellValue(String TestCaseName, String Sheetname ,int columnNumber
								) throws IOException
	{
		File file = new File(System.getProperty("user.dir")
				+ "/TS.xlsx");
				
	     FileInputStream fis = new FileInputStream(file);
	     FileOutputStream fos = null;
	     XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet(Sheetname);
		  System.out.println("sheetname:"+sheet);
			//DataFormatter fmt = new DataFormatter();
			for (Row r :sheet) {
				if(TestCaseName.equalsIgnoreCase(r.getCell(1).getStringCellValue()))
				{
					System.out.println("testcase"+TestCaseName);
			          //int columnIndex = c.getColumnIndex();
			          int  rowNumber = r.getRowNum();
			         // this.sharedResource.setrowNumber(rowNumber);
			          System.out.println("rownumber : " +rowNumber);
			          String ref = r.getCell(columnNumber).getStringCellValue();
			          System.out.println("col value : " +ref);
			          return ref;
				}
				
			  
			}
			return TestCaseName;
			
		
	}
 
}
