package com.catalina.vault.entities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.catalina.vault.steps.Login;

import org.openqa.selenium.JavascriptExecutor;
import java.io.*;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
 

public class PerformanceTiming
{
	private static final Logger Log = LogManager.getLogger(Login.class);	
		public static void writePerfMetricasJSON(WebDriver webdriver, String filePath, String page)
		{
			JavascriptExecutor js=((JavascriptExecutor)webdriver);
			try {
			Thread.sleep(5000);
			}catch(Exception e) {e.printStackTrace();}
			
			long endtoendrespTime= (Long)js.executeScript("return (window.performance.timing.loadEventEnd-window.performance.timing.responseStart)");
			double endtoendRespTimeSec=endtoendrespTime/1000.0;
			long pageLoadTime= (Long)js.executeScript("return (window.performance.timing.loadEventEnd-window.performance.timing.navigationStart)");
			double pageLoadTimeSec=pageLoadTime/1000.0;
			
			Date date = new Date();
	        Timestamp ts=new Timestamp(date.getTime());
	        System.out.println("Page name :"+page );
			System.out.println("PageLoadTime Time :"+pageLoadTimeSec +"seconds");			
			System.out.println("end to end response Time :"+endtoendRespTimeSec+"seconds");
			System.out.println("timeStamp"+ts);
			Log.info("Page name :"+page +"PageLoadTime Time :"+pageLoadTimeSec +"seconds\n end to end response Time :"+endtoendRespTimeSec+"seconds\n timeStamp"+ts );
			
			
		 }		
		}