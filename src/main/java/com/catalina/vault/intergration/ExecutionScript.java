package com.catalina.vault.intergration;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ExecutionScript {
	private String awards;
	private String store;

	public String getAwards() {
		return awards;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public ExecutionScript(String awards, String store) {
		super();
		this.awards = awards;
		this.store = store;
	}

	public ExecutionScript() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "ExecutionScript [awards=" + awards + ", store=" + store + "]";
	}

}
