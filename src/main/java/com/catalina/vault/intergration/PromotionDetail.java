package com.catalina.vault.intergration;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.Gson;

@XmlRootElement
public class PromotionDetail {

	private String id;
	private String promotionName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public PromotionDetail(String id, String promotionName) {
		super();
		this.id = id;
		this.promotionName = promotionName;
	}

	public PromotionDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		
		return "PromotionDetails [id=" + id + ", promotionName=" + promotionName + "]";
		
		
	}

}