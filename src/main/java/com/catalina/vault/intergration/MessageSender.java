package com.catalina.vault.intergration;

import java.util.Properties;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.util.SystemOutLogger;
import org.mortbay.log.Log;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.catalina.vault.steps.KafkaActions;
import com.catalina.vault.steps.SharedResource;
import com.catalina.vault.utils.JsonUtils;
import com.cucumber.listener.Reporter;
import com.google.gson.JsonElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class MessageSender {

	private static final String topicName = "test";
	private static final String topicNamestore = "splice";
	private static final String key = "Key1";
	private static final String ipAddress = "10.10.108.60:9092";
	SharedResource sharedResource=new SharedResource();
	//private static final String ipAddress = "10.8.60.57:9092";
	private Properties props = null;
	private static final Logger Log= LogManager.getLogger(KafkaActions.class);
	public MessageSender() {
		props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, ipAddress);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
	}

	public void sendData(Data data) {

		try  {
			Producer<String, Data> producer = new KafkaProducer<>(props);
			ProducerRecord<String, Data> record = new ProducerRecord<>(topicName, key, data);
			producer.send(record).get();
			
			String jsondata= JsonUtils.convertObjectToJsonString(data);
			System.out.println("jsondata :"+jsondata);
			this.sharedResource.setJsondata(jsondata);
			Log.info("jsondata: "+ jsondata);
			System.out.println("Sent message \n" + data.toString());
			System.out.println("json" + data);
			Reporter.addStepLog("Sent message \n" + data.toString());
		
			producer.close();
		} catch(Exception e){
			System.out.println(e);
		}
	}
	
	public void sendDatastore(Data data) {

		try  {
			Producer<String, Data> producer = new KafkaProducer<>(props);
			ProducerRecord<String, Data> record = new ProducerRecord<>(topicNamestore, key, data);
			producer.send(record).get();
			System.out.println("Sent message \n" + data.toString());
			Reporter.addStepLog("Sent message \n" + data.toString());
			producer.close();
		} catch(Exception e){
			System.out.println(e);
		}
	}

}
