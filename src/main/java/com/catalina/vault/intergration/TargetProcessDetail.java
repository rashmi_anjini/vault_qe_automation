package com.catalina.vault.intergration;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TargetProcessDetail {
	private String testPlanId;
	private String testPlanBuildId;
	private String testCaseId;

	public String getTestPlanId() {
		return testPlanId;
	}

	public void setTestPlainId(String testPlanId) {
		this.testPlanId = testPlanId;
	}

	public String getTestPlanBuildId() {
		return testPlanBuildId;
	}

	public void setTestPlanBuildId(String testPlanBuildId) {
		this.testPlanBuildId = testPlanBuildId;
	}

	public String getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}

	public TargetProcessDetail(String testPlanId, String testPlanBuildId, String testCaseId) {
		super();
		this.testPlanId = testPlanId;
		this.testPlanBuildId = testPlanBuildId;
		this.testCaseId = testCaseId;
	}

	public TargetProcessDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "TargetProcessDetails [testPlanId=" + testPlanId + ", testPlanBuildId=" + testPlanBuildId
				+ ", testCaseId=" + testCaseId + "]";
	}

}