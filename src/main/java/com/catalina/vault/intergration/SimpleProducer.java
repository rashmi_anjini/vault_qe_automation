package com.catalina.vault.intergration;


import java.util.Properties;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

public class SimpleProducer {
  
   public static void main(String[] args) throws Exception{
           
      String topicName = "test2";
	  String key = "Key1";
      
      Properties props = new Properties();
      props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "10.88.1.52:9092");
      props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
      props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
	      
      Data data = new Data("fadsfad","daffasdf","fdasfas","fadsfad");
      Producer<String, Data> producer = new KafkaProducer <>(props);
	
	  ProducerRecord<String, Data> record = new ProducerRecord<>(topicName,key,data);
	  producer.send(record);	       
      producer.close();
	  
	  System.out.println("SimpleProducer Completed.");
   }
   
   
}


@XmlRootElement
class Data {
	String blId;
	String testcaseid;
	String tsScript;
	String verficationStep;
	public String getBlId() {
		return blId;
	}
	public void setBlId(String blId) {
		this.blId = blId;
	}
	public String getTestcaseid() {
		return testcaseid;
	}
	public void setTestcaseid(String testcaseid) {
		this.testcaseid = testcaseid;
	}
	public String getTsScript() {
		return tsScript;
	}
	public void setTsScript(String tsScript) {
		this.tsScript = tsScript;
	}
	public String getVerficationStep() {
		return verficationStep;
	}
	public void setVerficationStep(String verficationStep) {
		this.verficationStep = verficationStep;
	}
	public Data(String blId, String testcaseid, String tsScript, String verficationStep) {
		super();
		this.blId = blId;
		this.testcaseid = testcaseid;
		this.tsScript = tsScript;
		this.verficationStep = verficationStep;
	}
	public Data() {
		// TODO Auto-generated constructor stub
	}
	
	
	
}