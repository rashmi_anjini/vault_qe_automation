package com.catalina.vault.intergration;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StoreDetail {
	private String store;
	private String chain;
	private String network;
	private String country;
	private String printerType;

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getChain() {
		return chain;
	}

	public void setChain(String chain) {
		this.chain = chain;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPrinterType() {
		return printerType;
	}

	public void setPrinterType(String printerType) {
		this.printerType = printerType;
	}

	public StoreDetail(String store, String chain, String network, String country, String printerType) {
		super();
		this.store = store;
		this.chain = chain;
		this.network = network;
		this.country = country;
		this.printerType = printerType;
	}

	public StoreDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "StoreDetails [store=" + store + ", chain=" + chain + ", network=" + network + ", country=" + country
				+ ", printerType=" + printerType + "]";
	}

}
