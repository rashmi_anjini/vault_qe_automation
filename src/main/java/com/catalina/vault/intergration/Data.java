package com.catalina.vault.intergration;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Data {

	private PromotionDetail promotionDetail;
	private ExecutionScript executionScript;
	private StoreDetail storeDetail;
	private TargetProcessDetail targetProcessDetail;

	public PromotionDetail getPromotionDetail() {
		return promotionDetail;
	}

	public void setPromotionDetail(PromotionDetail promotionDetail) {
		this.promotionDetail = promotionDetail;
	}

	public ExecutionScript getExecutionScript() {
		return executionScript;
	}

	public void setExecutionScript(ExecutionScript executionScript) {
		this.executionScript = executionScript;
	}

	public StoreDetail getStoreDetail() {
		return storeDetail;
	}

	public void setStoreDetail(StoreDetail storeDetail) {
		this.storeDetail = storeDetail;
	}

	public TargetProcessDetail getTargetProcessDetail() {
		return targetProcessDetail;
	}

	public void setTargetProcessDetail(TargetProcessDetail targetProcessDetail) {
		this.targetProcessDetail = targetProcessDetail;
	}

	public Data(PromotionDetail promotionDetail, ExecutionScript executionScript, StoreDetail storeDetail,
			TargetProcessDetail targetProcessDetail) {
		super();
		this.promotionDetail = promotionDetail;
		this.executionScript = executionScript;
		this.storeDetail = storeDetail;
		this.targetProcessDetail = targetProcessDetail;
	}

	public Data() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Data [promotionDetail=" + promotionDetail + ", executionScript=" + executionScript + ", storeDetail="
				+ storeDetail + ", targetProcessDetail=" + targetProcessDetail + "]";
	}
	
	
}

