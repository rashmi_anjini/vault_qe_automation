package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvertisementRetailTriggered extends BaseClass {

	WebDriver driver;
	
	@FindBy(xpath = "//a[contains(text(),'Add Criteria')]")
	WebElement addCreteria;
	
	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectType;
	
	@FindBy(xpath="/html/body/div[37]/div/input")
	WebElement selectTypeInput;
	
	@FindBy(xpath = "//*[@id='trigger-type_1']/section[16]/span[1]/span/a")
	WebElement generateMCLUIDButton;

	public AdvertisementRetailTriggered(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void clickOnAddCreteria(){
	
		this.addCreteria.click();
	}
	
	
	public void clickSelectType(){
		wait(this.selectType);
		this.selectType.click();
	}
	
	public void typeSelectTypeInput(String text) {
		wait(this.selectTypeInput);
		this.selectTypeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.selectTypeInput.sendKeys(Keys.ENTER);
	}
	
	public void clickGenerateMCLUIDButton(){
		wait(this.generateMCLUIDButton);
		this.generateMCLUIDButton.click();
	}
}
