package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.listener.Reporter;

public class CampaignPage extends BaseClass {

	WebDriver driver;

	@FindBy(xpath = "//*[@id='content']/div[2]/div[2]/div[1]/div[1]/a")
	WebElement newButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div[2]/div[1]/div[1]/ul/li[1]/a")
	WebElement displayNetworkCampaign;

	@FindBy(xpath = "//*[@id='content']/div[2]/div[1]/div/div[1]/button")
	WebElement type;

	@FindBy(xpath = "//*[@id='content']/div[2]/div[1]/div/div[2]/button")
	WebElement status;

	@FindBy(xpath = "//*[@id='content']/div[2]/div[1]/div/div[3]/button")
	WebElement userType;

	@FindBy(id = "table_search_txt")
	WebElement searchBox;

	@FindBy(id = "table_search_btn")
	WebElement searchButton;
	
	@FindBy(xpath = "//div[@rv-show='table.supports_create']/ul/li[@rv-on-click='table.create']/a[text()='Experimental Campaign']")
	WebElement experimentalCampaign;

	@FindBy(xpath = " //*[@id='content']/div[2]/div[2]/div[1]/div[8]/ul/li[4]/a")
	WebElement nextPage;

	@FindBy(xpath = "//*[@id='content']/div[1]/div/div/div[1]/button")
	WebElement importOption;

	@FindBy(xpath = "//*[@id='content']/div[1]/div/div/div[1]/ul/li/a")
	WebElement jsonImportOption;

	public CampaignPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickNewButton() {
		try{
		wait(this.newButton);
		this.newButton.click();
		}
		catch(Exception e){
			Reporter.addStepLog("clickNewButton"+e);
		}
	}

	
	public void clickExperimentalCampaign() {
		try{
		wait(this.experimentalCampaign);
		this.experimentalCampaign.click();
		}
		catch(Exception e){
			Reporter.addStepLog("clickExperimentalCampaign"+e);
			}
	}
	
	public void clickDisplayNetworkCampaign() {
		try{
		wait(this.displayNetworkCampaign);
		this.displayNetworkCampaign.click();
		}
		catch(Exception e){
			Reporter.addStepLog("clickDisplayNetworkCampaign: "+e);
			}
	}

	public void clickType() {
		
		this.type.click();
	}

	public void clickStatus() {
		this.status.click();
	}

	public void clickUserType() {
		this.userType.click();
	}

	public void typeSearchBox(String text) {
		this.searchBox.sendKeys(text);

	}

	public void clickSearchButton() {
		this.searchButton.click();
	}

	public void clickNextPage() {
		this.nextPage.click();
	}

	public void clickImportOption() {
		this.importOption.click();
	}

	public void clickJsonImportOption() {
		this.jsonImportOption.click();
	}

}
