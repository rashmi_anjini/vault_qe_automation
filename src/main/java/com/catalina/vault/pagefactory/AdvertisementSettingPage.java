package com.catalina.vault.pagefactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvertisementSettingPage extends BaseClass {

	WebDriver driver;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[1]/div[2]/div/input")
	WebElement nameInput;

	@FindBy(xpath = "//*[@id='custom_image_upload_control']")
	WebElement customImage;

	@FindBy(xpath = "//div[@aria-hidden='false']/div/div/div[@class='modal-footer']/a")
	WebElement saveButton;

	@FindBy(xpath = "//*[@id=\"start-date-schedule\"]")
	WebElement startDateFormat;

	@FindBy(xpath = "//*[@id=\"stop-date-schedule\"]")
	WebElement stopDateFormat;

	@FindBy(xpath = "//td[@class='available']")
	List<WebElement> dateAvailable;

	@FindBy(xpath = "//td[@class='weekend available']")
	List<WebElement> dateWeekendAvailable;

	@FindBy(xpath = "//td[@class='today active start-date active end-date available']")
	WebElement todayDate;

	@FindBy(xpath = "/html/body/div[215]/div[1]/div[2]/table/thead/tr[1]/th[3]")
	WebElement nextMonth;

	@FindBy(xpath = "/html/body/div[15]/div[1]/div[2]/table/thead/tr[1]/th[3]")
	WebElement stopDateNextMonth;

	public AdvertisementSettingPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void typeName(String text) {
		wait(this.nameInput);
		this.nameInput.clear();
		this.nameInput.sendKeys(text);
	}

	public void typeCustomImageUrl(String text) {
		this.customImage.sendKeys(text);
	}

	public void scrollDown(int start, int end) {
		// TODO Auto-generated method stub
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scrollTo(" + start + "," + end + ")");

	}

	DateFormat dateFormat = new SimpleDateFormat("dd");
	Date date = new Date();
	String currentDate = dateFormat.format(date);
	int currDate = Integer.parseInt(currentDate);

	int startDate = currDate + 0;
	int stopDate = startDate + 2;

	public void startDateFormatter() {

		this.startDateFormat.click();

		String str1 = todayDate.getText();
		int temp1 = Integer.parseInt(str1);
		if (temp1 == startDate) {
			todayDate.click();
		}
	}

	public void stopDateFormatter() {
		this.stopDateFormat.click();
		List<WebElement> dateAvailableList = dateAvailable;
		List<WebElement> dateWeekendAvailableList = dateWeekendAvailable;
		int flag = 0;
		for (int i = 0; i < dateAvailableList.size(); i++) {
			String str = dateAvailableList.get(i).getText();
			if (str.equalsIgnoreCase("")) {
				stopDateNextMonth.click();
				flag = 1;
			} else {
				int temp = Integer.parseInt(str);
				if (temp == stopDate) {
					dateAvailableList.get(i).click();
					flag = 1;
				}
			}
			for (int j = 0; j < dateWeekendAvailableList.size(); j++) {
				String strr = dateWeekendAvailableList.get(j).getText();
				if (strr.equalsIgnoreCase("") && flag == 1) {
					stopDateNextMonth.click();
				} else if (flag == 1) {
					int temp4 = Integer.parseInt(strr);
					if (temp4 == stopDate) {
						dateWeekendAvailableList.get(j).click();
					}
				}
			}
		}

	}

	public void clickOnSaveButton() {
		this.saveButton.click();
	}

}
