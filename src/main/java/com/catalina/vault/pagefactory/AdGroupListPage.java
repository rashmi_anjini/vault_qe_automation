package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdGroupListPage extends BaseClass{

	WebDriver driver;

	@FindBy(xpath="(//*[@class='btn-group btn-group-table-create'])[2]")
	//*[@id="campaign_adgroups"]/div[3]/div[1]/div[1]/a
	public WebElement newButton;

	@FindBy(xpath = "//*[@id='campaign_adgroups']/div[3]/div[1]/div[1]/ul/li[1]")
	WebElement adGroupButton;

	@FindBy(id="table_search_txt")
	WebElement searchBox;

	@FindBy(id="table_search_btn")
	WebElement searchButton;

	@FindBy(xpath = "//*[@id='campaign_adgroups']/div[3]/div[1]/div[8]/ul/li[4]/a")
	WebElement nextPageButton;
	
	@FindBy(xpath="//*[@id='content']/div[1]/div[1]/ol/li[3]/a")
	WebElement goToOnePreviousStep;
	
	@FindBy(xpath="//*[@id='campaign_adgroups']/div[3]/div[2]/table/tbody/tr/td[2]/span[1]/span[1]/a")
	WebElement firstElementInList;
	
	
	
	public AdGroupListPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickNewButton() {
		wait(this.newButton);
		this.newButton.click();
	}

	public void clcikAdGroupButton() {
		wait(this.adGroupButton);
		this.adGroupButton.click();
	}

	public void typeSeachBox(String text) {
		wait(this.searchBox);
		this.typeSeachBox(text);
	}

	public void clickSreachButton() {
		wait(this.searchButton);
		this.searchButton.click();
	}

	public void clickNextPageButton() {
		this.nextPageButton.click();
	}
	
	public void clickGoToOnePreviousStep(){
		wait(this.goToOnePreviousStep);
		this.goToOnePreviousStep.click();
	}
	
	
	public void clickFirstElementInList(){
		wait(this.firstElementInList);
		this.firstElementInList.click();
	}
	
	
}
