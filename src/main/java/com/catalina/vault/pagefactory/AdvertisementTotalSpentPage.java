package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvertisementTotalSpentPage extends BaseClass{
	
	WebDriver driver;
	
	@FindBy(xpath = "//*[@id='targeting_triggers']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCreteria;

	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;

	@FindBy(xpath = "(//a[span[text()='(measure)']])[2]")
	WebElement measureButton;

	@FindBy(xpath = "(//a[span[text()='(condition)']])[2]")
	WebElement conditionButton;
	
	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[4]")
	WebElement triggerScopeButton;
	
	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement selectTypeInput;

	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement measureInput;
	
	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement conditionInput;
	
	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement triggerScopeInput;

	@FindBy(xpath = "(//*/section/span/span/input)[9]")
	WebElement mainInput;

	@FindBy(xpath = "(//*/section/span/span/input)[11]")
	WebElement mainInput2;
	
	
	

	public AdvertisementTotalSpentPage(WebDriver driver){
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
	public void clickOnAddCreteria() {
		wait(this.addCreteria);
		this.addCreteria.click();
	}

	public void clickOnSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}

	public void clickOnMeasure() {
		wait(this.measureButton);
		this.measureButton.click();
	}
	
	public void clickOnCondition() {
		wait(this.conditionButton);
		this.conditionButton.click();
	}
	
	public void clickOnTriggerScope() {
		wait(this.triggerScopeButton);
		this.triggerScopeButton.click();
	}

	public void typeSelectTypeInput(String text) {
		wait(this.selectTypeInput);
		this.selectTypeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.selectTypeInput.sendKeys(Keys.ENTER);
	}

	public void typeMeasureInput(String text) {
		wait(this.measureInput);
		this.measureInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.measureInput.sendKeys(Keys.ENTER);
	}


	public void typeConditionInput(String text) {
		wait(this.conditionInput);
		this.conditionInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.conditionInput.sendKeys(Keys.ENTER);
	}

	public void typeTriggerScopeInput(String text) {
		wait(this.triggerScopeInput);
		this.triggerScopeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.triggerScopeInput.sendKeys(Keys.ENTER);
	}

	
	public void typeMainInput(String text) {
		wait(this.mainInput);
		this.mainInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.mainInput.sendKeys(Keys.ENTER);
	}
	
	public void typeMain2Input(String text) {
		wait(this.mainInput2);
		this.mainInput2.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.mainInput2.sendKeys(Keys.ENTER);
	}
	
	

}
