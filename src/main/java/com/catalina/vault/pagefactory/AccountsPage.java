package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountsPage {

	WebDriver driver;
	
	@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div[1]/a")
	WebElement newButton;
	
	@FindBy(id="table_search_txt")
	WebElement searchBox;
	
	@FindBy(id="table_search_btn")
	WebElement searchButton;
	
	@FindBy(xpath="//*[@id='content']/div[1]/div/div/div/button")
	WebElement importButton;
	
	@FindBy(xpath="//*[@id='content']/div[1]/div/div/div/ul/li[1]/a")
	WebElement importCSVOption;
	
	@FindBy(xpath="//*[@id='content']/div[1]/div/div/div/ul/li[3]/a")
	WebElement downloadExampleFileButton;
	
	@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div[8]/ul/li[4]/a")
	WebElement nextPage;
	
	AccountsPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickNewButton(){
		this.newButton.click();
	}
	
	public void typeSearchBox(String text){
		this.searchBox.sendKeys(text);
	}
	
	public void clickSearchButton(){
		this.searchButton.click();
	}
	
	public void clickImportButton(){
		this.importButton.click();
	}
	
	public void clickImportCSVOption(){
		this.importCSVOption.click();
	}
	
	public void clickDownloadExampleFileButton(){
		this.downloadExampleFileButton.click();
	}
	
	public void clickNextPage(){
		this.nextPage.click();
	}
}
