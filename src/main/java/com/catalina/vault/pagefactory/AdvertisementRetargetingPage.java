package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvertisementRetargetingPage extends BaseClass {

	WebDriver driver;

	@FindBy(xpath = "//*[@id='targeting_triggers']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteriaButton;

	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;

	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement selectTypeInput;

	@FindBy(xpath = "(//a[span[text()='(scope)']])[2]")
	WebElement scopeButton;

	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement scopeInput;

	@FindBy(xpath = "(//*/div/ul/li/input)[13]")
	WebElement inputDataBox;

	@FindBy(xpath = "(//a[span[text()='(action)']])[1]")
	WebElement actionButton;

	@FindBy(xpath = "/html/body/div[38]/div/input")
	WebElement actionInput;

	@FindBy(xpath = "(//a[span[text()='(operation)']])[2]")
	WebElement operationButton;

	@FindBy(xpath = "/html/body/div[38]/div/input")
	WebElement operationInput;

	@FindBy(xpath = "//*[@id='trigger-type_1']/section[5]/span[6]/span/span[1]/input[2]")
	WebElement conditionValueInput;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[5]")
	WebElement triggerScopeButton;

	@FindBy(xpath = "/html/body/div[38]/div/input")
	WebElement triggerScopeInput;

	@FindBy(xpath = "(//a[span[text()='(time period)']])[4]")
	WebElement timePeriodButton;

	@FindBy(xpath = "/html/body/div[38]/div/input")
	WebElement timePeriodInput;

	public AdvertisementRetargetingPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void clickAddCriteriaButton() {
		wait(this.addCriteriaButton);
		this.addCriteriaButton.click();
	}

	public void clickSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}

	public void typeSelectTypeInput(String text) {
		wait(this.selectTypeInput);
		this.selectTypeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectTypeInput.sendKeys(Keys.ENTER);
	}

	public void clickScopeButton() {
		wait(this.scopeButton);
		this.scopeButton.click();
	}

	public void typeScopeInput(String text) {
		wait(this.scopeInput);
		this.scopeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.scopeInput.sendKeys(Keys.ENTER);
	}

	public void typeInputDataBox(String text) {
		wait(this.inputDataBox);
		this.inputDataBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.inputDataBox.sendKeys(Keys.ENTER);
	}

	public void clickActionButton() {
		wait(this.actionButton);
		this.actionButton.click();
	}

	public void typeActionInput(String text) {
		wait(this.actionInput);
		this.actionInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.actionInput.sendKeys(Keys.ENTER);
	}

	public void clickOperationButton() {
		wait(this.operationButton);
		this.operationButton.click();
	}

	public void typeOperationInput(String text) {
		wait(this.operationInput);
		this.operationInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.operationInput.sendKeys(Keys.ENTER);
	}

	public void typeConditionValueInput(String text) {
		wait(this.conditionValueInput);
		this.conditionValueInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.conditionValueInput.sendKeys(Keys.ENTER);
	}

	public void clickTriggerScopeButton() {
		wait(this.triggerScopeButton);
		this.triggerScopeButton.click();
	}

	public void typeTriggerScopeInput(String text) {
		wait(this.triggerScopeInput);
		this.triggerScopeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.triggerScopeInput.sendKeys(Keys.ENTER);
	}

	public void clickTimePeriodButton() {
		wait(this.timePeriodButton);
		this.timePeriodButton.click();
	}

	public void typeTimePeriodInput(String text) {
		wait(this.timePeriodInput);
		this.timePeriodInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.timePeriodInput.sendKeys(Keys.ENTER);
	}

}
