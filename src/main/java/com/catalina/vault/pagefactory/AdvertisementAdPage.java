package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvertisementAdPage extends BaseClass {

	WebDriver driver;
	
	@FindBy(xpath="//*[label='USA - 6\" Advertisement']/parent::*/div/button")
	WebElement advertisementAddtoCart;
	
	@FindBy(xpath = "//*[@id='content']/div[3]/div[1]/nav/div/div[2]/div[2]/ul/li/a")
	WebElement nextButton;

	public AdvertisementAdPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void clickOnAddToCart(){
		wait(this.advertisementAddtoCart);
		this.advertisementAddtoCart.click();
	}
	
	public void clickOnNext(){
		this.nextButton.click();
	}
	
}
