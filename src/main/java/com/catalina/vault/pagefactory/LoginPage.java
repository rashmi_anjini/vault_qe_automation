package com.catalina.vault.pagefactory;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	WebDriver driver;

	@FindBy(name = "username")
	WebElement userName;

	@FindBy(name = "password")
	WebElement password;
	
	@FindBy(id="login-btn")
	WebElement loginButton;

	public LoginPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void enterUserName(String text){
		this.userName.sendKeys(text);
	}
	
	public void enterPassword(String password){
		this.password.sendKeys(password);
		//JavascriptExecutor js = (JavascriptExecutor) driver; 
		//((Object) driver).js("return window.performance.timing.navigationStart");
	}

	public void clickLogin(){
		
		this.loginButton.click();
		
	}
}
