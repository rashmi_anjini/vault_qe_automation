package com.catalina.vault.pagefactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManufacturerAdPage extends BaseClass {
	WebDriver driver;
	NewDisplayNetworkCampaignPage camppage;

	public ManufacturerAdPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		camppage = new NewDisplayNetworkCampaignPage(this.driver);
		PageFactory.initElements(this.driver, this);
	}

	@FindBy(xpath = "//a[contains(@href,'-45b318fc0e0b')]/ancestor::div[@class='footer']/button")
	// "//*[label='USA - 6in Manufacturing Coupon']/parent::*/div/button")
	// a[@href='/#/platform/templates/new/c8ff1443-fb9c-406e-9e0f-45b318fc0e0b']/parent::*/div/button
	WebElement manufacturerCouponAddToCart;

	@FindBy(xpath = "//*[@id='content']/div[3]/div[1]/nav/div/div[2]/div[2]/ul/li/a")
	WebElement nextButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[4]/div[2]/div/div/div[1]/a")
	WebElement frequencyCappingClickHereButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[4]/div[3]/div/div/div[1]/a")
	WebElement distributedCappingClickHereButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[4]/div[3]/div/div/div[2]/div[1]/input")
	WebElement distributedCappingTextBox;

	@FindBy(xpath = "//a[@rv-on-click='$view.addInventory']")
	WebElement addProductGroupButton;

	@FindBy(xpath = "//div[@rv-show='inventory.is_list_based']/div/ul/li/input")
	WebElement inputForProductValidation;

	@FindBy(xpath = "//input[@class='form-control rolling-days-input']")
	WebElement rollingDayInput_product;

	@FindBy(xpath = "//input[@rv-on-click='inventory.setManualBased']")
	WebElement manualListCheckBox;

	@FindBy(xpath = "//a[@rv-on-click='$view.addInventory']")
	WebElement productValidationTextBox;

	@FindBy(xpath = "//div[@class='col-sm-10']/a/div/input")
	WebElement productcustomImage;

	@FindBy(xpath = "(//ul/li/input)[3]")
	WebElement productValidationInputBox;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[1]/a")
	WebElement settingButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[2]/a")
	WebElement redemtionClearingButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[3]/a")
	WebElement targetingButton;

	@FindBy(xpath = "//a[span[text()='Choose Clearing Method']]")
	WebElement clearingMethodButton;

	@FindBy(xpath = "/html/body/div[16]/div/input")
	WebElement clearingMethodTypeBox;

	@FindBy(xpath = "(//a[span[text()='select...']])[3]")
	WebElement barCodeTypeButton;

	@FindBy(xpath = "/html/body/div[16]/div/input")
	WebElement barCodeTypeTextBox;

	@FindBy(xpath = "//*[@id='ad_redemption']/div/div/div/form/div[2]/div[5]/div[2]/div[2]/div/div[4]/div[1]/div/input")
	WebElement barCodeValue;

	@FindBy(xpath = "//*[@id='ad_redemption']/div/div/div/form/div[2]/div[5]/div[2]/div[2]/div/div[2]/div[1]/div/input")
	WebElement staticBarCodeValue;

	@FindBy(xpath = "//*[@id='ad_redemption']/div/div/div/form/div[2]/div[2]/div/div[1]/a")
	WebElement enableExpireDateButton;

	@FindBy(xpath = "(//a[span[text()='date formatting (default)']])[1]")
	WebElement dateFormatButton;

	@FindBy(xpath = "/html/body/div[16]/div/input")
	WebElement dateFormatTextBox;

	@FindBy(xpath = "//*[@id='ad_redemption']/div/div/div/form/div[2]/div[2]/div/div[2]/div[2]/button")
	WebElement calanderButton;

	@FindBy(xpath = "/html/body/div[11]/div[1]/div[2]/table/tbody/tr[5]/td[5]")
	WebElement dateOption;

	@FindBy(xpath = "//*[@id='static_image_upload_control']")
	WebElement addImage;

	@FindBy(xpath = "(//*[@id='mm_PRODUCT_SHOT']/div/div/div/div/div[3]/a)[10]")
	WebElement saveImageButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[1]/div/div[1]/div/div/div/a")
	WebElement templateOptionButton;

	@FindBy(xpath = "//div[span[text()='USA - 6in Manufacturing Coupon']]/button")
	WebElement USA_6in_Manufacturing_Coupon;

	@FindBy(xpath = "//div[span[text()='USA - Manufacturing Coupon - GS1']]/button")
	WebElement gs1CouponTemplateButton;

	@FindBy(xpath = "//*[@id='template_browse_modal']/div/div/div[3]/button")
	WebElement templateSaveButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[3]/div[7]/div[1]/textarea")
	WebElement qualiferLineInput;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[3]/div[9]/div[1]/textarea")
	WebElement saveLineInput;

	@FindBy(xpath = "//*[@id='custom_image_upload_control']")
	WebElement addStaticImage;

	@FindBy(xpath = "(//*[@id='mm_STATIC_IMAGE']/div/div/div/div/div[3]/a)[10]")
	WebElement staticImageSave;

	@FindBy(xpath = "//*[@id='ad-save-bottom']")
	WebElement saveButton;

	@FindBy(id = "ad-publish")
	WebElement publishButton;

	@FindBy(xpath = "//*[@id='global_confirm_dialog']/div/div/div[3]/button[2]")
	WebElement yesThisIsOkButton;

	@FindBy(xpath = "//*[@id='content']/div[1]/div[1]/h3/small")
	WebElement promotionID;

	@FindBy(xpath = "//*[@id='targeting_triggers']/div/div/div[1]/h4/a")
	WebElement addTiggerGroup;

	@FindBy(xpath = "//*[@id='targeting_triggers']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteriaButton;

	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;

	@FindBy(xpath = "/html/body/div[39]/div/input")
	WebElement selectTypeTextBox;

	@FindBy(xpath = "//a[span[text()='(measure)']]")
	WebElement measureButton;

	@FindBy(xpath = "/html/body/div[39]/div/input")
	WebElement measureTextBox;

	@FindBy(xpath = "//a[span[text()='(condition)']]")
	WebElement conditionButton;

	@FindBy(xpath = "/html/body/div[39]/div/input")
	WebElement conditionTextBox;

	@FindBy(xpath = "//*[@id='trigger-type_1']/section[1]/span[2]/span[1]/input[2]")
	WebElement inputUnitTextBox;

	@FindBy(xpath = "//a[span[text()='(product attribute)']]")
	WebElement productAttributeButton;

	@FindBy(xpath = "/html/body/div[39]/div/input")
	WebElement productAttributeTextBox;

	@FindBy(xpath = "//a[span[text()='(list source)']]")
	WebElement listSourceButton;

	@FindBy(xpath = "/html/body/div[39]/div/input")
	WebElement listSourceTextBox;

	@FindBy(xpath = "(//*/div/ul/li/input)[6]")
	WebElement inputForManager;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[2]")
	WebElement triggerScopeButton;

	@FindBy(xpath = "/html/body/div[40]/div/input")
	WebElement inputTriggerScope;

	@FindBy(xpath = "(//a[span[text()='(operation)']])[7]")
	WebElement operationButton;

	@FindBy(xpath = "(//a[span[text()='(measure)']])[2]")
	WebElement totalOrderMeasureButton;

	@FindBy(xpath = "(//a[span[text()='(condition)']])[2]")
	WebElement totalOrderConditionButton;

	@FindBy(xpath = "//*[@id='trigger-type_1']/section[4]/span/span[1]/input[2]")
	WebElement totalOrderValue;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[3]")
	WebElement totalOrderTriggerScope;

	@FindBy(xpath = "//*[@id='trigger-type_1']/section[1]/span[3]/span[6]/span/textarea")
	WebElement inputForDepartmentCodes;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[1]")
	WebElement departmentTriggerScopeButton;

	@FindBy(xpath = "//a[span[text()='Choose Company Prefix:']]")
	WebElement chooseCompanyPrefixButton;

	@FindBy(xpath = "//a[span[text()='Choose Clearing House:']]")
	WebElement chooseClearingHouseButton;

	@FindBy(xpath = "/html/body/div[16]/div/input")
	WebElement companyInputPrefixInput;

	@FindBy(xpath = "/html/body/div[16]/div/input")
	WebElement clearingHouseInput;

	@FindBy(xpath = "//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul")
	WebElement checkListSelected;

	public void addManufacturerCouponTocart() {
		wait(this.manufacturerCouponAddToCart);
		this.manufacturerCouponAddToCart.click();
	}

	public void clickOnNext() {
		wait(this.nextButton);
		this.nextButton.click();
	}

	public void clickFrequencyCappingClickHereButton() {
		wait(this.frequencyCappingClickHereButton);
		this.frequencyCappingClickHereButton.click();
	}

	public void clickDistributedCappingClickHereButton() {
		wait(this.distributedCappingClickHereButton);
		this.distributedCappingClickHereButton.click();
	}

	public void typeDistributedCappingTextBox(String text) {
		wait(this.distributedCappingTextBox);
		this.distributedCappingTextBox.clear();
		this.distributedCappingTextBox.sendKeys(text);
	}

	public void clickAddProductGroupButton() {
		wait(this.addProductGroupButton);
		this.addProductGroupButton.click();
	}

	public void ClickOnManualListCheckBox() {
		wait(this.manualListCheckBox);
		this.manualListCheckBox.click();
	}

	public void typeRollingDay() {
		wait(this.rollingDayInput_product);
		this.rollingDayInput_product.click();
		this.rollingDayInput_product.sendKeys(Keys.chord(Keys.CONTROL, "a"), "3");
		this.rollingDayInput_product.sendKeys(Keys.ENTER);
	}

	public void typeinputForProductValidation(String text) throws InterruptedException {
		wait(this.inputForProductValidation);
		this.inputForProductValidation.sendKeys(text);
////		WebElement dropdown = this.checkListSelected;
////
////		List<WebElement> optionss = dropdown.findElements(By.tagName("li"));
////		WebElement searchingElem = null;
////		try {
////			searchingElem = driver.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
////		} catch (Exception e) {
////			System.out.println("Searching element not found.");
////		}
////		int options = optionss.size();
////		while (options > 1 || (options == 1 && searchingElem != null)) {
////			Thread.sleep(200);
////			optionss = dropdown.findElements(By.tagName("li"));
////			options = optionss.size();
////			try {
////				searchingElem = driver
////						.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
////			} catch (Exception e) {
////				System.out.println("Searching element not found.");
////				searchingElem = null;
////			}
////			if (searchingElem != null) {
////				System.out.println("Waiting for Searching element to clear.");
////				camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
////				System.out.println("Done waiting for Searching element to clear.");
////				searchingElem = null;
////				optionss = dropdown.findElements(By.tagName("li"));
////				options = optionss.size();
////			}
////			System.out.println("Dropdown list size is " + options);
////			for (WebElement elem : optionss) {
////				try {
////					if (!elem.getText().contains(text)) {
////						System.out.println("Dropdown list contains text other than " + text);
////						options = optionss.size();
////						break;
////					} else {
////						System.out.println("Dropdown list contains same text");
////						options = 1;
////					}
////				} catch (StaleElementReferenceException e) {
////					// TODO: handle exception
////				}
////			}
////			try {
////				searchingElem = driver
////						.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
////			} catch (Exception e) {
////				System.out.println("Searching element not found.");
////				searchingElem = null;
////			}
//		}
//		System.out.println("Out of while loop. List contains " + optionss.size());
//		String optionName = optionss.get(0).getText();
//		System.out.println("optionName: " + optionName);
//		if (optionName.contains(text)) {
//			System.out.println("optionName name found, selecting the element");
//			driver.findElement(By
//					.xpath("(//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul/li/div)[1]"))
//					.click();
//		}
		this.inputForProductValidation.sendKeys(Keys.ENTER);
	}

	public void addCustomImage(String text) {
		wait(this.productcustomImage);

		this.productcustomImage.sendKeys(text);
	}

	public void typeProductValidationTextBox(String text) {
		wait(this.productValidationInputBox);
		// this.productValidationTextBox.click();
		this.productValidationInputBox.sendKeys(text);
		try {
			Thread.sleep(4 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.productValidationInputBox.sendKeys(Keys.ENTER);
	}

	public void clickRedemtionClearingButton() {
		wait(this.redemtionClearingButton);
		this.redemtionClearingButton.click();
	}

	public void clickSettingButton() {
		wait(this.settingButton);
		this.settingButton.click();
	}

	public void clickTargetingButton() {
		wait(this.targetingButton);
		this.targetingButton.click();
	}

	public void clickClearingMethodButton() {
		wait(this.clearingMethodButton);
		this.clearingMethodButton.click();
	}

	public void typeClearingMethodTypeBox(String text) {
		wait(this.clearingMethodTypeBox);
		this.clearingMethodTypeBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.clearingMethodTypeBox.sendKeys(Keys.ENTER);

	}

	public void clickBarCodeTypeButton() {
		wait(this.barCodeTypeButton);
		this.barCodeTypeButton.click();
	}

	public void typeBarCodeTypeTextBox(String text) {
		wait(this.barCodeTypeTextBox);
		this.barCodeTypeTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.barCodeTypeTextBox.sendKeys(Keys.ENTER);
	}

	public void typeBarCodeValue(String text) {
		wait(this.barCodeValue);
		this.barCodeValue.sendKeys(text);
	}

	public void typeStaticBarCodeValue(String text) {
		wait(this.staticBarCodeValue);
		this.staticBarCodeValue.sendKeys(text);
	}

	public void scrollToTop() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scrollTo(0,0)");
	}

	public void clickEnableExpireDateButton() {
		this.enableExpireDateButton.click();
	}

	public void clickDateFormatButton() {
		this.dateFormatButton.click();
	}

	public void typeDateFormatTextBox(String text) {
		wait(this.dateFormatTextBox);

		this.dateFormatTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dateFormatTextBox.sendKeys(Keys.ENTER);
	}

	public void clickCalanderButton() {
		this.calanderButton.click();
	}

	public void clickDateOption() {
		this.dateOption.click();
	}

	public void addImage(String text) {
		this.addImage.sendKeys(text);
	}

	public void clickSaveImageButton() {
		this.saveImageButton.click();
	}

	public void clickTemplateOptionButton() {
		this.templateOptionButton.click();
	}

	public void clickUSA_6in_Manufacturing_Coupon() {
		this.USA_6in_Manufacturing_Coupon.click();
	}

	public void clickGS1CouponTemplateButton() {
		this.gs1CouponTemplateButton.click();
	}

	public void clickTemplateSaveButton() {
		this.templateSaveButton.click();
	}

	public void typeQualiferLineInput(String text) {
		this.qualiferLineInput.sendKeys(text);
	}

	public void typesaveLineInput(String text) {
		this.saveLineInput.sendKeys(text);
	}

	public void addStaticImage(String text) {
		this.addStaticImage.sendKeys(text);
	}

	public void clickSaveStaticImage() {
		this.staticImageSave.click();
	}

	public void clickSaveButton() {
		this.saveButton.click();
	}

	public void clickPublishButton() {
		this.publishButton.click();
	}

	public void clickYesThisIsOkButton() {
		wait(this.yesThisIsOkButton);
		this.yesThisIsOkButton.click();
	}

	public String getPromotionId() {
		return this.promotionID.getText().split(" ")[this.promotionID.getText().split(" ").length - 1];
	}

	public void clickAddTiggerGroup() {
		wait(this.addTiggerGroup);
		this.addTiggerGroup.click();

	}

	public void clickAddCriteriaButton() {
		wait(this.addCriteriaButton);
		this.addCriteriaButton.click();
	}

	public void clickSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}

	public void typeSelectTypeTextBox(String text) {
		wait(this.selectTypeTextBox);
		this.selectTypeTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.selectTypeTextBox.sendKeys(Keys.ENTER);
	}

	public void clickMeasureButton() {
		wait(this.measureButton);
		this.measureButton.click();
	}

	public void typeMeasureTextBox(String text) {
		wait(this.measureTextBox);
		this.measureTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.measureTextBox.sendKeys(Keys.ENTER);
	}

	public void clickConditionButton() {
		wait(this.conditionButton);
		this.conditionButton.click();
	}

	public void typeConditionTextBox(String text) {
		wait(this.conditionTextBox);
		this.conditionTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.conditionTextBox.sendKeys(Keys.ENTER);
	}

	public void typeInputUnitTextBox(String text) {
		wait(this.inputUnitTextBox);
		this.inputUnitTextBox.sendKeys(text);
	}

	public void clickProductAttributeButton() {
		wait(this.productAttributeButton);
		this.productAttributeButton.click();
	}

	public void typeProductAttributeTextBox(String text) {
		wait(this.productAttributeTextBox);
		this.productAttributeTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.productAttributeTextBox.sendKeys(Keys.ENTER);
	}

	public void clickListSourceButton() {
		wait(this.listSourceButton);
		this.listSourceButton.click();
	}

	public void typeListSourceTextBox(String text) {
		wait(this.listSourceTextBox);
		this.listSourceTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.listSourceTextBox.sendKeys(Keys.ENTER);
	}

	public void typeInputForManager(String text) {
		wait(this.inputForManager);
		this.inputForManager.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputForManager.sendKeys(Keys.ENTER);
	}

	public void clickTriggerScopeButton() {
		wait(this.triggerScopeButton);
		this.triggerScopeButton.click();
	}

	public void typeInputTriggerScope(String text) {
		wait(this.inputTriggerScope);
		this.inputTriggerScope.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputTriggerScope.sendKeys(Keys.ENTER);
	}

	public void clickOperationButton() {
		this.operationButton.click();
	}

	public void clickTotalOrderMeasureButton() {
		this.totalOrderMeasureButton.click();
	}

	public void clickTotalOrderConditionButton() {
		this.totalOrderConditionButton.click();
	}

	public void typeTotalOrderValue(String text) {
		this.totalOrderValue.sendKeys(text);
	}

	public void clickTotalOrderTriggerScope() {
		this.totalOrderTriggerScope.click();
	}

	public void typeDepartmentCodes(String text) {
		this.inputForDepartmentCodes.sendKeys(text);
	}

	public void clickChooseCompanyPrefixButton() {
		this.chooseCompanyPrefixButton.click();
	}

	public void clickchooseClearingHouseButton() {
		this.chooseClearingHouseButton.click();
	}

	public void clickDepartmentTriggerScopeButton() {
		this.departmentTriggerScopeButton.click();
	}

	public void typeCompanyInputPrefixInput(String text) {
		wait(this.companyInputPrefixInput);
		this.companyInputPrefixInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.companyInputPrefixInput.sendKeys(Keys.ENTER);
	}

	public void typeClearingHouseInput(String text) {
		wait(this.clearingHouseInput);
		this.clearingHouseInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.clearingHouseInput.sendKeys(Keys.ENTER);
	}

}
