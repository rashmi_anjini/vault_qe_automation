package com.catalina.vault.pagefactory;

import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.catalina.vault.steps.SharedResource;
import com.catalina.vault.steps.Targeting;
import com.cucumber.listener.Reporter;
import com.gargoylesoftware.htmlunit.javascript.host.Console;



public class MainPage extends BaseClass{
	
	WebDriver driver;
	StopWatch pageLoad;
	SharedResource sharedResource;
	private static final Logger Log= LogManager.getLogger(MainPage.class);
	
	public MainPage(WebDriver driver){
		super(driver);
		this.driver =driver;
		this.sharedResource = sharedResource;
		PageFactory.initElements(this.driver, this);
		StopWatch pageLoad = new StopWatch();	
	}
	
	@FindBy(xpath="//*[@id='region']")
	WebElement region;
	
	@FindBy(xpath="//body[@class='loading']/div[@class='indicator']")
	WebElement Loading;
	
	@FindBy(xpath="//*[@id='site_nav']/div/div[2]/ul[1]/li[3]/ul/li")
	List<WebElement> countryList;
	
	
	public void clickOnRegion(){
		
		wait(this.region);
		this.region.click();
		}
	
	public boolean waitforLoading(String Element) throws InterruptedException{
		Thread.sleep(2 * 1000);
	int i= driver.findElements(By.xpath("//body[@class='loading']/div[@class='indicator']")).size();
	long start = System.currentTimeMillis();

	System.out.println("start  Time: " + start + "milliseconds");
	
			while(i > 0)
		{
				System.out.println(new Date().toString() +" Page Loading...");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("exception in loading"+e);
				e.printStackTrace();
			}    
				
			i =driver.findElements(By.xpath("//body[@class='loading']/div[@class='indicator']")).size();
			System.out.println(new Date().toString() + " found elemts "+i);
		 
		}
	System.out.println(new Date().toString() +" Page load completed");
	Log.info(new Date().toString() +" Page load completed");
	long stop = System.currentTimeMillis();
	
	long totalTime=stop-start;
	double seconds=((totalTime)/1000.0);
	double minutes=((totalTime)/1000.0)/60.0;
	System.out.println("stop  Time: " + stop + "milliseconds");
	System.out.println("Total Page Load Time in seconds: " + seconds + "seconds");
	System.out.println("Total Page Load Time for "+Element+" : " + minutes + "min");
	Log.info("Total Page Load Time for "+Element+" : " + minutes + "min");
	if (minutes >=5)
	{
		String timetaken ="Page load time  for "+Element+" is taking :" +minutes+"min but the threshold time configured is 5mins" ;
		Log.info("TimeTaken for Page to Load: " +timetaken);
		String Country=this.sharedResource.GetCountry();
		String PromotionName=this.sharedResource.GetPromotionName();
		String BuildName=this.sharedResource.getBuildName();
		com.catalina.slack.Example.slackMessage(timetaken,Country,PromotionName,BuildName);
		
	}
	return false;
	}
	
	public void selectCountry(String country){
		
		waitList(this.countryList);
		for(WebElement element : countryList){
			System.out.println("."+element.getText()+".");
			if(element.getText().equalsIgnoreCase(country)){
				
				element.click();
				break;
			}
		}
		
	}
	
	public boolean waitforofferCodeLease() throws InterruptedException{
		Thread.sleep(2 * 1000);
	int i= driver.findElements(By.xpath("//body[@class='loading modal-open']")).size();
			
		while(i > 0)
		{
			System.out.println(new Date().toString() +" Page Loading...");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("exception in loading"+e);
				e.printStackTrace();
			}
			Thread.sleep(500);
			i =driver.findElements(By.xpath("//body[@class='loading modal-open']")).size();
			System.out.println(new Date().toString() + " found elemts "+i);
		}				
		System.out.println(new Date().toString() +" Page load completed");
		return false;
	}

}