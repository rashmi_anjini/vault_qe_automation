package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdsListPage extends BaseClass {
	
	WebDriver driver;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/a")
	WebElement newButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[1]/a")
	WebElement adGallery;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[5]/button")
	WebElement editButton;
	
	@FindBy(id="table_search_txt")
	WebElement searchBox;
	
	@FindBy(id="table_search_btn")
	WebElement searchButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[9]/ul/li[4]/a")
	WebElement nextButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/a")
	WebElement firstAdGroupInList;
	
	@FindBy(xpath = "//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[3]/a")
	WebElement advertisementButton;
	
	@FindBy(xpath ="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[4]/a")
	WebElement couponButton;
	
	@FindBy(xpath = "//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[3]/ul/li[1]/a")
	WebElement bddName;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[3]/ul/li[2]/a")
	WebElement earnNPoints_GetNMorePointsButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[3]/ul/li[3]/a")
	WebElement buyN_for_dollor_yButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[3]/ul/li[4]/a")
	WebElement spendDollorX_GetNPointsButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[3]/ul/li[5]/a")
	WebElement mfdDiscountButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[3]/ul/li[6]/a")
	WebElement messageButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[3]/ul/li[7]/a")
	WebElement buyN_GetNPointButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[3]/ul/li[8]/a")
	WebElement testButton;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[4]/ul/li[1]/a")
	WebElement coupon_BuyN_SaveX_Dollors;
	
	@FindBy(xpath="//*[@id='adgroup_ads']/div[3]/div[1]/div[2]/ul/li[2]/ul/li[4]/ul/li[6]/a")
	WebElement GenericCouponAd;
	
	@FindBy(xpath="//a[@rv-route-platform-adgroups-edit='view.adgroup.id']")
	WebElement adgrouptab;
	
	
	public AdsListPage(WebDriver driver){
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickNewButton(){
		wait(this.newButton);
		this.newButton.click();
	}
	
	public void clickAdGallery(){
		wait(this.adGallery);
		this.adGallery.click();
	}
	
	
	public void clickEditButton(){
		wait(this.editButton);
		this.editButton.click();
	}
	
	public void typeSearchBox(String text){
		this.searchBox.sendKeys(text);
	}
	
	public void clickSearchButton(){
		this.searchButton.click();
	}
	
	public void clickNextButton(){
		this.nextButton.click();
	}
	
	public void clickAdvertisementButton(){
		wait(this.advertisementButton);
		this.advertisementButton.click();
	}
	
	
	public void clickCouponButton(){
		this.couponButton.click();
	}
	
	public void clickBddName(){
		this.bddName.click();
	}
	
	public void clickEarnNPoints_GetNMorePointsButton(){
		this.earnNPoints_GetNMorePointsButton.click();
	}
	
	public void clickBuyN_for_dollor_yButton(){
		this.buyN_for_dollor_yButton.click();
	}
	
	public void clickSpendDollorX_GetNPointsButton(){
		this.spendDollorX_GetNPointsButton.click();
	}
	
	public void clickMfdDiscountButton(){
		this.mfdDiscountButton.click();
	}
	
	public void clickMessageButton(){
		this.messageButton.click();
	}
	
	public void clickBuyN_GetNPointButton(){
		this.buyN_GetNPointButton.click();
	}

	public void clickTestButton(){
		this.testButton.click();
	}
	
	public void clickFirstAdGroupInList(){
		this.firstAdGroupInList.click();
	}
	
	public void clickCoupon_BuyN_SaveX_Dollors(){
		this.coupon_BuyN_SaveX_Dollors.click();
	}
	
	public void clickGenericCouponAd(){
		wait(this.GenericCouponAd);
		this.GenericCouponAd.click();
	}
	
	public void clickadgrouptab(){
		wait(this.adgrouptab);
		this.adgrouptab.click();
	}
}
