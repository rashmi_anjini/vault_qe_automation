package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManufacturerPaymentMethodPage extends BaseClass {

WebDriver driver;
	
	@FindBy(xpath = "//*[@id='targeting_triggers']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteriaButton;

	
	@FindBy(xpath="//a[span[text()='Select Type']]")
	WebElement selectTypeButton;
	
	@FindBy(xpath="/html/body/div[60]/div/input")
	WebElement selectTypeInput;
	
	@FindBy(xpath="(//a[span[text()='(trigger scope)']])[8]")
	WebElement triggerScopeButton;
	
	@FindBy(xpath="/html/body/div[61]/div/input")
	WebElement triggerScopeInput;
	
	@FindBy(xpath="(//*/div/ul/li/input)[35]")
	WebElement mainInput;
	
	public ManufacturerPaymentMethodPage(WebDriver driver){
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void clickAddCriteriaButton() {
		wait(this.addCriteriaButton);
		this.addCriteriaButton.click();
	}

	public void clickSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}
	
	public void clickTriggerScopeButton(){
		wait(this.triggerScopeButton);
		this.triggerScopeButton.click();
	}
	
	public void typeSelectTypeInput(String text) {
		wait(this.selectTypeInput);
		this.selectTypeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.selectTypeInput.sendKeys(Keys.ENTER);
	}


	public void typeInputDataBox(String text) {
		wait(this.mainInput);
		this.mainInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.mainInput.sendKeys(Keys.ENTER);
	}
	
	public void typeTriggerScopeInput(String text) {
		wait(this.triggerScopeInput);
		this.triggerScopeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.triggerScopeInput.sendKeys(Keys.ENTER);
	}

}
