package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManufacturerCatalinaRedemption extends BaseClass {

	WebDriver driver;
	
	@FindBy(xpath = "//*[@id='targeting_triggers']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteriaButton;
	
	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;
	
	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement selectTypeTextBox;
	
	@FindBy(xpath="(//a[span[text()='(scope)']])[1]")
	WebElement scopeButton;
	
	@FindBy(xpath="(//*/div/ul/li/input)[11]")
	WebElement inputAdDetails;
	
	@FindBy(xpath="(//a[span[text()='(operation)']])[1]")
	WebElement operationButton;
	
	@FindBy(xpath="(//a[span[text()='(trigger scope)']])[3]")
	WebElement triggerScopeButton;
	
	@FindBy(xpath="/html/body/div[38]/div/input")
	WebElement operationInput;
	
	@FindBy(xpath="/html/body/div[38]/div/input")
	WebElement inputTriggerScope;
	
	public ManufacturerCatalinaRedemption(WebDriver driver){
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void clickAddCriteriaButton() {
		wait(this.addCriteriaButton);
		this.addCriteriaButton.click();
	}

	public void clickScopeButton(){
		wait(this.scopeButton);
		this.scopeButton.click();
	}
	
	public void clickSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}
	
	public void clickOperationButton() {
		wait(this.operationButton);
		this.operationButton.click();
	}
	
	
	public void clickTriggerScopeButton() {
		wait(this.triggerScopeButton);
		this.triggerScopeButton.click();
	}
	
	
	public void typeSelectTypeTextBox(String text) {
		wait(this.selectTypeTextBox);
		this.selectTypeTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.selectTypeTextBox.sendKeys(Keys.ENTER);
	}
	
	public void typeInputAdDetails(String text) {
		wait(this.inputAdDetails);
		this.inputAdDetails.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputAdDetails.sendKeys(Keys.ENTER);
	}
	
	public void typeOperationInput(String text) {
		wait(this.operationInput);
		this.operationInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.operationInput.sendKeys(Keys.ENTER);
	}
	
	public void typeInputTriggerScope(String text) {
		wait(this.inputTriggerScope);
		this.inputTriggerScope.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputTriggerScope.sendKeys(Keys.ENTER);
	}
}
