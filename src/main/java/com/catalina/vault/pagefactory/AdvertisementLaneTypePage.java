package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvertisementLaneTypePage extends BaseClass {

	WebDriver driver;

	@FindBy(xpath = "//*[@id=\'targeting_triggers\']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteria;

	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;

	@FindBy(xpath = "/html/body/div[58]/div/input")
	WebElement selectTypeInput;

	@FindBy(xpath = "(//a[span[text()='(operation)']])[19]")
	WebElement operationButton;

	@FindBy(xpath = "/html/body/div[58]/div/input")
	WebElement operationInput;

	@FindBy(xpath = "(//*/div/ul/li/input)[41]")
	WebElement mainInput;

	public AdvertisementLaneTypePage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void clickOnAddCriteria() {
		wait(this.addCriteria);
		this.addCriteria.click();
	}

	public void clickSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}

	public void typeSelectTypeInput(String text) {
		wait(this.selectTypeInput);
		this.selectTypeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectTypeInput.sendKeys(Keys.ENTER);
	}

	public void clickOperationButton() {
		wait(this.operationButton);
		this.operationButton.click();
	}

	public void typeInputDataBox(String text) {
		wait(this.mainInput);
		this.mainInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.mainInput.sendKeys(Keys.ENTER);
	}

	public void typeOperationInput(String text) {
		wait(this.operationInput);
		this.operationInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.operationInput.sendKeys(Keys.ENTER);
	}

}
