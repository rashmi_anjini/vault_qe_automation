package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvertisementLoyaltyPointsPage extends BaseClass {

	WebDriver driver;

	@FindBy(xpath = "//*[@id=\'targeting_triggers\']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteria;

	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;

	@FindBy(xpath = "/html/body/div[59]/div/input")
	WebElement selectTypeInput;

	@FindBy(xpath = "(//a[span[text()='(action)']])[6]")
	WebElement actionButton;

	@FindBy(xpath = "/html/body/div[59]/div/input")
	WebElement actionInput;

	@FindBy(xpath = "(//a[span[text()='(operation)']])[15]")
	WebElement operationButton;

	@FindBy(xpath = "/html/body/div[59]/div/input")
	WebElement operationInput;

	@FindBy(xpath = "(//*[@id='trigger-type_1']/section[7]/span[4]/span[1]/span[2]/input[2])[2]")
	WebElement mainInput;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[16]")
	WebElement triggerScopeButton;

	@FindBy(xpath = "/html/body/div[59]/div/input")
	WebElement triggerScopeInput;

	public AdvertisementLoyaltyPointsPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void clickOnAddCriteria() {
		wait(this.addCriteria);
		this.addCriteria.click();
	}

	public void clickSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}

	public void typeSelectTypeInput(String text) {
		wait(this.selectTypeInput);
		this.selectTypeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectTypeInput.sendKeys(Keys.ENTER);
	}

	public void clickOnActionButton() {
		wait(this.actionButton);
		this.actionButton.click();
	}

	public void typeActionInput(String text) {
		wait(this.actionInput);
		this.actionInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.actionInput.sendKeys(Keys.ENTER);
	}

	public void clickOnOperationButton() {
		wait(this.operationButton);
		this.operationButton.click();
	}

	public void typeOperationInput(String text) {
		wait(this.operationInput);
		this.operationInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.operationInput.sendKeys(Keys.ENTER);
	}

	public void typeMainInput(String text) {
		wait(this.mainInput);
		this.mainInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.mainInput.sendKeys(Keys.ENTER);
	}

	public void clickOnTriggerScopeButton() {
		wait(this.triggerScopeButton);
		this.triggerScopeButton.click();
	}

	public void typeTriggerScopeInput(String text) {
		wait(this.triggerScopeInput);
		this.triggerScopeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.triggerScopeInput.sendKeys(Keys.ENTER);
	}

}
