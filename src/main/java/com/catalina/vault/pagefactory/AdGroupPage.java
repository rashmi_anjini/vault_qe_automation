package com.catalina.vault.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AdGroupPage extends BaseClass {

	WebDriver driver;

	@FindBy(xpath = "//*[@id='adgroup_settings']/form/fieldset[1]/div[1]/div/input")
	WebElement nameTextBox;
	
	
	@FindBy(xpath = "//div[@rv-hide='model.override | eq true']/a[text()='Click here']")
	WebElement distbutioncaplink;
	
		
	@FindBy(xpath = "//input[@rv-value='cap.count']")
	WebElement distbutioncapcount;
	
	@FindBy(xpath = "(//input[@rv-value='cap.count'])[2]")
	WebElement distbutioncapcount2;
	
	@FindBy(xpath = "//div[@rv-show='view.adgroup.billing | empty']/div")
	WebElement billingButton;
	
	@FindBy(xpath = "//div[@class='select2-container cap_level']")
	WebElement adgrouplevel;
	
	@FindBy(xpath = "(//div[@class='select2-container cap_level'])[2]")
	WebElement adgrouplevel2;
	
	@FindBy(xpath = "//div[@rv-show='model.override | eq true']/div/a[text()='+ add budget cap']")
	WebElement adddistributioncap;
	
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement billingTextBox;

	@FindBy(xpath = "//*[@id='s2id_autogen48']")
	WebElement channelsButton;
	
	@FindBy(xpath="//div[@rv-hide='model.frequency_caps | !empty']/a")
	WebElement frequncyClickHere;
	
	@FindBy(xpath="//div[@class='select2-container cap_time_period']/a")
	WebElement selectFrequncyDuration;
	
	@FindBy(xpath="//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement frequencyInput;

	@FindBy(xpath = "//*[@id='adgroup_settings']/form/fieldset[1]/div[4]/div/input")
	WebElement adGroupRankTextBox;

	@FindBy(xpath = "//*[@id='adgroup_settings']/form/fieldset[5]/a")
	WebElement saveButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[3]")
	WebElement adButton;

	@FindBy(xpath = "//span[@rv-hide='cap.multiple | eq true']/div")
	WebElement freqaddropdown;
	
	
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement freqaddropdowneditbox;
	
	@FindBy(xpath = "//div[@class='select2-container view_adgroup_delivery_method']/a/span")
	WebElement deliveryMethodDropdown;
	
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement deliveryMethodDropdownEditbox;
	
	public AdGroupPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void typeName(String text) throws InterruptedException {
		wait(this.nameTextBox);
		this.nameTextBox.click();
		Thread.sleep(2*1000);
		this.nameTextBox.sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
		this.nameTextBox.sendKeys(Keys.ENTER);
		
	}
	
	
	
	public void clickdistbutioncaplink() throws InterruptedException {
		wait(this.distbutioncaplink);
		this.distbutioncaplink.click();
			}
	
	
	public void typedistbutioncapcount(String text) throws InterruptedException {
		wait(this.distbutioncapcount);
		this.distbutioncapcount.click();
		this.distbutioncapcount.sendKeys(Keys.chord(Keys.CONTROL, "a"),text);
		}
	
	
	public void typedistbutioncapcount2(String text) throws InterruptedException {
		wait(this.distbutioncapcount2);
		this.distbutioncapcount2.click();
		this.distbutioncapcount2.sendKeys(Keys.chord(Keys.CONTROL, "a"),text);
		}
	
	public void typedistbutioncaplevel(String text) throws InterruptedException {
		wait(this.adgrouplevel);
		this.adgrouplevel.click();
		this.billingTextBox.click();
		this.billingTextBox.sendKeys(text);
		try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.billingTextBox.sendKeys(Keys.ENTER);
		
	}
	
	public void clickDeliveryMethodDropdown() throws InterruptedException
	{
		wait(this.deliveryMethodDropdown);
		this.deliveryMethodDropdown.click();
	}
	
    public void selectDeliveryMethod(String text) throws InterruptedException
    {
    	wait(this.deliveryMethodDropdownEditbox);
    	this.deliveryMethodDropdownEditbox.sendKeys(text);
    	try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.deliveryMethodDropdownEditbox.sendKeys(Keys.ENTER);
    }
	
	public void typeadgrouplevel2(String text) throws InterruptedException {
		wait(this.adgrouplevel2);
		this.adgrouplevel2.click();
		this.billingTextBox.click();
		this.billingTextBox.sendKeys(text);
		try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.billingTextBox.sendKeys(Keys.ENTER);
		
	}
	
	public void clickadddistributioncap() throws InterruptedException {
		wait(this.adddistributioncap);
		this.adddistributioncap.click();
	}
	
	
public void selectFrequncyCappingDuration(String text) throws InterruptedException {
		
		//JavascriptExecutor js = (JavascriptExecutor)driver;
       // js.executeScript("arguments[0].scrollIntoView();", this.frequncyClickHere);
	    wait(this.frequncyClickHere);
	    this.frequncyClickHere.click();
        wait(this.selectFrequncyDuration);
		this.selectFrequncyDuration.click();
		this.frequencyInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.frequencyInput.sendKeys(Keys.ENTER);
 
     
		
	}
	public void typeBilling(String text) {
		wait(this.billingButton);
		this.billingButton.click();
		wait(this.billingTextBox);
		this.billingTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.billingTextBox.sendKeys(Keys.ENTER);
	}

	public void clickChannel() {
		wait(this.channelsButton);
		this.channelsButton.click();
	}

	public void typeAdGroupRank(String text) {
		wait(this.adGroupRankTextBox);
		this.adGroupRankTextBox.sendKeys(text);
	}

	public void clickSaveButton() {
		this.saveButton.click();
	}

	public void clickAdButton() {
		this.adButton.click();
	}
	
	
	public void clickfreqaddropdown(String text) {
		wait(this.freqaddropdown);
		this.freqaddropdown.click();
		this.freqaddropdowneditbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.freqaddropdowneditbox.sendKeys(Keys.ENTER);
	}
	
	public void scrollToTop() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scrollTo(0,0)");
	}

}
