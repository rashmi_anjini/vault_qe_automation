package com.catalina.vault.pagefactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdPage extends BaseClass {

	WebDriver driver;
	NewDisplayNetworkCampaignPage camppage;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[4]/div[2]/div/div/div[1]/a")
	WebElement frequencyCappingClickHereButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[4]/div[3]/div/div/div[1]/a")
	WebElement distributedCappingClickHereButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[4]/div[3]/div/div/div[2]/div[1]/input")
	WebElement distributedCappingTextBox;

	@FindBy(xpath = "(//*[contains(text(), 'Add Validation Group')])[1]")
	WebElement addProductGroupButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[2]/div[2]/div/div/a")
	WebElement productValidationTextBox;

	@FindBy(xpath = "(//ul/li/input)[2]")
	WebElement productValidationInputBox;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[1]/a")
	WebElement settingButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[2]/a")
	WebElement redemtionClearingButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[3]/a")
	WebElement targetingButton;

	@FindBy(xpath = "//a[span[text()='Choose Clearing Method']]")
	WebElement clearingMethodButton;

	@FindBy(xpath = "/html/body/div[19]/div/input")
	WebElement clearingMethodTypeBox;

	@FindBy(xpath = "//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul")
	WebElement checkListSelected;
	
	@FindBy(xpath = "(//a[span[text()='select...']])[3]")
	WebElement barCodeTypeButton;
	
	@FindBy(xpath = "//*[@id='ad-save']")
	WebElement saveDraft;

	@FindBy(xpath = "/html/body/div[19]/div/input")
	WebElement barCodeTypeTextBox;
	
	@FindBy(xpath = "//a/span[contains(text(),'(select custom parameter group)')]")
	//"//*[@id='s2id_autogen274']/a/span")	
WebElement customParamGroup;

	@FindBy(xpath = "//*[@id='ad_redemption']/div/div/div/form/div[2]/div[5]/div[2]/div[2]/div/div[4]/div[1]/div/input")
	WebElement barCodeValue;

	@FindBy(xpath = "//*[@id='ad_redemption']/div/div/div/form/div[2]/div[5]/div[2]/div[2]/div/div[2]/div[1]/div/input")
	WebElement staticBarCodeValue;

	@FindBy(xpath = "//*[@id='ad_redemption']/div/div/div/form/div[2]/div[2]/div/div[1]/a")
	WebElement enableExpireDateButton;

	@FindBy(xpath = "(//a[span[text()='date formatting (default)']])[1]")
	WebElement dateFormatButton;

	@FindBy(xpath = "/html/body/div[19]/div/input")
	WebElement dateFormatTextBox;

	@FindBy(xpath = "//*[@id='ad_redemption']/div/div/div/form/div[2]/div[2]/div/div[2]/div[2]/button")
	WebElement calanderButton;

	@FindBy(xpath = "/html/body/div[12]/div[1]/div[2]/table/tbody/tr[6]/td[2]")
	WebElement dateOption;

	@FindBy(xpath = "//*[@id='static_image_upload_control']")
	WebElement addImage;
	
	@FindBy(xpath = "//span[text()='(optional custom parameters)']")
	WebElement customParametersDropDown;
	
	@FindBy(xpath = "//div[@rv-custom-parameters='$scope.ad']/div/div/div/a")
	WebElement customParameterEnable;
	
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement inputForCustomParam;
	

	// @FindBy(xpath="(//*[@id='mm_PRODUCT_SHOT']/div/div/div/div/div[3]/a)[5]")
	@FindBy(xpath = "//div[@aria-hidden='false']/div/div/div[@class='modal-footer']/a")
	WebElement saveImageButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[1]/div/div[1]/div/div/div/a")
	WebElement templateOptionButton;

	@FindBy(xpath = "//div[span[text()='USA - 6in Manufacturing Coupon']]/button")
	WebElement USA_6in_Manufacturing_Coupon;

	@FindBy(xpath = "//div[span[text()='USA - Manufacturing Coupon - GS1']]/button")
	WebElement gs1CouponTemplateButton;

	@FindBy(xpath = "//div[span[text()='Sainsburys Barcode and Deal Number']]/button")
	WebElement barcodeButton;

	@FindBy(xpath = "//div[span[text()='Sainsburys OCC Logo']]/button")
	WebElement logoButton;

	@FindBy(xpath = "//div[span[text()='Auth Code']]/button")
	WebElement authCodeButton;

	@FindBy(xpath = "//div[span[contains(text(),'Standard')]]/button")
	WebElement Sainsburytemplate;

	@FindBy(xpath = "//*[@id='template_browse_modal']/div/div/div[3]/button")
	WebElement templateSaveButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[3]/div[4]/div[1]/textarea")
	WebElement qualiferLineInput;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[3]/div[5]/div[1]/textarea")
	WebElement saveLineInput;

	@FindBy(xpath = "//*[@id='custom_image_upload_control']")
	WebElement addStaticImage;

	// @FindBy(xpath="(//*[@id='mm_STATIC_IMAGE']/div/div/div/div/div[3]/a)[5]")
	@FindBy(xpath = "//div[@aria-hidden='false']/div/div/div[@class='modal-footer']/a")
	WebElement staticImageSave;

	@FindBy(xpath = "//*[@id='ad-save-bottom']")
	WebElement saveButton;

	@FindBy(xpath = "//*[@id='ad-save-bottom']")
	WebElement EndButton;

	@FindBy(id = "ad-publish")
	WebElement publishButton;

	@FindBy(xpath = "//div[@id='global_confirm_dialog']/div/div/div/button[contains(text(),'Yes this is ok!')]")
	WebElement yesThisIsOkButton;

	@FindBy(xpath = "//*[@id='content']/div[1]/div[1]/h3/small")
	WebElement promotionID;

	@FindBy(xpath = "//div[@id='page-content-wrapper']")
	WebElement body;

	@FindBy(xpath = "//a[@rv-route-platform-adgroups-edit='view.adgroup.id']")
	WebElement adgrouptabclick;

	@FindBy(xpath = "//a[@rv-route-platform-campaigns-edit='view.campaign.id']")
	WebElement campaigntabclick;

	@FindBy(xpath = "//table[@class='table table-grid']/tbody/tr/td[@rv-data-text='column.value']")
	WebElement tableFirstrow;

	public AdPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void clickFrequencyCappingClickHereButton() {
		wait(this.frequencyCappingClickHereButton);
		this.frequencyCappingClickHereButton.click();
	}

	public void clickDistributedCappingClickHereButton() {
		wait(this.distributedCappingClickHereButton);
		this.distributedCappingClickHereButton.click();
	}

	public void clicktableFirstrow() {
		wait(this.tableFirstrow);
		this.tableFirstrow.click();
	}

	public void typeDistributedCappingTextBox(String text) {
		wait(this.distributedCappingTextBox);
		this.distributedCappingTextBox.clear();
		this.distributedCappingTextBox.sendKeys(text);
	}

	public void clickAddProductGroupButton() {
		wait(this.addProductGroupButton);
		this.addProductGroupButton.click();
	}

	public void typeProductValidationTextBox(String text) {
		wait(this.productValidationInputBox);
		// this.productValidationTextBox.click();
		this.productValidationInputBox.sendKeys(text);
		try {
			Thread.sleep(4 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.productValidationInputBox.sendKeys(Keys.ENTER);
	}

	public void clickRedemtionClearingButton() {
		wait(this.redemtionClearingButton);
		this.redemtionClearingButton.click();
	}

	public void clickSettingButton() {
		wait(this.settingButton);
		this.settingButton.click();
	}

	public void clickTargetingButton() {
		wait(this.targetingButton);
		this.targetingButton.click();
	}

	public void clickClearingMethodButton() {
		wait(this.clearingMethodButton);
		this.clearingMethodButton.click();
	}

	public void typeClearingMethodTypeBox(String text) {
		wait(this.clearingMethodTypeBox);
		this.clearingMethodTypeBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.clearingMethodTypeBox.sendKeys(Keys.ENTER);

	}

	public void clickBarCodeTypeButton() {
		wait(this.barCodeTypeButton);
		this.barCodeTypeButton.click();
	}

	public void typeBarCodeTypeTextBox(String text) {
		wait(this.barCodeTypeTextBox);
		this.barCodeTypeTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.barCodeTypeTextBox.sendKeys(Keys.ENTER);
	}

	public void typeBarCodeValue(String text) {
		wait(this.barCodeValue);
		this.barCodeValue.sendKeys(text);
	}

	public void typeStaticBarCodeValue(String text) {
		wait(this.staticBarCodeValue);
		this.staticBarCodeValue.sendKeys(text);
	}

	public void scrollToTop() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scrollTo(0,0)");
	}

	public void scrollTodown() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scrollBy(0,1000)");
	}
	
	public void clickSaveDraftButton() {
		wait(this.saveDraft);
		this.saveDraft.click();
	}

	public void clickEnableExpireDateButton() {
		this.enableExpireDateButton.click();
	}

	public void clickDateFormatButton() {
		this.dateFormatButton.click();
	}

	public void typeDateFormatTextBox(String text) {
		wait(this.dateFormatTextBox);

		this.dateFormatTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dateFormatTextBox.sendKeys(Keys.ENTER);
	}

	public void clickCalanderButton() {
		this.calanderButton.click();
	}

	public void clickDateOption() {
		this.dateOption.click();
	}

	public void addImage(String text) {
		this.addImage.sendKeys(text);
	}

	public void clickSaveImageButton() {
		this.saveImageButton.click();
	}

	public void clickTemplateOptionButton() {
		this.templateOptionButton.click();
	}

	public void clickUSA_6in_Manufacturing_Coupon() {
		this.USA_6in_Manufacturing_Coupon.click();
	}

	public void clickGS1CouponTemplateButton() {
		this.gs1CouponTemplateButton.click();
	}

	public void clickbarcodeButton() {
		this.barcodeButton.click();
	}

	public void clicklogoButton() {
		this.logoButton.click();
	}

	public void clickauthCodeButton() {
		this.authCodeButton.click();
	}

	public void clickSainsburytemplateButton() {
		this.Sainsburytemplate.click();
	}

	public void clickTemplateSaveButton() {
		this.templateSaveButton.click();
	}

	public void typeQualiferLineInput(String text) {
		this.qualiferLineInput.clear();
		this.qualiferLineInput.sendKeys(text);
	}

	public void clearsaveLineInput() {
		wait(this.saveLineInput);
		this.saveLineInput.clear();
	}

	public void typesaveLineInput(String text) {
		wait(this.saveLineInput);
		this.saveLineInput.sendKeys("A");
		this.saveLineInput.sendKeys(Keys.BACK_SPACE);
		this.saveLineInput.sendKeys(Keys.CONTROL + "a");
		this.saveLineInput.sendKeys(Keys.DELETE);
		this.saveLineInput.sendKeys(text);
	}

	public void addStaticImage(String text) {
		this.addStaticImage.sendKeys(text);
	}

	public void clickSaveStaticImage() {
		this.staticImageSave.click();
	}

	public void clickSaveButton() {
		// WebDriverWait wait= new WebDriverWait(driver,200);
		// wait.until(ExpectedConditions.elementToBeClickable(this.saveButton));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", this.saveButton);
		this.saveButton.click();
	}

	public void clickPublishButton() {
		wait(this.publishButton);
		this.publishButton.click();
	}

	public void clickYesThisIsOkButton() {
		wait(this.yesThisIsOkButton);
		this.yesThisIsOkButton.click();
	}

	public void clickadgrouptabclick() {
		wait(this.adgrouptabclick);
		this.adgrouptabclick.click();
	}

	public void clickcampaigntabclick() {
		wait(this.campaigntabclick);
		this.campaigntabclick.click();
	}

	public String getPromotionId() {
		return this.promotionID.getText().split(" ")[this.promotionID.getText().split(" ").length - 1];
	}

	public void clickbody() {
		wait(this.body);
		this.body.sendKeys(Keys.END);
	}
	
	public void clickOnCustomParameters() {
		wait(this.customParametersDropDown);
		this.customParametersDropDown.click();
	}
	
	public void enableCustomParameters() {
		wait(this.customParameterEnable);
		this.customParameterEnable.click();
	}
	
	public void typeinputForCustomParam(String text) throws InterruptedException {
		wait(this.inputForCustomParam);
		this.inputForCustomParam.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputForCustomParam.sendKeys(Keys.ARROW_DOWN);
		this.inputForCustomParam.sendKeys(Keys.ENTER);
	}
	
	public void selectCustomParamGroup(String text) throws InterruptedException {
		wait(this.customParamGroup);
		this.customParamGroup.click();
		this.inputForCustomParam.sendKeys(text);
		Thread.sleep(3*1000);
	this.inputForCustomParam.sendKeys(Keys.ENTER);

	}

}
