package com.catalina.vault.pagefactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseClass {

	WebDriverWait wait;
	
	long timeout=360;
	
	public BaseClass(WebDriver driver){
		wait = new WebDriverWait(driver, 300);
		
	}
	
	public void wait(WebElement element){
		try{
			
		wait.until(ExpectedConditions.visibilityOf(element));
		
		}catch(Exception e)
		{
		Log.debug("debug:" +element+"ERROR:"+e);
		}
	}
	
	public void waitandClick(WebElement element){
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public void waitList(List<WebElement> elements){
		wait.until(ExpectedConditions.visibilityOfAllElements(elements));
	}
	
	public void waitdropdown(WebElement element){
		wait.until(ExpectedConditions.elementToBeSelected(element));
		
	}
	
}


