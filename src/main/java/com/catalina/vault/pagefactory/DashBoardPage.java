package com.catalina.vault.pagefactory;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashBoardPage extends BaseClass{

	WebDriver driver;
	
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[1]")
	WebElement accounts;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[2]")
	WebElement creatives;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[3]")
	WebElement clients;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[4]")
	WebElement network;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[5]")
	WebElement campaign;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[6]")
	WebElement loyalty;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[7]")
	WebElement location;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[8]")
	WebElement places;
	
	@FindBy(xpath="//*[@id='parameter_files']/div/div/a/div/input")
	WebElement ImportNewFileButton;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[9]")
	WebElement audiences;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[10]")
	WebElement analytics;
	
	@FindBy(xpath="//*[@id='sidebar_targeting']/ul/li/div/a")
	WebElement analyticsScoringModel;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[11]")
	WebElement bulkContent;
	
	@FindBy(xpath="//*[@id='sidebar_batch_ingested_content']/ul/li/div/a")
	WebElement bulkContentPromotionReview;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[12]")
	WebElement operations;
	
	@FindBy(xpath="//*[@id='collapseTwo']/div/ul/li[13]")
	WebElement admin;
	
	@FindBy(xpath="//*[@id='accordion']/div/div/h4/a[contains(text(),'Shared Lists')]")
	WebElement sharedList;
	
	@FindBy(xpath="//*[@id='shared_lists']/div/ul/li[4]/div[1]/a")
	WebElement parameters;
	
	@FindBy(xpath="//*[@id='content']/div[1]/div[2]/div/button")
	WebElement uploadFile;
	
	@FindBy(xpath="//*[@id='content']/div[1]/div[2]/div/ul/li/a")
	WebElement CIDParameters;
	
	@FindBy(xpath="//*[@id=\"pin_files\"]/div[2]/div/a/div/input")
	WebElement importNewFileButton;
	
	@FindBy(xpath="//*[@id='content']/div[2]/div/div[2]/div/form/div[1]/div/input")
	WebElement nameTextBox;
	
	@FindBy(xpath="//*[@id='content']/div[2]/div/ul/li[2]/a")
	WebElement fileTab;
	
	@FindBy(xpath = "//td[text()='PROCESSING']")
	WebElement checkstatus;
	
	@FindBy(xpath = "//*[@id='site_nav']/div/div[2]/ul[2]/li/a")
	WebElement dashboard;
	
	
	@FindBy(xpath="//li/div/a[contains(text(),' PINs')]")
	WebElement PINs;
	
	public DashBoardPage(WebDriver driver){
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver,this);
	}
	public void clickPINs(){
		this.PINs.click();
	}
	
	
	public void clickAccounts(){
		this.accounts.click();
	}
	public void enterCustomParamFileName() throws IOException {
		wait(this.nameTextBox);
		this.nameTextBox.clear();
		String CustomParametersName = com.catalina.vault.utils.FileUtils.getCustomParameters();
		System.out.println(CustomParametersName);
		// String name = com.catalina.vault.utils.FileUtils.getAudienceFile(Country);
		this.nameTextBox.sendKeys(CustomParametersName);	
	}
	
	public void clickFileTab() {	
		this.fileTab.click();
	}
	
	public void clickImportNewFilePINs(String file) throws IOException, Exception {
		System.out.println(file);
		this.importNewFileButton.sendKeys(file);
		//Thread.sleep(2000); // driver Waiting for 2000 ms

	}
	
	public void clickCreatives(){
		this.accounts.click();
	}
	
	public void clickClients(){
		this.accounts.click();
	}
	
	
	public void clickNetwork(){
		this.accounts.click();
	}
	
	public void clickCampaign(){
		this.campaign.click();
	}
	
	
	public void clickLoyalty(){
		this.accounts.click();
	}
	
	public void clickLocation(){
		this.accounts.click();
	}
	
	public void clickSharedList(){
		wait(this.sharedList);
		this.sharedList.click();
	}
	
	public void clickParameters(){
		wait(this.parameters);
		this.parameters.click();
	}
	
	public void clickPlaces(){
		this.accounts.click();
	}
	
	public void clickUploadFile() {	
		this.uploadFile.click();
	}
	
	public void clickCIDParameters() {	
		this.CIDParameters.click();
	}
	public void clickAudiences(){
		wait(this.audiences);
		this.audiences.click();
	}
	
	
	public void clickAnalytics(){
		this.accounts.click();
	}
	
	public void clickAnalyticsScoringModel(){
		this.analyticsScoringModel.click();
	}
	
	
	public void clickBulkContent(){
		this.accounts.click();
	}
	
	public void clickBulkContentPromotionReview(){
		this.bulkContentPromotionReview.click();
	}
	
	public void clickOperations(){
		this.accounts.click();
	}
	
	public void clickAdmin(){
		this.accounts.click();
	}
	
	public void clickImportNewFileCustomButton(String file) throws IOException, Exception {
		//wait(this.ImportNewFileButton);
		this.ImportNewFileButton.sendKeys(file);
		//Thread.sleep(2000); // driver Waiting for 2000 ms

	}
	
	public void checkProcessingstatus() {
		wait(this.checkstatus);
		System.out.println("Status:"+this.checkstatus.getText());
		Assert.assertEquals("PROCESSING", this.checkstatus.getText());
			}
	
	public void clickDashboard(){
		this.dashboard.click();
	}
	
	
}
