package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SecondCriteria extends BaseClass {

	
	WebDriver driver;
	
	@FindBy(xpath="//*[@id='targeting_triggers']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteria;
	
	@FindBy(xpath="//a[span[text()='Select Type']]")
	WebElement selectType;
	
	@FindBy(xpath="/html/body/div[59]/div/input")
	WebElement input;
	
	@FindBy(xpath="(//a[span[text()='(operation)']])[14]")
	WebElement operation;
	
	@FindBy(xpath="(//*/div/ul/li/input)[34]")
	WebElement mainInput;
	
	public SecondCriteria(WebDriver driver){
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void clickAddCriteriaButton() {
		wait(this.addCriteria);
		this.addCriteria.click();
	}
	
	public void clickSelectTypeButton() {
		wait(this.selectType);
		this.selectType.click();
	}
	
	public void typeTextBox(String text) {
		wait(this.input);
		this.input.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.input.sendKeys(Keys.ENTER);
	}
	
	public void clickOperationButton() {
		this.operation.click();
	}
	
	public void typeMainInput(String text) {
		wait(this.mainInput);
		this.mainInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.mainInput.sendKeys(Keys.ENTER);
	}
}
