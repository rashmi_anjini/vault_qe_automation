package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NetworkPage {

	WebDriver driver;

	@FindBy(xpath = "//*[@id='content']/div[1]/div/div/div[1]/a")
	WebElement exportButton;

	@FindBy(xpath = "//*[@id='content']/div[1]/div/div/div[2]/a")
	WebElement newButton;

	@FindBy(xpath = "//*[@id='content']/div[1]/div/div/div[3]/button")
	WebElement importButton;

	@FindBy(xpath = "//*[@id='content']/div[1]/div/div/div[3]/ul/li/a")
	WebElement importCSVOption;

	public NetworkPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickExportButton() {
		this.exportButton.click();
	}

	public void clickNewButton() {
		this.newButton.click();
	}

	public void clickImportButton() {
		this.importButton.click();

	}

	public void clickImportCSVOption() {
		this.importCSVOption.click();
	}
}
