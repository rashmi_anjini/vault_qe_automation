package com.catalina.vault.pagefactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class TargetingPage extends BaseClass {

	WebDriver driver;
	NewDisplayNetworkCampaignPage camppage;
	
	

	@FindBy(xpath = "//*[@id='targeting_triggers']/div/div/div[1]/h4/a")
	WebElement addTiggerGroup;

	@FindBy(xpath = "//*[@id='targeting_triggers']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteriaButton;

	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;

	//@FindBy(xpath = "/html/body/div")
	//List<WebElement> divElements;	
	//int size = divElements.size();
	
	//@FindBy(xpath = "/html/body/div['(size)']/div/input")
	@FindBy(xpath = "//div[contains(@class, 'select2-drop-active')]/div/input")
	WebElement selectTypeTextBox;

	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/div[@class='select2-container trigger_purchase_measure']/a[span[text()='(measure)']]")
	WebElement totalspend;
	
	@FindBy(xpath = //"//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/div/a/span")
	"//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/div[@class='select2-container trigger_purchase_measure']/a[span[text()='(measure)']]")
	WebElement measureButton;

	
	@FindBy(xpath = "(//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/div/a/span)[2]")
			//"(//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/div[@class='select2-container trigger_purchase_measure']/a[span[text()='(measure)']])[2]")
	WebElement measureButtonaudienceSegment;

	//@FindBy(xpath = "/html/body/div[39]/div/input")
	@FindBy(xpath = "//div[contains(@class, 'select2-drop-active')]/div/input")
	WebElement measureTextBox;
	
	@FindBy(xpath ="//section[@rv-show='trigger.type | eq TriggerRetargetingEvent']/div/a[span[text()='(scope)']]")
	WebElement Scopebutton;
	
	@FindBy(xpath ="//section[@rv-show='trigger.type | eq TriggerRedeemEvent']/div/a[span[text()='(scope)']]")
	WebElement Scopebuttoncatalinaredem;
	
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement scopeTextBox;
	
	
	@FindBy(xpath = "//span[@rv-show='trigger.retargeting_scope | eq AD']/div/ul/li/input")
	WebElement adsTextBox;
	
				
	@FindBy(xpath = "(//span[@rv-show='trigger.redeem_scope | eq AD']/div/ul/li/input)")
    WebElement adsTextBoxcatalinaredem;
	
	@FindBy(xpath="//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul")
	WebElement checkBlSelected ;
	
	
	@FindBy(xpath ="//span[@rv-show='trigger.retargeting_scope | in CAMPAIGN,ADGROUP,AD']/div/a[span[text()='(action)']]")
	WebElement actionbutton;
	
	@FindBy(xpath ="//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement actiontextbox;
	
	@FindBy(xpath ="//span[@rv-show='trigger.event_type']/div/a[span[text()='(operation)']]")
	WebElement operationButton;
	
	@FindBy(xpath ="(//span[@rv-show='trigger.redeem_scope']/div/a[span[text()='(operation)']])")
	WebElement operationButtoncatalinaredem;
	
	@FindBy(xpath ="//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement operationtextbox;
	
	
	@FindBy(xpath ="//section[not(contains(@style,'display: none;')) and @rv-show='trigger.type | eq TriggerLaneType']/div[contains(@class,'select2-container select2-container-multi trigger_targeted_lane_types')]/ul/li/input")
	WebElement LaneTypeTextbox;
	
	@FindBy(xpath ="(//span[@rv-show='trigger.event_type']/section/section[@rv-show='trigger.scope | in User,Household']/div)[1]")
	WebElement timeperiodbutton;
	
	
	
	@FindBy(xpath ="//section[not(contains(@style,'display: none;')) and @rv-show='trigger.type | eq TriggerLoyaltyPointEvent']/div")
	WebElement actionbuttonLoyalty;
	
	
	@FindBy(xpath ="//section[not(contains(@style,'display: none;')) and @rv-show='trigger.type | eq TriggerLoyaltyPointEvent']/span[@rv-show='trigger.program']/div")
	WebElement actionbuttonLoyaltysecond;
	
	@FindBy(xpath ="//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement timeperiodtextbox;
	
	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/span[@rv-show='trigger.purchase_measure']/div[@class='select2-container trigger_value_op']/a[span[text()='(condition)']]")
	WebElement conditionButton;
	
	@FindBy(xpath ="//section[@rv-show='trigger.type | eq TriggerPaymentMethodEvent']/div/a[span[text()='(condition)']]")
	WebElement paymentmethodcondition;
	
	@FindBy(xpath ="(//section[@rv-show='trigger.type | eq TriggerPaymentMethodEvent']/div/a[span[text()='(condition)']])[2]")
	WebElement paymentmethodconditionsecondrow;
	
	@FindBy(xpath = "(//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/span[@rv-show='trigger.purchase_measure']/div[@class='select2-container trigger_value_op']/a[span[text()='(condition)']])[2]")
	WebElement conditionButtonaudienceSegment;
	
	//@FindBy(xpath = "/html/body/div[39]/div/input")
	@FindBy(xpath = "//div[contains(@class, 'select2-drop-active')]/div/input")
	WebElement conditionTextBox;

	@FindBy(xpath = "//*[@id='trigger-type_1']/section[1]/span[2]/span[1]/input[2]")
	WebElement inputUnitTextBox;
	
	
	
	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerCheckoutEvent']/span/span[@rv-show='trigger.value.op | in >=,==,between']/input[2]")
	WebElement inputUnitTextBoxTotalOrder;
	
	
	
	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerCheckoutEvent']/span/span[@rv-show='trigger.value.op | eq <=,between']/input[2]")
	WebElement inputUnitTextBoxTotalOrder2;
	
	@FindBy(xpath = "(//*[@id='trigger-type_1']/section[1]/span[2]/span[1]/input[2])[2]")
	WebElement inputUnitTextBoxsecondrow;
	
	@FindBy(xpath = "//*[@id='trigger-type_1']/section[1]/span[2]/span[2]/input[2]")
	WebElement inputUnitTextBox2;
	
	@FindBy(xpath ="(//section[@rv-show='trigger.type | eq TriggerPaymentMethodEvent']/span[@rv-show='trigger.value.op']/span[1]/input[2])[2]")
	WebElement paymentinputUnitTextBox;		
	
	@FindBy(xpath = "//span[@rv-show='trigger.event_type']/span/span[@rv-show='trigger.value.op | in >=,==,between']/input[not(contains(@style,'display: none;'))]")
	WebElement retargetinputUnitTextBox;
	
	@FindBy(xpath = "(//span[@rv-show='trigger.event_type']/section/div)[1]")
	WebElement retargettriggerscopebutton;
	
	@FindBy(xpath = "//section[not(contains(@style,'display: none;'))]/div/ul/li/input")
	WebElement InputTextBox;
	
	@FindBy(xpath ="(//*[@id='trigger-type_1']/section[1]/span[2]/span[@rv-show='trigger.value.op | in >=,==,between']/input[2])[2]")
	WebElement InputTextBoxconditionselected;
	
	@FindBy(xpath = "//section[@class='TriggerPurchaseEvent']/span/div/a[span[text()='(product attribute)']]")
	WebElement productAttributeButton;
	
	@FindBy(xpath = "(//section[@class='TriggerPurchaseEvent']/span/div[contains(@class,'select2-container trigger_attribute trigger_product_attribute')])[2]")
	WebElement productAttributeButtondeptid;
	
	@FindBy(xpath = "//section[@class='TriggerPurchaseEvent']/span/div[contains(@class,'select2-container trigger_attribute trigger_product_attribute')]")
	WebElement productAttributeButtondeptidfirstrow;
	
	//@FindBy(xpath = "/html/body/div[39]/div/input")
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement productAttributeTextBox;

	@FindBy(xpath = "//a[span[text()='(list source)']]")
	WebElement listSourceButton;
	
	@FindBy(xpath = "(//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/span[@rv-show='trigger.value.op']/div[@class='select2-container trigger_reference'])[2]")
	WebElement listSourceButtonsecondrow;
	
	//@FindBy(xpath = "/html/body/div[39]/div/input")
	@FindBy(xpath = "//div[contains(@class, 'select2-drop-active') and not(contains(@class,'select2-offscreen'))]/div/input")
	WebElement listSourceTextBox;
	
	@FindBy(xpath ="//span[@rv-show='trigger.product_attribute | eq Department']/span[(@rv-show='trigger.reference | eq manual') and (not(contains(@style,'display: none;')))]/textarea[@placeholder='Enter the department codes one line at a time...']")
	WebElement departmentidTextBox;
	
	@FindBy(xpath ="(//span[@rv-show='trigger.product_attribute | eq Department']/span[(@rv-show='trigger.reference | eq manual') and (not(contains(@style,'display: none;')))]/textarea[@placeholder='Enter the department codes one line at a time...'])")
	WebElement departmentidTextBoxsecondrow;
	
	
	@FindBy(xpath ="(//span[@rv-show='trigger.product_attribute | eq Department']/span[(@rv-show='trigger.reference | eq manual') and (not(contains(@style,'display: none;')))]/textarea[@placeholder='Enter the department codes one line at a time...'])[2]")
	WebElement departmentidTextBoxsecondrowtxtbox;
	
	@FindBy(xpath ="//div[@rv-show='trigger.reference | eq manual']/textarea[@id='target_list' and @placeholder='Enter in targets one line at a time...']")
	WebElement ManualListeditbox;
	
	@FindBy(xpath ="(//div[@rv-show='trigger.reference | eq manual']/textarea[@id='target_list' and @placeholder='Enter in targets one line at a time...'])[2]")
	WebElement ManualListsecondeditbox;
	
	//@FindBy(xpath = "(//*/div/ul/li/input)[8]")
	@FindBy(xpath = "(//span[@rv-show = 'trigger.reference | eq lmc']/div/ul/li/input)")
	WebElement inputForManager;
	
	@FindBy(xpath = "(//span[not(contains(style ,'display: none;')) and @rv-show='trigger.condition']/div)[2]")
	WebElement searchAudiences;
	
	@FindBy(xpath = "//span[not(contains(@style ,'display: none;')) and @rv-show='trigger.condition']/div")
	WebElement searchAudiencesfirstrow;
	
	@FindBy(xpath = "//span[not(contains(@style ,'display: none;')) and @rv-show='trigger.condition']/div/ul/li/input")
	WebElement inputForsearchAudiences;
	
	@FindBy(xpath="//div[contains(@class, 'select2-drop-active') and not(contains(@class,'select2-offscreen'))]/ul/li/div/span")
	WebElement inputManagerSelection;

	//@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[3]")
	@FindBy(xpath= "//span[@rv-hide='trigger.product_attribute | in ClassAndDepartmentCode,Department,FamilyCode,ItemCode']/section/div")
	WebElement triggerScopeButton;
	
	@FindBy(xpath= "(//span[@rv-hide='trigger.product_attribute | in ClassAndDepartmentCode,Department,FamilyCode,ItemCode']/section/div/a/span)[2]")
	WebElement triggerScopesecondButton;
	
	@FindBy(xpath="//span[@rv-show='trigger.regex' and (not(contains(@style,'display: none;')))]/section[@style='display:flex; align-items: flex-start']/div")
	WebElement triggerScopeButtondeptid;
	
	@FindBy(xpath= "//span[@rv-show='trigger.tender']/section/div/a/span[contains(text(),'(trigger scope)')]")
	WebElement secondtriggerScopeButton;
	
	@FindBy(xpath= "(//span[@rv-show='trigger.tender']/section/div/a/span[contains(text(),'(trigger scope)')])[2]")
	WebElement paymenttriggerScopeButton;
	
	
	@FindBy(xpath = "(//section[@class='TriggerPaymentMethodEvent']//a[span[text()='(condition)']])")
	WebElement paymentConditionButton;
	
	@FindBy(xpath = "(//div[contains(@class, 'select2-drop-active')]/div/input)[2]")
	WebElement paymentconditionTextBox;
	
	@FindBy(xpath = "(//*[@id='trigger-type_1']/section[10]/span[1]/span[1]/input[2])[2]")
	WebElement paymentinputsecondrow;
	
	
	@FindBy(xpath= "//span[@rv-show='trigger.value.op']/section/div/a/span[contains(text(),'(trigger scope)')]")
	WebElement loyaltytriggerScopeButton;
	
	@FindBy(xpath= "(//span[@rv-show='trigger.regex' and (not(contains(@style,'display: none;')))]/section[@style='display:flex; align-items: flex-start']/div)[2]")
	WebElement triggerScopeButtondeptsecondrow;

	//@FindBy(xpath = "/html/body/div[40]/div/input")
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement inputTriggerScope;
	
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement inputconditiontext;
	

	@FindBy(xpath = "(//span[@rv-show='trigger.redeem_scope']/div/a[span[text()='(trigger scope)']])")
	WebElement catalinaredemTriggerScope;

	//"//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/div/a/span")
	@FindBy(xpath ="//section[@rv-show='trigger.type | eq TriggerCheckoutEvent']/div/a/span[contains(text(),'total spend')]")
			//"//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/div/a/span")
			//"//section[@rv-show='trigger.type | eq TriggerCheckoutEvent']/div/a[span[text()='(measure)']]")
	WebElement totalOrderMeasureButton;
	
	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerCheckoutEvent']/div/a/span[contains(text(),'total spend')]")
			//"(//section[@rv-show='trigger.type | eq TriggerCheckoutEvent']/div/a[span[text()='(measure)']])[2]")
	WebElement totalOrderMeasureButtonaudiencesegment;

	@FindBy(xpath = "//section[@class='TriggerCheckoutEvent']//a[span[text()='(condition)']]")
	WebElement totalOrderConditionButton;
	
	@FindBy(xpath = "(//section[@class='TriggerCheckoutEvent']//a[span[text()='(condition)']])[2]")
	WebElement totalOrderConditionButtonaudiencesegment;

	@FindBy(xpath = "(//*[@id='trigger-type_1']/section[4]/span/span[1]/input[2])[2]")
	WebElement totalOrderValue;
	
	@FindBy(xpath="//*[@id='trigger-type_1']/section[4]/span/span[2]/input[2]")
	WebElement totalOrderValue2;
	
	@FindBy(xpath="//*[@id='trigger-type_1']/section[4]/span/span[1]/input[2]")
	WebElement totalOrderValue3;
	
	@FindBy(xpath="(//*[@id='trigger-type_1']/section[4]/span/span[1]/input[2])[3]")
	WebElement totalOrderValuethird;

	@FindBy(xpath = "//section[@class='TriggerCheckoutEvent']//a[span[text()='(trigger scope)']]")
	WebElement totalOrderTriggerScope;
	
	@FindBy(xpath = "(//section[@class='TriggerCheckoutEvent']//a[span[text()='(trigger scope)']])[2]")
	WebElement totalOrderTriggerScopeaudienceseg;
	
	@FindBy(xpath ="//section[@rv-show='trigger.type | eq TriggerCheckoutEvent']/section/div/a[span[text()='(trigger scope)']]")
	WebElement totalOrderTriggerScopethird;

	@FindBy(xpath = "//span[@rv-show='trigger.product_attribute | eq Department']/span/textarea")
	WebElement inputForDepartmentCodes;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[1]")
	WebElement departmentTriggerScopeButton;
	
	@FindBy(xpath = "(//*/div/ul/li/input)[15]")
	WebElement inputForAudienceSegment;
	
	//@FindBy(xpath = "//section[@class='TriggerUserSegment']/div/a[contains(@tabindex, '-1')]")
	@FindBy(xpath = "(//section[@class='TriggerUserSegment']/div)[2]")
	WebElement operationbutton ;
	
	@FindBy(xpath = "//section[not(contains(@style,'display: none;')) and @rv-show='trigger.type | eq TriggerLaneType']/div")
	WebElement operationlanebutton ;
	
	
	@FindBy(xpath = "//span[@rv-show='trigger.value.op']/span/span[@rv-hide='trigger.measure | eq ProgressSpend']/input[not(contains(@style,'display: none;'))]")
	WebElement inputforloyaltypoints ;
	
	@FindBy(xpath = "//section[not(contains(@style,'display: none;')) and @rv-show='trigger.type | eq TriggerLoyaltyPointEvent']/span[@rv-show='trigger.measure | in Balance,Redeemed,Earned']/div")
	WebElement operationloyaltybutton ;
	
	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerRetailerTriggeredEvent']/span/span/a")
	WebElement generateMCLUIDButton;
	
	@FindBy(xpath = "//section[@class='TriggerUserSegment']/div")
	WebElement operationbuttonfirstrow;
	
	//@FindBy(xpath = "//div[contains(@class,'select2-drop select2-with-searchbox select2-drop-active select2-drop-above')]/div/input")
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement inputForoperationbutton ;
	
	@FindBy(xpath = "//section[not(contains(@style,'display: none;'))]/span/textarea[@id='wallet_list']")
	WebElement externalidField ;
	
	@FindBy(xpath = "(//section[@class='TriggerUserIdentifierEvent']/div)[2]")
	WebElement userIdentifieroperationbutton ;
	
	//@FindBy(xpath ="//div[(@class='select2-drop select2-with-searchbox select2-drop-above select2-drop-active') or (@class='select2-drop select2-with-searchbox select2-drop-active')]/div/input")
	@FindBy(xpath ="//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement inputuserIdentifieroperation ;
	
	@FindBy(xpath ="//div[@rv-show='inventory.is_manual_based']/textarea[@rv-textbox-rows='inventory.rows']")
	WebElement ProductManualListeditbox;

	public TargetingPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.camppage=new NewDisplayNetworkCampaignPage(driver);
	}
	
	public void clickGenerateMCLUIDButton(){
		wait(this.generateMCLUIDButton);
		this.generateMCLUIDButton.click();
	}
	public boolean checkkAddTiggerGroup() {
		
		//[not(contains(@style,'display: none;'))]
		int i=driver.findElements(By.xpath("//*[@id='targeting_triggers']/div/div/div[1][@style='display: none;']/h4/a")).size();
		if(i>0 )
		{
			
			return false;
		}
		
		return true;
	}
	
	
	public void clickAddTiggerGroup() {
		wait(this.addTiggerGroup);
		this.addTiggerGroup.click();
	}
		
	
	
	public void clickAddCriteriaButton() {
		wait(this.addCriteriaButton);
		this.addCriteriaButton.click();
	}
	
	public void paymentInput(String text) {
		this.paymentinputsecondrow.sendKeys(text);
	}
	
	public void typepaymentConditionTextBox(String text) {
		wait(this.paymentconditionTextBox);
		this.paymentconditionTextBox.sendKeys(text);
		try {
			Thread.sleep(1 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.paymentconditionTextBox.sendKeys(Keys.ENTER);
	}
	
	public void clickPaymentConditionButton() {
		this.paymentConditionButton.click();
	}
	

	
	public void clickManualListeditbox(String text) {
		wait(this.ManualListeditbox);
		this.ManualListeditbox.click();
		this.ManualListeditbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.ManualListeditbox.sendKeys(Keys.ENTER);
	}
	
	public void clickProductManualListeditbox(String text) {
		wait(this.ProductManualListeditbox);
		this.ProductManualListeditbox.click();
		this.ProductManualListeditbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.ProductManualListeditbox.sendKeys(Keys.ENTER);
	}
	
	
	public void clickinputforloyaltypoints(String text) {
		wait(this.inputforloyaltypoints);
		this.inputforloyaltypoints.click();
		this.inputforloyaltypoints.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputforloyaltypoints.sendKeys(Keys.ENTER);
	}
	
	public void clickManualListsecondeditbox(String text) {
		wait(this.ManualListsecondeditbox);
		this.ManualListsecondeditbox.click();
		this.ManualListsecondeditbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.ManualListsecondeditbox.sendKeys(Keys.ENTER);
	}
	
	public void clickSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}
	
	public void clickManagerInputSelection(){
		wait(this.inputManagerSelection);
		this.inputManagerSelection.click();
	}

	public void typeSelectTypeTextBox(String text) {
		wait(this.selectTypeTextBox);
		this.selectTypeTextBox.sendKeys(text);
		try {
			Thread.sleep(1 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.selectTypeTextBox.sendKeys(Keys.ENTER);
	}
	
	public void typeadsTextBox(String text) throws InterruptedException {
		wait(this.adsTextBox);
		this.adsTextBox.clear();
		this.adsTextBox.sendKeys(text);
		this.adsTextBox.sendKeys(Keys.ARROW_DOWN);
		camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		WebElement dropdown = this.checkBlSelected;
		
		List<WebElement> optionss = dropdown.findElements(By.tagName("li"));
		int count=0;
		    while((optionss.size() > 1)&&count<25)
		    {
		    	
		    	Thread.sleep(200);
		    	optionss = dropdown.findElements(By.tagName("li"));
		    	System.out.println("size"+optionss.size());
		    	count=count+1;
		    }
		    String optionName = optionss.get(0).getText();
		    System.out.println("optionName: "+optionName); 
		    if(optionName.contains(text)){
		    	System.out.println("optionName name found, selecting the element"); 
		       // optionss.get(0).click();
		    	driver.findElement(By.xpath("(//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul/li/div)[1]")).click();
		    }	
	}
	public void typeadsTextBoxcatalinaredem(String text) throws InterruptedException {
		wait(this.adsTextBoxcatalinaredem);
		this.adsTextBoxcatalinaredem.click();
		try{
		this.adsTextBoxcatalinaredem.sendKeys(text);
		this.adsTextBoxcatalinaredem.sendKeys(Keys.ARROW_DOWN);
		camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		
		WebElement dropdown = this.checkBlSelected;
		
		List<WebElement> optionss = dropdown.findElements(By.tagName("li"));
		int count=0;
		    while(optionss.size() > 1 && count<25)
		    {
		    	Thread.sleep(200);
		    	optionss = dropdown.findElements(By.tagName("li"));
		    	System.out.println("size: "+optionss.size());
		    }
		    String optionName = optionss.get(0).getText();
		    System.out.println("optionName: "+optionName); 
		    if(optionName.contains(text)){
		    	System.out.println("optionName name found, selecting the element"); 
		       // optionss.get(0).click();
		    	driver.findElement(By.xpath("(//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul/li/div)[1]")).click();
		    }
	  
	
	}catch(Exception e)
	{
		Log.debug(e);
	}
	}
	
	public void typeInputTextBox(String text) {
		wait(this.InputTextBox);
		this.InputTextBox.sendKeys(text);
		try {
			Thread.sleep(5 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.InputTextBox.sendKeys(Keys.ENTER);
	}
	public void typeretargetinputUnitTextBox(String text) {
		wait(this.retargetinputUnitTextBox);
		this.retargetinputUnitTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.retargetinputUnitTextBox.sendKeys(Keys.ENTER);
	}
	
	public void typedepartmentidTextBox(String text) {
		wait(this.departmentidTextBox);
		departmentidTextBox.click();
		this.departmentidTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.departmentidTextBox.sendKeys(Keys.ENTER);
	}
	
	public void typedepartmentidTextBoxsecondrow(String text) {
		wait(this.departmentidTextBoxsecondrow);
		departmentidTextBoxsecondrow.click();
		this.departmentidTextBoxsecondrow.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.departmentidTextBoxsecondrow.sendKeys(Keys.ENTER);
	}
	public void typedepartmentidTextBoxsecondrowtxtbox(String text) {
		wait(this.departmentidTextBoxsecondrowtxtbox);
		departmentidTextBoxsecondrowtxtbox.click();
		this.departmentidTextBoxsecondrowtxtbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.departmentidTextBoxsecondrowtxtbox.sendKeys(Keys.ENTER);
	}
	
	public void typeAudienceSegmentInput(String text) {
		wait(this.inputForAudienceSegment);
		this.inputForAudienceSegment.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputForAudienceSegment.sendKeys(Keys.ENTER);
	}
	
	public void list(){
		//wait(this.ProductManualListeditbox);
		//this.ProductManualListeditbox.click();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementByclass('form-control').setAttribute('value', '12345')");
	}
	
	public void typeIPUPC(String text) {
		System.out.println("BLNumber : " + text);
		wait(this.ProductManualListeditbox);
		this.ProductManualListeditbox.click();
//		this.ProductManualListeditbox.sendKeys(text);
//		try {
//			Thread.sleep(2 * 1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		this.ProductManualListeditbox.sendKeys(Keys.ENTER);
	}

	
	
	public void clickoperationbutton() {
		wait(this.operationbutton);
		System.out.println("click operation");
		this.operationbutton.click();
	}
	
	
	
	public void clickoperationlanebutton() {
		wait(this.operationlanebutton);
		this.operationlanebutton.click();
	}
	
	public void clickoperationloyaltybutton() {
		wait(this.operationloyaltybutton);
		this.operationloyaltybutton.click();
	}
	
	public void clickoperationButtoncatalinaredem() {
		wait(this.operationButtoncatalinaredem);
				this.operationButtoncatalinaredem.click();
	}
	
	public void clickoperationbuttonfirstrow() {
		wait(this.operationbuttonfirstrow);
		this.operationbuttonfirstrow.click();
	}
	
	
	public void clickoperationButtonRetarget() {
		wait(this.operationButton);
		this.operationButton.click();
	}
	public void typeinputForoperationbutton(String text) {
		wait(this.inputForoperationbutton);
		this.inputForoperationbutton.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputForoperationbutton.sendKeys(Keys.ENTER);
	}
	
	public void typeoperationtextbox(String text) {
		wait(this.operationtextbox);
		this.operationtextbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.operationtextbox.sendKeys(Keys.ENTER);
	}
	
	public void typeLaneTypeTextbox(String text) {
		wait(this.LaneTypeTextbox);
		this.LaneTypeTextbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.LaneTypeTextbox.sendKeys(Keys.ENTER);
	}
	public void clicktimeperiodbutton() {
		wait(this.timeperiodbutton);
		this.timeperiodbutton.click();
	}
	public void typetimeperiodtextbox(String text) {
		wait(this.timeperiodtextbox);
		this.timeperiodtextbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.timeperiodtextbox.sendKeys(Keys.ENTER);
	}
	
	public void clickexternalidField(String text) throws InterruptedException {
		wait(this.externalidField);
		this.externalidField.click();
		Thread.sleep(2*1000);
		this.externalidField.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.externalidField.sendKeys(Keys.ENTER);
	}
	
	
	public void clicksearchAudiences() {
		wait(this.searchAudiences);
		this.searchAudiences.click();
	}
	public void clicksearchAudiencesfirstrow() {
		wait(this.searchAudiencesfirstrow);
		this.searchAudiencesfirstrow.click();
	}
	
	
	
	public void typeinputForsearchAudiences(String text) throws InterruptedException {
		wait(this.inputForsearchAudiences);
		this.inputForsearchAudiences.sendKeys(text);
		
		WebElement dropdown = this.checkBlSelected;
		
		List<WebElement> optionss = dropdown.findElements(By.tagName("li"));
		 WebElement searchingElem = null;
		 try
		 {
			 searchingElem = driver.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
		 }
		 catch(Exception e)
		 {
	   			System.out.println("Searching element not found.");
	   			
		 }
		int options = optionss.size();
		int count=0;
		while(options>1 || (options == 1 && searchingElem != null)&&count<10)
    	{
			count = count+1;
			Thread.sleep(200);
   			optionss = dropdown.findElements(By.tagName("li"));
   			options = optionss.size();
   			try
	   		 {
   				searchingElem = driver.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
	   		 }
	   		 catch(Exception e)
	   		 {
	   			System.out.println("Searching element not found.");
	   			searchingElem = null;
	   		 }
   			if(searchingElem != null)
   			{
   				System.out.println("Waiting for Searching element to clear.");
	   			camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
   				System.out.println("Done waiting for Searching element to clear.");
   				searchingElem = null;
   	   			optionss = dropdown.findElements(By.tagName("li"));
   	   			options = optionss.size();
   			}
		  	System.out.println("Dropdown list size is "+options);
    			 for(WebElement elem : optionss)
    			 {
    				 try {
    					 if(!elem.getText().contains(text)){
        					 System.out.println("Dropdown list contains text other than " + text);
        					 options = optionss.size();
        					 break;
        				 }
        				 else
        				 {
        					 System.out.println("Dropdown list contains same text");
        					 options = 1;
        				 }
					} catch (StaleElementReferenceException e) {
						// TODO: handle exception
					}
    			 }
    			 try
    	   		 {
       				searchingElem = driver.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
    	   		 }
    	   		 catch(Exception e)
    	   		 {
    	   			System.out.println("Searching element not found.");
    	   			searchingElem = null;
    	   		 }
		 }
		System.out.println("Out of while loop. List contains "+ optionss.size());
		    String optionName = optionss.get(0).getText();
		    System.out.println("optionName: "+optionName); 
		    if(optionName.contains(text)){
		    	System.out.println("optionName name found, selecting the element");
		      driver.findElement(By.xpath("(//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul/li/div)[1]")).click();
		    }
    	}
	
	public void clickTotalSpendButton() {
		wait(this.totalspend);
		this.totalspend.click();
	}
	
	public void clickMeasureButton() {
		wait(this.measureButton);
		this.measureButton.click();
	}
	public void clickmeasureButtonaudienceSegment() {
		wait(this.measureButtonaudienceSegment);
		this.measureButtonaudienceSegment.click();
	}
	
	public void clickScopebutton() {
		wait(this.Scopebutton);
		this.Scopebutton.click();
	}
	public void clickScopebuttoncatalinaredem() {
		wait(this.Scopebuttoncatalinaredem);
		this.Scopebuttoncatalinaredem.click();
	}
	

	public void typeMeasureTextBox(String text) {
		wait(this.measureTextBox);
		this.measureTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.measureTextBox.sendKeys(Keys.ENTER);
	}
	public void typescopeTextBox(String text) {
		wait(this.scopeTextBox);
		this.scopeTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.scopeTextBox.sendKeys(Keys.ENTER);
	}
	public void clickactionbutton() {
		wait(this.actionbutton);
		this.actionbutton.click();
	}
	
	public void clickactionbuttonLoyalty() {
		wait(this.actionbuttonLoyalty);
		this.actionbuttonLoyalty.click();
	}
	
	public void clickactionbuttonLoyaltysecond() {
		wait(this.actionbuttonLoyaltysecond);
		this.actionbuttonLoyaltysecond.click();
	}
	
	public void typeactiontextbox(String text) {
		wait(this.actiontextbox);
		this.actiontextbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.actiontextbox.sendKeys(Keys.ENTER);
	}
	public void clickConditionButton() {
		wait(this.conditionButton);
		this.conditionButton.click();
	}
	
	
	public void clickpaymentmethodcondition() {
		wait(this.paymentmethodcondition);
		this.paymentmethodcondition.click();
	}
	
	
	
	public void clickpaymentmethodconditionsecondrow() {
		wait(this.paymentmethodconditionsecondrow);
		this.paymentmethodconditionsecondrow.click();
	}
	
	public void clickconditionButtonaudienceSegment() {
		wait(this.conditionButtonaudienceSegment);
		this.conditionButtonaudienceSegment.click();
	}
	
	public void typeConditionTextBox(String text) {
		wait(this.conditionTextBox);
		this.conditionTextBox.sendKeys(text);
		try {
			Thread.sleep(1 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.conditionTextBox.sendKeys(Keys.ENTER);
	}

	public boolean checktypeInputUnitTextBox() {
		
		//[not(contains(@style,'display: none;'))]
		int i=driver.findElements(By.xpath("//*[@id='trigger-type_1']/section[1]/span[2]/span[1]/input[2]")).size();
		if(i>0 )
		{
			
			return true;
		}
		
		return false;
	}
	public void typeInputUnitTextBox(String text) {
		wait(this.inputUnitTextBox);
		
		this.inputUnitTextBox.sendKeys(text);
	}
	
	public void typeinputUnitTextBoxTotalOrder(String text) {
		wait(this.inputUnitTextBoxTotalOrder);
		inputUnitTextBoxTotalOrder.click();
		this.inputUnitTextBoxTotalOrder.sendKeys(text);
	}
	
	public void typeinputinputUnitTextBoxTotalOrder2(String text) {
		wait(this.inputUnitTextBoxTotalOrder2);
		inputUnitTextBoxTotalOrder2.click();
		this.inputUnitTextBoxTotalOrder2.sendKeys(text);
	}
	
	public void typeinputUnitTextBoxsecondrow(String text) {
		wait(this.inputUnitTextBoxsecondrow);
		this.inputUnitTextBoxsecondrow.sendKeys(text);
	}
	
   public boolean checktypelistsourceTextBox() {
		
		//[not(contains(@style,'display: none;'))]
		int i=driver.findElements(By.xpath("//a[span[text()='(list source)']]")).size();
		if(i>0 )
		{
			
			return true;
		}
		
		return false;
	}
   
   public void clicklistSourceButtonsecondrow() {
		wait(this.listSourceButtonsecondrow);
		this.listSourceButtonsecondrow.click();
	}
   
	
	public void typeInputTextBoxconditionselected(String text) {
		wait(this.InputTextBoxconditionselected);
		this.InputTextBoxconditionselected.sendKeys(text);
	}
	
	public void typeInputUnitTextBox2(String text) {
		wait(this.inputUnitTextBox2);
		this.inputUnitTextBox2.sendKeys(text);
	}
	
	public void typepaymentinputUnitTextBox(String text) {
		wait(this.paymentinputUnitTextBox);
		this.paymentinputUnitTextBox.sendKeys(text);
	}
	
	public void clickProductAttributeButton() {
		wait(this.productAttributeButton);
		this.productAttributeButton.click();
	}
	public void clickproductAttributeButtondeptid() {
		wait(this.productAttributeButtondeptid);
		this.productAttributeButtondeptid.click();
	}
	public void clickproductAttributeButtondeptidfirstrow() {
		wait(this.productAttributeButtondeptidfirstrow);
		this.productAttributeButtondeptidfirstrow.click();
	}
	
	
	public void typeProductAttributeTextBox(String text) {
		wait(this.productAttributeTextBox);
		this.productAttributeTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.productAttributeTextBox.sendKeys(Keys.ENTER);
	}

	public void clickListSourceButton() {
		wait(this.listSourceButton);
		this.listSourceButton.click();
	}

	public void typeListSourceTextBox(String text) {
		wait(this.listSourceTextBox);
		this.listSourceTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.listSourceTextBox.sendKeys(Keys.ENTER);
	}

	public void typeInputForManager(String text) {
		wait(this.inputForManager);
		//this.inputForManager.click();
		this.inputForManager.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		
		} catch (InterruptedException e) {
			// TODO Auto-gen
			e.printStackTrace();
		}
		this.inputForManager.sendKeys(Keys.ENTER);
	}
	
	public void clickTriggerScopeButton() {
		wait(this.triggerScopeButton);
		this.triggerScopeButton.click();
	}
	public void clickretargettriggerscopebutton() {
		wait(this.retargettriggerscopebutton);
		this.retargettriggerscopebutton.click();
	}
	
	public void clicktriggerScopeButtondeptid() {
		wait(this.triggerScopeButtondeptid);
		this.triggerScopeButtondeptid.click();
	}
	public void clicktriggerScopeButtondeptsecondrow() {
		wait(this.triggerScopeButtondeptsecondrow);
		this.triggerScopeButtondeptsecondrow.click();
	}
	
	public void clicktriggerScopesecondButton() {
		wait(this.triggerScopesecondButton);
		this.triggerScopesecondButton.click();
	}
	public void clicktotalOrderTriggerScopethird() {
		wait(this.totalOrderTriggerScopethird);
		this.totalOrderTriggerScopethird.click();
	}
	
	public void clicktotalOrderTriggerScopeaudienceseg() {
		wait(this.totalOrderTriggerScopeaudienceseg);
		this.totalOrderTriggerScopeaudienceseg.click();
	}
	
	
	public void clicksecondtriggerScopeButton(){
		wait(this.secondtriggerScopeButton);
		this.secondtriggerScopeButton.click();
	}
	public void clickpaymenttriggerScopeButton(){
		wait(this.paymenttriggerScopeButton);
		this.paymenttriggerScopeButton.click();
	}
	
	
	public void clickloyaltytriggerScopeButton(){
		wait(this.loyaltytriggerScopeButton);
		this.loyaltytriggerScopeButton.click();
	}
	public void clickcatalinaredemTriggerScope(){
		wait(this.catalinaredemTriggerScope);
		this.catalinaredemTriggerScope.click();
	}
	

	public void typeInputTriggerScope(String text) throws InterruptedException {
		wait(this.inputTriggerScope);
		this.inputTriggerScope.sendKeys(text);
		//Thread.sleep(2*1000);
	try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputTriggerScope.sendKeys(Keys.ENTER);
	}
	
	public void typeinputconditiontext(String text) throws InterruptedException {
		wait(this.inputconditiontext);
		this.inputconditiontext.sendKeys(text);
		//Thread.sleep(2*1000);
	try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputconditiontext.sendKeys(Keys.ENTER);
	}
	
	

	public void clickOperationButton() {
		this.operationbutton.click();
	}

	public void clickTotalOrderMeasureButton() {
		this.totalOrderMeasureButton.click();
	}
	public void clicktotalOrderMeasureButtonaudiencesegment() {
		this.totalOrderMeasureButtonaudiencesegment.click();
	}
	
	public void clickTotalOrderConditionButton() {
		this.totalOrderConditionButton.click();
	}
	public void clicktotalOrderConditionButtonaudiencesegment() {
		this.totalOrderConditionButtonaudiencesegment.click();
	}
	
	public void typeTotalOrderValue(String text) {
		this.totalOrderValue.sendKeys(text);
	}
	
public boolean checktypeordervalue() {
		
		//[not(contains(@style,'display: none;'))]
		int i=driver.findElements(By.xpath("(//*[@id='trigger-type_1']/section[4]/span/span[1]/input[2])[2]")).size();
		if(i>0 )
		{
			
			return false;
		}
		
		return true;
	}
	
	public void typeTotalOrderValue2(String text) {
		this.totalOrderValue2.sendKeys(text);
	}
	
	public void typeTotalOrderValue3(String text) {
		this.totalOrderValue3.sendKeys(text);
	}
	public void typetotalOrderValuethird(String text) {
		this.totalOrderValuethird.sendKeys(text);
	}
	

	public void clickTotalOrderTriggerScope() {
		this.totalOrderTriggerScope.click();
	}
	
public boolean checktriggerscope() {
		
		//[not(contains(@style,'display: none;'))]
		int i=driver.findElements(By.xpath("//section[@class='TriggerCheckoutEvent']//a[span[text()='(trigger scope)']]")).size();
		if(i>0 )
		{
			
			return true;
		}
		
		return false;
	}

	public void typeDepartmentCodes(String text) {
		this.inputForDepartmentCodes.sendKeys(text);
	}
	
	public void clickDepartmentTriggerScopeButton(){
		this.departmentTriggerScopeButton.click();
	}
	
	public void clickuserIdentifieroperationbutton() {
		wait(this.userIdentifieroperationbutton);
		this.userIdentifieroperationbutton.click();
	}
	
	public void typeinputuserIdentifieroperation(String text) throws InterruptedException {
		wait(this.inputuserIdentifieroperation);
		this.inputuserIdentifieroperation.sendKeys(text);
		Thread.sleep(2*1000);
	try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputuserIdentifieroperation.sendKeys(Keys.ENTER);
	}
	
	
}
