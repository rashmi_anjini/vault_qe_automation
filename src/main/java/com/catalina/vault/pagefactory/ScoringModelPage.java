package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ScoringModelPage {

	WebDriver driver;

	@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div[2]/a")
	WebElement importButton;

	@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div[2]/ul/li/a")
	WebElement importh2oScoringModel;

	@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div[4]/button")
	WebElement editButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/div[1]/div[4]/ul/li[1]/a")
	WebElement publishButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/div[1]/div[4]/ul/li[2]/a")
	WebElement copyButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/div[1]/div[4]/ul/li[10]/a")
	WebElement deleteButton;

	@FindBy(id = "table_search_txt")
	WebElement searchBox;

	@FindBy(id="table_search_btn")
	WebElement searchButton;

	@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div[8]/ul/li[4]/a")
	WebElement nextButton;

	public ScoringModelPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickImportButton() {
		this.importButton.click();
	}

	public void clickImportH2OScoringModel() {
		this.importh2oScoringModel.click();
	}

	public void clickEditButton() {
		this.editButton.click();
	}

	public void clickPublishButton() {
		this.publishButton.click();
	}

	public void clickCopyButton() {
		this.copyButton.click();
	}

	public void clickDeleteButton() {
		this.deleteButton.click();
	}

	public void typeSearchBox(String text) {
		this.searchBox.sendKeys(text);
	}

	public void clickSearchButton() {
		this.searchButton.click();
	}

	public void clickNextButton() {
		this.nextButton.click();
	}
}
