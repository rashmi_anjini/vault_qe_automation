package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LocationsPage {

	WebDriver driver;
	
	@FindBy(xpath="//*[@id='content']/div[1]/div/div/div/a")
	WebElement newButton;
	
	public LocationsPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickNewButton(){
		this.newButton.click();
	}
	
}
