package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdvertisementItemPurchasePage extends BaseClass {

	WebDriver driver;

	@FindBy(xpath = "//*[@id=\'targeting_triggers\']/div/div/div[1]/h4/a")
	WebElement addTriggerGroup;

	@FindBy(xpath = "//*[@id=\'targeting_triggers\']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteria;

	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;

	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement selectTypeInput;

	@FindBy(xpath = "//a[span[text()='(measure)']]")
	WebElement measureButton;

	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement selectMeasureInput;

	@FindBy(xpath = "//a[span[text()='(condition)']]")
	WebElement conditionButton;

	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement selectConditionInput;

	@FindBy(xpath = "//*[@id='trigger-type_1']/section[1]/span[2]/span[1]/input[2]")
	WebElement triggerConditionInput;

	@FindBy(xpath = "//a[span[text()='(product attribute)']]")
	WebElement productAttributeButton;

	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement selectProductAttributeInput;

	@FindBy(xpath = "//a[span[text()='(list source)']]")
	WebElement listSourceButton;

	@FindBy(xpath = "/html/body/div[37]/div/input")
	WebElement selectListSourceInput;

	@FindBy(xpath = "(//*/div/ul/li/input)[6]")
	WebElement listManagerGroups;

	@FindBy(xpath = "(//*[@id='target_list'])[1]")
	WebElement targetListInput;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[2]")
	WebElement triggerScopeButon;

	@FindBy(xpath = "/html/body/div[37]/div/input") 
	WebElement inputTriggerScopeTextBoxLaneType;

	@FindBy(xpath = "/html/body/div[38]/div/input")
	WebElement inputTriggerScopeTextBox;

	public AdvertisementItemPurchasePage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void clickOnAddTriggerGroup() {
		wait(this.addTriggerGroup);
		this.addTriggerGroup.click();
	}

	public void clickOnAddCriteria() {
		wait(this.addCriteria);
		this.addCriteria.click();
	}

	public void clickOnSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}

	public void typeSelectTypeInput(String text) {
		wait(this.selectTypeInput);
		this.selectTypeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectTypeInput.sendKeys(Keys.ENTER);
	}

	public void clickOnMeasureButton() {
		wait(this.measureButton);
		this.measureButton.click();
	}

	public void typeMeasureInput(String text) {
		wait(this.selectMeasureInput);
		this.selectMeasureInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectMeasureInput.sendKeys(Keys.ENTER);
	}

	public void clickOnConditionButton() {
		wait(this.conditionButton);
		this.conditionButton.click();
	}

	public void typeConditionInput(String text) {
		wait(this.selectConditionInput);
		this.selectConditionInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectConditionInput.sendKeys(Keys.ENTER);
	}

	public void typeTriggerConditionInput(String text) {
		wait(this.triggerConditionInput);
		this.triggerConditionInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.triggerConditionInput.sendKeys(Keys.ENTER);
	}

	public void clickOnProductAttributeButton() {
		wait(this.productAttributeButton);
		this.productAttributeButton.click();
	}

	public void typeProductAttributeInput(String text) {
		wait(this.selectProductAttributeInput);
		this.selectProductAttributeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectProductAttributeInput.sendKeys(Keys.ENTER);
	}

	public void clickOnListSourceButton() {
		wait(this.listSourceButton);
		this.listSourceButton.click();
	}

	public void typeListSourceInput(String text) {
		wait(this.selectListSourceInput);
		this.selectListSourceInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectListSourceInput.sendKeys(Keys.ENTER);
	}

	public void typeListManagerGroupsInput(String text) {
		wait(this.listManagerGroups);
		this.listManagerGroups.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.listManagerGroups.sendKeys(Keys.ENTER);
	}

	public void typeTargetListInput(String text) {
		wait(this.targetListInput);
		this.targetListInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.targetListInput.sendKeys(Keys.ENTER);
	}

	public void clickOnTriggerScopeButton() {
		wait(this.triggerScopeButon);
		this.triggerScopeButon.click();
	}

	public void typeTriggerScopeInput(String text) {
		wait(this.inputTriggerScopeTextBox);
		this.inputTriggerScopeTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.inputTriggerScopeTextBox.sendKeys(Keys.ENTER);
	}

	public void typeTriggerScopeInputLaneType(String text) {
		wait(this.inputTriggerScopeTextBoxLaneType);
		this.inputTriggerScopeTextBoxLaneType.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.inputTriggerScopeTextBoxLaneType.sendKeys(Keys.ENTER);
	}

}
