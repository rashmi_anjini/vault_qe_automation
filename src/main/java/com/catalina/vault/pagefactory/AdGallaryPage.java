package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdGallaryPage extends BaseClass {

	WebDriver driver;

	public AdGallaryPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	@FindBy(xpath = "//a[contains(@href,'723c52011f4c')]/ancestor::div[@class='footer']/button")
	WebElement indesign3CouponAddToCart;
	

	@FindBy(xpath = "//a[contains(@href,'f57c75964736')]/ancestor::div[@class='footer']/button")
	WebElement indesign1CouponAddToCart;
	
	@FindBy(xpath = "//a[contains(@href,'8f0f374aac1c')]/ancestor::div[@class='footer']/button")
	WebElement indesign2CouponAddToCart;
	
	@FindBy(xpath = "//a[contains(@href,'a816ad4968ad')]/ancestor::div[@class='footer']/button")
	WebElement indesign4CouponAddToCart;

	@FindBy(xpath = "//*[@id='content']/div[3]/div[2]/div[342]/div/div[2]/button[1]")
	WebElement retailCouponAddToCart;

	@FindBy(xpath = "//*[@id='content']/div[3]/div[2]/div[371]/div/div[2]/button[1]/span[1]")
	WebElement manufacturerCouponAddToCart;

	@FindBy(xpath = "//*[@id='content']/div[3]/div[1]/nav/div/div[2]/div[2]/ul/li/a")
	WebElement nextButton;

	public void addRetailCouponTocart() {
		wait(this.retailCouponAddToCart);
		this.retailCouponAddToCart.click();
	}

	public void addManufacturerCouponTocart() {
		wait(this.manufacturerCouponAddToCart);
		this.manufacturerCouponAddToCart.click();
	}

	public void clickOnNext() {
		wait(this.nextButton);
		this.nextButton.click();
	}
	
	public void addIndesign3CouponTocart() {
		wait(this.indesign3CouponAddToCart);
		this.indesign3CouponAddToCart.click();
	}
	
	public void addIndesign2CouponTocart() {
		wait(this.indesign2CouponAddToCart);
		this.indesign2CouponAddToCart.click();
	}
	
	
	public void addIndesign1CouponTocart() {
		wait(this.indesign1CouponAddToCart);
		this.indesign1CouponAddToCart.click();
	}
	
	public void addIndesign4CouponTocart() {
		wait(this.indesign4CouponAddToCart);
		this.indesign4CouponAddToCart.click();
	}
}
