package com.catalina.vault.pagefactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.function.Function;

import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;

public class RetailerCouponPage extends BaseClass {
	WebDriver driver;
	NewDisplayNetworkCampaignPage camppage;
	public static String bl="";

	public RetailerCouponPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
		this.camppage = new NewDisplayNetworkCampaignPage(driver);
	}

	@FindBy(xpath = "//div[label='USA - 6in Manufacturing Coupon']/parent::*/div/button")

	WebElement manufacturerCouponUSAddToCart;

	@FindBy(xpath = "//input[@rv-checked='$scope.ad.blind']")
	WebElement blindCheckbox;

	@FindBy(xpath = "//input[@rv-checked='$scope.ad.experience.advertised']")
	WebElement advertisedFlag;

	@FindBy(xpath = "//div[@rv-show='$scope.ad.products.legal_restrictions.nbic_use | eq true']//input")
	WebElement nbicInput;

	@FindBy(xpath = "//div[label='USA - 6in Retail Coupon22']/parent::*/div/button")

	WebElement retailCouponUSAddToCart;

	@FindBy(xpath = "//div[label='Coupon 9cm DATE VARIABLE']/parent::*/div/button")
	WebElement retailCouponAddToCart;

	@FindBy(xpath = "//div[contains(label,'Standard')]/parent::*/div/button")
	WebElement ukCouponAddToCart;
	
	@FindBy(xpath="//textarea[@elastic='X €']")
	WebElement saveLineInputOffercode;
	
	@FindBy(xpath="//div[@data-section='required_amount']/textarea")
	WebElement requiredPurchaseAmount;

	@FindBy(xpath = "//div[contains(label,'ITA_0003_C_Print 90mm')]/parent::*/div/button")
	WebElement ItalyCouponAddToCart;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[1]/h5[1]")
	WebElement pageName;

	@FindBy(xpath = "//*[@id='content']/div[3]/div[1]/nav/div/div[2]/div[2]/ul/li/a")
	WebElement nextButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[4]/div[2]/div/div/div[1]/a")
	WebElement frequencyCappingClickHereButton;

	@FindBy(xpath = "//div[@rv-show='model.frequency_caps | !empty' and (not(contains(@stlye,'margin: 0px; display: none;')))]/div/input")
	WebElement frequencyCappingClickTextBox;

	@FindBy(xpath = "//div[@class='checkbox']/label/input[@type='checkbox' and @rv-checked='$scope.ad.experience.unlimited']")
	WebElement unlimitedChecbox;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[4]/div[3]/div/div/div[1]/a")
	WebElement distributedCappingClickHereButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[4]/div[3]/div/div/div[2]/div[1]/input")
	WebElement distributedCappingTextBox;
	
	
	@FindBy(xpath = "(//input[@rv-value='cap.count' and @type='number'])[2]")
	WebElement seconddistributedCappingTextBox;
	
	
	@FindBy(xpath = "//div[@class='select2-container cap_level']")
	WebElement levelsdropdown;
	
	
	@FindBy(xpath = "(//div[@class='select2-container cap_level'])[2]")
	WebElement levelsdropdown2;
	
	
	@FindBy(xpath = "//div[@class='select2-container cap_time_period']")
	WebElement caplevelsdropdown;
	
	@FindBy(xpath = "//div[@class='select2-container cap_time_window']/a[@class='select2-choice']")
	WebElement captimewindow;
	
	@FindBy(xpath="//div[@rv-barcode='$view.ad']/div[@rv-show='model.type']/div/div/div/a/span")
	WebElement staticbarcode;
	
	@FindBy(xpath = "//div[contains(@class,'select2-container cap_week_group')]/a[@class='select2-choice']")
	WebElement capweekgroup;
	
	@FindBy(xpath = "(//div[@class='select2-container cap_time_period'])[2]")
	WebElement caplevelsdropdown2;
	
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement dropdownTextBox;
	
	@FindBy(xpath = "//a[contains(text(),'add budget cap')]")
	WebElement adddistcapping;

	@FindBy(xpath = "//a[text()='Settings']")
	WebElement settingstab;

	// @FindBy(xpath = "(//*[contains(text(), 'Add Validation Group')])[1]")
	@FindBy(xpath = "(//div[@class='col-sm-10']//a[contains(.,'Add Validation Group')])[1]")
	WebElement addProductGroupButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[2]/div[2]/div[2]/div/div/a")
	WebElement productValidationTextBox;

	@FindBy(xpath = "(//ul/li/input)[3]")
	WebElement productValidationInputBox;

	@FindBy(xpath = "(//div/ul/li/input)[1]")
	WebElement channels;

	@FindBy(xpath = "//input[@rv-value='$scope.ad.rank']")
	WebElement defaultRankInput;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[1]/a")
	WebElement settingButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[2]/a")
	WebElement redemtionClearingButton;

	@FindBy(xpath = "//div[contains(@class,'indicator')]")
	WebElement indicatorElement;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/ul/li[3]/a")
	WebElement targetingButton;

	@FindBy(xpath = "//*[@id=\"content\"]/div[2]/div/ul/li[3]/a")
	WebElement digitalsetupButton;
	
	@FindBy(xpath = "//a[span[text()='Choose Clearing Method']]")
	WebElement clearingMethodButton;

	// @FindBy(xpath = "/html/body/div[17]/div/input")
	@FindBy(xpath = "//div[contains(@class, 'select2-drop-active')]/div/input")
	WebElement clearingMethodTypeBox;

	@FindBy(xpath = "//div[@data-section='promoted_legal_restrictions']//div//input[@rv-checked='$scope.ad.experience.competitive']")
	WebElement competitiveCheckbox;

	@FindBy(xpath = "//div[@data-section='promoted_legal_restrictions']//div//input[@rv-checked='$scope.ad.products.legal_restrictions.sensitive_use']")
	WebElement sensitiveCheckbox;

	@FindBy(xpath = "//div[@rv-show='$scope.funding.type | eq Manufacturer,Cooperative,Digital']/div/div/div")
	WebElement companyprefixfield;

	//changed xpath on 17/10/2019 by rashmi
	@FindBy(xpath = "//div[@class='col-sm-10']/div/a[span[text()='select...']]")
			//"(//a[@tabindex='-1'][span[text()='select...']])")
	WebElement barCodeTypeButton;

	// @FindBy(xpath = "/html/body/div[17]/div/input")
	@FindBy(xpath = "//div[contains(@class, 'select2-drop-active')]/div/input")
	WebElement barCodeTypeTextBox;

	@FindBy(xpath = "//*[@id='ad_redemption']/div/div/div/form/div[2]/div[5]/div[2]/div[2]/div/div[4]/div[1]/div/input")
	WebElement barCodeValue;

	@FindBy(xpath = "//button[text()='Ok']")
	WebElement alertAccept;

	@FindBy(xpath = "//div[@rv-hide='model.type | eq databarexpandedstacked,gs1northamericancoupon']/div/div[contains(@class,'select2-container model_format_type')]")
	WebElement barCodeFormat;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement companyprefixTextBox;

	@FindBy(xpath = "//div[@rv-show='$scope.funding.type | eq Manufacturer,Cooperative,Digital' and @class='form-group']/div/div")
	WebElement clearinghouse;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement clearinghouseTextBox;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement barCodeFormatTextBox;
	
	@FindBy(xpath="//div[@class='select2-container view_barcode__selected']/a/span")
	WebElement selectSymbol;
	

	@FindBy(xpath = "//div[@class='select2-container view_barcode__selected']")
	WebElement barCodevalue;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement barCodevalueTextBox;

	@FindBy(xpath = "//a[text()='Add Symbol']")
	WebElement addSymbol;
	
	@FindBy(xpath = "//div[@rv-ad-pins='variable.ref']/div/div/a/span")
	WebElement addpinDropdown;

	@FindBy(xpath="//a[@rv-on-click='view.commit_component']")
	WebElement barcodeValueSaveButton;
	
	@FindBy(xpath="//a[contains(text(),'Enable Offer Codes')]")
	WebElement enableOfferCode;
	
	@FindBy(xpath="//input[@value='GENERATED']")
	WebElement generatedRadioButton;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement inputForPins;

	@FindBy(xpath = "//div[@class='input-group']/input[@placeholder='static barcode value' and @rv-value='model.format.value | upca']")
	WebElement USABarCodeValue;

	@FindBy(xpath = "//span[@rv-on-click='view.barcode.focus']/input[@rv-show='element.type | eq text']")
	WebElement inputTextbox;

	@FindBy(xpath = "//div[@rv-hide='view.barcode.edit.type | eq gs1_identifier']/div/input[@type='number']")
	WebElement symbolropdown;

	@FindBy(xpath = "//div[@rv-show='view.barcode.edit']/a[text()='Save']")
	WebElement savebuttonbarcode;

	@FindBy(xpath = "//div[@class='input-group']/input[@placeholder='static barcode value' and @rv-value='model.format.value | ean13']")
	WebElement staticBarCodeValue;

	@FindBy(xpath = "//div[@class='input-group']/input[@placeholder='static barcode value' and @rv-value='model.format.value | upca']")
	WebElement staticBarCodeValueupca;

	@FindBy(xpath = "//div[@class='input-group']/input[@placeholder='GS1 barcode value' and @rv-value='model.format.value']")
	WebElement gs1barcodevalue;

	@FindBy(xpath = "//*[@id=\"ad_redemption\"]/div/div/div/form/div[1]/div/div[1]/a")
	WebElement enableExpireDateButton;

	@FindBy(xpath = "//*[@id='radio_reward_rolling_expiration_date']")
	WebElement rollingexpirationradioButton;

	@FindBy(xpath = "(//a[span[text()='date formatting (default)']])[1]")
	WebElement dateFormatButton;
	
	@FindBy(xpath="(//input[@rv-hide='element.type | eq text,value_code,currency_code'])[2]")
	WebElement offercodevalue;
	
	@FindBy(xpath="(//input[@rv-show='element.type | eq value_code,currency_code'])[4]")
	WebElement valuecodevalue;
	
	@FindBy(xpath="//input[@rv-value='view.barcode.edit.pad']")
	WebElement symbolLengthTextBox;
	
	@FindBy(xpath="//div[@rv-hide='view.barcode.edit.type | eq gs1_identifier']/div/input")
	WebElement symbolLength;

	// @FindBy(xpath = "/html/body/div[17]/div/input")
	@FindBy(xpath = "//div[contains(@class, 'select2-drop-active')]/div/input")
	WebElement dateFormatTextBox;

	@FindBy(xpath = "//*[@id=\"ad_redemption\"]/div/div/div/form/div[1]/div/div[2]/div[2]/button/span")
	WebElement calanderButton;

	@FindBy(xpath = "//*[@id='start-date-schedule']")
	WebElement calanderstartButton;
	
	@FindBy(xpath="//a[contains(text(),'Offer Code Management')]")
	WebElement offerCodeManagement;
	
	
	@FindBy(xpath="//div[label='BOOK BA Trafic Automne']/parent::*/div/button")
	WebElement dynamicOfferCodeAddToCart;

	@FindBy(xpath = "//*[@id='stop-date-schedule']")
	WebElement calanderstopButton;

	@FindBy(xpath = "(//div[@class='calendar left single']/div/table/thead/tr[1]/th[@class='prev available']/i)[1]")
	WebElement calanderleftButton;

	@FindBy(xpath = "//div[@data-section='promoted_legal_restrictions']//div//input[@rv-checked='$scope.ad.products.legal_restrictions.nbic_use']")
	WebElement nbicCheckbox;

	@FindBy(xpath = "(//div[@class='calendar left single']/div/table/thead/tr[1]/th[@class='next available']/i)[1]")
	WebElement calanderrightButton;

	@FindBy(xpath = "//div[@class='calendar left single']/div/table/thead/tr[1]/th[@class='next available']/i")
	WebElement calanderrightButtonsecond;
	
	@FindBy(xpath = "(//div[@class='calendar left single']/div/table/thead/tr[1]/th[@class='next available']/i)[2]")
	WebElement secondcalanderrightButtonsecond;

	@FindBy(xpath = "(//div[@class='calendar left single']/div/table/thead/tr[1]/th[@class='next available']/i)[1]")
	WebElement calanderrightenddateButton;

	@FindBy(xpath = "(//div[@class='calendar left single']/div/table/thead/tr[1]/th[@class='next available']/i)[2]")
	WebElement calanderrightenddateButtonfortodaydate;

	@FindBy(xpath = "//div[@class='calendar-table']/table/tbody/tr[6]/td[5]")
	WebElement dateOption;

	@FindBy(xpath = "//*[@id='static_image_upload_control']")
	WebElement addImage;

	@FindBy(xpath = "//div[@rv-show='variable.type | eq image,product_shot' and (not(contains(@style,'display: none;')))]/div[@rv-show='variable.id | eq product_shot_url']/a[2]/div/input")
	WebElement productShot;

	@FindBy(xpath = "//a[span[text()='search billing products']]")
	WebElement billingProducts;

	@FindBy(xpath = "//div/a[span[text()='search billing targeting methods']]")
	WebElement billingProductsTargeted;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement inputbillingProducts;

	// @FindBy(xpath =
	// "(//*[@id='mm_PRODUCT_SHOT']/div/div/div/div/div[3]/a)[10]")
	@FindBy(xpath = "//div[@aria-hidden='false']/div/div/div[@class='modal-footer']/a")
	WebElement saveImageButton;

	@FindBy(xpath = "//*[@id='ad_settings']/div[2]/div/form[1]/div/div[1]/div/div/div/a")
	WebElement templateOptionButton;

	@FindBy(xpath = "//div[span[text()='USA - 6in Manufacturing Coupon']]/button")
	WebElement USA_6in_Manufacturing_Coupon;

	@FindBy(xpath = "//div[span[text()='USA - Manufacturing Coupon - GS1']]/button")
	WebElement gs1CouponTemplateButton;

	@FindBy(xpath = "//*[@id='template_browse_modal']/div/div/div[3]/button")
	WebElement templateSaveButton;

	// @FindBy(xpath =
	// "//*[@id='ad_settings']/div[2]/div/form[2]/div[3]/div[7]/div[1]/textarea")
	@FindBy(xpath = "//textarea[@elastic='Qualifier Line 1']")
	WebElement qualiferLineInput;

	@FindBy(xpath = "//div[@data-section= 'verbiage1']/textarea")
	WebElement qualiferLineInputafter;

	// div[@data-section='verbiage5']/textarea
	@FindBy(xpath = "//textarea[contains(@elastic, 'Offre valable jusqu')]")
	WebElement qualiferLineInput5;

	@FindBy(xpath = "//div[@data-section= 'verbiage5']/textarea")
	WebElement qualiferLineInput5after;

	// @FindBy(xpath =
	// "//*[@id='ad_settings']/div[2]/div/form[2]/div[3]/div[9]/div[1]/textarea")
	@FindBy(xpath = "//textarea[@elastic='Save Line']")
	WebElement saveLineInput;

	// Added by rashmi 9/4/2019
	@FindBy(xpath = "//div[@data-section='discount_amount_off']/textarea")
	WebElement saveLineInputafter;

	@FindBy(xpath = "//div[(@rv-show='variable.type | eq image,product_shot') and not(contains(@style,'display: none;'))]/div[@rv-show='variable.id | in static_image']/a/div/input")
	WebElement addStaticImage;

	// @FindBy(xpath =
	// "(//*[@id='mm_STATIC_IMAGE']/div/div/div/div/div[3]/a)[10]")
	@FindBy(xpath = "//div[@aria-hidden='false']/div/div/div[@class='modal-footer']/a")
	WebElement staticImageSave;
	
	@FindBy(xpath = "//input[@id='product_shot_image_upload_control']")
	WebElement productcustomImage;
	
	@FindBy(xpath = "//div[@aria-hidden='false']/div/div/div[@class='modal-footer']/a")
	WebElement productcustomImageSave;

	@FindBy(xpath = "//*[@id='ad-save-bottom']")
	WebElement saveButton;

	@FindBy(id = "ad-publish")
	WebElement publishButton;

	@FindBy(xpath = "//div[@id='global_confirm_dialog']/div/div/div/button[contains(text(),'Yes this is ok!')]")
	WebElement yesThisIsOkButton;

	@FindBy(xpath = "//*[@id='content']/div[1]/div[1]/h3/small")
	WebElement promotionID;

	@FindBy(xpath = "//*[@id='targeting_triggers']/div/div/div[1]/h4/a")
	WebElement addTiggerGroup;

	@FindBy(xpath = "//*[@id=\"purchase_requirements\"]/div[1]/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteriaButton;

	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;
	
	@FindBy(xpath = "/html/body/div[40]/div/input")
	WebElement selectTypeTextBox;

	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/div[@class='select2-container trigger_purchase_measure']/a[span[text()='(measure)']]")
	WebElement measureButton;
	
	@FindBy(xpath = "//div[@data-section='promoted_legal_restrictions']/div/div/label/strong[text()='High Risk']")
	//"//div[@data-section='promoted_legal_restrictions']//div//input[@rv-checked='$scope.ad.experience.end_of_order']")
	//"//*[@id='ad_settings']/div[2]/div/form[2]/div[1]/div[6]/div/div[5]/label/input")
	//"//input[@rv-checked='$scope.ad.experience.end_of_order']")
WebElement highRiskCheckbox;

	@FindBy(xpath = "/html/body/div[40]/div/input")
	WebElement measureTextBox;

	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/span[@rv-show='trigger.purchase_measure']/div/a[span[text()='(condition)']]")
	WebElement conditionButton;

	@FindBy(xpath = "/html/body/div[40]/div/input")
	WebElement conditionTextBox;

	@FindBy(xpath = "//*[@id='trigger-type_1']/section[1]/span[2]/span[1]/input[2]")
	WebElement inputUnitTextBox;
	

	@FindBy(xpath = "//section[@class='TriggerPurchaseEvent']/span/div/a[span[text()='(product attribute)']]")
	WebElement productAttributeButton;

	@FindBy(xpath = "/html/body/div[40]/div/input")
	WebElement productAttributeTextBox;

	@FindBy(xpath = "//a[span[text()='(list source)']][1]")
	WebElement listSourceButton;
	
	@FindBy(xpath="(//span[@rv-show='trigger.reference | eq lmc']/div/ul/li/input)[1]")
	WebElement listmanagerTextBox;
	
	@FindBy(xpath="//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul")
	WebElement checkListSelected;
	
	@FindBy(xpath = "/html/body/div[40]/div/input")
	WebElement listSourceTextBox;

	@FindBy(xpath = "(//*/div/ul/li/input)[6]")
	WebElement inputForManager;

	@FindBy(xpath = "//span[@rv-hide='trigger.product_attribute | in ClassAndDepartmentCode,Department,FamilyCode,ItemCode']/section/div/a[span[text()='(trigger scope)']]")
	WebElement triggerScopeButton;

	@FindBy(xpath = "/html/body/div[41]/div/input")
	WebElement inputTriggerScope;

	@FindBy(xpath = "(//a[span[text()='(operation)']])[7]")
	WebElement operationButton;
	
	@FindBy(xpath = "//div[(@rv-show='variable.type | eq image,product_shot') and not(contains(@style,'display: none;'))]/div[@rv-show='variable.id | eq product_shot_url']/a/div/input")
	WebElement addProductShotImage;

	@FindBy(xpath = "(//a[span[text()='(measure)']])[2]")
	WebElement totalOrderMeasureButton;

	@FindBy(xpath = "(//a[span[text()='(condition)']])[2]")
	WebElement totalOrderConditionButton;

	@FindBy(xpath = "//*[@id='trigger-type_1']/section[4]/span/span[1]/input[2]")
	WebElement totalOrderValue;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[3]")
	WebElement totalOrderTriggerScope;

	@FindBy(xpath = "//*[@id='trigger-type_1']/section[1]/span[3]/span[6]/span/textarea")
	WebElement inputForDepartmentCodes;

	@FindBy(xpath = "//a[contains(text(),' AdGroup')]")
	WebElement adgrouptab;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[1]")
	WebElement departmentTriggerScopeButton;
	
	@FindBy(xpath="//div[@id='offer_code_management_modal']/div/div/div/button[contains(text(),'Close')]")
	WebElement close;

	@FindBy(xpath = "//div[@data-section='promoted_legal_restrictions']//div//input[@rv-checked='$scope.ad.experience.unlimited']")
	WebElement unLimitedCheckbox;

	@FindBy(xpath = "//div[@class='form-group']/div/input[@placeholder='Display Name'  and not(contains(@disabled,'disabled'))]")
	WebElement adname;

	@FindBy(xpath = "//div[@class='form-inline inline-input']/div[@class='select2-container view_expiration_event']")
	WebElement selectbuttonrolling;

	// @FindBy(xpath =
	// "(//div[@class='calendar-table']/table/tbody/tr[5]/td[2])[1]")
	// WebElement startDate;

	// @FindBy(xpath =
	// "(//div[@class='calendar-table']/table/tbody/tr[6]/td[2])[1]")
	// WebElement stopDate;

	@FindBy(xpath = "//div[contains(@style, 'top: ') or contains(@style,'display: block; top:')]/div/input[@tabindex='-1']")
	WebElement selectinputtextbox;

	@FindBy(xpath = "//td[@class='available']")
	List<WebElement> dateAvailable;

	@FindBy(xpath = "//td[@class='weekend available']")
	List<WebElement> dateWeekendAvailable;

	@FindBy(xpath = "//td[@class='today active start-date active end-date available']")
	WebElement todayactivedate;

	@FindBy(xpath = "//input[@rv-value='$scope.ad.rank']")
	WebElement defaultRankinput;

	@FindBy(xpath = "//a[@rv-route-platform-adgroups-edit='view.adgroup.id']")
	WebElement adgroup;

	@FindBy(xpath = "//table[@class='table table-grid']/tbody/tr/td[@rv-data-text='column.value']/span/span/a[contains(text(),'- Buy N/Save $X')]")
	WebElement firstElementInListadgroup;
	
	@FindBy(xpath= "//*[@id=\"purchase_requirements\"]/div[1]/div/div[1]/h4/a")
	WebElement addredemptionrequirementButton;
	
	
	public void clicktodayactivedate() {

		this.todayactivedate.getText();

	}

	public void addRetailCouponTocart() {

		wait(this.retailCouponAddToCart);
		this.retailCouponAddToCart.click();
	}

	public void clickfirstElementInListadgroup() {
		wait(this.firstElementInListadgroup);
		this.firstElementInListadgroup.click();
	}

	public void addUSRetailCouponTocart() {
		WebDriverWait wait = new WebDriverWait(driver, 400);
		wait.until(ExpectedConditions.visibilityOf(this.retailCouponUSAddToCart));
		// wait(this.retailCouponAddToCart);
		this.retailCouponUSAddToCart.click();
	}

	public void addukCouponTocart() {
		WebDriverWait wait = new WebDriverWait(driver, 200);
		wait.until(ExpectedConditions.visibilityOf(this.ukCouponAddToCart));
		// wait(this.retailCouponAddToCart);
		this.ukCouponAddToCart.click();
	}

	public void addItalyCouponAddToCart() {
		System.out.println(new Date().toString() + " waiting for visibility of ItalyCouponAddToCart element");
		WebDriverWait wait = new WebDriverWait(driver, 200);
		wait.until(ExpectedConditions.visibilityOf(this.ItalyCouponAddToCart));
		System.out.println(new Date().toString() + " found ItalyCouponAddToCart element");
		// wait(this.retailCouponAddToCart);
		this.ItalyCouponAddToCart.click();
	}

	public void clickOnNext() {

		wait(this.nextButton);
		this.nextButton.click();
	}

	public void pageDispalyed() {
		wait(this.pageName);
		this.pageName.getText();
		System.out.println(this.pageName.getText());
	}

	public void clickFrequencyCappingClickHereButton() {
		wait(this.frequencyCappingClickHereButton);
		this.frequencyCappingClickHereButton.click();
	}

	public void typefrequencyCappingClickTextBox(String text) {
		wait(this.frequencyCappingClickTextBox);
		this.frequencyCappingClickTextBox.clear();
		this.frequencyCappingClickTextBox.sendKeys(text);
	}

	public void clickunlimitedChecbox() {
		wait(this.unlimitedChecbox);
		this.unlimitedChecbox.click();
	}

	public void typeDefaultrank(String rank) {
		wait(this.defaultRankinput);
		this.defaultRankinput.sendKeys(rank);
		try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.defaultRankinput.sendKeys(Keys.ENTER);
	}

	public void clickDistributedCappingClickHereButton() {
		wait(this.distributedCappingClickHereButton);
		this.distributedCappingClickHereButton.click();
	}

	public void typeDistributedCappingTextBox(String text) {
		wait(this.distributedCappingTextBox);
		this.distributedCappingTextBox.clear();
		this.distributedCappingTextBox.sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
	}
	
	public void typeseconddistributedCappingTextBox(String text) {
		wait(this.seconddistributedCappingTextBox);
		this.seconddistributedCappingTextBox.clear();
		this.seconddistributedCappingTextBox.sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
	}
	
	

	public void clicksettingstab() {
		wait(this.settingstab);
		this.settingstab.click();

	}
	
	public void clickadddistcapping() {
		wait(this.adddistcapping);
		this.adddistcapping.click();

	}
	
	
	public void clicklevelsdropdown() {
		wait(this.levelsdropdown);
		this.levelsdropdown.click();

	}
	
	public void clicklevelsdropdown2() {
		wait(this.levelsdropdown2);
		this.levelsdropdown2.click();

	}
	
	
	public void clickcaplevelsdropdown() {
		wait(this.caplevelsdropdown);
		this.caplevelsdropdown.click();

	}
	
	
	public void clickcaptimewindow() {
		wait(this.captimewindow);
		this.captimewindow.click();

	}
	
	
	
	public void clickcapweekgroup() {
		wait(this.capweekgroup);
		this.capweekgroup.click();

	}
	public void clickdropdownTextBox(String text) {
		wait(this.dropdownTextBox);
		this.dropdownTextBox.sendKeys(text);
		try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dropdownTextBox.sendKeys(Keys.ENTER);

	}
	public void clickAddProductGroupButton() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", this.addProductGroupButton);
		System.out.println(this.addProductGroupButton.getText());
		// WebDriverWait wait= new WebDriverWait(driver, 100);
		// wait.until(ExpectedConditions.visibilityOf(this.addProductGroupButton));
		// wait(this.addProductGroupButton);
		// wait(this.productValidationTextBox);
		// productValidationTextBox.click();
		// this.;
	}

	public void typeProductValidationTextBox(String text) {
		WebDriverWait wait = new WebDriverWait(driver, 200);
		wait.until(ExpectedConditions.visibilityOf(this.productValidationInputBox));
		// this.productValidationTextBox.click();
		this.productValidationInputBox.sendKeys(text);
		try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.productValidationInputBox.sendKeys(Keys.ENTER);
	}

	public void clickRedemtionClearingButton() {
		// WebDriverWait wait= new WebDriverWait(driver,200);
		// wait.until(ExpectedConditions.elementToBeClickable(this.redemtionClearingButton));
		// wait.until(ExpectedConditions.invisibilityOf(this.indicatorElement));
		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();",
		// this.redemtionClearingButton);
		wait(this.redemtionClearingButton);
		this.redemtionClearingButton.click();
	}

	public void clickSettingButton() {
		wait(this.settingButton);
		this.settingButton.click();
	}

	public void clickTargetingButton() {
		wait(this.targetingButton);
		this.targetingButton.click();
	}

	public void clickDigitalSetupButton(){
		wait(this.digitalsetupButton);
		this.digitalsetupButton.click();
	}
	public void clickClearingMethodButton() {
		WebDriverWait wait = new WebDriverWait(driver, 200);
		wait.until(ExpectedConditions.visibilityOf(clearingMethodButton));
		// wait(this.clearingMethodButton);
		this.clearingMethodButton.click();
	}

	public void typeClearingMethodTypeBox(String text) throws InterruptedException {

		wait(this.clearingMethodTypeBox);
		this.clearingMethodTypeBox.sendKeys(text);
		Thread.sleep(2 * 1000);
		this.clearingMethodTypeBox.sendKeys(Keys.ENTER);

	}

	public void typecompanyprefixfield() {

		wait(this.companyprefixfield);
		this.companyprefixfield.click();
	}

	public void clickBarCodeTypeButton() {
		wait(this.barCodeTypeButton);
		this.barCodeTypeButton.click();
	}
	
	
	public void clickaddRequirementButton() {
		wait(this.addredemptionrequirementButton);
		this.addredemptionrequirementButton.click();
	}
	public void typeBarCodeTypeTextBox(String text) throws InterruptedException {
		wait(this.barCodeTypeTextBox);
		this.barCodeTypeTextBox.sendKeys(text);
		Thread.sleep(2 * 1000);
		this.barCodeTypeTextBox.sendKeys(Keys.ENTER);

	}

	public void typeBarCodeValue(String text) {
		wait(this.barCodeValue);
		this.barCodeValue.sendKeys(text);
	}

	public void typebarCodeFormat(String text) throws InterruptedException {
		wait(this.barCodeFormat);
		this.barCodeFormat.click();
		this.barCodeFormatTextBox.click();
		this.barCodeFormatTextBox.sendKeys(text);
		Thread.sleep(2 * 1000);
		this.barCodeTypeTextBox.sendKeys(Keys.ENTER);
		// camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		// this.barCodeTypeTextBox.sendKeys(Keys.ENTER);
	}

	public void typeclearinghouse(String text) throws InterruptedException {
		wait(this.clearinghouse);
		this.clearinghouse.click();
		this.clearinghouseTextBox.click();
		this.clearinghouseTextBox.sendKeys(text);
		this.clearinghouseTextBox.sendKeys(Keys.ENTER);
		camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		Thread.sleep(2 * 1000);
		//this.clearinghouseTextBox.sendKeys(Keys.ARROW_DOWN);
		this.clearinghouseTextBox.sendKeys(Keys.ENTER);
	}

	public void typecompanyprefixTextBox(String text) throws InterruptedException {
		wait(this.companyprefixTextBox);
		this.companyprefixTextBox.click();
		this.companyprefixTextBox.sendKeys(text);
		this.companyprefixTextBox.sendKeys(Keys.ENTER);
		camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		Thread.sleep(2 * 1000);
		this.companyprefixTextBox.sendKeys(Keys.ARROW_DOWN);
		this.companyprefixTextBox.sendKeys(Keys.ENTER);

	}
	
	public void typecompanyprefixTextBox_Digital(String text) throws InterruptedException {
		wait(this.companyprefixTextBox);
		this.companyprefixTextBox.click();
		this.companyprefixTextBox.sendKeys(text);
		this.companyprefixTextBox.sendKeys(Keys.ENTER);
		camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		Thread.sleep(2 * 1000);
		this.companyprefixTextBox.sendKeys(Keys.ARROW_DOWN);
		this.companyprefixTextBox.sendKeys(Keys.ARROW_DOWN);
		this.companyprefixTextBox.sendKeys(Keys.ARROW_DOWN);
		this.companyprefixTextBox.sendKeys(Keys.ARROW_DOWN);
		this.companyprefixTextBox.sendKeys(Keys.ARROW_DOWN);
		this.companyprefixTextBox.sendKeys(Keys.ENTER);

	}

	public void typebarCodevalue(String text) {
		wait(this.barCodevalue);
		this.barCodevalue.click();
		this.barCodevalueTextBox.click();
		this.barCodevalueTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.barCodevalueTextBox.sendKeys(Keys.ENTER);
	}

	public void clickaddSymbol() {
		wait(this.addSymbol);
		this.addSymbol.click();
	}

	public void clickTextbox() {
		wait(this.inputTextbox);
		this.inputTextbox.click();
	}

	public void typesymbolropdown(String text) {
		wait(this.symbolropdown);
		this.symbolropdown.clear();
		this.symbolropdown.sendKeys(text);
	}

	public void typeinputTextbox(String text) throws InterruptedException {
		wait(this.inputTextbox);
		this.inputTextbox.click();
		Thread.sleep(2 * 1000);
		this.inputTextbox.sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
	}

	public void clicksavebuttonbarcode() {
		wait(this.savebuttonbarcode);
		this.savebuttonbarcode.click();
	}

	public void typeStaticBarCodeValue(String text) {
		wait(this.staticBarCodeValue);
		this.staticBarCodeValue.sendKeys(text);
	}

	public void typestaticBarCodeValueupca(String text) {
		wait(this.staticBarCodeValueupca);
		this.staticBarCodeValueupca.click();
		this.staticBarCodeValueupca.sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
	}

	public void typegs1barcodevalue(String text) {
		wait(this.gs1barcodevalue);
		this.gs1barcodevalue.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.gs1barcodevalue.sendKeys(Keys.ENTER);
	}

	public void scrollToTop() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scrollTo(0,0)");
	}
	
	public void scrollToDown() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public void clickEnableExpireDateButton() {
		wait(this.enableExpireDateButton);
		this.enableExpireDateButton.click();
	}

	public void clickrollingexpirationradioButton() {
		wait(this.rollingexpirationradioButton);
		this.rollingexpirationradioButton.click();
	}

	public void clickDateFormatButton() {
		wait(this.dateFormatButton);
		this.dateFormatButton.click();
	}

	public void typeDateFormatTextBox(String text) {
		wait(this.dateFormatTextBox);

		this.dateFormatTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dateFormatTextBox.sendKeys(Keys.ENTER);
	}

	public void clickCalanderButton() {
		wait(this.calanderButton);
		this.calanderButton.click();
	}

	public void clickDateOption() {
		wait(this.dateOption);
		this.dateOption.click();
	}

	public void clickCalanderstartButton() throws InterruptedException {
		Thread.sleep(4 * 1000);
		wait(this.calanderstartButton);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", this.calanderstartButton);
		System.out.println(this.calanderstartButton.getText());
	}

	public void clickCalanderstopButton() throws InterruptedException {
		Thread.sleep(3 * 1000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", this.calanderstopButton);
		System.out.println(this.calanderstopButton.getText());

	}

	public void clickcalanderleftButton() {
		wait(this.calanderleftButton);
		this.calanderleftButton.click();
	}

	public void clickcalanderrightButton() {
		wait(this.calanderrightButton);
		this.calanderrightButton.click();
	}

	public void clickcalanderrightButtonsecond() {
		wait(this.calanderrightButtonsecond);
		this.calanderrightButtonsecond.click();
	}
	
	
	public void clicksecondcalanderrightButtonsecond() {
		wait(this.secondcalanderrightButtonsecond);
		this.secondcalanderrightButtonsecond.click();
	}
	public void clickcalanderrightenddateButton() {
		wait(this.calanderrightenddateButton);
		this.calanderrightenddateButton.click();
	}
	
	public void clickOnHighRisk() {
		wait(this.highRiskCheckbox);
		try {
			Thread.sleep(2*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.highRiskCheckbox.click();
	}

	
	public boolean checkcalanderrightenddateButton() {
		
		
		int i=driver.findElements(By.xpath("(//div[@class='calendar left single']/div/table/thead/tr[1]/th[@class='next available']/i)[1]")).size();
		if(i>0 )
		{
			
			return true;
		}
		
		return false;
	}
   

	public void clickcalanderrightenddateButtonfortodaydate() {
		wait(this.calanderrightenddateButtonfortodaydate);
		this.calanderrightenddateButtonfortodaydate.click();
	}

	public void typeChannel(String text) {

		try {
			wait(this.channels);
			this.channels.sendKeys(text);
			Thread.sleep(1 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.channels.sendKeys(Keys.ENTER);
	}

	public void addImage(String text) {

		this.addImage.sendKeys(text);
	}
	
	public void clickOnPins() {
		wait(this.addpinDropdown);
		this.addpinDropdown.click();
	}
	
	public void clickBarcodeValueSaveButton() {
		wait(this.barcodeValueSaveButton);
		this.barcodeValueSaveButton.click();
	}
	
	public void clickEnableOfferCode() {
		wait(this.enableOfferCode);
		this.enableOfferCode.click();
	}
	
	public void clickOnGenerated() {
		wait(this.generatedRadioButton);
		this.generatedRadioButton.click();
	}
	

	public void typeinputForPins(String text) throws InterruptedException {
		wait(this.inputForPins);
		this.inputForPins.sendKeys(text);
//		WebElement dropdown = this.checkListSelected;
//
//		List<WebElement> optionss = dropdown.findElements(By.tagName("li"));
//		System.out.println("options:"+optionss);
//
//		WebElement searchingElem = null;
//		try {
//			searchingElem = driver.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
//		} catch (Exception e) {
//			System.out.println("Searching element not found.");
//		}
//		int options = optionss.size();
//		while (options > 1 || (options == 1 && searchingElem != null)) {
//			Thread.sleep(200);
//			optionss = dropdown.findElements(By.tagName("li"));
//			options = optionss.size();
//			try {
//				searchingElem = driver
//						.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
//			} catch (Exception e) {
//				System.out.println("Searching element not found.");
//				searchingElem = null;
//			}
//			if (searchingElem != null) {
//				System.out.println("Waiting for Searching element to clear.");
//				camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
//				System.out.println("Done waiting for Searching element to clear.");
//				searchingElem = null;
//				optionss = dropdown.findElements(By.tagName("li"));
//				options = optionss.size();
//			}
//			System.out.println("Dropdown list size is " + options);
//			for (WebElement elem : optionss) {
//				try {
//					if (!elem.getText().contains(text)) {
//						System.out.println("Dropdown list contains text other than " + text);
//						options = optionss.size();
//						break;
//					} else {
//						System.out.println("Dropdown list contains same text");
//						options = 1;
//					}
//				} catch (StaleElementReferenceException e) {
//					// TODO: handle exception
//				}
//			}
//			try {
//				searchingElem = driver
//						.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
//			} catch (Exception e) {
//				System.out.println("Searching element not found.");
//				searchingElem = null;
//			}
//		}
//		System.out.println("Out of while loop. List contains " + optionss.size());
//		String optionName = optionss.get(0).getText();
//		System.out.println("optionName: " + optionName);
//		if (optionName.contains(text)) {
//			System.out.println("optionName name found, selecting the element");
//			driver.findElement(By.xpath(
//					"(//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul/li/div)[1]"))
//					.click();
//		}
//		this.inputForPins.sendKeys(Keys.ENTER);
		//this.companyprefixTextBox.sendKeys(text);
		camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		Thread.sleep(4*1000);
		this.inputForPins.sendKeys(Keys.ARROW_DOWN);
		this.inputForPins.sendKeys(Keys.ENTER);
		camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		//Thread.sleep(2 * 1000);
//		this.inputForPins.sendKeys(Keys.ARROW_DOWN);
//		this.inputForPins.sendKeys(Keys.ENTER);
	}

	
	public void addProductShotImage(String text) {

		this.addProductShotImage.sendKeys(text);
	}
	
	

	public void addproductShot(String text) throws InterruptedException {
		// this.productShot.click();
		// Thread.sleep(3*1000);
		this.productShot.sendKeys(text);
	}

	public void clickbillingProducts() {
		wait(this.billingProducts);
		this.billingProducts.click();
	}

	public void typeUSABarCodeValue(String text) {
		wait(this.USABarCodeValue);
		this.USABarCodeValue.sendKeys(text);
	}

	public void clickbillingProductsTargeted() {
		wait(this.billingProductsTargeted);
		this.billingProductsTargeted.click();
	}

	public void typeinputbillingProducts(String text) {
		wait(this.inputbillingProducts);
		this.inputbillingProducts.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.inputbillingProducts.sendKeys(Keys.ARROW_DOWN);
		this.inputbillingProducts.sendKeys(Keys.ENTER);
	}

	public void clickSaveImageButton() {
		wait(this.saveImageButton);
		this.saveImageButton.click();
	}

	public void clickTemplateOptionButton() {
		wait(this.templateOptionButton);
		this.templateOptionButton.click();
	}

	public void clickUSA_6in_Manufacturing_Coupon() {
		wait(this.USA_6in_Manufacturing_Coupon);
		this.USA_6in_Manufacturing_Coupon.click();
	}

	public void clickGS1CouponTemplateButton() {
		wait(this.gs1CouponTemplateButton);
		this.gs1CouponTemplateButton.click();
	}

	public void clickOnSensitive() {
		wait(this.sensitiveCheckbox);
		this.sensitiveCheckbox.click();
	}

	public void clickTemplateSaveButton() {
		wait(this.templateSaveButton);
		this.templateSaveButton.click();
	}

	public boolean QualiferLineInput() {
		int i = driver.findElements(By.xpath("//textarea[@elastic='Qualifier Line 1']")).size();
		if (i > 0) {
			return true;
		}
		return false;
	}

	public void typequaliferLineInput5(String text) {
		wait(this.qualiferLineInput5);
		qualiferLineInput5.click();
		qualiferLineInput5.clear();
		wait(this.qualiferLineInput5after);
		this.qualiferLineInput5after.click();
		this.qualiferLineInput5after.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.qualiferLineInput5after.sendKeys(Keys.ENTER);
	}

	public void typeQualiferLineInputafter(String text) {
		// wait(this.qualiferLineInput);
		// qualiferLineInput.click();

		wait(this.qualiferLineInputafter);
		qualiferLineInputafter.clear();
		this.qualiferLineInputafter.click();
		this.qualiferLineInputafter.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.qualiferLineInputafter.sendKeys(Keys.ENTER);
	}

	public void typesaveLineInput(String text) {
		wait(this.saveLineInput);
		this.saveLineInput.clear();

	}

	public void clickOnBlind() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", this.blindCheckbox);
	}

	public void typesaveLineInputafter(String text) {
		this.saveLineInputafter.clear();
		this.saveLineInputafter.sendKeys(text);
	}

	public void addStaticImage(String text) {

		this.addStaticImage.sendKeys(text);
	}

	public void clickSaveStaticImage() {
		wait(this.staticImageSave);
		this.staticImageSave.click();
	}
	
	public void addprodcutCustomImage(String text) {

		this.productcustomImage.sendKeys(text);
	}

	public void clickSaveCustomImage() {
		wait(this.productcustomImageSave);
		this.productcustomImageSave.click();
	}

	public void clickSaveButton() {
		wait(this.saveButton);
		this.saveButton.click();
	}

	public boolean alertOkClick() {

		// [not(contains(@style,'display: none;'))]
		int i = driver
				.findElements(By.xpath("//div[contains(@style,'display: block;')]/div/div/div/button[text()='Ok']"))
				.size();
		if (i > 0) {
			System.out.println("I am here true");
			return true;
		}
		System.out.println("I am here false");
		return false;
	}

	public void clickalertAccept() {
		wait(this.alertAccept);
		this.alertAccept.click();
	}

	public void clickPublishButton() {
		wait(this.publishButton);
		this.publishButton.click();
	}

	public void clickYesThisIsOkButton() {
		wait(this.yesThisIsOkButton);
		this.yesThisIsOkButton.click();
	}

	public void clickOnNBIC() {
		wait(this.nbicCheckbox);
		this.nbicCheckbox.click();
	}

	public String getPromotionId() {
		return this.promotionID.getText().split(" ")[this.promotionID.getText().split(" ").length - 1];
	}

	public void clickOnCreatedAdGroup() {
		wait(this.adgroup);
		this.adgroup.click();
	}
	public void clickOnClose() {
		wait(this.close);
		this.close.click();
	}
	public void clickOnUnlimited() {
		wait(this.unLimitedCheckbox);
		this.unLimitedCheckbox.click();
	}

	public void clickAddTiggerGroup() {
		wait(this.addTiggerGroup);
		this.addTiggerGroup.click();

	}

	public void clickAddCriteriaButton() {
		wait(this.addCriteriaButton);
		this.addCriteriaButton.click();
	}

	public void clickSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}

	public void typeSelectTypeTextBox(String text) {
		wait(this.dropdownTextBox);
		this.dropdownTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dropdownTextBox.sendKeys(Keys.ENTER);
	}

	public void typeselectinputtextbox(String text) {
		wait(this.selectinputtextbox);
		this.selectinputtextbox.click();
		this.selectinputtextbox.sendKeys(text);
		try {
			Thread.sleep(4 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.selectinputtextbox.sendKeys(Keys.ENTER);
	}

	public void clickMeasureButton() {
		wait(this.measureButton);
		this.measureButton.click();
	}

	public void typeMeasureTextBox(String text) {
		wait(this.dropdownTextBox);
		this.dropdownTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dropdownTextBox.sendKeys(Keys.ENTER);
	}

	public void clickConditionButton() {
		wait(this.conditionButton);
		this.conditionButton.click();
	}

	public void typeConditionTextBox(String text) {
		wait(this.dropdownTextBox);
		this.dropdownTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dropdownTextBox.sendKeys(Keys.ENTER);
	}

	public void clickOnCompetitive() {
		wait(this.competitiveCheckbox);
		this.competitiveCheckbox.click();
	}

	public void typeInputUnitTextBox(String text) {
		wait(this.inputUnitTextBox);
		this.inputUnitTextBox.sendKeys(text);
	}

	public void clickProductAttributeButton() {
		wait(this.productAttributeButton);
		this.productAttributeButton.click();
	}

	public void typeProductAttributeTextBox(String text) {
		wait(this.dropdownTextBox);
		this.dropdownTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dropdownTextBox.sendKeys(Keys.ENTER);
	}

	public void clickListSourceButton() {
		wait(this.listSourceButton);
		this.listSourceButton.click();
	}

	public void clickselectbuttonrolling() {
		wait(this.selectbuttonrolling);
		this.selectbuttonrolling.click();

	}
	
	public void clickoffercodevalue() {
		wait(this.offercodevalue);
		this.offercodevalue.click();
	}
	public void clickvaluecodevalue() {
		wait(this.valuecodevalue);
		this.valuecodevalue.click();
	}
	
	public void clickSymbolLengthTextBox(String text) {
		wait(this.symbolLengthTextBox);
		this.symbolLengthTextBox.click();
		this.symbolLengthTextBox.clear();
		wait(this.symbolLength);
		this.symbolLength.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.symbolLength.sendKeys(Keys.ENTER);
		
	}

	public void typeDefaultRank(String rank) {
		wait(this.defaultRankInput);
		this.defaultRankInput.sendKeys(rank);
		try {
			Thread.sleep(2 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.defaultRankInput.sendKeys(Keys.ENTER);
	}

	public void typeListSourceTextBox(String text) {
		wait(this.dropdownTextBox);
		this.dropdownTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dropdownTextBox.sendKeys(Keys.ENTER);
	}
	
	public void typeListManagerTextBox(String text)throws InterruptedException
	{
		wait(this.listmanagerTextBox);
		this.listmanagerTextBox.sendKeys(text);
		Thread.sleep(3000);
		WebElement dropdown = this.checkListSelected;

		List<WebElement> optionss = dropdown.findElements(By.tagName("li"));
		WebElement searchingElem = null;
		try {
			searchingElem = driver.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
		} catch (Exception e) {
			System.out.println("Searching element not found.");
		}
		int options = optionss.size();
		while (options > 1 || (options == 1 && searchingElem != null)) {
			Thread.sleep(200);
			optionss = dropdown.findElements(By.tagName("li"));
			options = optionss.size();
			try {
				searchingElem = driver
						.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
			} catch (Exception e) {
				System.out.println("Searching element not found.");
				searchingElem = null;
			}
			if (searchingElem != null) {
				System.out.println("Waiting for Searching element to clear.");
				camppage.waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
				System.out.println("Done waiting for Searching element to clear.");
				searchingElem = null;
				optionss = dropdown.findElements(By.tagName("li"));
				options = optionss.size();
			}
			System.out.println("Dropdown list size is " + options);
			for (WebElement elem : optionss) {
				try {
					if (!elem.getText().contains(text)) {
						System.out.println("Dropdown list contains text other than " + elem.getText());
						System.out.println("Dropdown list contains text other than " + text);
						options = optionss.size();
						break;
					} else {
						System.out.println("Dropdown list contains same text");
						options = 1;
					}
				} catch (StaleElementReferenceException e) {
					// TODO: handle exception
				}
			}
			try {
				searchingElem = driver
						.findElement(By.xpath("//ul[@class='select2-results']/li[text()='Searching...']"));
			} catch (Exception e) {
				System.out.println("Searching element not found.");
				searchingElem = null;
			}
		}
		System.out.println("Out of while loop. List contains " + optionss.size());
		String optionName = optionss.get(0).getText();
		System.out.println("optionName: " + optionName);
		if (optionName.
				startsWith(text))
				//contains(text)) 
		{
			System.out.println("optionName name found, selecting the element");
			driver.findElement(By
					.xpath("(//div[contains(@class,'select2-drop select2-drop-multi select2-drop-active')]/ul/li/div)[1]"))
					.click();
		}
		this.listmanagerTextBox.sendKeys(Keys.ENTER);

	}

	public void typeInputForManager(String text) {
		wait(this.inputForManager);
		this.inputForManager.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inputForManager.sendKeys(Keys.ENTER);
	}
	
public void clickOnLease(String text) {
		
		
		
		WebElement leasewithDescription=driver.findElement(By.xpath("//tr/td[contains(text(),'"+text+"')]/following-sibling::*"));
		
		Actions actions = new Actions(driver);
		
		try {
			Thread.sleep(2*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	      // Scroll Down using Actions class
	      actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		
//		 JavascriptExecutor js = (JavascriptExecutor) this.driver;
//		   this.driver.manage().window();	
//		   wait(leasewithDescription);	
//		   js.executeScript("window.scrollTo(0, "+leasewithDescription+")");
		wait(leasewithDescription);	
		leasewithDescription.click();
	}

	public void clickTriggerScopeButton() {
		wait(this.triggerScopeButton);
		this.triggerScopeButton.click();
	}
	
	public void clickOnOfferCodeManagement() {
		wait(this.offerCodeManagement);
		this.offerCodeManagement.click();
	}
	
//	public void waitTillNetwork() {
//		WebDriverWait wait = new WebDriverWait(driver,300);
//	    wait.until(ExpectedConditions.textToBePresentInElement(network, "Network"));
//	}

	public void typeInputTriggerScope(String text) {
		wait(this.dropdownTextBox);
		this.dropdownTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dropdownTextBox.sendKeys(Keys.ENTER);
	}
	public void enterSelectSymbol(String text) {
		wait(this.selectSymbol);
		this.selectSymbol.click();
		wait(this.dateFormatTextBox);
		this.dateFormatTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dateFormatTextBox.sendKeys(Keys.ENTER);
	}
	
	public void clickAddSymbol() {
		wait(this.addSymbol);
		this.addSymbol.click();
	}
	public void clickOnAdvertisedFlag() {
		wait(this.advertisedFlag);
		this.advertisedFlag.click();
	}

	public void typeNBICInput(String text) {
		wait(this.nbicInput);

		this.nbicInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.nbicInput.sendKeys(Keys.ENTER);
	}

	public void clickOperationButton() {
		this.operationButton.click();
	}

	public void clickTotalOrderMeasureButton() {
		this.totalOrderMeasureButton.click();
	}

	public void clickTotalOrderConditionButton() {
		this.totalOrderConditionButton.click();
	}

	public void typeTotalOrderValue(String text) {
		this.totalOrderValue.sendKeys(text);
	}

	public void clickTotalOrderTriggerScope() {
		this.totalOrderTriggerScope.click();
	}

	public void clickadgrouptab() {
		wait(this.adgrouptab);
		this.adgrouptab.click();
	}

	public void typeDepartmentCodes(String text) {
		this.inputForDepartmentCodes.sendKeys(text);
	}

	public void clickDepartmentTriggerScopeButton() {
		this.departmentTriggerScopeButton.click();
	}

	public void clickadname(String text) {
		wait(this.adname);
		this.adname.click();
		this.adname.sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
		this.adname.sendKeys(Keys.ENTER);
	}

	public void StartDate(int strdate) throws InterruptedException {
		DateFormat dateFormat = new SimpleDateFormat("dd");
		DateFormat dateMonthFormat = new SimpleDateFormat("MM");

		// Date date = new Date();
		Calendar calToday = Calendar.getInstance();
		Calendar calStart = Calendar.getInstance();
		calStart.add(Calendar.DATE, strdate);

		int stDate = Integer.parseInt(dateFormat.format(calStart.getTime()));
		System.out.println("start date:" + stDate);

		int stMonth = Integer.parseInt(dateMonthFormat.format(calStart.getTime()))
				- Integer.parseInt(dateMonthFormat.format(calToday.getTime()));
		System.out.println("start month:" + stMonth);

		System.out.println("Clicking start date button");
		this.clickCalanderstartButton();
		Thread.sleep(3 * 1000);
if (calStart.get(Calendar.YEAR)<calToday.get(Calendar.YEAR))
			
		{
			this.clickcalanderleftButton();
			//this.clicksecondcalanderrightButtonsecond();
		}
		Thread.sleep(3 * 1000);
		if (stMonth < 0) {
			System.out.println("Clicking previous month");
			this.clickcalanderleftButton();
		}
		// System.out.println("Setting start date to "+ stDate);
		dateSelector(stDate);
		Thread.sleep(5 * 1000);
	}

	public void getBLText() {
		
		bl=getPromotionId();
		System.out.println("BL: "+bl);
		}
 
	
	public void addDynamicOfferCodeTocart() {

		wait(this.dynamicOfferCodeAddToCart);
		this.dynamicOfferCodeAddToCart.click();
	}
	public void waitForElementToappear(String xpath) {
		int count = driver.findElements(By.xpath(xpath)).size();
		while (count == 0) {
			try {
				System.out.println(new Date().toString() + " Element not found for xpath : " + xpath);
				Thread.sleep(500);
				count = driver.findElements(By.xpath(xpath)).size();
				System.out.println(new Date().toString() + " found " + count + " elements for xpath : " + xpath);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println(new Date().toString() + " Error");
				e.printStackTrace();
			}
		}
		System.out.println(new Date().toString() + " Out of element wait loop.");
	}

	public void EndDate(int endDate) throws InterruptedException {
		DateFormat dateFormat = new SimpleDateFormat("dd");
		DateFormat dateMonthFormat = new SimpleDateFormat("MM");
		DateFormat dateYearFormat = new SimpleDateFormat("YYYY");

		Calendar calToday = Calendar.getInstance();
		Calendar calEnd = Calendar.getInstance();
		calEnd.add(Calendar.DATE, endDate);
		int enDate = Integer.parseInt(dateFormat.format(calEnd.getTime()));
		System.out.println("end date:" + enDate);
System.out.println("cal tody"+calToday.getTime());
System.out.println("cal end"+calEnd.getTime());

		int enMonth = Integer.parseInt(dateMonthFormat.format(calEnd.getTime()))
				- Integer.parseInt(dateMonthFormat.format(calToday.getTime()));
		System.out.println("end month:" + enMonth);

		System.out.println("Clicking stop date button");
		this.clickCalanderstopButton();
		Thread.sleep(3 * 1000);
		System.out.println("End Year:"+Integer.parseInt(dateYearFormat.format(calEnd.getTime())));
		System.out.println("Start Year:" + calToday.get(Calendar.YEAR));
		
		if (calEnd.get(Calendar.YEAR)>calToday.get(Calendar.YEAR))
			
		{
			this.clickcalanderrightenddateButton();
			//this.clicksecondcalanderrightButtonsecond();
		}

		if (enMonth > 0) {
			System.out.println("Clicking next month");
			this.clickcalanderrightenddateButton();
//			if ((this.checkcalanderrightenddateButton())==true)
//			{
//				System.out.println("Clicking first element");
//				Reporter.addStepLog("Clicking on list source");
//				this.clickcalanderrightenddateButton();
//
//			}else{
//				System.out.println("Clicking second element");
//			this.clicksecondcalanderrightButtonsecond();
//		}
		// System.out.println("Setting stop date to " + enDate);
		}
			
		dateSelector(enDate);
		Thread.sleep(8 * 1000);
	}
	

	public void dateSelector(int Date) throws InterruptedException {
		DateFormat dateFormat = new SimpleDateFormat("dd");
		Calendar calToday = Calendar.getInstance();
		int todaydate = Integer.parseInt(dateFormat.format(calToday.getTime()));
		System.out.println("today DATE " + todaydate);

		if (Date == todaydate) {
			System.out.println("Today date code");
			wait(todayactivedate);
			String curretndate = todayactivedate.getText();
			System.out.println("curretndatecode" + curretndate);
			todayactivedate.click();

		} else {

			List<WebElement> dateList = dateAvailable;
			for (int i = 0; i < dateList.size(); i++) {
				String str = dateList.get(i).getText();
				// System.out.println("startdatechecking: "+str);
				if (str.equalsIgnoreCase("")) {
					continue;
				} else {
					int temp = Integer.parseInt(str);

					if (temp == Date) {

						dateList.get(i).click();
						// int startdate= dateList.get(i).click();

					}
				}
			}
		}

		List<WebElement> dateWeekendAvailableList = dateWeekendAvailable;
		for (int i = 0; i < dateWeekendAvailableList.size(); i++) {
			String str1 = dateWeekendAvailableList.get(i).getText();

			if (str1.equalsIgnoreCase("")) {
				continue;
			} else {
				int temp1 = Integer.parseInt(str1);
				// System.out.println("temp is "+temp1);
				if (temp1 == Date) {
					dateWeekendAvailableList.get(i).click();
				}
			}
		}
	}

	public void expirationDate() throws InterruptedException {
		DateFormat dateFormat = new SimpleDateFormat("dd");
		DateFormat dateMonthFormat = new SimpleDateFormat("MM");
		DateFormat dateYearFormat =new SimpleDateFormat("YYYY");

		Date date = new Date();
		Calendar calToday = Calendar.getInstance();
		Calendar calexpirationDate = Calendar.getInstance();
		Calendar calexpirationMonth =Calendar.getInstance();
		calexpirationDate.add(Calendar.DATE, 7);

		int exDate = Integer.parseInt(dateFormat.format(calexpirationDate.getTime()));
		System.out.println("Expiration date:" + exDate);
		System.out.println("1month:" + Integer.parseInt(dateYearFormat.format(calexpirationDate.getTime())));
		System.out.println("2 month" + Integer.parseInt(dateYearFormat.format(calToday.getTime())));
		int exMonth = Integer.parseInt(dateMonthFormat.format(calexpirationDate.getTime()))
				- Integer.parseInt(dateMonthFormat.format(calToday.getTime()));
		System.out.println("expiration month:" + exMonth);
		
		System.out.println("1month:" + Integer.parseInt(dateMonthFormat.format(calexpirationDate.getTime())));
		System.out.println("2 month" + Integer.parseInt(dateMonthFormat.format(calToday.getTime())));
		Reporter.addStepLog("Clicking on Calander");

		this.clickCalanderButton();
		
		System.out.println("Start Year:" + calToday.get(Calendar.YEAR));
		
		if (calexpirationDate.get(Calendar.YEAR)>calToday.get(Calendar.YEAR))
					
				{
					this.clickcalanderrightenddateButton();
				}
		// Thread.sleep(3 * 1000);
		if (exMonth > 0) {
			this.clickcalanderrightButtonsecond();
		}

		dateSelector(exDate);
		Thread.sleep(4 * 1000);

	}
	
	public void clickDynamiccode(String text) {
		wait(this.staticbarcode);
		this.staticbarcode.click();
		wait(this.dateFormatTextBox);
		this.dateFormatTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dateFormatTextBox.sendKeys(Keys.ENTER);
	}
	
	public void typesaveLineInputOffercode(String text) {
		wait(this.saveLineInputOffercode);
		this.saveLineInputOffercode.click();
		this.saveLineInputOffercode.clear();
		this.saveLineInputafter.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//this.saveLineInputafter.sendKeys(Keys.ENTER);
	}
	
	public void typerequiredAmount() {
		wait(this.requiredPurchaseAmount);
		this.requiredPurchaseAmount.click();
		this.requiredPurchaseAmount.clear();
		this.requiredPurchaseAmount.sendKeys("1");
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.requiredPurchaseAmount.sendKeys(Keys.ENTER);
	}

}
