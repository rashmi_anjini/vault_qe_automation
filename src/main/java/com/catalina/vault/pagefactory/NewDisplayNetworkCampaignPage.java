package com.catalina.vault.pagefactory;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewDisplayNetworkCampaignPage extends BaseClass{

	WebDriver driver;

	@FindBy(xpath = "//*[@id='campaign_settings']/div[2]/div/form/div[1]/div/input")
	WebElement campaignName;

	@FindBy(xpath = "//*[@id='s2id_autogen1']/a")
	WebElement client;
	
	//@FindBy(xpath="/html/body/div[11]/div/input")
	@FindBy(xpath="//div[contains(@class,'select2-drop-active')]/div/input")
	WebElement clientField;

	@FindBy(xpath="//*[@id='content']/div[2]/div[2]/div[2]/table/tbody/tr/td[2]/span[1]/span[1]/div[1]/small/a")
	WebElement edit;
	
	@FindBy(id = "s2id_billing-choice-input")
	WebElement billingButton;
	
	//@FindBy(xpath="/html/body/div[11]/div/input")
	@FindBy(xpath="//div[contains(@class,'select2-drop-active')]/div/input")
	WebElement billingTextBox;

	@FindBy(xpath = "//*[@id='campaign_settings']/div[2]/div/form/div[4]/div/div/label/input")
	WebElement ignoreLTCButton;
	
	
	@FindBy(xpath = "//input[@type='text' and @placeholder='Search...']")
	WebElement searchBox;
	
	@FindBy(xpath = "//table[@class='table table-grid']/tbody/tr/td[@rv-data-text='column.value']/span/span")
	WebElement firstcampaignelement;
	
	@FindBy(xpath = "(//div/ul/li/input)[1]")
	WebElement channels;

	@FindBy(id = "campaign_save_btn")
	WebElement saveButton;
	
	@FindBy(xpath = "//*[@id='tp_op_2']")
	WebElement touchPointCustomButton;
	

	@FindBy(xpath = "//*[@id='campaign_settings']/div[2]/div/form/a[2]")
	WebElement deleteButton;
	
	@FindBy(xpath="(//a[span[text()='Select Attribute...']])[1]")
	WebElement selectAttributeButton;
	
	@FindBy(xpath="//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement attributeInput;
	
	@FindBy(xpath="(//a[span[contains(text(),'pick')]])[3]")
	WebElement pickNetworkButton;
	
	@FindBy(xpath="(//a[span[contains(text(),'pick')]])[1]")
	WebElement pickNetworkButton1;
	
	
	@FindBy(xpath="//span[@rv-show='view.attribute_type | eq CLUSTER']/div/a")
	WebElement pickclusterButton;
	
	@FindBy(xpath="//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement networkInput;
	

	public NewDisplayNetworkCampaignPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		//wait = new WebDriverWait(driver, 180);
	}

	public void typeCampaignName(String text) throws InterruptedException {
		wait(this.campaignName);
		this.campaignName.sendKeys(text);
		Thread.sleep(3*1000);
	}

	public void typeClient(String text) throws InterruptedException {
		
		wait(this.client);
		this.client.click();
		
		wait(this.clientField);
		this.clientField.sendKeys(text);
		//this.clientField.sendKeys(Keys.ENTER);
			
		waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		/*int element= driver.findElements(By.xpath("//ul[@class='select2-results']/li/div")).size();
		System.out.println("dropdownlist" +element);
		if(element == 0)
		{
			Thread.sleep(1 * 1000);
		}*/
		//NewDisplayNetworkCampaignPage.returnOneElement(driver);	
			Thread.sleep(2*1000);	
       
		this.clientField.sendKeys(Keys.ARROW_DOWN);
		this.clientField.sendKeys(Keys.ENTER);
	}
	
	private static WebElement element = null;
	 private static List<WebElement> elements = null;

	 public static WebElement returnOneElement (WebDriver driver){
		 elements = driver.findElements(By.xpath("//ul[@class='select2-results']/li/div"));
		    for (WebElement element : elements){
		        String myText = element.findElement(By.xpath("//ul[@class='select2-results']/li/div")).getText();
		        if (myText.contains("catalina marketing")) {
		        	System.out.println("text is present");
		        	return element;
		        }
		    }return null;
	}

	public void clickBilling() {
		
		wait(this.billingButton);
		this.billingButton.click();
	
	}
	
	public void waitForElementToDisappear(String xpath)
	{
		int count =driver.findElements(By.xpath(xpath)).size(); 				
		while(count > 0)
		{
			try {
				System.out.println(new Date().toString() +" Element found for xpath : "+xpath);
				Thread.sleep(500);
				count =driver.findElements(By.xpath(xpath)).size();
				System.out.println(new Date().toString() +" found " + count +" elements for xpath : "+xpath);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println(new Date().toString() +" Error");
				e.printStackTrace();
			}
		}
		System.out.println(new Date().toString() + " Out of wait loop.");
	}
	
	
	public void typeBilling(String text) throws InterruptedException {
		wait(this.billingTextBox);
		this.billingTextBox.sendKeys(text);
			//Thread.sleep(4 * 1000);
		this.billingTextBox.sendKeys(Keys.ENTER);
		waitForElementToDisappear("//ul[@class='select2-results']/li[text()='Searching...']");
		Thread.sleep(2*1000);
		this.billingTextBox.sendKeys(Keys.ENTER);
		System.out.println(new Date().toString() +" Selected item");
	}
	
	public void typesearchBox(String text) {
		wait(this.searchBox);
		this.searchBox.click();
			//Thread.sleep(4 * 1000);
		this.searchBox.sendKeys(text);
		this.searchBox.sendKeys(Keys.ENTER);
		System.out.println(text);
	}
	
	public void clickfirstcampaignelement() {
		wait(this.firstcampaignelement);
		this.firstcampaignelement.click();
				
	}
	
public void clickEdit() {
		
		wait(this.edit);
		this.edit.click();
	
	}
	
	public void changekIgnoreLTCButton() {
		wait(this.ignoreLTCButton);
		this.ignoreLTCButton.click();
	}

	public void typeChannel(String text) {
		wait(this.channels);
		this.channels.sendKeys(text);
		try{
			Thread.sleep(1*1000);
		}
		catch(Exception e ){
			e.printStackTrace();
		}
		this.channels.sendKeys(Keys.ENTER);
	}

	public void clickSaveButton() {
		wait(this.saveButton);
		this.saveButton.click();
	}

	public void clickDeleteButton() {
		wait(this.deleteButton);
		this.deleteButton.click();
	}
	
	public void clickTouchPointCustomButton(){
		wait(this.touchPointCustomButton);
		this.touchPointCustomButton.click();
	}
	
	public void clickPickNetworkButton(){
		wait(this.pickNetworkButton);
		this.pickNetworkButton.click();
	}
	
	public void clickPickNetworkButton1(){
		wait(this.pickNetworkButton1);
		this.pickNetworkButton1.click();
	}
	public void clickpickclusterButton(){
		wait(this.pickclusterButton);
		this.pickclusterButton.click();
	}
	
	
	public void clickSelectAttributeButton(){
		wait(this.selectAttributeButton);
		this.selectAttributeButton.click();
	}
	
	
	public void typeNetworkInput(String text) throws InterruptedException {
		wait(this.networkInput);
		this.networkInput.sendKeys(text);
		Thread.sleep(2*1000);
		this.networkInput.sendKeys(Keys.ENTER);
		
}
	
	public void typeAttributeInput(String text) throws InterruptedException {
		wait(this.attributeInput);
		this.attributeInput.sendKeys(text);
		Thread.sleep(2*1000);
		this.attributeInput.sendKeys(Keys.ENTER);
		
		
}

}
