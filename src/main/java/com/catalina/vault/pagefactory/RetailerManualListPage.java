package com.catalina.vault.pagefactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RetailerManualListPage extends BaseClass {

	WebDriver driver;

	@FindBy(xpath = "//*[@id=\'targeting_triggers\']/div/div/div[2]/div/div[3]/a[1]")
	WebElement addCriteria;

	@FindBy(xpath = "//a[span[text()='Select Type']]")
	WebElement selectTypeButton;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement selectTypeInput;

	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerPurchaseEvent' and (not(contains(@style,'display: none;')))]/div[@class='select2-container trigger_purchase_measure']/a[span[text()='(measure)']]")
	WebElement measureButton;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement selectMeasureInput;
	
	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerPointOfSaleCouponEvent' and (not(contains(@style,'display: none;')))]/div/a[span[text()='(coupon type)']]")
	WebElement selectcoupontype;
	
	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement Inputcoupontype;
	
	@FindBy(xpath = "//section[@rv-show='trigger.type | eq TriggerPurchaseEvent']/span[@rv-show='trigger.purchase_measure']/div[@class='select2-container trigger_value_op']/a[span[text()='(condition)']]")
	WebElement conditionButton;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement selectConditionInput;

	@FindBy(xpath = "(//*[@id='trigger-type_1']/section[1]/span[2]/span[1]/input[2])[2]")
	WebElement triggerConditionInput;
	
	@FindBy(xpath = "//*[@id='coupon_code']")
	WebElement coupontypetextbox;
	
	@FindBy(xpath = "(//a[span[text()='(product attribute)']])[4]")
	WebElement productAttributeButton;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement selectProductAttributeInput;

	@FindBy(xpath = "(//a[span[text()='(list source)']])[5]")
	WebElement listSourceButton;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement selectListSourceInput;

	@FindBy(xpath = "(//*[@id='target_list'])[5]")
	WebElement targetListInput;

	@FindBy(xpath = "(//a[span[text()='(trigger scope)']])[2]")
	WebElement triggerScopeButon;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement inputTriggerScopeTextBox;

	@FindBy(xpath = "(//a[span[text()='(time period)']])[2]")
	WebElement timePeriodButton;

	@FindBy(xpath = "//div[contains(@style,'top:' ) or (contains(@style,'display: block; top: '))]/div/input[@tabindex='-1']")
	WebElement timePeriodInput;

	@FindBy(xpath = "(//*[@id='schedule'])[25]")
	WebElement fromDate;

	@FindBy(xpath = "(//span[@rv-show='trigger.time_window | eq Fixed' and (not(contains(@style,'display: none;')))]/button[@id='schedule' and @rv-calendar='trigger.stop_date'])[2]")
	WebElement toDate;

	@FindBy(xpath = "//td[@class='available']")
	List<WebElement> dateAvailable;

	@FindBy(xpath = "//td[@class='weekend available']")
	List<WebElement> dateWeekendAvailable;

	public RetailerManualListPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void clickOnAddCriteria() {
		wait(this.addCriteria);
		this.addCriteria.click();
	}

	public void clickOnSelectTypeButton() {
		wait(this.selectTypeButton);
		this.selectTypeButton.click();
	}

	public void typeSelectTypeInput(String text) {
		wait(this.selectTypeInput);
		this.selectTypeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectTypeInput.sendKeys(Keys.ENTER);
	}

	public void clickOnMeasureButton() {
		wait(this.measureButton);
		this.measureButton.click();
	}

	public void typeMeasureInput(String text) {
		wait(this.selectMeasureInput);
		this.selectMeasureInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectMeasureInput.sendKeys(Keys.ENTER);
	}
	
	public void clickselectcoupontype() {
		wait(this.selectcoupontype);
		this.selectcoupontype.click();
	}
	
	public void typeInputcoupontype(String text) {
		wait(this.Inputcoupontype);
		this.Inputcoupontype.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.Inputcoupontype.sendKeys(Keys.ENTER);
	}

	public void clickOnConditionButton() {
		wait(this.conditionButton);
		this.conditionButton.click();
	}

	public void typeConditionInput(String text) {
		wait(this.selectConditionInput);
		this.selectConditionInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectConditionInput.sendKeys(Keys.ENTER);
	}
	public void typecoupontypetextbox(String text) {
		wait(this.coupontypetextbox);
		this.coupontypetextbox.click();
		this.coupontypetextbox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.coupontypetextbox.sendKeys(Keys.ENTER);
	}
	
	public void typeTriggerConditionInput(String text) {
		wait(this.triggerConditionInput);
		this.triggerConditionInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.triggerConditionInput.sendKeys(Keys.ENTER);
	}

	public void clickOnProductAttributeButton() {
		wait(this.productAttributeButton);
		this.productAttributeButton.click();
	}

	public void typeProductAttributeInput(String text) {
		wait(this.selectProductAttributeInput);
		this.selectProductAttributeInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectProductAttributeInput.sendKeys(Keys.ENTER);
	}

	public void clickOnListSourceButton() {
		wait(this.listSourceButton);
		this.listSourceButton.click();
	}

	public void typeListSourceInput(String text) {
		wait(this.selectListSourceInput);
		this.selectListSourceInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.selectListSourceInput.sendKeys(Keys.ENTER);
	}

	public void typeTargetListInput(String text) {
		wait(this.targetListInput);
		this.targetListInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.targetListInput.sendKeys(Keys.ENTER);
	}

	public void clickOnTriggerScopeButton() {
		wait(this.triggerScopeButon);
		this.triggerScopeButon.click();
	}

	public void typeTriggerScopeInput(String text) {
		wait(this.inputTriggerScopeTextBox);
		this.inputTriggerScopeTextBox.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.inputTriggerScopeTextBox.sendKeys(Keys.ENTER);
	}

	public void clickTimePeriodButton() {
		wait(this.timePeriodButton);
		this.timePeriodButton.click();
	}

	public void typeTimePeriodInput(String text) {
		wait(this.timePeriodInput);
		this.timePeriodInput.sendKeys(text);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.timePeriodInput.sendKeys(Keys.ENTER);
	}

	DateFormat dateFormat = new SimpleDateFormat("dd");
	Date date = new Date();
	String currentDate = dateFormat.format(date);
	int currDate = Integer.parseInt(currentDate);

	int startDate = currDate -1;
	int stopDate = currDate + 11;

	public void fromDateFormatter() {

		this.fromDate.click();
		List<WebElement> dateAvailableList = dateAvailable;
		for (int i = 0; i < dateAvailableList.size(); i++) {
			String str = dateAvailableList.get(i).getText();
			if (str.equalsIgnoreCase("")) {
				continue;
			} else {
				int temp = Integer.parseInt(str);
				if (temp == startDate) {
					dateAvailableList.get(i).click();
				}
			}
		}
		List<WebElement> dateWeekendAvailableList = dateWeekendAvailable;
		for (int i = 0; i < dateWeekendAvailableList.size(); i++) {
			String str1 = dateWeekendAvailableList.get(i).getText();
			if (str1.equalsIgnoreCase("")) {
				continue;
			} else {
				int temp1 = Integer.parseInt(str1);
				if (temp1 == startDate) {
					dateWeekendAvailableList.get(i).click();
				}
			}
		}
		
	}

	public void toDateFormatter() throws InterruptedException {
		Thread.sleep(5*1000);
		wait(this.toDate);
		this.toDate.click();
		Thread.sleep(3*1000);
		List<WebElement> dateAvailableList = dateAvailable;
		for (int i = 0; i < dateAvailableList.size(); i++) {
			String str = dateAvailableList.get(i).getText();
			
			if (str.equalsIgnoreCase("")) {
				continue;
			} else {
				int temp = Integer.parseInt(str);
				if (temp == stopDate) {
					dateAvailableList.get(i).click();
				}
			}
		}
		List<WebElement> dateWeekendAvailableList = dateWeekendAvailable;
		for (int i = 0; i < dateWeekendAvailableList.size(); i++) {
			String str1 = dateWeekendAvailableList.get(i).getText();
			if (str1.equalsIgnoreCase("")) {
				continue;
			} else {
				int temp1 = Integer.parseInt(str1);
				if (temp1 == stopDate) {
					dateWeekendAvailableList.get(i).click();
				}
			}
		}
	}

}
