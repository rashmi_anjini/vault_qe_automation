package com.catalina.vault.pagefactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AudiencePage extends BaseClass{
	
	WebDriver driver;
	
	@FindBy(xpath="//*[@id='content']/div[1]/div[2]/div/a")
	WebElement newAudienceButton;
	
	@FindBy(xpath="//*[@id=\"content\"]/div/div[@class='options']/div/button")
	WebElement newPINsButton;
	
	@FindBy(xpath="//a[contains(text(),'Pre-defined Static PINs')]")
	WebElement PredefinedStaticPINs;
	
	@FindBy(xpath="//div/label[contains(text(),'Examples')]/following-sibling::div/div/ul/li/input")
	//"//div/label[contains(text(),'Examples')]/following-sibling::div/div/ul")
	WebElement examplesTextBox;
	
	@FindBy(xpath="//a[contains(text(),'Files')]")
	WebElement filesTab;
	
	@FindBy(id="table_search_txt")
	WebElement searchBox;
	
	@FindBy(id="table_search_btn")
	WebElement searchButton;
	
	@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div[8]/ul/li[4]/a")
	WebElement nextPageButton;
	
	@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div[4]/button")
	WebElement editButton;
	
	@FindBy(xpath="//*[@id='content']/div[2]/div/div[1]/div[4]/ul/li[10]/a")
	WebElement deleteButton;
	
	
	
	public AudiencePage(WebDriver driver){
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickNewAudienceButton(){
		wait(this.newAudienceButton);
		this.newAudienceButton.click();
	}
	
	
	public void clickNewPINsButton(){
		wait(this.newPINsButton);
		this.newPINsButton.click();
	}
	
	public void clickExamplesTextBox(String text) throws InterruptedException{
		wait(this.examplesTextBox);
		this.examplesTextBox.click();
		this.examplesTextBox.sendKeys(text);
		Thread.sleep(3*1000);
	this.examplesTextBox.sendKeys(Keys.ENTER);
	}
	public void clickPredefinedStaticPINs(){
		wait(this.PredefinedStaticPINs);
		this.PredefinedStaticPINs.click();
	}
	
	public void typeSearchBox(String text){
		wait(this.searchBox);
		this.searchBox.sendKeys(text);
	}
	
	public void clickSearchBox(){
		wait(this.searchButton);
		this.searchButton.click();
	}
	
	public void clickNextPageButton(){
		wait(this.nextPageButton);
		this.nextPageButton.click();
	}
	
	public void clickEditButton(){
		wait(this.editButton);
		this.editButton.click();
	}
	public void clickonFilesTab(){
		wait(this.filesTab);
		this.filesTab.click();
	}
	
	public void clickDeleteButton(){
		wait(this.deleteButton);
		this.deleteButton.click();
	}

}
