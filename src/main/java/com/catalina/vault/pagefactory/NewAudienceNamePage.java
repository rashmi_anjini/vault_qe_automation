package com.catalina.vault.pagefactory;

import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.catalina.vault.steps.SharedResource;
import com.catalina.vault.utils.PropertiesFile;

public class NewAudienceNamePage extends BaseClass {

	WebDriver driver;
	//private BaseClass audienceList;
	static String environmentPropertiesFilePath = System.getProperty("user.dir") + "\\prp\\environment.properties";
	private static String audienceSegementName;
	private  static PropertiesFile propFile;
	String timeStamp = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss").format(new Date());
	 SharedResource sharedResources;

	public static String getValueFromPropertiesFile(String path, String value) throws IOException {
		FileReader reader = new FileReader(path);
		Properties properties = new Properties();
		properties.load(reader);
		return properties.getProperty(value);
		
	}

	@FindBy(xpath = "//div/input[@rv-value='view.audience.name']")
	WebElement nameTextBox;

	@FindBy(xpath = "//*[@id=\"content\"]/div[2]/div/div[2]/div/a")
	WebElement saveChangesButton;

	@FindBy(xpath = "//*[@id=\"content\"]/div[2]/div/ul/li[2]/a")
	WebElement staticlists;

	
	@FindBy(xpath="//div/input[@rv-value='view.pin.name']")
	WebElement nameTextBoxPINs;
	
	// @FindBy(xpath = "//*[@id=\"audience_files\"]/div/div/a")
	@FindBy(xpath = "//*[@id='audience_files']/div/div/a/div/input")
	WebElement ImportNewFileButton;

	@FindBy(xpath = "//*[@id=\\\\\\\"audience_files\\\\\\\"]/div/div/a/div/input")
	WebElement uploadElement;

	@FindBy(xpath = "//*[@id=\"content\"]/div[1]/div/div/a")
	WebElement saveChangeButtonAfterImport;

	
	@FindBy(xpath = "//td[text()='PROCESSING']")
	WebElement checkstatus;
	

	@FindBy(xpath = "//*[@id='content']/div[2]/div/div[1]/div[4]/button")
	WebElement editButton;

	@FindBy(xpath = "//*[@id='content']/div[2]/div/div[1]/div[4]/ul/li[10]/a")
	WebElement deleteButton;

	@FindBy(xpath = "//form[@rv-on-submit='table.query']/input[@type='text' and @placeholder='Search...']")
	WebElement searchBox;

	@FindBy(xpath = "//span[@rv-hide='table.hide_edit_links']")
	WebElement audienceLink;

	@FindBy(xpath = "//tr[@rv-show='spec.type | eq File']/td[contains(text(),'COMPLETE')]")
	WebElement status;
	
	@FindBy(xpath="//a[contains(text(),'Save Changes')]")
	WebElement saveChangesButtonPin;

	public NewAudienceNamePage(WebDriver driver) {
		super(driver);
		//this.audienceList = audienceList;
		PageFactory.initElements(driver, this);
		this.sharedResources=new SharedResource();
		this.propFile=new PropertiesFile("ReRunScenarioNames");
}

	public void enterAudienceListName(String Country) throws IOException {
		wait(this.nameTextBox);
		this.nameTextBox.clear();
		String audienceListName = com.catalina.vault.utils.FileUtils.getAudienceFile(Country);
		System.out.println(audienceListName);
		// String name = com.catalina.vault.utils.FileUtils.getAudienceFile(Country);
		this.nameTextBox.sendKeys(audienceListName);
		audienceSegementName = audienceListName;
		// HashMap<String ,String > al=new HashMap<>();
		// al.put(Country, audienceListName);
		String key="AUDIENCE_SEGMENT_"+com.catalina.vault.steps.SharedResource.GetCountry();
		propFile.SetProperty(key, audienceListName);
		this.sharedResources.setAudienceListName(audienceListName);
	}
	
	public void enterAudienceListLoyaltyRewards(String testCase) throws IOException {
		wait(this.nameTextBox);
		this.nameTextBox.clear();
		if (testCase.equalsIgnoreCase("ItemUnits")){
		String audienceListLoyaltyRewards =sharedResources.getItemunits();
		System.out.println(audienceListLoyaltyRewards);
		this.nameTextBox.sendKeys(audienceListLoyaltyRewards);
		String LoyaltyRewardName = audienceListLoyaltyRewards;
		String key="AUDIENCE_SEGMENT_Loyalty_ItemUnits"+com.catalina.vault.steps.SharedResource.GetCountry();
		propFile.SetProperty(key, LoyaltyRewardName);
		}
		else if(testCase.equalsIgnoreCase("ItemSpend")){
		String audienceListLoyaltyRewards =sharedResources.getItemSpend();
		System.out.println(audienceListLoyaltyRewards);
		this.nameTextBox.sendKeys(audienceListLoyaltyRewards);
		String LoyaltyRewardName = audienceListLoyaltyRewards;
		String key="AUDIENCE_SEGMENT_Loyalty_ItemSpend"+com.catalina.vault.steps.SharedResource.GetCountry();
		propFile.SetProperty(key, LoyaltyRewardName);
		}
		else{
			String audienceListLoyaltyRewards =sharedResources.getManualUpc();
			System.out.println(audienceListLoyaltyRewards);
			this.nameTextBox.sendKeys(audienceListLoyaltyRewards);
			String LoyaltyRewardName = audienceListLoyaltyRewards;
			String key="AUDIENCE_SEGMENT_Loyalty_ManualUPC"+com.catalina.vault.steps.SharedResource.GetCountry();
			propFile.SetProperty(key, LoyaltyRewardName);
			}
		
	}
	
	public void enterPINsListName(String Country) throws IOException {
		wait(this.nameTextBoxPINs);
		this.nameTextBoxPINs.clear();
		String pinsListName = com.catalina.vault.utils.FileUtils.getPINsFile(Country);
		System.out.println(pinsListName);
		// String name = com.catalina.vault.utils.FileUtils.getAudienceFile(Country);
		this.nameTextBoxPINs.sendKeys(pinsListName);
		pinsListName = pinsListName;
		// HashMap<String ,String > al=new HashMap<>();
		// al.put(Country, audienceListName);
		String key="PINs_"+com.catalina.vault.steps.SharedResource.GetCountry();
		propFile.SetProperty(key, pinsListName);
		this.sharedResources.setPinListName(pinsListName);
	}


	public static String getaudienceSegementName(String country) {
		String name = com.catalina.vault.steps.SharedResource.getAudienceListName();
		audienceSegementName = name;
		return audienceSegementName;
	}
	

//	public void getAudience(String country) throws IOException {
//		String name = com.catalina.vault.utils.FileUtils.getAudienceFile(country);
//		audienceSegementName = name;
//	}

	public void clicksaveChangesButton() {
		wait(this.saveChangesButton);
		this.saveChangesButton.click();
	}

	public void clickstaticlistsTab() {
		wait(this.staticlists);
		this.staticlists.click();
	}

	public void clicksaveChangeButtonAfterImportButton() {
		wait(this.saveChangeButtonAfterImport);
		this.saveChangeButtonAfterImport.click();
	}

	public void clickImportNewFileButton(String file) throws IOException, Exception {
		//wait(this.ImportNewFileButton);
		this.ImportNewFileButton.sendKeys(file);
		//Thread.sleep(2000); // driver Waiting for 2000 ms

	}

	// here need to write the java code for file upload from windows...

	public void clickDeleteButton() {
		this.deleteButton.click();
	}
	
	
	public void checkProcessingstatus() {
		wait(this.checkstatus);
		System.out.println("Status:"+this.checkstatus.getText());
		Assert.assertEquals("PROCESSING", this.checkstatus.getText());
			}
	
	
	public void clicksaveChangesButtonPins() {
		wait(this.saveChangesButtonPin);
		this.saveChangesButtonPin.click();
	}

	public void SearchAudience(String country) {

		System.out.println("List NAme " + getaudienceSegementName(country));
		this.searchBox.click();
		this.searchBox.sendKeys(getaudienceSegementName(country));
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.searchBox.sendKeys(Keys.ENTER);
	}

	public void clickOnAudienceLink() {
		wait(this.audienceLink);
		this.audienceLink.click();
	}

	public void checkStatus() {
		System.out.println("Checking Status");
		Assert.assertEquals("COMPLETE", status.getText());

	}

}
