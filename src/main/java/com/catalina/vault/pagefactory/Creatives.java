package com.catalina.vault.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Creatives {

	WebDriver driver;
	
	@FindBy(xpath="//*[@id='creative_placements']/div/div[1]/div/div[1]/div[1]/a")
	WebElement newPlacement;
	
	@FindBy(xpath="//*[@id='creative_placements']/div/div[1]/div/div[1]/div[3]/a")
	WebElement importButton;
	
	@FindBy(xpath="//*[@id='content']/div[2]/div/ul/li[1]/a")
	WebElement placements;
	
	@FindBy(xpath="//*[@id='content']/div[2]/div/ul/li[2]/a")
	WebElement templates;
	
	
	@FindBy(xpath="//*[@id='creative_placements']/div/div[1]/div/div[1]/div[7]/div/form/input")
	WebElement searchBox;
	
	@FindBy(xpath="//*[@id='creative_placements']/div/div[1]/div/div[1]/div[7]/div/form/a")
	WebElement searchButton;
	
	@FindBy(xpath="//*[@id='creative_placements']/div/div[1]/div/div[1]/div[9]/ul/li[4]/a")
	WebElement nextPage;
	
	
	public Creatives(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickNewPlacement(){
		this.newPlacement.click();
	}
	
	public void typeSearchBox(String text){
		this.searchBox.sendKeys(text);
	}
	
	public void clickSearchButton(){
		this.searchButton.click();
	}
	
	public void clickImportButton(){
		this.importButton.click();
	}
	
	
	public void clickPlacements(){
		this.placements.click();
	}
	
	public void clickTemplates(){
		this.templates.click();
	}
	
	public void clickNextPage(){
		this.nextPage.click();
	}
	
	
}
