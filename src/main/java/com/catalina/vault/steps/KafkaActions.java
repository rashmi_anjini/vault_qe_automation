package com.catalina.vault.steps;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;


import org.apache.logging.log4j.LogManager;

import com.catalina.vault.intergration.MessageSender;
import com.catalina.vault.utils.PropertiesFile;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class KafkaActions {

	private static final Logger Log= LogManager.getLogger(KafkaActions.class);
	private final SharedResource sharedResource;
	private static PropertiesFile propertiesFile;
	private static PropertiesFile propFile;
	

	public KafkaActions(SharedResource sharedResource) {
		Log.info("Initializing Resources");
		this.sharedResource = sharedResource;
	}
	
	@Then("^: Send data to Kafka Queue \"([^\"]*)\"$")
	public void send_data_to_Kafka_Queue(String testcaseid) throws Throwable {
		sharedResource.TestCaseId = Integer.parseInt(testcaseid);
		// Write code here that turns the phrase above into concrete actions
		MessageSender sender = new MessageSender();
		//String strAmt = String.valueOf(Integer.parseInt(amt)*100);
		sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
		if(sharedResource.GetCountry().equalsIgnoreCase("USA")){
		sender.sendDatastore(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
			Thread.sleep(2 * 1000);
	}
		//sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
		//sender.sendDatastore(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
	}

	
	@Then("^: Send data to Kafka Queue with spendupc and unitsupc \"([^\"]*)\"$")
	public void send_data_to_Kafka_Queue_spendupc_unitsupc_amt(String testcaseid) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		MessageSender sender = new MessageSender();
		String Spendupc="";
		sharedResource.AddScenarioSpecificMap("SpendUPC",Spendupc);
		String Unitsupc="";
		sharedResource.AddScenarioSpecificMap("UnitsUPC",Unitsupc);
		String SpendAmt="";
		sharedResource.AddScenarioSpecificMap("SpendAmt",SpendAmt);
		DateFormat dateFormat = new SimpleDateFormat("ddMMyyHHm");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		String today=dateFormat.format(date);
		String jUPC=today;
		sharedResource.AddScenarioSpecificMap("jUPC",jUPC);
		//String strAmt = String.valueOf(Integer.parseInt(amt)*100);
		sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
		Thread.sleep(2 * 1000);
		//sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
	}
	
	@Then("^: Send data to Kafka Queue betweenQty \"([^\"]*)\"$")
	public void send_data_to_Kafka_Queue_upc_betweenqty_amt(String testcaseid) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		MessageSender sender = new MessageSender();
		String betweenQty="";
		sharedResource.AddScenarioSpecificMap("betweenQty",betweenQty);
		System.out.println("betweenQty"+betweenQty);
		//String strAmt = String.valueOf(Integer.parseInt(amt)*100);
		sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
		Thread.sleep(2 * 1000);
		//sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
	}
	
	@Then("^: Send data to Kafka Queue barcode \"([^\"]*)\"$")
	public void send_data_to_Kafka_Queue_barcode_qty_amt(String testcaseid) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		MessageSender sender = new MessageSender();
		String barcode=com.catalina.vault.steps.SharedResource.getbarcode();
		sharedResource.AddScenarioSpecificMap("barcode",barcode);
		System.out.println("barcode"+barcode);
		//String strAmt = String.valueOf(Integer.parseInt(amt)*100);
		sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
		Thread.sleep(2 * 1000);
		//sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
	}
	
	@Then("^: Send data to Kafka Queue with min and max amt for dept \"([^\"]*)\"$")
	public void send_data_to_Kafka_Queue_min_max_amt(String testcaseid) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		MessageSender sender = new MessageSender();
		String minAmt = "50";
		sharedResource.AddScenarioSpecificMap("Min",minAmt);
		String maxAmt = "100";
		sharedResource.AddScenarioSpecificMap("Max",maxAmt);
		String deptid=com.catalina.vault.steps.SharedResource.getdepartmentid();
		sharedResource.AddScenarioSpecificMap("Deptid",deptid);
		 DateFormat dateFormat = new SimpleDateFormat("ddMMyyHHm");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			String today=dateFormat.format(date);
			String junkUpc=today;
			sharedResource.AddScenarioSpecificMap("jUPC",junkUpc);
			String betweenQty="";
			sharedResource.AddScenarioSpecificMap("betweenQty",betweenQty);
			System.out.println("betweenQty "+betweenQty);
		sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
		Thread.sleep(2 * 1000);
		//sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
	}
	
	@Then("^: Send data to Kafka Queue with cid min and max amt for dept \"([^\"]*)\"$")
	public void send_data_to_Kafka_Queue_cid_min_max_amt(String testcaseid) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		MessageSender sender = new MessageSender();
		String minAmt = "50";
		sharedResource.AddScenarioSpecificMap("Min",minAmt);
		String maxAmt = "100";
		sharedResource.AddScenarioSpecificMap("Max",maxAmt);
		String deptid=com.catalina.vault.steps.SharedResource.getdepartmentid();
		sharedResource.AddScenarioSpecificMap("Deptid",deptid);
		 DateFormat dateFormat = new SimpleDateFormat("ddMMyyHHm");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			String today=dateFormat.format(date);
			String junkUpc=today;
			sharedResource.AddScenarioSpecificMap("jUPC",junkUpc);
		
			this.propertiesFile = new PropertiesFile("environment");
			this.propFile=new PropertiesFile("ReRunScenarioNames");
			String cidPath = propertiesFile.getProperty("sharedPath");
			
			String audienceListNamePropFile="AUDIENCE_SEGMENT_"+com.catalina.vault.steps.SharedResource.GetCountry();
			System.out.println("sudience name"+audienceListNamePropFile);
			String audienceListName= propFile.getProperty(audienceListNamePropFile);
			
			System.out.println("get path for list: "+cidPath+audienceListName);
			
			List<String> lines = Files.readAllLines(Paths.get(cidPath+audienceListName+".txt"), StandardCharsets.UTF_8);
			
			int noOflines=lines.size();
			System.out.println("lines: "+noOflines);
			//String Cid = lines.get(0);
			String Cid = lines.get(0).substring(10, 19);
			System.out.println("CID : " + Cid);
			sharedResource.AddScenarioSpecificMap("CID",Cid);
			
		sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
		Thread.sleep(2 * 1000);
		//sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
	}
	
	@Then("^: Send data to Kafka Queue with cid \"([^\"]*)\"$")
	public void send_data_to_Kafka_Queue_with_cid(String testcaseid) throws Throwable {
		
		this.propertiesFile = new PropertiesFile("environment");
		this.propFile=new PropertiesFile("ReRunScenarioNames");
		String cidPath = propertiesFile.getProperty("sharedPath");
		
		String audienceListNamePropFile="AUDIENCE_SEGMENT_"+com.catalina.vault.steps.SharedResource.GetCountry();
		System.out.println("audience name"+audienceListNamePropFile);
		String audienceListName= propFile.getProperty(audienceListNamePropFile);
		
		System.out.println("get path for list: "+cidPath+audienceListName);
		
		List<String> lines = Files.readAllLines(Paths.get(cidPath+audienceListName+".txt"), StandardCharsets.UTF_8);
		
		int noOflines=lines.size()-2;
		System.out.println("lines: "+noOflines);
		String Cid = lines.get(0).substring(9, 19);


		System.out.println("CID : " + Cid);
		sharedResource.AddScenarioSpecificMap("CID",Cid);
		// Write code here that turns the phrase above into concrete actions
		MessageSender sender = new MessageSender();
		
		sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
	
	}
	@Then("^: Send data to Kafka Queue betweenAmt \"([^\"]*)\"$")
	public void send_data_to_Kafka_Queue_upc_betweenAmt(String testcaseid) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		MessageSender sender = new MessageSender();
		String betweenAmt="";
		sharedResource.AddScenarioSpecificMap("betweenAmt",betweenAmt);
		System.out.println("betweenAmt"+betweenAmt);
		//String strAmt = String.valueOf(Integer.parseInt(amt)*100);
		sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
		Thread.sleep(2 * 1000);
		//sender.sendData(sharedResource.getData(this.sharedResource.GetPromotionName(),this.sharedResource.getPromotionId(),testcaseid));
	}
	
	
}
