package com.catalina.vault.steps;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assert;

import com.catalina.vault.jsoncomparisondemo.Main;
import com.catalina.vault.utils.Country;
import com.catalina.vault.utils.FileUtils;
import com.catalina.vault.utils.JsonUtils;
import com.catalina.vault.utils.NetworkUtils;
import com.catalina.vault.utils.PropertiesFile;
import com.cucumber.listener.Reporter;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import cucumber.api.java.en.Then;

public class PausingPromotion {

	private static final Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static String DMP_PROPERTY = "DMP";
	private static String STORE_PROPERTY = "STORE_PAYLOAD";
	private static String COUNTRY_PARAMETER = "ctry";
	private static String COUNTRY_JSON_PROPERTY = "ctry";
	private static String CHAIN_JSON_PROPERTY = "chn";
	private static String MOST_RECENT_MSG_JSON_PROPERTY = "most_recent_msg";
	private PropertiesFile propertiesFile;
	private static SharedResource sharedResource;
	public static String jsonResponse ="";

	public PausingPromotion(SharedResource sharedResource) {
		LOGGER.log(Level.INFO, "Initializing Resources");
		this.propertiesFile = new PropertiesFile("PrintedPromotions");
		this.sharedResource = sharedResource;
		
	}

	@Then("^: Verify Promotion paused for country \\\"([^\\\"]*)\\\" and number of promotion ids \\\"([^\\\"]*)\\\"$")
	public  void pausePrintedPromotions(String country,String noOfPromotionId) throws Throwable {
		
		int noOfPromotionIds=Integer.parseInt(noOfPromotionId);
		String url = this.propertiesFile.getProperty("pauseEndpoint")+country+"-BLIP-";
		System.out.println("Print url: " +url);
		for(int i=1;i<=noOfPromotionIds;i++)
		{
		
		String finalHit=url+propertiesFile.getProperty("BL"+ String.valueOf(i));
		System.out.println(finalHit);
		String response = NetworkUtils.putCall(finalHit,"");
		JsonElement element = JsonUtils.convertToJsonElement(response);
    	String status = ((JsonObject) element).get("status").getAsString();
		System.out.println(status);
		Reporter.addStepLog("Getting Status for promotion " 
				+ sharedResource.getPromotionId() + " is " + status);
				
		}
			
		
		
	}	

}
