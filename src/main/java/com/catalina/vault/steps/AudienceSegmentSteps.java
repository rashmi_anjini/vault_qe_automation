package com.catalina.vault.steps;

import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.catalina.vault.pagefactory.AudiencePage;
import com.catalina.vault.pagefactory.BaseClass;
import com.catalina.vault.pagefactory.CampaignPage;
import com.catalina.vault.pagefactory.DashBoardPage;
import com.catalina.vault.pagefactory.MainPage;
import com.catalina.vault.pagefactory.NewAudienceNamePage;
import com.catalina.vault.utils.PropertiesFile;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AudienceSegmentSteps  {

	WebDriver driver;
	AudiencePage audiencepage;
	PropertiesFile pFile;
	SharedResource shareResource;
	DashBoardPage dashboard;
	CampaignPage campaignPage;
	NewAudienceNamePage newAudienceNamePage;
	public static String audienceList;
	String environmentPropertiesFilePath = System.getProperty("user.dir") + "\\prp\\environment.properties";
	String timeStamp = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss").format(new Date());
	
	public static String getValueFromPropertiesFile(String path, String value) throws IOException {
		
		FileReader reader = new FileReader(path);
		Properties properties = new Properties();
		properties.load(reader);
		return properties.getProperty(value);
	}
	
	public AudienceSegmentSteps(SharedResource shareResource) {
		// TODO Auto-generated constructor stub
		//super(this.driver);
		//this.audienceList = new AudienceList(this.driver);
		//Reporter.addStepLog("Creating a new AudienceSegmentPage");
		this.driver = shareResource.init();
		dashboard = new DashBoardPage(this.driver);
		audiencepage = new AudiencePage(this.driver);
		newAudienceNamePage = new NewAudienceNamePage(this.driver);
		this.pFile = new PropertiesFile("environment");

	}
	
	

	@When("^: Click on Audience")
	public void click_on_Audience() throws Throwable {
		MainPage page = new MainPage(this.driver);
		
		dashboard.clickAudiences();
		Reporter.addStepLog("Clicked on audiences");
		page.waitforLoading("Clicked on audiences");
	}
	
	@Then("^: Click on New PINs with \"([^\"]*)\" and click on Save changes$")
	public void click_on_New_PINs_with_and_click_on_Save_changes(String Country) throws Throwable {
		
		MainPage page = new MainPage(this.driver);
		
		page.waitforLoading("pins button");
		Reporter.addStepLog("Clicking on New PINs button");
		audiencepage.clickNewPINsButton();
		
		
		Reporter.addStepLog("Clicking on PredefinedStaticPINs");
		audiencepage.clickPredefinedStaticPINs();
		
		
		Reporter.addStepLog("Enter PINs name");
		newAudienceNamePage.enterPINsListName(Country);
		
		
		Reporter.addStepLog("Entering Example");
		audiencepage.clickExamplesTextBox("xxxxxxxxxx");
		
		
		Reporter.addStepLog("Clicking on Save Changes");
		newAudienceNamePage.clicksaveChangesButtonPins();	
		
		
	}

	
	@When("^: Click on PINS")
	public void click_on_PINS() throws Throwable {
		MainPage page = new MainPage(this.driver);
		
		dashboard.clickSharedList();
		Reporter.addStepLog("Clicked on shared lists");
		page.waitforLoading("click on shared list");
		
		Reporter.addStepLog("Clicking on PINS");
		dashboard.clickPINs();
		
	}
	
	@Then("^: Click on Import New PINs File and Upload file with counrty \"([^\"]*)\"$")
	public void click_on_Import_New_PINs_File_and_Upload_file_with_counrty(String country)
			throws Throwable {
		MainPage page = new MainPage(this.driver);
		
		audiencepage.clickonFilesTab();
		String pinlist = com.catalina.vault.utils.FileUtils.getPINsFile(country);
		System.out.println("upload file from: "+pFile.getProperty("sharedPath") + pinlist + ".txt");
		this.pFile = new PropertiesFile("environment");
		dashboard.clickImportNewFilePINs(pFile.getProperty("sharedPath") + pinlist + ".txt");
		Thread.sleep(3*1000);
		newAudienceNamePage.clicksaveChangeButtonAfterImportButton();
		page.waitforLoading("save after import");
		//newAudienceNamePage.checkProcessingstatus();
	}

	//
	static Random rand = new Random();
	static long endNumber = rand.nextInt(11);

	@Then("^: Click on New Audience with \"([^\"]*)\" and click on Save changes$")
	public void click_on_New_Audience_with_and_click_on_Save_changes(String Country) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		//Thread.sleep(2000);
		// dashboard.clickAudiences();
		MainPage page = new MainPage(this.driver);
		
		page.waitforLoading("Click on New audience button");
		audiencepage.clickNewAudienceButton();
		Reporter.addStepLog("Click on New audience button");
		
		newAudienceNamePage.enterAudienceListName(Country);
		Reporter.addStepLog("Enter AudienceListName");
		
		newAudienceNamePage.clicksaveChangesButton();
		Reporter.addStepLog("Clicked on save changes button");
		
		page.waitforLoading("Clicked on save changes button");
	
	}
	
	@Then("^: Click on New Audience loyaltyRewards \"([^\"]*)\" and click on Save changes$")
	public void click_on_New_Audience_loyaltyRewards(String TestCase) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		//Thread.sleep(2000);
		// dashboard.clickAudiences();
		MainPage page = new MainPage(this.driver);
		
		page.waitforLoading("Click on New audience button");
		audiencepage.clickNewAudienceButton();
		Reporter.addStepLog("Click on New audience button");
		
		newAudienceNamePage.enterAudienceListLoyaltyRewards(TestCase);
		Reporter.addStepLog("Enter AudienceListName");
		
		newAudienceNamePage.clicksaveChangesButton();
		Reporter.addStepLog("Clicked on save changes button");
		
		page.waitforLoading("Clicked on save changes button");
	
	}

	//// After click on Import file Windows will open and Import file is a
	//// button anyway i have tried with the same it does not work for me

	@Then("^: Click on Static List$")
	public void click_on_Static_List() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//Thread.sleep(2000);
		System.out.println("Click on static list");
		
		newAudienceNamePage.clickstaticlistsTab();
		Reporter.addStepLog("Click on static list");
	}

	public void file(String country) throws Throwable {
		String filepath = com.catalina.vault.steps.SharedResource.getBuildName()+ "_" + country + ".txt";
		System.out.println("filepath:" + filepath);
	}

	@Then("^: Click on Import New File and Upload file with counrty \"([^\"]*)\"$")
	public void click_on_Import_New_File_and_Upload_file_with_counrty(String country)
			throws Throwable {
		MainPage page = new MainPage(this.driver);
		audienceList = com.catalina.vault.steps.SharedResource.getAudienceListName();
		System.out.println("upload file from: "+pFile.getProperty("sharedPath") + audienceList + ".txt");
		this.pFile = new PropertiesFile("environment");
		newAudienceNamePage.clickImportNewFileButton(pFile.getProperty("sharedPath") + audienceList + ".txt");
		Thread.sleep(3*1000);
		newAudienceNamePage.clicksaveChangeButtonAfterImportButton();
		page.waitforLoading("clicksaveChangeButtonAfterImportButton");
		newAudienceNamePage.checkProcessingstatus();
	}
	
	@Then("^: Click on Import New File and Upload LoyaltyRewards file \"([^\"]*)\"$")
	public void click_on_Import_New_File_and_Upload_LoyaltyRewardsfile_with_counrty(String testCase)
			throws Throwable {
		MainPage page = new MainPage(this.driver);
		if(testCase.equalsIgnoreCase("ItemUnits")){
			audienceList = shareResource.getItemunits();
			System.out.println("upload file from: "+pFile.getProperty("sharedPath") + audienceList + ".txt");
		}
		else if(testCase.equalsIgnoreCase("ItemSpend")){
			audienceList = shareResource.getItemSpend();
			System.out.println("upload file from: "+pFile.getProperty("sharedPath") + audienceList + ".txt");
		}
		else{
			audienceList = shareResource.getManualUpc();
			System.out.println("upload file from: "+pFile.getProperty("sharedPath") + audienceList + ".txt");
		}
		this.pFile = new PropertiesFile("environment");
		newAudienceNamePage.clickImportNewFileButton(pFile.getProperty("sharedPath") + audienceList + ".txt");
		Thread.sleep(3*1000);
		newAudienceNamePage.clicksaveChangeButtonAfterImportButton();
		page.waitforLoading("clicksaveChangeButtonAfterImportButton");
		newAudienceNamePage.checkProcessingstatus();
	}

	@Then("^: Search for AudienceList with")
	public void search_for_AudienceList_with() throws Throwable {
		
		MainPage page = new MainPage(this.driver);
		page.waitforLoading("Search Audience list name");
		//newAudienceNamePage.getaudienceSegementName(country);
		Reporter.addStepLog("Search Audience list name");
		System.out.println("List name:"+com.catalina.vault.steps.SharedResource.getAudienceListName());
		newAudienceNamePage.SearchAudience(com.catalina.vault.steps.SharedResource.getAudienceListName());
		
		page.waitforLoading("Search Audience list name");
		newAudienceNamePage.clickOnAudienceLink();
	}

	@Then("^: Verify the Status of AudienceList$")
	public void verify_the_Status_of_AudienceList() throws Throwable {
		Thread.sleep(2000);
		newAudienceNamePage.checkStatus();
	}

	/*
	 * @Then("^: Import new File \"file name\"") public void
	 * logged_in_create_campaign() throws Throwable { dashboard.clickAudiences();
	 * newaudienceName.clickImportNewFileButton();
	 * 
	 * // Thread.sleep(2000); - page should load (wrong mechanism)// never use it -
	 * bad coding .. Implicit Wait -- Driver sleeps here until the time mentioned..
	 * whereas Explicit wait -- based on condition, it will wait for defined time if
	 * the condition is satisfied the driver will be in active state... and check
	 * for the next step to be executed...
	 * 
	 * 
	 * 
	 * }
	 */

}

// WebDriver driver = new FirefoxDriver();
// driver.get("http://somedomain/someurl");
// WebElement dynamicElement = (new WebDriverWait(driver,
// 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("dynamicElement")));
