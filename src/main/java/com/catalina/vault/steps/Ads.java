package com.catalina.vault.steps;

import java.nio.file.FileSystems;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.vault.entities.BLsToExcel;
import com.catalina.vault.entities.PerformanceTiming;
import com.catalina.vault.pagefactory.AdGallaryPage;
import com.catalina.vault.pagefactory.AdPage;
import com.catalina.vault.pagefactory.AdsListPage;
import com.catalina.vault.pagefactory.AdvertisementSettingPage;
import com.catalina.vault.pagefactory.DashBoardPage;
import com.catalina.vault.pagefactory.MainPage;
import com.catalina.vault.pagefactory.ManufacturerAdPage;
import com.catalina.vault.pagefactory.NewAudienceNamePage;
import com.catalina.vault.pagefactory.NewDisplayNetworkCampaignPage;
import com.catalina.vault.pagefactory.RetailerCouponPage;
import com.catalina.vault.pagefactory.TargetingPage;
import com.catalina.vault.utils.FileUtils;
import com.catalina.vault.utils.PropertiesFile;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Then;

public class Ads {

	WebDriver driver;
	AdsListPage adsListPage;
	AdPage adPage;
	SharedResource sharedResource;
	RetailerCouponPage couponPage;
	TargetingPage targetingPage;
	ManufacturerAdPage manufacturerAdPage;
	MainPage page;
	Targeting target;
	AdGallaryPage adGallerypage;
	FileUtils fileUtil;
	BLsToExcel bls;
	DashBoardPage dashboard;
	NewAudienceNamePage newAudienceNamePage;
	NewDisplayNetworkCampaignPage campaginForm;
	private static final Logger Log = LogManager.getLogger(Ads.class);
	private static PropertiesFile propertiesFile;

	public Ads(SharedResource sharedResourse) {
		this.driver = sharedResourse.init();
		adsListPage = new AdsListPage(this.driver);
		this.adPage = new AdPage(this.driver);
		this.adGallerypage= new AdGallaryPage(this.driver);
		this.couponPage = new RetailerCouponPage(this.driver);
		this.sharedResource = new SharedResource();
		this.manufacturerAdPage = new ManufacturerAdPage(this.driver);
		this.newAudienceNamePage=new NewAudienceNamePage(this.driver);
		this.dashboard=new DashBoardPage(this.driver);
		this.page = new MainPage(this.driver);
		this.target = new Targeting(sharedResourse);
		this.campaginForm= new NewDisplayNetworkCampaignPage(this.driver);
		this.bls=new BLsToExcel(this.driver);
		this.propertiesFile=new PropertiesFile("ReRunScenarioNames");
	}

	@Then("^: Create ad using Ad Galary with manufacturer$")
	public void create_ad_using_Ad_Galary_with_manufacturer() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		// Thread.sleep(2*1000);
		page.waitforLoading("Clicking on Ad Group");
		Thread.sleep(2 * 1000);
		adsListPage.clickAdGallery();
		// Thread.sleep(2*1000);
		page.waitforLoading("clickAdGallery");
		manufacturerAdPage.addManufacturerCouponTocart();
		System.out.println("Added to cart");
		manufacturerAdPage.clickOnNext();
		// Thread.sleep(2*1000);
		page.waitforLoading("next");
		Thread.sleep(2 * 1000);

	}
	
	@Then("^: Fill ad form for manufacturer for Manual LMC Product Carried \"([^\"]*)\" with \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_manufacturer_for_Manual_LMC_Product_Carried_with(String country,String image, int startdate, int endDate)
			throws Throwable {

		TargetingPage targetingpage = new TargetingPage(this.driver);

		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad form manufacturer");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		// Reporter.addStepLog("Selecting the image to add : image");

		/*
		 * couponPage.addImage(FileSystems.getDefault().getPath(image).
		 * toAbsolutePath().toString()); Thread.sleep(3 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(3 * 1000);
		 */

		Reporter.addStepLog("Adding Product Validation Group");
		manufacturerAdPage.clickAddProductGroupButton();

		Reporter.addStepLog("Clicking on Manual List");
		manufacturerAdPage.ClickOnManualListCheckBox();
		

		Reporter.addStepLog("Clicking on YES this is OK");
		couponPage.clickYesThisIsOkButton();
		System.out.println("clicked popo up");
		page.waitforLoading("clicked popo up");

//		Reporter.addStepLog("Enter the Manual UPC");
//		int manuallist = target.get_Manual_List();
		String manualListupc=this.bls.getCellData(country, 2, 3);
	System.out.println("manualListupc"+manualListupc);
		targetingpage.clickProductManualListeditbox(this.bls.getCellData(country, 2, 3));
//		Log.info("entered value for list  : from manual list" + String.valueOf(manuallist));
//		// upc is set to be written to excel
//		target.sharedResource.setPromotionUpc(String.valueOf(manuallist));
		//Thread.sleep(2*1000);
	//	targetingPage.list();
		Reporter.addStepLog("Entering rolling day 3");
		manufacturerAdPage.typeRollingDay();
				

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");
		
		
		

		

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(9* 1000);
		page.waitforLoading("clickSaveButton");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
		// page.waitforLoading();
	}


	@Then("^: Enter EORD Unlimited ad details name \"([^\"]*)\" image \"([^\"]*)\"$")
	public void fill_ad_form_Unlimited(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("Eord unlimited ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		couponPage.clickOnAdvertisedFlag();
		// Thread.sleep(2 * 1000);

		couponPage.clickOnUnlimited();

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("clickSaveButton");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("clickSaveButton");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();


		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeUSABarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		
		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Enter EORD Blind ad details name \"([^\"]*)\" image \"([^\"]*)\"$")
	public void fill_ad_form_Blind(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("eord blind ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Thread.sleep(2 * 1000);
		couponPage.clickOnAdvertisedFlag();

		Thread.sleep(2 * 1000);
		Reporter.addStepLog("Click on Blind");
		couponPage.clickOnBlind();
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();
		page.waitforLoading("clickSaveButton");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}
		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();
		page.waitforLoading("clickSaveButton");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeUSABarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Create ad$")
	public void create_ad() throws Throwable {
		Reporter.addStepLog("Creating a Ad");
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Clicking on new button");
		adsListPage.clickNewButton();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Selecting adgroup from the list");
		adsListPage.clickFirstAdGroupInList();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Coupon Butoon");
		adsListPage.clickCouponButton();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Selecting Promotion Type");
		adsListPage.clickCoupon_BuyN_SaveX_Dollors();

		Reporter.addStepLog("Click Frequency Caping");
		adPage.clickFrequencyCappingClickHereButton();

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		adPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : 99999");
		adPage.typeDistributedCappingTextBox("99999");

		Reporter.addStepLog("Clicking on Add Product Validation button");
		adPage.clickAddProductGroupButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for search : krish");
		adPage.typeProductValidationTextBox("Animalis");

		Reporter.addStepLog("Selecting the image to add : C:\\Vault UI Template\\Productshot1.png");
		adPage.addImage("C:\\Vault UI Template\\Productshot1.png");
		Thread.sleep(10 * 1000);

		//// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		Reporter.addStepLog("Clicking on save Image button");
		adPage.clickSaveImageButton();
		Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		adPage.typeQualiferLineInput("Save Save Save");

		Reporter.addStepLog("Entering save line Save 50%");
		adPage.clearsaveLineInput();
		adPage.typesaveLineInput("50%");

		Reporter.addStepLog("Entering static image : C:\\Vault UI Template\\Productshot1.png");
		adPage.addStaticImage("C:\\Vault UI Template\\Productshot1.png");

		Thread.sleep(10 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		adPage.clickSaveStaticImage();
		Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Save");
		adPage.clickSaveButton();

		Thread.sleep(10 * 1000);

		Reporter.addStepLog("Scrolling to Top");
		adPage.scrollToTop();

		//// ScreenShot.addScreenshotToScreen(this.driver, "Ad settings");
		adPage.clickTemplateOptionButton();

		Reporter.addStepLog("Try to select template. Waiting for 5 mins to show all templates");
		Thread.sleep(5 * 60 * 1000);

		//// ScreenShot.addScreenshotToScreen(this.driver, "AdTemplate");
		Reporter.addStepLog("Selecting coupon template");
		adPage.clickUSA_6in_Manufacturing_Coupon();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Selecting GS1 Coupon Template");
		adPage.clickGS1CouponTemplateButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Save Button");
		adPage.clickTemplateSaveButton();

		Thread.sleep(5 * 1000);

		//// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		adPage.clickRedemtionClearingButton();

		//// ScreenShot.addScreenshotToScreen(this.driver,
		//// "BeforeEnteringValuesToRedemtionClearing");
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : manufacture");
		adPage.typeClearingMethodTypeBox("manufacturer");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		adPage.clickBarCodeTypeButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type :GS1 Databar North American (with text");
		adPage.typeBarCodeTypeTextBox("GS1 Databar North American (with text)");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : (8110)1007192113889131001203103181231");
		adPage.typeBarCodeValue("(8110)1007192113889131001203103181231");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		adPage.clickEnableExpireDateButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		adPage.clickDateFormatButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		adPage.typeDateFormatTextBox("MM/DD/YY");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Calander");
		adPage.clickCalanderButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date option");
		adPage.clickDateOption();

		Reporter.addStepLog("Scrolling to top");
		adPage.scrollToTop();

		//// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		adPage.clickTargetingButton();

	}

	@Then("^: Fill ad form with CapsPerSession for USAretailer with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_CapsPerSession(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page cap per session");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Thread.sleep(2 * 1000);
		couponPage.clickOnAdvertisedFlag();
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("clickSaveButton");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("clickSaveButton");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeUSABarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Enter EORD Sensitive ad details name \"([^\"]*)\" image \"([^\"]*)\"$")
	public void fill_ad_form_Sensitive(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page sensitive ");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking AdvertisedFlag");
		couponPage.clickOnAdvertisedFlag();
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking sensitive");
		couponPage.clickOnSensitive();

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("clickSaveButton");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("clickSaveButton");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();
		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeUSABarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Enter Retailer ad details name \\\"([^\\\"]*)\\\" image \\\"([^\\\"]*)\\\" with rank \"([^\"]*)\"$")
	public void fill_ad_form_promotion_allocation(String barcode, String image, String rank) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page for retailer ");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Enter name");
		couponPage.clickadname("Rank_" + rank);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering Default rank");
		couponPage.typeDefaultrank(rank);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		// couponPage.clickFrequencyCappingClickHereButton();

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();
		page.waitforLoading("clickSaveButton");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}
		// Thread.sleep(5 * 1000);

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("clickSaveButton");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeUSABarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Go to created Ad Group$")
	public void click_on_created_adGroup() throws InterruptedException {

		Thread.sleep(2 * 1000);
		couponPage.clickOnCreatedAdGroup();
	}

	@Then("^: Fill ad form with frequency cap for USAretailer with \"([^\"]*)\" and image \"([^\"]*)\"$")
	public void fill_ad_form_frequency(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		couponPage.clickFrequencyCappingClickHereButton();

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();
		page.waitforLoading("clickSaveButton");

		// Thread.sleep(5 * 1000);

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();
		page.waitforLoading("clickSaveButton");
		// Thread.sleep(6* 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeUSABarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form NBIC for USAretailer with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_Nbic(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking AdvertisedFlag");
		couponPage.clickOnAdvertisedFlag();
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking clickOnNBIC");
		couponPage.clickOnNBIC();
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entrering typeNBICInput");
		couponPage.typeNBICInput("1");
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("clickSaveButton");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("clickSaveButton");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeUSABarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Enter EORD Competitve ad details name \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_Competitive(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Thread.sleep(2 * 1000);
		couponPage.clickOnAdvertisedFlag();
		// Thread.sleep(2 * 1000);

		couponPage.clickOnCompetitive();
		// Thread.sleep(2 * 1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("save button");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();
		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeUSABarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Create ad with retail $")
	public void create_ad_with_retail() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on new button");
		adsListPage.clickNewButton();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Selecting adgroup from the list");
		adsListPage.clickFirstAdGroupInList();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Coupon Butoon");
		adsListPage.clickCouponButton();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Selecting Promotion Type");
		adsListPage.clickCoupon_BuyN_SaveX_Dollors();

		Reporter.addStepLog("Click Frequency Caping");
		adPage.clickFrequencyCappingClickHereButton();

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		adPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : 99999");
		adPage.typeDistributedCappingTextBox("99999");

		Reporter.addStepLog("Clicking on Add Product Validation button");
		adPage.clickAddProductGroupButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for search : krish");
		adPage.typeProductValidationTextBox("krish");

		Reporter.addStepLog("Selecting the image to add : C:\\Vault UI Template\\Productshot1.png");
		adPage.addImage("C:\\Vault UI Template\\Productshot1.png");
		Thread.sleep(10 * 1000);

		//// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		Reporter.addStepLog("Clicking on save Image button");
		adPage.clickSaveImageButton();
		Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		adPage.typeQualiferLineInput("Save Save Save");

		Reporter.addStepLog("Entering save line Save 50%");
		adPage.clearsaveLineInput();
		adPage.typesaveLineInput("50%");

		Reporter.addStepLog("Entering static image : C:\\Vault UI Template\\Productshot1.png");
		adPage.addStaticImage("C:\\Vault UI Template\\Productshot1.png");

		Thread.sleep(10 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		adPage.clickSaveStaticImage();
		Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Save");
		adPage.clickSaveButton();

		Thread.sleep(10 * 1000);

		Reporter.addStepLog("Scrolling to Top");
		adPage.scrollToTop();

		//// ScreenShot.addScreenshotToScreen(this.driver, "Ad settings");
		adPage.clickTemplateOptionButton();

		Reporter.addStepLog("Try to select template. Waiting for 5 mins to show all templates");
		Thread.sleep(5 * 60 * 1000);

		//// ScreenShot.addScreenshotToScreen(this.driver, "AdTemplate");
		Reporter.addStepLog("Selecting coupon template");
		adPage.clickUSA_6in_Manufacturing_Coupon();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Selecting GS1 Coupon Template");
		adPage.clickGS1CouponTemplateButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Save Button");
		adPage.clickTemplateSaveButton();

		Thread.sleep(5 * 1000);

		//// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		adPage.clickRedemtionClearingButton();

		//// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		adPage.typeClearingMethodTypeBox("Retail");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		adPage.clickBarCodeTypeButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		adPage.typeBarCodeTypeTextBox("UPC-A");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111");
		adPage.typeStaticBarCodeValue("00000011111");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		adPage.clickEnableExpireDateButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		adPage.clickDateFormatButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		adPage.typeDateFormatTextBox("MM/DD/YY");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Calander");
		adPage.clickCalanderButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date option");
		adPage.clickDateOption();

		Reporter.addStepLog("Scrolling to top");
		adPage.scrollToTop();

		//// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		adPage.clickTargetingButton();

	}

	@Then("^: Create ad using Ad Galary with retail$")
	public void create_ad_using_Ad_Galary_with_retail() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		page.waitforLoading("Clicking on Ad Group");
		adsListPage.clickAdGallery();
		Log.info("Clicked on adgallery");
		page.waitforLoading("Clicked on adgallery");
		
		
		couponPage.addRetailCouponTocart();
		Log.info("Clicked on add retail coupon to cart");
		
		
		couponPage.clickOnNext();
		Log.info("Clicked on next button");
		page.waitforLoading("next button");
		Thread.sleep(5 * 1000);
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"ADGallery");
	}

	@Then("^: Click on adgroup tab to create new adgroup for same campaign$")
	public void Click_on_adgroup_tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on new button");
		adsListPage.clickadgrouptab();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Selecting adgroup from the list");
		adsListPage.clickFirstAdGroupInList();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Coupon Butoon");
		adsListPage.clickCouponButton();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Selecting Promotion Type");
		adsListPage.clickGenericCouponAd();

		Thread.sleep(8 * 1000);

		System.out.println(driver.getTitle());

		Thread.sleep(6 * 1000);
	}

	@Then("^: Create US ad using Ad Galary with retail$")
	public void create_US_ad_using_Ad_Galary_with_retail() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		page.waitforLoading("ad group");
		adsListPage.clickAdGallery();
		page.waitforLoading("ad gallery");
		couponPage.addUSRetailCouponTocart();
		couponPage.clickOnNext();

		page.waitforLoading("next");

		Thread.sleep(4 * 1000);
	}

	@Then("^: Create ad using Ad new with retail for UK$")
	public void create_ad_using_Ad_new_for_UK() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on new button");
		adsListPage.clickNewButton();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Selecting adgroup from the list");
		adsListPage.clickFirstAdGroupInList();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Coupon Butoon");
		adsListPage.clickCouponButton();
		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Selecting Promotion Type");
		adsListPage.clickGenericCouponAd();

		Thread.sleep(8 * 1000);

		System.out.println(driver.getTitle());

		Thread.sleep(6 * 1000);
	}

	@Then("^: Create ad using Ad Galary with retail for UK$")
	public void create_ad_using_Ad_Galary_for_UK() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		adsListPage.clickAdGallery();
		couponPage.addukCouponTocart();
		couponPage.clickOnNext();

		Thread.sleep(8 * 1000);

		for (String str : driver.getWindowHandles()) {
			driver.switchTo().window(str);
		}
	}

	@Then("^: Create ad using Ad Galary with retail for Italy$")
	public void create_ad_using_Ad_Galary_for_Italy() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		// Thread.sleep(2*1000);
		page.waitforLoading("Clicking on Ad Group");
		adsListPage.clickAdGallery();
		// Thread.sleep(2*1000);
		page.waitforLoading("click AdGallery");
		couponPage.addItalyCouponAddToCart();
		couponPage.clickOnNext();
		// Thread.sleep(2*1000);
		page.waitforLoading("clickOnNext");

		Thread.sleep(4 * 1000);
	}

	@Then("^: Fill ad form for retailer with Rolling Expirationdate \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_rolling_Expiration_date(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		// Thread.sleep(3*1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		// Reporter.addStepLog("Selecting the image to add : C:\\Vault UI
		// Template\\chocimage.jpg");
		// couponPage.addImage(image);
		// Thread.sleep(5 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		// Reporter.addStepLog("Clicking on save Image button");
		// couponPage.clickSaveImageButton();
		// Thread.sleep(5 * 1000);

		// if ((couponPage.QualiferLineInput())==true)
		// {
		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typequaliferLineInput5("Buy N Save");
		// }//
		 Reporter.addStepLog("Entering save line Save 50%");
		 couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image : C:\\Vault UI Template\\chocimage.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		//// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(1 * 1000);

		// Reporter.addStepLog("Clicking on Calander");
		// couponPage.clickCalanderButton();

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking rolling expiration radio button");
		couponPage.clickrollingexpirationradioButton();

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking rolling expiration select field");
		couponPage.clickselectbuttonrolling();

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("select rolling expiration ");
		couponPage.typeselectinputtextbox("Impression");
		//// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		
		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		//// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for retailer with Rolling Expirationdate dynamic barcode \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_rolling_Expiration_date_dyanamic_barcode(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		// Thread.sleep(3*1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		/*
		 * Reporter.addStepLog(
		 * "Selecting the image to add : C:\\Vault UI Template\\chocimage.jpg");
		 * couponPage.addImage(image); Thread.sleep(5 * 1000);
		 * 
		 * ////ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(5 * 1000);
		 */

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("save button");
		//// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(1 * 1000);

		// Reporter.addStepLog("Clicking on Calander");
		// couponPage.clickCalanderButton();

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking rolling expiration radio button");
		couponPage.clickrollingexpirationradioButton();

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking rolling expiration select field");
		couponPage.clickselectbuttonrolling();

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("select rolling expiration ");
		couponPage.typeselectinputtextbox("Impression");

		


		//// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typebarCodeFormat("Dynamic Barcode");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode value : UPC-A");
		couponPage.typebarCodevalue("Text");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("click add symbol");
		couponPage.clickaddSymbol();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("click text box");
		couponPage.clickTextbox();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("symboldrop down");
		couponPage.typesymbolropdown("11");
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("click save on barcode");
		couponPage.typeinputTextbox(barcode);
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("click save on barcode");
		couponPage.clicksavebuttonbarcode();
		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();
		// Thread.sleep(3 * 1000);
		

		// Thread.sleep(3 * 1000);
		

		// Thread.sleep(3 * 1000);
		
		//// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for retailer with frequency capping \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_frequency_capping(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		/*
		 * Reporter.addStepLog("Clicking stopDate button");
		 * couponPage.stopDateFormatter(); Thread.sleep(5*1000);
		 */

		// Thread.sleep(3* 1000);
		Reporter.addStepLog("Click Frequency Caping");
		couponPage.clickFrequencyCappingClickHereButton();

		// Thread.sleep(3* 1000);
		Reporter.addStepLog("Click Frequency Caping :5");
		couponPage.typefrequencyCappingClickTextBox("2");

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		couponPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : 19");
		couponPage.typeDistributedCappingTextBox("5");

		// if ((couponPage.QualiferLineInput())==true)
		// {
		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typequaliferLineInput5("Buy N Save");
		// }//
		
		 Reporter.addStepLog("Entering save line Save 50%");
		 couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image : C:\\Vault UI Template\\donut image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Calander");
		couponPage.clickCalanderButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.clickDateOption();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for retailer with unlimited \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_unlimited(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking unlimited checkbox");
		couponPage.clickunlimitedChecbox();
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		/*
		 * Reporter.addStepLog("Selecting the image to add : image");
		 * couponPage.addImage(image); Thread.sleep(5 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(5 * 1000);
		 */

		if ((couponPage.QualiferLineInput()) == false) {
			Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
			couponPage.typequaliferLineInput5("Buy N Save");
		}
		 Reporter.addStepLog("Entering save line Save 50%");
		 couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for manufacturer with unlimited \"([^\"]*)\"$")
	public void fill_ad_form_manufacturer_unlimited(String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking unlimited checkbox");
		couponPage.clickunlimitedChecbox();
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		/*
		 * Reporter.addStepLog("Selecting the image to add : image");
		 * couponPage.addImage(image); Thread.sleep(5 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(5 * 1000);
		 */

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		page.waitforLoading("static message");

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

	}
	
	@Then("^: Fill ad form HighRisk for USAretailer with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_HighRisk(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

//		Reporter.addStepLog("Clicking AdvertisedFlag");
//		couponPage.clickOnAdvertisedFlag();
//		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking clickOnHighRisk");
		couponPage.clickOnHighRisk();
		// Thread.sleep(2 * 1000);

//		Reporter.addStepLog("Entrering typeNBICInput");
//		couponPage.typeNBICInput("1");
//		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("save button");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();


		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : UPC-A");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeUSABarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for manufacturer with \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_manufacturer_with(String image, int startdate, int endDate) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		// Reporter.addStepLog("Selecting the image to add : image");

		/*
		 * couponPage.addImage(FileSystems.getDefault().getPath(image).
		 * toAbsolutePath().toString()); Thread.sleep(3 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(3 * 1000);
		 */

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(9* 1000);
		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
		// page.waitforLoading();
	}

	@Then("^: Fill ad form for manufacturer for LMC Product Carried with \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_manufacturer_for_LMC_Product_Carried_with(String image,String list, int startdate, int endDate)
			throws Throwable {

		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		// Reporter.addStepLog("Selecting the image to add : image");

		/*
		 * couponPage.addImage(FileSystems.getDefault().getPath(image).
		 * toAbsolutePath().toString()); Thread.sleep(3 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(3 * 1000);
		 */

		Reporter.addStepLog("Adding Product Validation Group");
		manufacturerAdPage.clickAddProductGroupButton();
//
//		Reporter.addStepLog("Entering Product image :  image.jpg");
//		manufacturerAdPage.addCustomImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());
//
//		Reporter.addStepLog("Clicking on Save Product image");
//		couponPage.clickSaveStaticImage();
//		Thread.sleep(3 * 1000);
//		
//		Reporter.addStepLog("Clicking on Save");
//		couponPage.clickSaveButton();

		Reporter.addStepLog("Entering rolling day 3");
		manufacturerAdPage.typeRollingDay();

		Reporter.addStepLog("Selecting the UPC List");
		manufacturerAdPage.typeinputForProductValidation(list);

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(9* 1000);
		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
		// page.waitforLoading("save button");
	}

	@Then("^: Fill ad form for manufacturer for Manual LMC Product Carried with \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_manufacturer_for_Manual_LMC_Product_Carried_with(String image, int startdate, int endDate)
			throws Throwable {

		TargetingPage targetingpage = new TargetingPage(this.driver);

		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		// Reporter.addStepLog("Selecting the image to add : image");

		/*
		 * couponPage.addImage(FileSystems.getDefault().getPath(image).
		 * toAbsolutePath().toString()); Thread.sleep(3 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(3 * 1000);
		 */

		Reporter.addStepLog("Adding Product Validation Group");
		manufacturerAdPage.clickAddProductGroupButton();

		Reporter.addStepLog("Clicking on Manual List");
		manufacturerAdPage.ClickOnManualListCheckBox();

		Reporter.addStepLog("Clicking on YES this is OK");
		couponPage.clickYesThisIsOkButton();

		Reporter.addStepLog("Enter the Manual UPC");
		int manuallist = target.get_Manual_List();
		targetingpage.clickProductManualListeditbox(String.valueOf(manuallist));
		Log.info("entered value for list  : from manual list" + String.valueOf(manuallist));
		// upc is set to be written to excel
		target.sharedResource.setPromotionUpc(String.valueOf(manuallist));

		Reporter.addStepLog("Entering rolling day 3");
		manufacturerAdPage.typeRollingDay();

		Reporter.addStepLog("Entering Product image :  image.jpg");
		manufacturerAdPage.addCustomImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Reporter.addStepLog("Clicking on Save Product image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(9* 1000);
		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
		// page.waitforLoading();
	}

	@Then("^: Fill ad form for manufacturer with start date current date \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_manufacturer_with_current_date(String image, int startdate, int endDate) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		// Reporter.addStepLog("Selecting the image to add : image");

		/*
		 * couponPage.addImage(FileSystems.getDefault().getPath(image).
		 * toAbsolutePath().toString()); Thread.sleep(3 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(3 * 1000);
		 */

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(9* 1000);
		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
		// page.waitforLoading();
	}

	@Then("^: Fill ad form for manufacturer with distribution cap \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_manufacturer_with_distribution_cap(String image, int startdate, int endDate)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		/*
		 * Reporter.addStepLog("Selecting the image to add : image");
		 * couponPage.addImage(image); //Thread.sleep(3 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(3 * 1000);
		 */

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		couponPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : 19");
		couponPage.typeDistributedCappingTextBox("1");

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
	}

	@Then("^: Fill ad form for retailer with two distribution cap at diff level \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_retailer_with_two_distribution_cap_at_diflevel(String image, int startdate, int endDate,
			String count, String level, String count2, String level2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(3 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		/*
		 * Reporter.addStepLog("Selecting the image to add : image");
		 * couponPage.addImage(image); //Thread.sleep(3 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(3 * 1000);
		 */

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		couponPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : ");
		couponPage.typeDistributedCappingTextBox(count);
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clicklevelsdropdown();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox(level);
		
		Reporter.addStepLog("add distribution capping");
		couponPage.clickadddistcapping();
		
		
		Reporter.addStepLog("enter count for second distribution capping");
		couponPage.typeseconddistributedCappingTextBox(count2);
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clicklevelsdropdown2();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox(level2);
		
		
		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
	}
	
	@Then("^: Fill ad form for retailer with distribution cap \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_retailer_with_two_distribution_cap(String image, int startdate, int endDate,
			String count, String level, String count2, String level2, String caplevel) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(3 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);
		/*
		 * Reporter.addStepLog("Selecting the image to add : image");
		 * couponPage.addImage(image); //Thread.sleep(3 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(3 * 1000);
		 */

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		couponPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : ");
		couponPage.typeDistributedCappingTextBox(count);
		
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clickcaplevelsdropdown();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox(caplevel);
		
				
		Reporter.addStepLog("select levels from drop down");
		couponPage.clicklevelsdropdown();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox(level);
		
		Reporter.addStepLog("add distribution capping");
		couponPage.clickadddistcapping();
		
		
		Reporter.addStepLog("enter count for second distribution capping");
		couponPage.typeseconddistributedCappingTextBox(count2);
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clicklevelsdropdown2();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox(level2);
		
		
		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
	}
	
	@Then("^: Fill ad form for retailer with distribution cap weekly \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_retailer_with_two_distribution_cap_weekly(String image, int startdate, int endDate,
			String count, String level, String count2, String level2, String week) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(3 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		
		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		couponPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : ");
		couponPage.typeDistributedCappingTextBox(count);
		
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clickcaplevelsdropdown();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox("per week");
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clickcaptimewindow();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox("anchored");
		
		
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clickcapweekgroup();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox(week);
		
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clicklevelsdropdown();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox(level);
		
		Reporter.addStepLog("add distribution capping");
		couponPage.clickadddistcapping();
		
		
		Reporter.addStepLog("enter count for second distribution capping");
		couponPage.typeseconddistributedCappingTextBox(count2);
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clicklevelsdropdown2();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox(level2);
		
		
		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
	}
	
	@Then("^: Fill ad form for retailer with distribution cap at diffs level \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_retailer_with_distribution_cap_at_diff_levels(String image, int startdate, int endDate,
			String count, String level) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(3 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		
		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		couponPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : ");
		couponPage.typeDistributedCappingTextBox(count);
		
		Reporter.addStepLog("select levels from drop down");
		couponPage.clicklevelsdropdown();
		
		Reporter.addStepLog("Enter text in drop down text box");
		couponPage.clickdropdownTextBox(level);
		
		//Reporter.addStepLog("Click on add budget cap button");
		//couponPage.clickadddistcapping();
		
		
		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		// Thread.sleep(6* 1000);
	}
	
	@Then("^: Fill ad form for Indesign1 \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_for_Indesign1(String image, int startdate, int endDate) throws Throwable {

		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());


		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(9* 1000);
		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

	}
	
	@Then("^: Fill ad form for Indesign2 \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_for_Indesign2(int startdate, int endDate) throws Throwable {

		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

//		Reporter.addStepLog("Entering static image :  image.jpg");
//		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());
//
//
//		Thread.sleep(3 * 1000);
//
//		Reporter.addStepLog("Clicking on Save static image");
//		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(9* 1000);
		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

	}
	
	@Then("^: Fill ad form for Indesign4 \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_for_Indesign4(int startdate, int endDate) throws Throwable {

		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");
//		Reporter.addStepLog("Entering static image :  image.jpg");
//		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());


//		Thread.sleep(3 * 1000);
//
//		Reporter.addStepLog("Clicking on Save static image");
//		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(9* 1000);
		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

	}
	@Then("^: Create ad using Ad Galary with Indesign1 Template$")
	public void create_ad_using_Ad_Galary_with_Indesign1_Template() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		// Thread.sleep(2*1000);
		page.waitforLoading("ad page");
		Thread.sleep(2 * 1000);
		adsListPage.clickAdGallery();
		// Thread.sleep(2*1000);
		page.waitforLoading("ad gallery");
		adGallerypage.addIndesign1CouponTocart();
		System.out.println("Added to cart");
		manufacturerAdPage.clickOnNext();
		// Thread.sleep(2*1000);
		page.waitforLoading("next");
		Thread.sleep(2 * 1000);

	}
	
	@Then("^: Create ad using Ad Galary with Indesign4 Template$")
	public void create_ad_using_Ad_Galary_with_Indesign4_Template() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		// Thread.sleep(2*1000);
		page.waitforLoading("as group");
		Thread.sleep(2 * 1000);
		adsListPage.clickAdGallery();
		// Thread.sleep(2*1000);
		page.waitforLoading("ad gallery");
		adGallerypage.addIndesign4CouponTocart();
		System.out.println("Added to cart");
		manufacturerAdPage.clickOnNext();
		// Thread.sleep(2*1000);
		page.waitforLoading("next");
		Thread.sleep(2 * 1000);

	}
	
	@Then("^: Fill ad form for Indesign3 Custom Param with \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_for_Indesign3_Custom_Param_with(String country, String image, int startdate, int endDate)
			throws Throwable {

		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("adpage");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate button");
		couponPage.StartDate(startdate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stop button");
		couponPage.EndDate(endDate);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Clicking On Pin DropDown");
		couponPage.clickOnPins();

		Reporter.addStepLog("Adding the Pin List ");
		String PinListname="PINs_"+com.catalina.vault.steps.SharedResource.GetCountry();
		System.out.println("PinList"+PinListname);
		String PinList= propertiesFile.getProperty(PinListname);
		couponPage.typeinputForPins(PinList);

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addProductShotImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);
		
		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(9* 1000);
		page.waitforLoading("save");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

	}
	@Then("^: Fill ad form for retailer with Dynamic offerCode$")
	public void fill_ad_form_with_Dynamic_offercode() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		AdvertisementSettingPage Apage = new AdvertisementSettingPage(this.driver);
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");
		Log.debug("Switching to ads form page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		Log.debug(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);
		Log.debug("Switched to ads form page");
		// System.out.println("BLNumber "+adPage.getPromotionId());
		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);
		Log.debug("Clicking startDate");
		Log.info("Clicking startDate");
		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);
		Log.debug("Clicking stopDate");
		Log.info("Clicking stopDate");

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		Log.info("Entering channel : In-store");

//		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
//		couponPage.typequaliferLineInput5("Buy N Save");
//		Log.info("Entering value for Qualifer Line Input : Save Save Save");
		
		Reporter.addStepLog("Entering Required Purchase ammount");
		couponPage.typerequiredAmount();
		Log.info("Entering Required Purchase ammount");
		
		Reporter.addStepLog("Entering SaveLine");
		couponPage.typesaveLineInputOffercode("1,12");
		Log.info("Entering SaveLine");


		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();
		Log.info("Clicking on Save");

		page.waitforLoading("save");
		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();
		Log.info("Scrolling to Top");

		page.waitforLoading("scroll");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		Log.info("Changing the tab to Redemtion Clearing");
		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();
		Log.info("Clicking on Enable Expiration Date");
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();
		Log.info("Clicking on date Format Button");
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");
		Log.info("Entering date formatter MM/DD/YY");
		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();
		Log.info("Clicking on date option");
		
		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();
		Log.info("Clicking on Clearing Method");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");
		Log.info("Entering the value to search box : Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();
		Log.info("Clicking on BarCode Type");
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");
		Log.info("Entering value for barcode type : EAN-13");
		// Thread.sleep(1 * 1000);
		
		Reporter.addStepLog("clicking on Dynamic barcode dropdown");
		couponPage.clickDynamiccode("Dynamic Barcode");
		Log.info("clicking on Dynamic barcode dropdown");
		
		Reporter.addStepLog("clicking on Select symbol : Currency Code");
		couponPage.enterSelectSymbol("Currency Code");
		Log.info("clicking on Select symbol : Currency Code");
		page.waitforLoading("scroll");
		Reporter.addStepLog("clicking on Add Symbol");
		couponPage.clickAddSymbol();
		Log.info("clicking on Add Symbol");
		page.waitforLoading("scroll");
		Thread.sleep(2*1000);
		Reporter.addStepLog("clicking on Select symbol : Offer Code");
		couponPage.enterSelectSymbol("Offer Code");
		Log.info("clicking on Select symbol : Offer Code");
		page.waitforLoading("scroll");
		System.out.println("Offer Code selected");
		
		Reporter.addStepLog("clicking on Add Symbol");
		couponPage.clickAddSymbol();
		Log.info("clicking on Add Symbol");
		page.waitforLoading("scroll");
		System.out.println("Add Symbol is clicked");
		
		Reporter.addStepLog("clicking on Offer Code value : 99999");
		couponPage.clickoffercodevalue();
		Log.info("clicking on Offer Code value : 99999");
		page.waitforLoading("scroll");
		System.out.println("Clicked offer code value");
		
		Reporter.addStepLog("Entering the symbol length 4");
		couponPage.clickSymbolLengthTextBox("4");
		Log.info("Entering the symbol length 4");
		page.waitforLoading("scroll");
		Reporter.addStepLog("Clicking on Save Button");
		couponPage.clickBarcodeValueSaveButton();
		Log.info("Clicking on Save Button");
		page.waitforLoading("scroll");
		Reporter.addStepLog("clicking on Select symbol : Trigger #");
		couponPage.enterSelectSymbol("Trigger #");
		Log.info("clicking on Select symbol : Trigger #");
		page.waitforLoading("scroll");
		Reporter.addStepLog("clicking on Add Symbol");
		couponPage.clickAddSymbol();
		Log.info("clicking on Add Symbol");
		page.waitforLoading("scroll");
		Reporter.addStepLog("clicking on Select symbol : Value Code");
		couponPage.enterSelectSymbol("Value Code");
		Log.info("clicking on Select symbol : Value Code");
		page.waitforLoading("scroll");
		Reporter.addStepLog("clicking on Add Symbol");
		couponPage.clickAddSymbol();
		Log.info("clicking on Add Symbol");
		page.waitforLoading("scroll");
		Reporter.addStepLog("clicking on Offer Code value : 99");
		couponPage.clickvaluecodevalue();
		Log.info("clicking on Offer Code value : 99");
		page.waitforLoading("scroll");
		Reporter.addStepLog("Entering the symbol length 3");
		couponPage.clickSymbolLengthTextBox("3");
		Log.info("Entering the symbol length 3");
		page.waitforLoading("scroll");
		Reporter.addStepLog("Clicking on Save Button");
		couponPage.clickBarcodeValueSaveButton();
		Log.info("Clicking on Save Button");
		page.waitforLoading("scroll");
		Reporter.addStepLog("Clicking on Enable Offer code");
		couponPage.clickEnableOfferCode();
		Log.info("Clicking on Enable Offer code");
		
		Reporter.addStepLog("Clicking on Generated Radio Button");
		couponPage.clickOnGenerated();
		Log.info("Clicking on Generated Radio Button");
		
		Thread.sleep(2*1000);
		
		Reporter.addStepLog("Clicking on Offer Code Management");
		couponPage.clickOnOfferCodeManagement();
		Log.info("Clicking on Offer Code Management");
		
		//Thread.sleep(10*1000);
		
		//System.out.println("Page got Loaded");		
		
		//Thread.sleep(50*1000);
		
		page.waitforofferCodeLease();
	   Thread.sleep(3*1000);
	   
	   //window.scrollTo(0,document.body.scrollHeight);
	  
      // js.executeScript("driver.manage().window().scrollBy(0,1000)");
	  // Apage.scrollDown(0,3000);
	    System.out.println("clicking on lease");
		Reporter.addStepLog("Clicking on Lease");
		couponPage.clickOnLease("Wine test release");
		Log.info("Clicking on Lease");
		
		Reporter.addStepLog("Clicking on Yes This is OK");
		couponPage.clickYesThisIsOkButton();
		Log.info("Clicking on Yes This is OK");
		
		page.waitforLoading("yes this ok");
		
		Reporter.addStepLog("Clicking on Close");
		couponPage.clickOnClose();
		Log.info("Clicking on Close");
		page.waitforLoading("yes this ok");
		

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();
		Log.info("Scrolling to top");
		Thread.sleep(2*1000);
		page.waitforLoading("yes this ok");
		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();
		Log.info("Click on Target button");
		
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"Ad Settings Page");

	}
	
	@Then("^: Create ad using Ad Galary with Dynamic Offer Code retail$")
	public void create_ad_using_Ad_Galary_with_Dynamic_Offer_Code_retail() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		page.waitforLoading("click on ad page");
		adsListPage.clickAdGallery();
		Log.info("Clicked on adgallery");
		page.waitforLoading("Click on ad gallery");
		couponPage.addDynamicOfferCodeTocart();
		Log.info("Clicked on add retail coupon to cart");
		couponPage.clickOnNext();
		Log.info("Clicked on next button");
		page.waitforLoading("next button");
		Thread.sleep(5 * 1000);
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"ADGallery");
	}
	
	

	
	@Then("^: Add Custom Parameter")
	public void add_Custom_Parameter() throws Throwable {
		Thread.sleep(2 * 1000);
		page.waitforLoading("add custom parameter");

		Thread.sleep(2 * 1000);
		Reporter.addStepLog("scrolling to top");
		adPage.scrollToTop();
		Log.info("Scroll to Top");
		
		Reporter.addStepLog("Getting the BL");
		couponPage.getBLText();
		
		Reporter.addStepLog("Clicking on Save Draft");
		adPage.clickSaveDraftButton();
		Log.info("Clicking on Save Draft");

		
		Reporter.addStepLog("Writing in Text Document");
		fileUtil.customParameter();
		
		page.waitforLoading("custom parameters");
		
		Reporter.addStepLog("Clicking on shared List");
		dashboard.clickSharedList();
		
		Reporter.addStepLog("Clicking on Parameters");
		dashboard.clickParameters();
		
		page.waitforLoading("parameters");
		
		Reporter.addStepLog("Clicking on Upload File");
		dashboard.clickUploadFile();
		
		Reporter.addStepLog("Clicking on CID+Parameters");
		dashboard.clickCIDParameters();
		
		Reporter.addStepLog("Enetring the CustomParameter name");
		dashboard.enterCustomParamFileName();
		
		newAudienceNamePage.clicksaveChangesButton();
		Reporter.addStepLog("Clicked on save changes button");

		Reporter.addStepLog("Clicking on File Tab");
		dashboard.clickFileTab();
		
		propertiesFile = new PropertiesFile("environment");
		String cidPath = propertiesFile.getProperty("sharedPath");
		
		String CustomParamFile = cidPath + "CustomParameters_for_" + com.catalina.vault.pagefactory.RetailerCouponPage.bl +".txt";
		System.out.println("upload file from: "+CustomParamFile);
	
		dashboard.clickImportNewFileCustomButton(CustomParamFile);
		Thread.sleep(3*1000);
		
//		dashboard.clicksaveChangeButtonAfterImportButton();
//		page.waitforLoading();
		//dashboard.checkProcessingstatus();
		
		Reporter.addStepLog("Clicking on Dashboard");
		dashboard.clickDashboard();
		
		page.waitforLoading("dashboard");
		
		campaginForm.typesearchBox(com.catalina.vault.pagefactory.RetailerCouponPage.bl);
		
		page.waitforLoading("search");
		
		campaginForm.clickEdit();
		Reporter.addStepLog("Clicked on Edit");
		page.waitforLoading("edit");
		
		adPage.scrollTodown();

		Reporter.addStepLog("Clicking on Custom Parameters Enable");
		adPage.enableCustomParameters();
		Log.info("clicked on Custom Parameters Enable");
		
		Reporter.addStepLog("Clicking on Custom Parameters DropDown");
		adPage.clickOnCustomParameters();
		Log.info("clicked on Custom Parameters DropDown");

		Reporter.addStepLog("Adding Custom Parameter");
		adPage.typeinputForCustomParam("CustomParameters_for_" + com.catalina.vault.pagefactory.RetailerCouponPage.bl);

		Reporter.addStepLog("Select custom parameter group");
		adPage.selectCustomParamGroup(com.catalina.vault.pagefactory.RetailerCouponPage.bl);
		
		
		
	}
	
	
	@Then("^: Create ad using Ad Galary with Indesign3 Template$")
	public void create_ad_using_Ad_Galary_with_Indesign3_Template() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		// Thread.sleep(2*1000);
		page.waitforLoading("ad group");
		Thread.sleep(2 * 1000);
		adsListPage.clickAdGallery();
		// Thread.sleep(2*1000);
		page.waitforLoading("ad gallery");
		adGallerypage.addIndesign3CouponTocart();
		System.out.println("Added to cart");
		manufacturerAdPage.clickOnNext();
		// Thread.sleep(2*1000);
		page.waitforLoading("next");
		Thread.sleep(2 * 1000);

	}
	@Then("^: Create ad using Ad Galary with Indesign2 Template$")
	public void create_ad_using_Ad_Galary_with_Indesign2_Template() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		// Thread.sleep(2*1000);
		page.waitforLoading("ad group");
		Thread.sleep(2 * 1000);
		adsListPage.clickAdGallery();
		// Thread.sleep(2*1000);
		page.waitforLoading("gallery");
		adGallerypage.addIndesign2CouponTocart();
		System.out.println("Added to cart");
		manufacturerAdPage.clickOnNext();
		// Thread.sleep(2*1000);
		page.waitforLoading("next");
		Thread.sleep(2 * 1000);

	}

	
	
	
	@Then("^: Fill ad form for retailer redemption tab \"([^\"]*)\"$")
	public void fill_ad_form_retailer(String barcode) throws Throwable {

		// Thread.sleep(2*1000);
		page.waitforLoading("ad page");

		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type ");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typestaticBarCodeValueupca(barcode);

		// Thread.sleep(1 * 1000);

		

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	
	}


	@Then("^: Fill ad form for manufacturer redemption tab \"([^\"]*)\"$")
	public void fill_ad_form_manufacturer(String barcode) throws Throwable {

		// Thread.sleep(2*1000);
		page.waitforLoading("ad page");

		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Manufacturer");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on company prefix");
		couponPage.typecompanyprefixfield();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering company prefix");
		couponPage.typecompanyprefixTextBox("OLD EL");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering clearing house");
		couponPage.typeclearinghouse("OLD EL");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("GS1 Databar North American (with text)");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typegs1barcodevalue(barcode);
		this.sharedResource.setbarcode(barcode);
		// Thread.sleep(1 * 1000);

		
		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for retailer with Rank \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form(String barcode, String image, String ad, String Rank) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Enter name");
		couponPage.clickadname(ad);
		// Thread.sleep(3*1000);

		// System.out.println("BLNumber "+adPage.getPromotionId());
		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering rank :rank");
		couponPage.typeDefaultRank(Rank);
		// Thread.sleep(3*1000);

		/*
		 * Reporter.addStepLog("Selecting the image to add : image");
		 * couponPage.addImage(image); //Thread.sleep(5 * 1000);
		 * 
		 * //ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		 * Reporter.addStepLog("Clicking on save Image button");
		 * couponPage.clickSaveImageButton(); Thread.sleep(5 * 1000);
		 */

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("save button");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type ");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typestaticBarCodeValueupca(barcode);

		// Thread.sleep(1 * 1000);


		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for retailer with name \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_name(String barcode, String image, String ad) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Enter name");
		couponPage.clickadname(ad);
		// Thread.sleep(3*1000);

		// System.out.println("BLNumber "+adPage.getPromotionId());
		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typeQualiferLineInputafter("Buy N Save");

		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("save button");

		if ((couponPage.alertOkClick()) == true) {
			couponPage.clickalertAccept();
		}

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		page.waitforLoading("save button");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type ");
		couponPage.typeBarCodeTypeTextBox("UPC-A");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typestaticBarCodeValueupca(barcode);

		// Thread.sleep(1 * 1000);

		

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}
	@Then("^: Fill ad form for retailer with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("Ad page Loading");
		Log.debug("Switching to ads form page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		Log.debug(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);
		Log.debug("Switched to ads form page");
		// System.out.println("BLNumber "+adPage.getPromotionId());
		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);
		Log.debug("Clicking startDate");
		Log.info("Clicking startDate");
		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);
		Log.debug("Clicking stopDate");
		Log.info("Clicking stopDate");

		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		Log.info("Entering channel : In-store");

		Reporter.addStepLog("Entering value for Qualifer Line Input : Save Save Save");
		couponPage.typequaliferLineInput5("Buy N Save");
		Log.info("Entering value for Qualifer Line Input : Save Save Save");
		
		Reporter.addStepLog("Entering save line Save 50%");
		 couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());
		Log.info("Entering static image :  image.jpg");

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Log.info("Clicking on Save static image");
		Thread.sleep(3 * 1000);
		
		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();
		Log.info("Clicking on Save");
		
		page.waitforLoading("Clicking on Save");
		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();
		Log.info("Scrolling to Top");

		page.waitforLoading("Clicking on Save");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		Log.info("Changing the tab to Redemtion Clearing");
		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();
		Log.info("Clicking on Enable Expiration Date");
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();
		Log.info("Clicking on date Format Button");
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");
		Log.info("Entering date formatter MM/DD/YY");
		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();
		Log.info("Clicking on date option");
		
		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();
		Log.info("Clicking on Clearing Method");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");
		Log.info("Entering the value to search box : Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();
		Log.info("Clicking on BarCode Type");
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");
		Log.info("Entering value for barcode type : EAN-13");
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);
		Log.info("Entering value for barcode type : EAN-13");
		
		// Thread.sleep(2 * 1000);
		 this.sharedResource.setbarcode(barcode);
		

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();
		Log.info("Scrolling to top");

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();
		Log.info("Click on Target button");
		
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"Ad Settings Page");

	}

	@Then("^: Fill ad form for UKretailer with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_UKretailer(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		long start = System.currentTimeMillis();

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		long finish = System.currentTimeMillis();

		long totalTime = finish - start;

		System.out.println("Total Time for page load - " + totalTime + "sec");

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		Thread.sleep(3 * 1000);

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Selecting the image to add ");
		couponPage.addproductShot(image);
		Thread.sleep(3 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		Reporter.addStepLog("Clicking on save Image button");
		couponPage.clickSaveImageButton();
		Thread.sleep(3 * 1000);

		// if ((couponPage.QualiferLineInput())==true)
		// {
		// Reporter.addStepLog("Entering value for Qualifer Line Input : Save
		// Save Save");
		// couponPage.typequaliferLineInput5("Buy N Save");
		// }//
		// Reporter.addStepLog("Entering save line Save 50%");
		// couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		Thread.sleep(9 * 1000);

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(6 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for UKretailer with rolling expirationDate\"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_UKretailer_rolling_expirationDate(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		long start = System.currentTimeMillis();

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		long finish = System.currentTimeMillis();

		long totalTime = finish - start;

		System.out.println("Total Time for page load - " + totalTime + "sec");

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		Thread.sleep(3 * 1000);

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Selecting the image to add ");
		couponPage.addproductShot(image);
		Thread.sleep(3 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		Reporter.addStepLog("Clicking on save Image button");
		couponPage.clickSaveImageButton();
		Thread.sleep(3 * 1000);

		// if ((couponPage.QualiferLineInput())==true)
		// {
		// Reporter.addStepLog("Entering value for Qualifer Line Input : Save
		// Save Save");
		// couponPage.typequaliferLineInput5("Buy N Save");
		// }//
		// Reporter.addStepLog("Entering save line Save 50%");
		// couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		Thread.sleep(9 * 1000);

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(6 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();
		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking rolling expiration radio button");
		couponPage.clickrollingexpirationradioButton();

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking rolling expiration select field");
		couponPage.clickselectbuttonrolling();

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("select rolling expiration ");
		couponPage.typeselectinputtextbox("Impression");

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for UKretailer with freqcap \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_UKretailer_freq_cap(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		long start = System.currentTimeMillis();

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		long finish = System.currentTimeMillis();

		long totalTime = finish - start;

		System.out.println("Total Time for page load - " + totalTime + "sec");

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		Thread.sleep(3 * 1000);

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Click Frequency Caping");
		couponPage.clickFrequencyCappingClickHereButton();

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Click Frequency Caping :5");
		couponPage.typefrequencyCappingClickTextBox("5");

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		couponPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : 19");
		couponPage.typeDistributedCappingTextBox("19");

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Selecting the image to add ");
		couponPage.addproductShot(image);
		Thread.sleep(3 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		Reporter.addStepLog("Clicking on save Image button");
		couponPage.clickSaveImageButton();
		Thread.sleep(3 * 1000);

		// if ((couponPage.QualiferLineInput())==true)
		// {
		// Reporter.addStepLog("Entering value for Qualifer Line Input : Save
		// Save Save");
		// couponPage.typequaliferLineInput5("Buy N Save");
		// }//
		// Reporter.addStepLog("Entering save line Save 50%");
		// couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		Thread.sleep(9 * 1000);

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(6 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for UKretailer with unlimited \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_UKretailer_unlimited(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		long start = System.currentTimeMillis();

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		long finish = System.currentTimeMillis();

		long totalTime = finish - start;

		System.out.println("Total Time for page load - " + totalTime + "sec");

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		Thread.sleep(3 * 1000);

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		Reporter.addStepLog("Clicking unlimited checkbox");
		couponPage.clickunlimitedChecbox();
		Thread.sleep(3 * 1000);

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Selecting the image to add ");
		couponPage.addproductShot(image);
		Thread.sleep(3 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		Reporter.addStepLog("Clicking on save Image button");
		couponPage.clickSaveImageButton();
		Thread.sleep(3 * 1000);

		// if ((couponPage.QualiferLineInput())==true)
		// {
		// Reporter.addStepLog("Entering value for Qualifer Line Input : Save
		// Save Save");
		// couponPage.typequaliferLineInput5("Buy N Save");
		// }//
		// Reporter.addStepLog("Entering save line Save 50%");
		// couponPage.typesaveLineInputafter("50%");

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		Thread.sleep(9 * 1000);

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(6 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for Italyretailer with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_for_italyretailer(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("Fill ad form for Italy retailer");
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("Template Loading");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		// Thread.sleep(2*1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		// Thread.sleep(2*1000);
		// Reporter.addStepLog("Selecting the image to add ");
		// couponPage.addproductShot(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());
		// Thread.sleep(2 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		// Reporter.addStepLog("Clicking on save Image button");
		// couponPage.clickSaveImageButton();
		// Thread.sleep(2 * 1000);

		// if ((couponPage.QualiferLineInput())==true)
		// {
		// Reporter.addStepLog("Entering value for Qualifer Line Input : Save
		// Save Save");
		// couponPage.typequaliferLineInput5("Buy N Save");
		// }//
		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");
		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering static image : C:\\Vault UI Template\\donut image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		// Thread.sleep(3* 1000);
		Thread.sleep(2 * 1000);
		page.waitforLoading("save button ad page");

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");
		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();
		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");
		//
		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);
		//
		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for Italyretailer with rolling Expirationdate \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_for_italyretailer_rolling_Expirationdate(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		long start = System.currentTimeMillis();

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		long finish = System.currentTimeMillis();

		long totalTime = finish - start;

		System.out.println("Total Time for page load - " + totalTime + "sec");

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		// Thread.sleep(3*1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		// Thread.sleep(3*1000);
		// Reporter.addStepLog("Selecting the image to add ");
		// couponPage.addproductShot(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());
		// Thread.sleep(3 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		// Reporter.addStepLog("Clicking on save Image button");
		// couponPage.clickSaveImageButton();
		// Thread.sleep(3 * 1000);

		// if ((couponPage.QualiferLineInput())==true)
		// {
		// Reporter.addStepLog("Entering value for Qualifer Line Input : Save
		// Save Save");
		// couponPage.typequaliferLineInput5("Buy N Save");
		// }//
		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");
		Reporter.addStepLog("Entering static image : C:\\Vault UI Template\\donut image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking rolling expiration radio button");
		couponPage.clickrollingexpirationradioButton();

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking rolling expiration select field");
		couponPage.clickselectbuttonrolling();

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("select rolling expiration ");
		couponPage.typeselectinputtextbox("Impression");


		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		
		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for Italyretailer with freqcap \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_for_italyretailer_freqcap(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		// Thread.sleep(3*1000);
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		// Thread.sleep(3* 1000);
		Reporter.addStepLog("Click Frequency Caping");
		couponPage.clickFrequencyCappingClickHereButton();

		// Thread.sleep(3* 1000);
		Reporter.addStepLog("Click Frequency Caping :5");
		couponPage.typefrequencyCappingClickTextBox("2");

		Reporter.addStepLog("Clicking on Distributed " + "Caping");
		couponPage.clickDistributedCappingClickHereButton();

		Reporter.addStepLog("Entering the Distributed Capping value : 19");
		couponPage.typeDistributedCappingTextBox("5");

		// Thread.sleep(3*1000);
		// Reporter.addStepLog("Selecting the image to add ");
		// couponPage.addproductShot(image);
		// Thread.sleep(3 * 1000);

		// ScreenShot.addScreenshotToScreen(this.driver, "enteredImage");
		// Reporter.addStepLog("Clicking on save Image button");
		// couponPage.clickSaveImageButton();
		// Thread.sleep(3 * 1000);

		// if ((couponPage.QualiferLineInput())==true)
		// {
//		 Reporter.addStepLog("Entering value for Qualifer Line Input : Save	 Save Save");
//		 couponPage.typequaliferLineInput5("Buy N Save");
		// }//
		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");
		Reporter.addStepLog("Entering static image : C:\\Vault UI Template\\donut image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		//// Thread.sleep(9* 1000);
		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");
		
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Fill ad form for Italyretailer with unlimited \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_for_italyretailer_unlimited(String barcode, String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("fill_ad_form_for_italyretailer_unlimited");
		Thread.sleep(2 * 1000);
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page");

		WebDriverWait wait = new WebDriverWait(driver, 30);
		boolean status = wait.until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
		System.out.println(status);

		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" + isTheTextPresent);
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		// Thread.sleep(3*1000);

		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(5);
		// Thread.sleep(3*1000);

		// Thread.sleep(3*1000);
		Reporter.addStepLog("Clicking unlimited checkbox");
		couponPage.clickunlimitedChecbox(); 
		
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");

		
		

		 if ((couponPage.QualiferLineInput())==true)
		 {
		 Reporter.addStepLog("Entering value for Qualifer Line Input : SaveSave Save");
		 couponPage.typequaliferLineInput5("Buy N Save");
		 }
		 
		Reporter.addStepLog("Entering save line Save 50%");
		couponPage.typesaveLineInputafter("50%");
		Reporter.addStepLog("Entering static image : C:\\Vault UI Template\\donut image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");

		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();

		Thread.sleep(2 * 1000);
		page.waitforLoading("save button");


		// ScreenShot.addScreenshotToScreen(this.driver, "fullAdStetting");
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();
		
		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();
		
		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		// ScreenShot.addScreenshotToScreen(this.driver,"BeforeEnteringValuesToRedemtionClearing");

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Retail");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : EAN-13");
		couponPage.typeBarCodeTypeTextBox("EAN-13");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typeStaticBarCodeValue(barcode);

		// Thread.sleep(1 * 1000);

		

		// Thread.sleep(1 * 1000);

		

		// Reporter.addStepLog("Clicking on date option");
		// couponPage.clickDateOption();

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Scrolling to top");
		couponPage.scrollToTop();

		// ScreenShot.addScreenshotToScreen(this.driver,"afterDetailsToRedemtionClearing");
		couponPage.clickTargetingButton();

	}

	@Then("^: Enter ad details name \"([^\"]*)\" image \"([^\"]*)\"$")
	public void enter_ad_details_name_image(String name, String url) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		AdvertisementSettingPage page = new AdvertisementSettingPage(this.driver);

		page.typeName(name);

		Thread.sleep(2 * 1000);

		page.startDateFormatter();

		// page.stopDateFormatter();

		Thread.sleep(10 * 1000);

		page.scrollDown(0, 1000);

		page.typeCustomImageUrl(url);

		Thread.sleep(10 * 1000);

		page.scrollDown(0, 1000);

		Thread.sleep(2 * 1000);

		page.clickOnSaveButton();

	}
	
	

}
