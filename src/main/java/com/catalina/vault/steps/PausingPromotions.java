package com.catalina.vault.steps;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assert;
import org.mortbay.log.Log;

import com.catalina.vault.utils.FileUtils;
import com.catalina.vault.utils.JsonUtils;
import com.catalina.vault.utils.NetworkUtils;
import com.catalina.vault.utils.PropertiesFile;
import com.cucumber.listener.Reporter;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import cucumber.api.java.en.Then;

public class PausingPromotions {

	private static final Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static String DMP_PROPERTY = "DMP";
	private static String STORE_PROPERTY = "STORE_PAYLOAD";
	private static String COUNTRY_PARAMETER = "ctry";
	private static String COUNTRY_JSON_PROPERTY = "ctry";
	private static String CHAIN_JSON_PROPERTY = "chn";
	private static String MOST_RECENT_MSG_JSON_PROPERTY = "most_recent_msg";
	private static PropertiesFile propertiesFile;
	private static PropertiesFile propertiesFile1;
	public static String audienceList;
	private static PropertiesFile pFile;
	

	private static SharedResource sharedResource;
	public static String jsonResponse ="";
	private static List<String> contentForRpubFile = new ArrayList<>();

	public PausingPromotions() {
		//this.driver = sharedResource.init();
		LOGGER.log(Level.INFO, "Initializing Resources");
		this.sharedResource =new  SharedResource();
		
		
	}
	 
	private  ArrayList<String> filename()
	{
		audienceList = com.catalina.vault.steps.SharedResource.getAudienceListName();
		
		this.pFile = new PropertiesFile("environment");
		File folder = new File(pFile.getProperty("sharedPath"));
		System.out.println("folder name:"+folder);
		File[] listOfFiles = folder.listFiles();
		
		ArrayList<String> Files = new ArrayList<String>();
		
		for (int i = 0; i < listOfFiles.length; i++) {
		  if (listOfFiles[i].isFile()) {
			  Files.add(listOfFiles[i].getName());
		   // System.out.println("File " + Files);
		  }
		}
		return Files;
		
	}

	@Then("^: Verify customParameters \\\"([^\\\"]*)\\\"$")
	public  void createTxtFiles(String Country) throws IOException {
		//String timeStamp = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());
		//System.out.println(timeStamp);
		audienceList = com.catalina.vault.steps.SharedResource.getAudienceListName();
		ArrayList<String> files = filename();
		for (String file : files) {
			if(file == null)
				continue;
			String fileName = file;
			String finalHit = null;
			int i=1;
			List<String> lines = Files.readAllLines(Paths.get(pFile.getProperty("sharedPath")+audienceList+ ".txt"), StandardCharsets.UTF_8); 
			System.out.println("lines:"+lines);
			int audList=lines.size();
			//Reporter.addScenarioLog("IN: " + fileName+" No Of Bl's are : " +audList);
			//System.out.println("no of bl" +noOfBl);
			while(i<=audList)
			{
			FileWriter fileWriter = new FileWriter(lines.get(i) + "_" + "BL_1234_5678" + "_" + "123456789" + ".txt");
			System.out.println("custom"+fileWriter);
			}
		} 

	}
	
	@Then("^: Verify Promotion paused for country in AWS \\\"([^\\\"]*)\\\"$")
	public  void pausePrintedPromotions_AWS(String country) throws Throwable {
		
		this.propertiesFile1 = new PropertiesFile("environment");
		String url = propertiesFile1.getProperty("pauseEndpointAWS");
		String geturl = propertiesFile1.getProperty("verifyPauseEndpointstatusAWS");
		System.out.println("Print url: " +url);
		Reporter.addScenarioLog("Print url : " + url);
		ArrayList<String> files = filename();
		for (String file : files) {
			if(file == null)
				continue;
			String fileName = file.replace(".properties","");
			
			this.propertiesFile =new PropertiesFile(fileName,"//prp/Pause//");
			String finalHit = null;
			int i=1;
			List<String> lines = Files.readAllLines(Paths.get("./prp/Pause//"+ file), StandardCharsets.UTF_8);
			
			int noOfBl=lines.size()-2;
			Reporter.addScenarioLog("IN: " + fileName+" No Of Bl's are : " +noOfBl);
			//System.out.println("no of bl" +noOfBl);
			while(i<=noOfBl)
			{
			//System.out.println("bl " +String.valueOf(i));
			if(country.equals("USA")) {
			if(propertiesFile.getProperty("BL"+ String.valueOf(i)).contains("USA"))
			finalHit=url+propertiesFile.getProperty("BL"+ String.valueOf(i));
			Reporter.addScenarioLog("Endpoint  : " + finalHit);
			}
			else if(country.equals("FRA")) {
			if(propertiesFile.getProperty("BL"+ String.valueOf(i)).contains("FRA"))
			finalHit=url+propertiesFile.getProperty("BL"+ String.valueOf(i));
			Reporter.addScenarioLog("Endpoint  : " + finalHit);
			}
			else if (country.equals("ITA")) {
				System.out.println("BL"+ String.valueOf(i));
			if(propertiesFile.getProperty("BL"+ String.valueOf(i)).contains("ITA"))
			finalHit=url+propertiesFile.getProperty("BL"+ String.valueOf(i));
			Reporter.addScenarioLog("Endpoint  : " + finalHit);
			}
			else if(country.equals("ALL")) {
			finalHit=url+propertiesFile.getProperty("BL"+ String.valueOf(i));
			Reporter.addScenarioLog("Endpoint  : " + finalHit);
			}
			System.out.println("Calling the pause with promotion id: "+finalHit);
			String responseput = NetworkUtils.putCall(finalHit,"");
			System.out.println("Response is "+responseput);
			String response = NetworkUtils.getCall(geturl + propertiesFile.getProperty("BL"+ String.valueOf(i)));
			//System.out.println("Response is "+response);
			JsonElement element = JsonUtils.convertToJsonElement(response);
			JsonElement statusElement = ((JsonObject) element).get("status");
			
			//Take Message from response when promotion is not found
			JsonElement messageElement = ((JsonObject) element).get("message");	
			
			boolean isStatusPresent = statusElement != null? true:false;
			
			boolean isMessagePresent = messageElement != null? true:false;
			String statusnotfound = "PROMOTION_NOT_FOUND";
			if(isMessagePresent && statusnotfound.equalsIgnoreCase(messageElement.getAsString()))
			{
			System.out.println("PROMOTION_NOT_FOUND");
			}
			else{
			String status =((JsonObject) element).get("status").getAsString();
	    	//String status = statusElem != null ? statusElem.getAsString():"PUBLISHED";
			System.out.println("Status after pausing : " +status);
			
			Reporter.addScenarioLog("Status after pausing  : " + status);
			}
			Thread.sleep(1*1000);
			i++;
			}
			
		}
	}	

}

