package com.catalina.vault.steps;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
//import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import com.google.gson.JsonElement;
import gherkin.formatter.model.Result;
import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.commons.lang.reflect.FieldUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.json.simple.JSONObject;
import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import com.catalina.schemaComparison.ValidationUtils;
import com.catalina.vault.entities.BLsToExcel;
import com.catalina.vault.entities.TestCaseRun;
import com.catalina.vault.intergration.Data;
import com.catalina.vault.intergration.ExecutionScript;
import com.catalina.vault.intergration.StoreDetail;
import com.catalina.vault.intergration.TargetProcessDetail;
import com.catalina.vault.jsoncomparisondemo.Main;
import com.catalina.vault.utils.CacheConnection;
import com.catalina.vault.utils.JsonUtils;
import com.catalina.vault.utils.NetworkUtils;
import com.catalina.vault.utils.PropertiesFile;
import com.catalina.vault.utils.ScreenShot;
import com.catalina.vault.utils.TargetProcess;
import com.cucumber.listener.Reporter;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.io.Files;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.runtime.ScenarioImpl;

public class SharedResource<PromotionDetail> {

	WebDriver driver;
	private static String promotionId = "";
	private static String fileName;
	BLsToExcel bls = new BLsToExcel(this.driver);
	boolean captureDebugScreenshot = false;
	private static int rowNumberForResult = 1;
	private static int rowNumberForJsonDate = 1;
	private static String upc = "";
	private static String deptid = "";
	private static String startDate = "";
	private static String stopDate = "";
	private static String promotionStatus = "";
	private static String country = "";
	private static String unitsUPC = "";
	private static String PromotionName = "";
	private static String op = "";
	private static String min = "";
	private static String measure = "";
	private static String Amt = "";
	private static String SpendUPC = "";
	private static String previousCountry = "";
	private static String CampaignName = "";
	private static String TestcaseName = "";
	private static String AudienceListName = "";
	private static String barcode = "";
	private static PropertiesFile propertiesFile;
	// private static PropertiesFile propertiesFile2;
	private static int BLCounter = 1;
	private static int rowNumber;
	private static String tsvalues;
	private static String tsvaluesStore;
	private Data data;
	private static final Logger Log = LogManager.getLogger(SharedResource.class);
	public static int TestPlanRunId;
	public static int TestCaseId;
	private static String BuildName = "";
	private static String PinListName = "";
	private static String jsondata = "";
	private static String vaultUrl;
	private static String audienceFileLoyaltyRewards;
	private static String loyaltyRewardsItemUnits;
	private static String loyaltyRewardsItemSpend;
	private static String loyaltyRewardsManualUpc;

	@Before
	public WebDriver init() {

		if (driver == null) {
			// System.setProperty("webdriver.driver.chrome",
			// "./chromedriver.exe");
			// driver = new ChromeDriver(getChromeOptions());
			// setup the chromedriver using WebDriverManager
			WebDriverManager.chromedriver().setup();

			// Create driver object for Chrome

			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(360, TimeUnit.SECONDS);
			LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
			File file = new File("src/resource/log4j2.xml");
			System.out.println("pathe" + file.getAbsolutePath());
			context.setConfigLocation(file.toURI());
		}
		return driver;
	}

	private ChromeOptions getChromeOptions() {

		ChromeOptions options = new ChromeOptions();

		options.setHeadless(true);
		options.addArguments(Arrays.asList("--disable-gpu", "--remote-debugging-port=9222", "window-size=1920,1080",
				"allow-running-insecure-content", "ignore-certificate-errors", "--disable-web-security",
				"enable-automation"));

		options.addArguments("--window-size=1600,900");

		return options;
	}

	@Before
	public void beforeHook(Scenario scenario) {
		Log.info("**********************Start Scenario-" + scenario.getName() + "****************************");
		System.out.println(scenario.getName());
		ScreenShot.scenario = scenario;
		this.setPromotionName(scenario.getName());

	}

	public void setPromotionName(String PromotionName) {
		SharedResource.PromotionName = PromotionName;
		System.out.print(SharedResource.PromotionName);
	}

	public static String GetPromotionName() {
		return SharedResource.PromotionName;
	}

	public static String GetCountry() {
		return SharedResource.country;
	}

	public static String getPromotionId() {
		return SharedResource.promotionId;
	}

	public void setPromotionId(String promotionId) {
		SharedResource.promotionId = promotionId;
		System.out.print(SharedResource.promotionId);
	}

	public static void setPropertyFilename(String fileName) {
		// TODO Auto-generated method stub
		SharedResource.fileName = fileName;
		System.out.print(SharedResource.fileName);
	}

	public void setFilename(String fileName) {
		// TODO Auto-generated method stub
		SharedResource.fileName = fileName;
		System.out.print(SharedResource.fileName);
	}

	public String getFilename() {
		return SharedResource.fileName;
	}

	public void setPromotionUpc(String upc) {
		SharedResource.upc = upc;
		System.out.print("UPC : " + SharedResource.upc);
	}

	public static String getPromotionUpc() {
		return SharedResource.upc;

	}

	public void setItemunits(String audienceFileLoyaltyRewards) {
		SharedResource.loyaltyRewardsItemUnits = audienceFileLoyaltyRewards;
		System.out.print("loyaltyRewardsItemUnits : " + SharedResource.loyaltyRewardsItemUnits);

	}

	public static String getItemunits() {
		return SharedResource.loyaltyRewardsItemUnits;

	}

	public void setItemSpend(String audienceFileLoyaltyRewards) {
		SharedResource.loyaltyRewardsItemSpend = audienceFileLoyaltyRewards;
		System.out.print("loyaltyRewardsItemSpend : " + SharedResource.loyaltyRewardsItemSpend);

	}

	public static String getItemSpend() {
		return SharedResource.loyaltyRewardsItemSpend;

	}

	public void setManualUpc(String audienceFileLoyaltyRewards) {
		SharedResource.loyaltyRewardsManualUpc = audienceFileLoyaltyRewards;
		System.out.print("loyaltyRewardsManualUpc : " + SharedResource.loyaltyRewardsManualUpc);

	}

	public static String getManualUpc() {
		return SharedResource.loyaltyRewardsManualUpc;

	}

	public void setJsondata(String jsondata) {
		SharedResource.jsondata = jsondata;
		System.out.print("jsondata : " + SharedResource.jsondata);

	}

	public String getJsondata(String jsondata) {
		return SharedResource.jsondata;

	}

	private void setSpendUPC(String SpendUPC) {
		SharedResource.SpendUPC = SpendUPC;
		System.out.print("SpendUPC : " + SharedResource.SpendUPC);
	}

	private static String getSpendUPC() {
		return SharedResource.SpendUPC;
	}

	public void setrowNumber(int rowNumber) {
		SharedResource.rowNumber = rowNumber;
		System.out.print("rowNumber : " + SharedResource.rowNumber);
	}

	public static int getrowNumber() {
		return SharedResource.rowNumber;

	}

	public void setPinListName(String pinListName) {
		SharedResource.PinListName = pinListName;
		System.out.print("PinListName : " + SharedResource.PinListName);
	}

	public static String getPinListName() {
		return SharedResource.PinListName;
	}

	public void setBuildName(String BuildName) {
		SharedResource.BuildName = BuildName;
		System.out.print("BuildName : " + SharedResource.BuildName);

	}

	public static String getBuildName() {
		// TODO Auto-generated method stub
		return SharedResource.BuildName;
	}

	private void settsValues(String tsvalues) {
		SharedResource.tsvalues = tsvalues;
		System.out.print("tsvalues : " + SharedResource.tsvalues);
	}

	public static String gettsvalues() {
		return tsvalues;

	}

	private void settsValuesStore(String tsvaluesStore) {
		SharedResource.tsvaluesStore = tsvaluesStore;
		System.out.print("tsvaluesStore : " + SharedResource.tsvaluesStore);

	}

	public static String gettsvaluesStore() {
		return tsvaluesStore;

	}

	public void setbarcode(String barcode) {
		// TODO Auto-generated method stub
		SharedResource.barcode = barcode;
		System.out.print("barcode : " + SharedResource.barcode);

	}

	public static String getbarcode() {
		return SharedResource.barcode;

	}

	public void setrowNumberReult(int rowNumberForResult) {
		SharedResource.rowNumberForResult = rowNumberForResult;
		System.out.print("rowNumber : " + SharedResource.rowNumberForResult);
	}

	public static int getrowNumberForResult() {
		return SharedResource.rowNumberForResult;

	}

	private void setvaultUrl(String vaultUrl) {
		SharedResource.vaultUrl = vaultUrl;
		System.out.print("vaultUrl : " + SharedResource.vaultUrl);

	}

	public static String getvaultUrl() {
		return SharedResource.vaultUrl;

	}

	public void setCampaignName(String CampaignName) {
		SharedResource.CampaignName = CampaignName;
		System.out.print("CampaignName : " + SharedResource.CampaignName);
	}

	public static String getCampaignName() {
		return SharedResource.CampaignName;

	}

	public void setAudienceListName(String AudienceListName) {
		SharedResource.AudienceListName = AudienceListName;
		System.out.print("AudienceListName : " + SharedResource.AudienceListName);
	}

	public static String getAudienceListName() {
		return SharedResource.AudienceListName;
	}

	private void setSpendAmt(String Amt) {
		SharedResource.Amt = Amt;
		System.out.print("SpendAmt : " + SharedResource.Amt);

	}

	public static String getamt() {
		return SharedResource.Amt;

	}

	private void setunitsUPC(String unitsUPC) {
		SharedResource.unitsUPC = unitsUPC;
		System.out.print("unitsUPC : " + SharedResource.unitsUPC);

	}

	public static String getunitsUPC() {
		return SharedResource.unitsUPC;

	}

	public void setTestcaseName(String TestcaseName) {
		SharedResource.TestcaseName = TestcaseName;
		System.out.print("TestcaseName : " + SharedResource.TestcaseName);

	}

	public static String getTestcaseName() {
		return SharedResource.TestcaseName;

	}

	public void setPromotionop(String op) {
		SharedResource.op = op;
		System.out.print("op : " + SharedResource.op);
	}

	public void setPromotionmin(String min) {
		SharedResource.min = min;
		System.out.print("min : " + SharedResource.min);
	}

	public static String getPromotionmin() {
		return SharedResource.min;

	}

	public void setmeasure(String measure) {
		SharedResource.measure = measure;
		System.out.print("measure : " + SharedResource.measure);
	}

	public void setdepartmentid(String deptid) {
		SharedResource.deptid = deptid;
		System.out.print("deptid : " + SharedResource.deptid);
	}

	public static String getdepartmentid() {
		return SharedResource.deptid;

	}

	public void setCountry(String country) {
		SharedResource.country = country;
		System.out.println("country : " + SharedResource.country);
		System.out.println("previousCountry : " + SharedResource.previousCountry);
		if (SharedResource.previousCountry != SharedResource.country) {
			SharedResource.rowNumberForResult = 1;
			SharedResource.previousCountry = SharedResource.country;

		}
	}

	public void setPromotionValues(String startDate, String stopDate, String promotionStatus) {
		SharedResource.startDate = startDate;
		SharedResource.stopDate = stopDate;
		SharedResource.promotionStatus = promotionStatus;

	}

	@After
	public void afterHook(Scenario scenario) throws Exception {
		propertiesFile = new PropertiesFile(SharedResource.fileName,
				"//test_storage1/spliceshare/VaultPromotionPause//");

		String Status = scenario.getStatus();
		if (SharedResource.TestPlanRunId != 0) {
			TestCaseRun testCaseRun = new TestCaseRun();
			testCaseRun.setStatus(Status);
			System.out.println("testcase run Status" + Status);
			testCaseRun.setComment("Promotion Id: " + this.getPromotionId() + "\n" + "Status :" + Status + "\n"
					+ " TS script: " + this.gettsvalues());

			System.out.println("testcase run status: " + testCaseRun.getStatus());
			System.out.println("testcase run Promotion id: " + testCaseRun.getComment());
			System.out.println("testcase rub getPromotionId(): " + this.getPromotionId());
			System.out.println("testcase SharedResource.TestPlanRunId" + SharedResource.TestPlanRunId);
			System.out.println("testcase SharedResource.TestCaseId" + SharedResource.TestCaseId);

			new TargetProcess().updateStatus(SharedResource.TestPlanRunId, SharedResource.TestCaseId, testCaseRun);
		}
		addingScreenShot(scenario);
		// Main.jsoncomparemain(scenario);

		Reporter.addScenarioLog("Finished Scinario : " + scenario.getName());

		System.out.println("Scenario status :- " + scenario.getStatus());

		System.out.println("rowNumberForResult :- " + rowNumberForResult);
		if ((Status == "passed") && this.getPromotionId() != null && !this.getPromotionId().trim().isEmpty()) {
			if (SharedResource.country.equalsIgnoreCase("France"))
				propertiesFile.SetPropertyfolder("BL" + BLCounter, "FRA" + "-BLIP-" + this.getPromotionId());
			else if (SharedResource.country.equalsIgnoreCase("Italy"))
				propertiesFile.SetPropertyfolder("BL" + BLCounter, "ITA" + "-BLIP-" + this.getPromotionId());
			else if (SharedResource.country.equalsIgnoreCase("Usa"))
				propertiesFile.SetPropertyfolder("BL" + BLCounter, "USA" + "-BLIP-" + this.getPromotionId());
			// System.out.println("BL"+BLCounter++ +"=");
			this.bls.setCellData(scenario.getName(), rowNumberForResult, 1, SharedResource.country);
			this.bls.setCellData(this.getPromotionId(), rowNumberForResult, 2, SharedResource.country);
			this.bls.setCellData(SharedResource.upc, rowNumberForResult, 3, SharedResource.country);
			this.bls.setCellData((this.tsvalues), rowNumberForResult, 4, SharedResource.country);
			this.bls.setCellData((this.tsvaluesStore), rowNumberForResult++, 5, SharedResource.country);
			// write json data to xls

			this.bls.setCellData(scenario.getName(), rowNumberForJsonDate, 1, "TS_" + SharedResource.country);
			this.bls.setCellData(this.getPromotionId(), rowNumberForJsonDate, 2, "TS_" + SharedResource.country);
			this.bls.setCellData(SharedResource.jsondata, rowNumberForJsonDate++, 3, "TS_" + SharedResource.country);

			System.out.println("GET DONE");
			SharedResource.promotionId = "";
			SharedResource.upc = "";
			SharedResource.startDate = "";
			SharedResource.stopDate = "";
			SharedResource.promotionStatus = "";
			BLCounter++;
		} else if (Status == "failed") {
			boolean orgScreenshotValue = captureDebugScreenshot;
			captureDebugScreenshot = true;
			addScreenshotToScreen(scenario.getName());
			captureDebugScreenshot = orgScreenshotValue;
			logError(scenario);
			SharedResource.promotionId = "";
			SharedResource.upc = "";
			SharedResource.startDate = "";
			SharedResource.stopDate = "";
			SharedResource.promotionStatus = "";

		}
		Log.info("**********************************End Scenario" + scenario.getName()
				+ "***********************************");
		driver.quit();
		driver = null;
		Runtime.getRuntime().exec("taskkill /F /T /IM chrome.exe'");
		Runtime.getRuntime().exec("taskkill /F /T /IM chromedriver.exe'");
		Thread.sleep(15 * 1000);

	}

	private static void logError(Scenario scenario) {

		Field field = FieldUtils.getField(((ScenarioImpl) scenario).getClass(), "stepResults", true);
		field.setAccessible(true);
		try {
			ArrayList<Result> results = (ArrayList<Result>) field.get(scenario);
			for (Result result : results) {
				if (result.getError() != null) {
					Throwable err = result.getError();
					Log.error("Error Scenario: {} {}", scenario.getId(), err.getMessage());
					StringBuilder stackTrace = new StringBuilder();
					StackTraceElement[] stacks = err.getStackTrace();
					for (StackTraceElement stack : stacks) {
						stackTrace.append(stack.toString());
					}
					Log.error("Stack Trace {}", stackTrace.toString());
				}
			}
		} catch (Exception e) {
			Log.error("Error while logging error", e);
		}
	}

	private void addingScreenShot(Scenario scenario) throws IOException, InterruptedException {
		if (!captureDebugScreenshot) {
			return;
		}
		Thread.sleep(10 * 1000);
		File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		File destinationPath = new File(System.getProperty("user.dir") + "/target/cucumber-reports/"
				+ scenario.getName().replace(" ", "_").substring(2) + ".png");

		Files.copy(sourcePath, destinationPath);

		// This attach the specified screenshot to the test
		Reporter.addScreenCaptureFromPath(
				"./cucumber-reports/" + scenario.getName().replace(" ", "_").substring(2) + ".png");
	}

	public void addScreenshotToScreen(String imageName) throws InterruptedException, IOException {
		if (!captureDebugScreenshot) {
			return;
		}
		Thread.sleep(5 * 1000);
		File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		File destinationPath = new File(
				System.getProperty("user.dir") + "/target/cucumber-reports/" + imageName + ".png");
		Files.copy(sourcePath, destinationPath);

		Reporter.addStepLog("<img src=\"./cucumber-reports/" + imageName + ".png" + "\">");
	}

	public static void SwitchToCurrentWindowAfterClosingPreviousWindows(WebDriver driver) {
		String[] handles = driver.getWindowHandles().toArray(new String[0]);
		int size = handles.length - 1;
		System.out.println("window count " + size);
		for (int count = 0; count < size; count++) {
			System.out.println("Close window");
			driver.switchTo().window(handles[count]);
			driver.close();
		}
		driver.switchTo().window(handles[size]);
	}

	HashMap<String, String> promotionDetailMap = new HashMap<String, String>();
	String BL = "";

	public void AddScenarioSpecificMap(String key, String value) {
		promotionDetailMap.put(key, value);

	}

	public Data getData(String PromotionName, String PromotionId, String testcaseid) throws IOException {

		// Populate the rest of teh values from the promotion json object
		populatePromotionDetailMap(PromotionId, promotionDetailMap);
		System.out.println("detaile map:" + promotionDetailMap);
		if (data == null) {
			com.catalina.vault.intergration.PromotionDetail details = new com.catalina.vault.intergration.PromotionDetail(
					this.getPromotionId(), this.GetPromotionName());

			ExecutionScript script = new ExecutionScript(getTsScript(PromotionName, promotionDetailMap, "awards"),getTsScript(PromotionName, promotionDetailMap, "store"));

			TargetProcess tp = new TargetProcess();
			String testplanid = String.valueOf(tp.getTestPlanId());
			String tetplanbuildid = String.valueOf(new TargetProcess().getBuild());
			if (this.GetCountry().equalsIgnoreCase("USA")) {
				data = new Data(details, script, new StoreDetail("8859", "389", "34", "USA", "CMC6"),
						new TargetProcessDetail(testplanid, tetplanbuildid, testcaseid));

			} else if (this.GetCountry().equalsIgnoreCase("France")) {
				data = new Data(details, script, new StoreDetail("1413", "014", "04", "FRA", "CMC6"),
						new TargetProcessDetail(testplanid, tetplanbuildid, testcaseid));
			} else {
				data = new Data(details, script, new StoreDetail("6700", "245", "29", "ITA", "CMC6"),
						new TargetProcessDetail(testplanid, tetplanbuildid, testcaseid));
				// data = new Data(details, script, new StoreDetail("8859",
				// "389", "34", "USA", "CMC6"),
				// new TargetProcessDetail(testplanid, String.valueOf(new
				// TargetProcess().getBuild()), testcaseid));
			}
		}
		return data;
	}

	/**
	 * 
	 * @param PromotionName
	 * @param promotionMap
	 *            This will have the prompotion field map
	 * @return
	 * @throws IOException
	 */
	public String getTsScript(String PromotionName, HashMap<String, String> promotionDetailMap, String awardsOrStore)
			throws IOException {
		// Please add this to a logger. lets stop using SOP
		Log.info("name:" + PromotionName);
		String ts = null;
		String tsvalues = ts;
		
		try{// Read the string from the
		if ("awards".equals(awardsOrStore)) {
			// Have updated the getCellValue method to take the column number as
			// well
			ts = this.bls.getCellValue(PromotionName, "TS_" + this.GetCountry(), 2); 
			
		} else {
			ts = this.bls.getCellValue(PromotionName, "TS_" + this.GetCountry(), 3); 		
			}
		System.out.println("ts value from the excel before sanitizing: " + ts);
		}catch(NullPointerException e){
			System.out.println("Excel" +e);
		}
		// Loop through the string ts and get all the content that start and end
		// with a <>
		if(ts != null)
		{
			tsvalues = ts;
			while (tsvalues.contains("<") & tsvalues.contains(">")) {
				// we can use regular expressions to get all the keys from "ts" that
				// are between <> like <UPC>, <QTY> etc.,
				String key = tsvalues.substring(tsvalues.indexOf('<') + 1, tsvalues.indexOf('>'));
				System.out.println("Key identifed : " + key);
				// for every such key, get the value form the hashmap and replace
				// the string with the value from hashmap
				tsvalues = tsvalues.replace("<" + key + ">", promotionDetailMap.get(key));
				System.out.println("Key replaced : " + tsvalues);
	
			}
			
			if ("store".equals(awardsOrStore)) {
				this.settsValuesStore(tsvalues);
				System.out.println("store ts values " + tsvalues);
			} else {
				this.settsValues(tsvalues);
				System.out.println("awards ts values " + tsvalues);
			}
			
			Log.info("ts value after sanitizing: " + tsvalues);
			Reporter.addStepLog("TS script for " + PromotionName + " is : " + tsvalues);
		}
		return tsvalues;
}
	

	/**
	 * @Rashmi there need not be a separate method for store since it is just a
	 *         replacement of the key fields
	 * @return
	 */
	private String getTsScriptForStore() {
		return "";
	}

	/**
	 * 
	 * @param promotionId
	 * @param activationTriggerType
	 *            Based on this type, we will know which field in the activation
	 *            trigger json Object, we have to pick our values
	 * 
	 * @Rashmi - we may have to add more params based on the field that we may
	 *         have to pick up. so you can add varargs type param as well
	 * @return
	 */
	public HashMap<String, String> populatePromotionDetailMap(String PromotionId,
			HashMap<String, String> promotionDetailMap) {
		PropertiesFile envProps = new PropertiesFile("environment");
		String country = this.GetCountry().substring(0, 3).toUpperCase();
		if (country.equalsIgnoreCase("USA")) {
			String vaultUrl = envProps.getProperty("AWS_DMP");
			this.setvaultUrl(vaultUrl);
		} else {
			String vaultUrl = envProps.getProperty("AZURE_DMP");
			this.setvaultUrl(vaultUrl);
		}
		// String vaultUrl = envProps.getProperty("AZURE_DMP");
		String getPromoEndpoint = this.getvaultUrl() + country + "-BLIP-" + PromotionId;
		// String getPromoEndpoint =vaultUrl +"USA-BLIP-BL_5198_5851";
		String response = NetworkUtils.getCall(getPromoEndpoint);
		com.google.gson.JsonElement element = JsonUtils.convertToJsonElement(response);

		// Iterate through the child objects to get the specific value
		System.out.println("element" + element);
		JsonObject experience = (JsonObject) ((JsonObject) element).get("experience");
		JsonObject triggers = (JsonObject) experience.get("triggers");
		JsonArray activationTriggers = triggers.get("activation_triggers").getAsJsonArray();

		String Qty = "";
		String Amt = "";
		int betweenQty;
		int betweenAmt;

		String purchase_measure = "";
		if (activationTriggers.size() > 0) {
			JsonObject firstActTrigger = activationTriggers.get(0).getAsJsonObject();
			if (firstActTrigger.get("purchase_measure") != null) {
				JsonElement purchaseMeasure = firstActTrigger.get("purchase_measure");
				String purchase_Measure = purchaseMeasure.getAsString();
				if (purchase_Measure.equalsIgnoreCase("Units")) {
					JsonObject value = (JsonObject) firstActTrigger.get("value");
					if (value != null) {
						Qty = value.get("min").getAsString();
						System.out.println(Qty);
						if (Qty.equalsIgnoreCase("0.0")) {
							String Qty1 = "1";
							promotionDetailMap.put("Qty", Qty1);
							this.setPromotionmin(Qty1);
						}

						else {
							promotionDetailMap.put("Qty", Qty.replace(".0", ""));
							this.setPromotionmin(Qty);
						}
					}

				} else {
					JsonObject value = (JsonObject) firstActTrigger.get("value");
					if (value != null) {
						Amt = value.get("min").getAsString();
						String strAmt = String.valueOf(Double.parseDouble(Amt) * 100);
						promotionDetailMap.put("Amt", strAmt.replace(".0", ""));
						this.setSpendAmt(strAmt.replace(".0", ""));
					}
				}
				if (firstActTrigger.get("regex").getAsJsonArray().size() > 0) {
					JsonArray regrex = firstActTrigger.get("regex").getAsJsonArray();
					String UPC = regrex.get(0).getAsString();
					promotionDetailMap.put("UPC", UPC);
					this.setSpendUPC(UPC);
				} else {
					DateFormat dateFormat = new SimpleDateFormat("ddMMyyHHm");
					Date date = new Date();
					System.out.println(dateFormat.format(date));
					String today = dateFormat.format(date);
					String UPC = today;
					promotionDetailMap.put("UPC", UPC);
				}
			}
		}

		if (activationTriggers.size() > 1) {
			JsonObject secondActTrigger = activationTriggers.get(1).getAsJsonObject();
			if (secondActTrigger.get("purchase_measure") != null) {
				JsonElement purchaseMeasure = secondActTrigger.get("purchase_measure");
				String purchase_Measure = purchaseMeasure.getAsString();
				if (purchase_Measure.equals("Units")) {
					JsonObject value2 = (JsonObject) secondActTrigger.get("value");
					if (value2 != null) {
						Qty = value2.get("min").getAsString();
						promotionDetailMap.put("Qty", Qty.replace(".0", ""));
						this.setPromotionmin(Qty);
					}
					if (secondActTrigger.get("regex").getAsJsonArray().size() > 0) {
						JsonArray regrex = secondActTrigger.get("regex").getAsJsonArray();
						String UPC = regrex.get(0).getAsString();
						promotionDetailMap.put("UPC", UPC);
						this.setunitsUPC(UPC);
					} else {
						DateFormat dateFormat = new SimpleDateFormat("ddMMyyHHm");
						Date date = new Date();
						System.out.println(dateFormat.format(date));
						String today = dateFormat.format(date);
						String UPC = today;
						promotionDetailMap.put("UPC", UPC);
					}
				} else if (purchase_Measure.equals("Spend")) {
					JsonObject value = (JsonObject) secondActTrigger.get("value");
					if (value != null) {
						Amt = value.get("min").getAsString();
						String strAmt = String.valueOf(Double.parseDouble(Amt) * 100);
						promotionDetailMap.put("Amt", strAmt.replace(".0", ""));
					}

				}

			} else if (secondActTrigger.get("tender") != null) {
				JsonObject value3 = (JsonObject) secondActTrigger.get("value");
				if (value3 != null) {
					Amt = value3.get("min").getAsString();
					String strAmt = String.valueOf(Double.parseDouble(Amt) * 100);
					promotionDetailMap.put("Amt", strAmt.replace(".0", ""));
				}
			} else {
				Qty = "1";
				if (!promotionDetailMap.containsKey("Qty")) {
					promotionDetailMap.put("Qty", Qty);
				}
				DateFormat dateFormat = new SimpleDateFormat("ddMMyyHHm");
				Date date = new Date();
				System.out.println(dateFormat.format(date));
				String today = dateFormat.format(date);
				String UPC = today;
				if (!promotionDetailMap.containsKey("UPC")) {
					promotionDetailMap.put("UPC", UPC);
				}
			}
		}

		if (!promotionDetailMap.containsKey("Amt")) {
			Amt = "1";
			promotionDetailMap.put("Amt", Amt);
		}

		if (!promotionDetailMap.containsKey("UPC")) {
			DateFormat dateFormat = new SimpleDateFormat("ddMMyyHHm");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			String today = dateFormat.format(date);
			String UPC = today;
			promotionDetailMap.put("UPC", UPC);
		}

		if (promotionDetailMap.containsKey("betweenQty")) {
			Qty = com.catalina.vault.steps.SharedResource.getPromotionmin();
			System.out.println("qty:" + Qty);
			betweenQty = Integer.parseInt(Qty.replace(".0", "")) + 1;
			promotionDetailMap.put("betweenQty", String.valueOf(betweenQty));
			System.out.println("betweenQty" + betweenQty);
		}

		if (promotionDetailMap.containsKey("betweenAmt")) {
			Amt = this.getamt();
			betweenAmt = Integer.parseInt(Amt) + 1000;
			promotionDetailMap.put("betweenAmt", String.valueOf(betweenAmt));
			System.out.println("betweenAmt" + betweenAmt);
		}

		if (promotionDetailMap.containsKey("SpendUPC")) {
			String SpendUPC = com.catalina.vault.steps.SharedResource.getSpendUPC();
			promotionDetailMap.put("SpendUPC", SpendUPC);
			System.out.println("SpendUPC" + SpendUPC);
		}

		if (promotionDetailMap.containsKey("SpendAmt")) {
			String SpendAmt = com.catalina.vault.steps.SharedResource.getamt();
			promotionDetailMap.put("SpendAmt", SpendAmt);
			System.out.println("SpendAmt" + SpendAmt);
		}

		if (promotionDetailMap.containsKey("UnitsUPC")) {
			String UnitsUPC = com.catalina.vault.steps.SharedResource.getunitsUPC();
			promotionDetailMap.put("UnitsUPC", UnitsUPC);
			System.out.println("UnitsUPC" + UnitsUPC);
		}

		if (!promotionDetailMap.containsKey("Qty")) {
			Qty = "1";
			promotionDetailMap.put("Qty", Qty);
		}

		if (!promotionDetailMap.containsKey("jUPC")) {
			DateFormat dateFormat = new SimpleDateFormat("ddMMyyHHm");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			String today = dateFormat.format(date);
			String jUPC = today;
			promotionDetailMap.put("jUPC", jUPC);
		}

		return promotionDetailMap;
	}

}
