package com.catalina.vault.steps;

import java.io.File;
import java.nio.file.FileSystems;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mortbay.log.Log;
import org.openqa.selenium.WebDriver;

import com.catalina.vault.pagefactory.AdPage;
import com.catalina.vault.pagefactory.MainPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class CommonActions {

	WebDriver driver;
	AdPage page;
	private final SharedResource sharedResource;
	private static final Logger LOG = LogManager.getLogger(CommonActions.class);
	public CommonActions(SharedResource sharedResource) {
		this.driver = sharedResource.init();
		page = new AdPage(this.driver);
		this.sharedResource = sharedResource;
	}

	@Then("^: Go back and save go\\.$")
	public void go_back_and_save_go() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		this.page.clickSettingButton();

		Thread.sleep(2 * 1000);

		this.page.clickSaveButton();

		Thread.sleep(25 * 1000);

	}

	@Then("^: Go to targeting tab\\.$")
	public void go_to_targeting_tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		this.page.scrollToTop();
		Thread.sleep(3 * 1000);
		this.page.clickTargetingButton();
	}
	
	@Then("^: Change region to \"([^\"]*)\"$")
	public void change_region_to(String country) throws Throwable 
	{
		// Write code here that turns the phrase above into concrete actions		
		MainPage page = new MainPage(this.driver);
		//Thread.sleep(2*1000);
		page.waitforLoading("Login");

		page.clickOnRegion();
		System.out.println("Click on region");
		Log.debug("Click on region");
		page.selectCountry(country);
		Log.debug("selectCountry");
		//Thread.sleep(2*1000);
		page.waitforLoading("selectCountry");
		
		this.sharedResource.setCountry(country);
		
	}
	
	
	


}
