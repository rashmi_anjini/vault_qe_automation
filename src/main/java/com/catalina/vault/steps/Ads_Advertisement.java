package com.catalina.vault.steps;

import java.nio.file.FileSystems;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.vault.pagefactory.AdGallaryPage;
import com.catalina.vault.pagefactory.AdPage;
import com.catalina.vault.pagefactory.AdsListPage;
import com.catalina.vault.pagefactory.AdvertisementAdPage;
import com.catalina.vault.pagefactory.AdvertisementSettingPage;
import com.catalina.vault.pagefactory.MainPage;
import com.catalina.vault.pagefactory.ManufacturerAdPage;
import com.catalina.vault.pagefactory.RetailerCouponPage;
import com.catalina.vault.utils.ScreenShot;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Then;

public class Ads_Advertisement {

	WebDriver driver;
	AdsListPage adsListPage;
	AdPage adPage;
	RetailerCouponPage couponPage;
	ManufacturerAdPage manufacturerAdPage;
	MainPage page;
	
	public Ads_Advertisement(SharedResource sharedResourse) {
		this.driver = sharedResourse.init();
		adsListPage = new AdsListPage(this.driver);
		this.adPage = new AdPage(this.driver);
		this.couponPage = new RetailerCouponPage(this.driver);
		this.page = new MainPage(this.driver);
		this.manufacturerAdPage = new ManufacturerAdPage(this.driver);
	}

	@Then("^: Create ad using Ad Galary with Advertisement$")
	public void create_ad_using_Ad_Galary_with_Advertisement() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Creating a Ad");
		Reporter.addStepLog("Clicking on Ad Group");
		page.waitforLoading("Clicking on Ad Group");
		adsListPage.clickAdGallery();
		page.waitforLoading("clickAdGallery");

		AdvertisementAdPage adsPage = new AdvertisementAdPage(this.driver);
System.out.println("in gallery");
		adsPage.clickOnAddToCart();
		adsPage.clickOnNext();
		page.waitforLoading("click next");
		
		Thread.sleep(5 * 1000);
	}
	
	@Then("^: Fill ad form for advertisement with \"([^\"]*)\"$")
	public void fill_ad_form_advertisement_with(String image) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		page.waitforLoading("ad page advertisement");
		
		WebDriverWait wait= new WebDriverWait(driver,30);
		boolean status= wait.until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		
		boolean isTheTextPresent = driver.getPageSource().contains("Required Settings");
		System.out.println("Is element present on page:" +isTheTextPresent);
		Thread.sleep(3*1000);
		
		Reporter.addStepLog("Clicking startDate and stopDate button");
		couponPage.StartDate(-1);
		//Thread.sleep(3*1000);
		
		Reporter.addStepLog("Clicking stopDate button");
		couponPage.EndDate(6);
		//Thread.sleep(3*1000);
		
		//Reporter.addStepLog("Clicking unlimited checkbox");
		//couponPage.clickunlimitedChecbox();
		//Thread.sleep(3*1000);
		
	
		Reporter.addStepLog("Entering channel : In-store");
		couponPage.typeChannel("In-store");
		
		
		Reporter.addStepLog("Entering static image :  image.jpg");
		couponPage.addStaticImage(FileSystems.getDefault().getPath(image).toAbsolutePath().toString());

		Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Save static image");
		couponPage.clickSaveStaticImage();
		Thread.sleep(3 * 1000);
			
		page.waitforLoading("clickSaveStaticImage");

		Reporter.addStepLog("Clicking on Save");
		couponPage.clickSaveButton();

		page.waitforLoading("Clicking on Save");
		
		if((couponPage.alertOkClick())==true)
		{
			couponPage.clickalertAccept();
		}
		
		Reporter.addStepLog("Scrolling to Top");
		couponPage.scrollToTop();
		
		page.waitforLoading("Clicking on Save");
		
		Reporter.addStepLog("Clicking on targeting tab");
		couponPage.clickTargetingButton();
	
	}
	
	


	
	

}
