package com.catalina.vault.steps;

import java.nio.file.FileSystems;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mortbay.log.Log;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.vault.entities.BLsToExcel;
import com.catalina.vault.entities.PerformanceTiming;
import com.catalina.vault.pagefactory.CampaignPage;
import com.catalina.vault.pagefactory.DashBoardPage;
import com.catalina.vault.pagefactory.MainPage;
import com.catalina.vault.pagefactory.NewDisplayNetworkCampaignPage;
import com.catalina.vault.pagefactory.RetailerCouponPage;
import com.catalina.vault.pagefactory.Testing;
import com.catalina.vault.utils.PropertiesFile;
import com.catalina.vault.utils.ScreenShot;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Campaign {
	WebDriver driver;
	CampaignPage campaignPage;
	BLsToExcel bls;
	NewDisplayNetworkCampaignPage campaginForm;
	MainPage page;
	PropertiesFile pFile;
	PropertiesFile propFile;
	SharedResource shareResource;
	DashBoardPage dashboard;
	
	private static final  Logger Log= LogManager.getLogger(Campaign.class);
	
	public Campaign(SharedResource shareResource) {
		// TODO Auto-generated constructor stub
		Reporter.addStepLog("Creating a new AdGroup");
		this.driver = shareResource.init();
		this.bls=new BLsToExcel(this.driver);
		campaignPage = new CampaignPage(this.driver);
		campaginForm = new NewDisplayNetworkCampaignPage(driver);
		this.page = new MainPage(this.driver);
		this.pFile= new PropertiesFile("environment");
		this.propFile= new PropertiesFile("ReRunScenarioNames");
		this.shareResource =shareResource ;
		dashboard = new DashBoardPage(this.driver);
		
	}

	@When("^: Logged in create campaign$")
	public void logged_in_create_campaign() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Creating a new Campaign");
		Reporter.addStepLog("Clicking on New Butoon");
		campaignPage.clickNewButton();
		Log.debug("clickNewButton");
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Display Network Campaign");
		campaignPage.clickDisplayNetworkCampaign();
		Log.debug("clickDisplayNetworkCampaign");
		//Thread.sleep(2*1000);
		page.waitforLoading("clickDisplayNetworkCampaign");
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"Dashboard");
	}

	@When("^: new tab open Switch to that tab$")
	public void new_tab_open_Switch_to_that_tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		String currentTab = driver.getWindowHandle();
		Reporter.addStepLog("Trying to swtich to new tab");
		Log.debug("Trying to swtich to new tab");
		SharedResource.SwitchToCurrentWindowAfterClosingPreviousWindows(driver);
		Reporter.addStepLog("Switched to window current url is "
				+ driver.getCurrentUrl());
		Log.debug("Switched to window current url is "
				+ driver.getCurrentUrl());

	}

	
	@Then("^: Enter form details for new campaign for France \"([^\"]*)\"$")
	public void enter_form_details_for_new_campaign_with_network(String type)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		WebDriverWait wait= new WebDriverWait(driver,80);
		boolean status= wait.until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-YYYY");
		Date date = new Date();
		//System.out.println(dateFormat.format(date));
		String today=dateFormat.format(date);
		if (System.getProperty("BUILD_NAME") == null) {
			String BUILD_NAME_propfile=this.pFile.getProperty("BUILD_NAME");
			this.shareResource.setBuildName(BUILD_NAME_propfile);
			} else {
			String Build_NAME_jenkins=System.getProperty("BUILD_NAME");
			this.shareResource.setBuildName(Build_NAME_jenkins);
			}
		String BUILD_NAME=SharedResource.getBuildName();
		System.out.println("BuildName"+BUILD_NAME);
		Reporter.addStepLog("Entering the campaign name : "+BUILD_NAME );
		campaginForm.typeCampaignName(BUILD_NAME+"_"+type+"_"+today);
		Log.info("Entered the campaign name : "+BUILD_NAME+"_"+type+"_"+today );
		this.shareResource.setCampaignName(BUILD_NAME+"_"+type+"_"+today);
		String key="BUILD_NAME_"+com.catalina.vault.steps.SharedResource.GetCountry()+"_"+type;
		propFile.SetProperty(key, BUILD_NAME+"_"+type+"_"+today);
		String typeKey= "type_"+com.catalina.vault.steps.SharedResource.GetCountry();
		propFile.SetProperty(typeKey, type);		
	
		Reporter.addStepLog("Entering Client Type : Catalina Marketing");
		campaginForm.typeClient("Catalina Marketing");
		//Thread.sleep(2*1000);
		Log.info("Entering Client Type : Catalina Marketing" );
		
		Reporter.addStepLog("clicking Billing Type");
		campaginForm.clickBilling();
		Log.info("clicking Billing Type" );
		
		Reporter.addStepLog("Entering Billing Type : USA-CATS-76620_2 - Prod-code : CF-template-1");
		campaginForm.typeBilling("FRA-CATS-211169_1 - test : test");
		Log.info("Entering Billing Type :FRA-CATS-211169_1 - test : test");
		
		Reporter.addStepLog("Entering channel : In-store");
		campaginForm.typeChannel("In-store");
		Log.info("Entering channel : In-store");
		
		campaginForm.clickTouchPointCustomButton();
		Log.info("Clicking on Touch Point Button");
		Reporter.addStepLog("Clicking on Attribute Button");
		campaginForm.clickSelectAttributeButton();
		Log.info("Clicking on Attribute Button");
		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Entering value for Attribute : network");
		campaginForm.typeAttributeInput("network");
		Log.info("Entering value for Attribute : network");
		Reporter.addStepLog("Clikcing on pick network button");
		campaginForm.clickPickNetworkButton1();
		Log.info("Clikcing on pick network button");
		Reporter.addStepLog("Entering value forr Network : " );
		campaginForm.typeNetworkInput("FRA-0004");
		Log.info("Entering value forr Network :FRA-0004");
		
		//PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"campign page");
	}
	
	
	
	@Then("^: Enter form details for new campaign for GreatBritain with  \"([^\"]*)\"$")
	public void enter_form_details_for_new_campaign_for_GreatBritain_with(String name)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		WebDriverWait wait= new WebDriverWait(driver,30);
		boolean status= wait.until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		//String pageSource = driver.getPageSource();


		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		String today=dateFormat.format(date);
		
		Reporter.addStepLog("Entering the campaign name : Automation Test" +name);
		campaginForm.typeCampaignName(name+": Bitain");
		Thread.sleep(2*1000);
		Reporter.addStepLog("Entering Client Type : ");
		campaginForm.typeClient("GBR-0003");
		Thread.sleep(2*1000);
		
		Reporter.addStepLog("clicking Billing Type");
		//campaginForm.clickBilling();
		//Thread.sleep(3*1000);
		Reporter.addStepLog("Entering Billing Type : USA-CATS-76620_2 - Prod-code : CF-template-1");
		//campaginForm.typeBilling("Catalina Test Sainsbury");
		//campaginForm.typeBilling("USA-CATS-16246_1 - Test 2001/2002 : Test Prizes 2001/2002");
		Thread.sleep(2*1000);
		Reporter.addStepLog("Entering channel : In-store");
		campaginForm.typeChannel("In-store");
		Thread.sleep(2*1000);
		Reporter.addStepLog("Clicking on Touch Point Button");
		campaginForm.clickTouchPointCustomButton();
		Reporter.addStepLog("Clicking on Attribute Button");
		campaginForm.clickSelectAttributeButton();

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for Attribute : network");
		campaginForm.typeAttributeInput("network");
		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clikcing on pick network button");
		campaginForm.clickPickNetworkButton1();
		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value forr Network : " );
		campaginForm.typeNetworkInput("Sainsbury's");
		Thread.sleep(3 * 1000);
	}
	
	@Then("^: Enter form details for new campaign for Italy \"([^\"]*)\"$")
	public void enter_form_details_for_new_campaign_for_Italy_with(String type)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		WebDriverWait wait= new WebDriverWait(driver,30);
		boolean status= wait.until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
		System.out.println(status);
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		String today=dateFormat.format(date);
		if (System.getProperty("BUILD_NAME") == null) {
			String BUILD_NAME_propfile=this.pFile.getProperty("BUILD_NAME");
			this.shareResource.setBuildName(BUILD_NAME_propfile);
			} else {
			String Build_NAME_jenkins=System.getProperty("BUILD_NAME");
			this.shareResource.setBuildName(Build_NAME_jenkins);
			}
		String BUILD_NAME=SharedResource.getBuildName();
		System.out.println("BuildName"+BUILD_NAME);
		
		Reporter.addStepLog("Entering the campaign name : "+BUILD_NAME );
		campaginForm.typeCampaignName(BUILD_NAME+"_"+type+"_"+today);
		Log.info("Entering the campaign name : "+BUILD_NAME+"_"+type+"_"+today);
		this.shareResource.setCampaignName(BUILD_NAME+"_"+type+"_"+today);
		
		String key="BUILD_NAME_"+com.catalina.vault.steps.SharedResource.GetCountry()+"_"+type;
		String typeKey= "type_"+com.catalina.vault.steps.SharedResource.GetCountry();
		propFile.SetProperty(typeKey, type);
		propFile.SetProperty(key, BUILD_NAME+"_"+type+"_"+today);
		Reporter.addStepLog("Entering Client Type : ");
		campaginForm.typeClient("ITA-CATS-3129");
		//Thread.sleep(2*1000);
		
		
		Log.info("Entering Client Type :ITA-CATS-3129 ");
		Reporter.addStepLog("clicking Billing Type");
		campaginForm.clickBilling();
		//Thread.sleep(3*1000);
		Log.info("clicking Billing Type");
		Reporter.addStepLog("Entering Billing Type : USA-CATS-76620_2 - Prod-code : CF-template-1");
		campaginForm.typeBilling("Sun Light Advertising Services Srl");
		Log.info("Entering Billing Type Sun Light Advertising Services Srl");
		Thread.sleep(2*1000);
		System.out.println(new Date().toString() + "Entering channel : In-store"); 
		Reporter.addStepLog("Entering channel : In-store");
		campaginForm.typeChannel("In-store");
		//Thread.sleep(2*1000);
		Log.info("Entering channel : In-store");
		Reporter.addStepLog("Clicking on Touch Point Button");
		campaginForm.clickTouchPointCustomButton();
		Log.info("Clicking on Touch Point Button");
		Reporter.addStepLog("Clicking on Attribute Button");
		campaginForm.clickSelectAttributeButton();
		Log.info("Clicking on Attribute Button");
		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Entering value for Attribute : network");
		campaginForm.typeAttributeInput("network");
		Log.info("Entering value for Attribute : network");
		Reporter.addStepLog("Clikcing on pick network button");
		campaginForm.clickPickNetworkButton1();
		Log.info("Clikcing on pick network button");
		Reporter.addStepLog("Entering value forr Network : " );
		campaginForm.typeNetworkInput("ITA-0029");
		Log.info("Entering value forr Network :ITA-0029");
		//Thread.sleep(2 * 1000);
	}

	@Then("^: Click on Save to create campagin$")
	public void click_on_Save_to_create_campagin() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Clicking on Save");
		//ScreenShot.addScreenshotToScreen(this.driver, "Campaign");
		campaginForm.clickSaveButton();
		Log.debug("clickSaveButton");
		Log.info("clickSaveButton");
		page.waitforLoading("clickSaveButton");
		
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"save campaign");
	}
	
	@Then("^: Enter form details for new campaign for USA with network \"([^\"]*)\" and \"([^\"]*)\"$")
	public void enter_form_details_for_new_campaign_for_USA_with(String network, String type)
			throws Throwable {
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		String today=dateFormat.format(date);
		if (System.getProperty("BUILD_NAME") == null) {
			String BUILD_NAME_propfile=this.pFile.getProperty("BUILD_NAME");
			this.shareResource.setBuildName(BUILD_NAME_propfile);
			} else {
			String Build_NAME_jenkins=System.getProperty("BUILD_NAME");
			this.shareResource.setBuildName(Build_NAME_jenkins);
			}
		String BUILD_NAME=SharedResource.getBuildName();
		System.out.println("BuildName"+BUILD_NAME);
		
		Reporter.addStepLog("Entering the campaign name : "+BUILD_NAME );
		 campaginForm.typeCampaignName(BUILD_NAME+"_"+type+"_"+today);
		 
		 this.shareResource.setCampaignName(BUILD_NAME+"_"+type+"_"+today);
		 String key= "BUILD_NAME_"+com.catalina.vault.steps.SharedResource.GetCountry()+"_"+type;
		 propFile.SetProperty(key, BUILD_NAME+"_"+type+"_"+today);
		 Log.info("Entering the campaign name"+BUILD_NAME+"_"+type+"_"+today);
		
		 propFile.SetProperty(key, BUILD_NAME+"_"+type+"_"+today);
		 String typeKey= "type_"+com.catalina.vault.steps.SharedResource.GetCountry();
		 propFile.SetProperty(typeKey, type);
		 
		
				
		 Reporter.addStepLog("Entering Client Type : ");
		campaginForm.typeClient("CATALINA MARKETING");
		 Log.info("Entering Client Type : ");
		Reporter.addStepLog("clicking Billing Type");
		campaginForm.clickBilling();
		Log.info("clicking Billing Type");
		Reporter.addStepLog("Entering Billing Type : USA-CATS-76620_2 - Prod-code : CF-template-1");
		campaginForm.typeBilling("USA-CATS-76620_2 - Prod-code : CF-template-1");
		Log.info("Entering Billing Type :USA-CATS-76620_2 - Prod-code : CF-template-1");
		//campaginForm.typeBilling("USA-CATS-16246_1 - Test 2001/2002 : Test Prizes 2001/2002");
		//Thread.sleep(2*1000);
		Reporter.addStepLog("Entering channel : In-store");
		campaginForm.typeChannel("In-store");
		Log.info("Entering channel : In-store");
		Reporter.addStepLog("Clicking on Touch Point Button");
		campaginForm.clickTouchPointCustomButton();
		Log.info("Clicking on Touch Point Button");
	    Reporter.addStepLog("Clicking on Attribute Button");
		campaginForm.clickSelectAttributeButton();
		Log.info("Clicking on Attribute Button");
		Thread.sleep(2 * 1000);
		Reporter.addStepLog("Entering value for Attribute : network");
		campaginForm.typeAttributeInput("network");
		Log.info("Entering value for Attribute : network");
		Reporter.addStepLog("Clikcing on pick network button");
		campaginForm.clickPickNetworkButton1();
		Log.info("Clikcing on pick network button");
		Reporter.addStepLog("Entering value forr Network : " );
		campaginForm.typeNetworkInput(network);
		Thread.sleep(2 * 1000);
		Log.info("Entering value forr Network : "+network);
		//PerformanceTiming.writePerfMetricasJSON(this.driver,"C://Catalina//Vault//Demo3.json","Campaign Page");
	}
	
	@Then("^: Enter form details for new campaign for USA with cluster \"([^\"]*)\" and \"([^\"]*)\"$")
	public void enter_form_details_for_new_campaign_for_USA_with_cluster(String type, String cluster)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		String today=dateFormat.format(date);
		
		if (System.getProperty("BUILD_NAME") == null) {
			String BUILD_NAME_propfile=this.pFile.getProperty("BUILD_NAME");
			this.shareResource.setBuildName(BUILD_NAME_propfile);
			} else {
			String Build_NAME_jenkins=System.getProperty("BUILD_NAME");
			this.shareResource.setBuildName(Build_NAME_jenkins);
			}
		String BUILD_NAME=SharedResource.getBuildName();
		System.out.println("BuildName"+BUILD_NAME);
			//Thread.sleep(2*1000);
		
		Reporter.addStepLog("Entering the campaign name : "+BUILD_NAME );
		 campaginForm.typeCampaignName(BUILD_NAME+"_"+type+"_"+today);
		 
		 this.shareResource.setCampaignName(BUILD_NAME+"_"+type+"_"+today);
		 String key= "BUILD_NAME_"+com.catalina.vault.steps.SharedResource.GetCountry()+"_"+type;
		 propFile.SetProperty(key, BUILD_NAME+"_"+type+"_"+today);
		 Log.info("Entering the campaign name"+BUILD_NAME+"_"+type+"_"+today);
		
		 propFile.SetProperty(key, BUILD_NAME+"_"+type+"_"+today);
		 String typeKey= "type_"+com.catalina.vault.steps.SharedResource.GetCountry();
		 propFile.SetProperty(typeKey, type);
		 
		Reporter.addStepLog("Entering Client Type : ");
		campaginForm.typeClient("CATALINA MARKETING");
		Log.info("Entering Client Type : ");
		
		Reporter.addStepLog("clicking Billing Type");
		campaginForm.clickBilling();
		Log.info("clicking Billing Type");
		//Thread.sleep(3*1000);
		Reporter.addStepLog("Entering Billing Type : USA-CATS-76620_2 - Prod-code : CF-template-1");
		campaginForm.typeBilling("USA-CATS-76620_2 - Prod-code : CF-template-1");
		Log.info("Entering Billing Type :");
		//campaginForm.typeBilling("USA-CATS-16246_1 - Test 2001/2002 : Test Prizes 2001/2002");
		//Thread.sleep(2*1000);
		Reporter.addStepLog("Entering channel : In-store");
		campaginForm.typeChannel("In-store");
		Log.info("Entering channel : In-store");
		//Thread.sleep(2*1000);
		Reporter.addStepLog("Clicking on Touch Point Button");
		campaginForm.clickTouchPointCustomButton();
		Log.info("Clicking on Touch Point Button");
	    Reporter.addStepLog("Clicking on Attribute Button");
		campaginForm.clickSelectAttributeButton();
		Log.info("Clicking on Attribute Button");
		Thread.sleep(2 * 1000);
		Reporter.addStepLog("Entering value for Attribute : network");
		campaginForm.typeAttributeInput("cluster");
		//Thread.sleep(2 * 1000);
		Log.info("Entering value for Attribute : network");
		Reporter.addStepLog("Clikcing on pick network button");
		campaginForm.clickpickclusterButton();
		Log.info("Clikcing on pick network button");
		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Entering value forr Network : " );
		campaginForm.typeNetworkInput(cluster);
		Log.info("Entering value forr Network :"+cluster);
		//Thread.sleep(2 * 1000);
	}
	
	@When("^: Logged in create Simulation campaign$")
	public void logged_in_create_Simulation_campaign() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Creating a new Campaign");
		Reporter.addStepLog("Clicking on New Butoon");
		campaignPage.clickNewButton();
		Log.debug("clickNewButton");
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on ExperimentalCampaign");
		campaignPage.clickExperimentalCampaign();
		Log.debug("clickExperimentalCampaign");
		//Thread.sleep(2*1000);
		page.waitforLoading("clickExperimentalCampaign");
		PerformanceTiming.writePerfMetricasJSON(this.driver,"C://Catalina//Vault//Demo3.json","Dashboard");
	}
	
	@Then("^: Click on campaign$")
	public void click_on_campaign()throws Throwable {
	page.waitforLoading("click on campaign");
	Reporter.addStepLog("Search for campaign Name" );
	dashboard.clickCampaign();
	page.waitforLoading("click on campaign");
	}
	
	
@Then("^: Search for the Campaign name$")
	public void search_for_campaign_name()throws Throwable {
	
	
	page.waitforLoading("Search for campaign Name");
	Reporter.addStepLog("Search for campaign Name" );
	System.out.println("country"+com.catalina.vault.steps.SharedResource.GetCountry());
	String CampaignNametype=propFile.getProperty("type_"+com.catalina.vault.steps.SharedResource.GetCountry());
	String CampaignName= "BUILD_NAME_"+com.catalina.vault.steps.SharedResource.GetCountry()+"_"+CampaignNametype;
	System.out.println("camp name"+CampaignName);
	String SearchCampName=propFile.getProperty("BUILD_NAME_"+com.catalina.vault.steps.SharedResource.GetCountry()+"_"+CampaignNametype);
	campaginForm.typesearchBox(SearchCampName);
	System.out.println("path:"+SearchCampName);
	page.waitforLoading("Search for campaign Name");
	
	
	Reporter.addStepLog("Slect first campaign from the list" );
	campaginForm.clickfirstcampaignelement();
	Log.debug("select first campaign from list ");
	page.waitforLoading("select first campaign from list ");
	Thread.sleep(2*1000);
	
//	page.waitforLoading();
//	Reporter.addStepLog("Search for campaign Name" );
//	campaginForm.typesearchBox(com.catalina.vault.steps.SharedResource.getCampaignName());
//	System.out.println("path:"+com.catalina.vault.steps.SharedResource.getCampaignName());
//	Log.debug("Enter Campaign name" +com.catalina.vault.steps.SharedResource.getCampaignName());
//	page.waitforLoading();
//	
//	
//	Reporter.addStepLog("Slect first campaign from the list" );
//	campaginForm.clickfirstcampaignelement();
//	Log.debug("select first campaign from list ");
//	page.waitforLoading();
//	Thread.sleep(2*1000);
//	
	
}
}
