package com.catalina.vault.steps;

import java.nio.file.FileSystems;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.vault.entities.BLsToExcel;
import com.catalina.vault.entities.PerformanceTiming;
import com.catalina.vault.pagefactory.AdPage;
import com.catalina.vault.pagefactory.AdvertisementItemPurchasePage;
import com.catalina.vault.pagefactory.MainPage;
import com.catalina.vault.pagefactory.RetailerCouponPage;
import com.catalina.vault.pagefactory.RetailerManualListPage;
import com.catalina.vault.pagefactory.SecondCriteria;
import com.catalina.vault.pagefactory.TargetingPage;
import com.catalina.vault.utils.NetworkUtils;
import com.catalina.vault.utils.PropertiesFile;
import com.cucumber.listener.Reporter;
import cucumber.api.Scenario;
import cucumber.api.java.en.Then;


public class Targeting {

	WebDriver driver;
	TargetingPage targetingPage;
	BLsToExcel bls;
	AdPage adPage;
	SecondCriteria secondCriteria;
	SharedResource sharedResource;
	Scenario scenario;
	MainPage page;
	RetailerCouponPage couponPage;
	private PropertiesFile propertiesFile;
	private static String PROPERTY_FILE_NAME = "environment";
	private PropertiesFile propFile;
	private static final Logger Log= LogManager.getLogger(Targeting.class);
	static Random rand = new Random();
	static long endNumber = 100000000L + ((long) rand.nextInt(9000000) * 100);
	
	public Targeting(SharedResource sharedResource) {
		this.driver = sharedResource.init();
		this.targetingPage = new TargetingPage(this.driver);
		this.bls=new BLsToExcel(this.driver);
		this.adPage = new AdPage(this.driver);
		this.secondCriteria = new SecondCriteria(this.driver);
		this.sharedResource = sharedResource;
		this.page = new MainPage(this.driver);
		this.propertiesFile = new PropertiesFile(PROPERTY_FILE_NAME);
		this.propFile=new PropertiesFile("ReRunScenarioNames");
		this.couponPage = new RetailerCouponPage(this.driver);
	}
	
	Integer get_Manual_List()
	{
		String key = System.getenv().get("COMPUTERNAME");
		String value = this.propertiesFile.getProperty(key);
		if(value == null)
		{
			value = this.propertiesFile.getProperty("ManualList");
			key = "ManualList";
		}
		System.out.println("Node Value: "+value);
		int nodeManualList = Integer.parseInt(value);
		String countryKey = com.catalina.vault.steps.SharedResource.GetCountry()+ "ManualList";
		System.out.println(countryKey);
		int countryManualList = Integer.parseInt(this.propertiesFile.getProperty(countryKey));
		System.out.println("Country Value: "+countryManualList);
		int manualList = nodeManualList + countryManualList;
		Reporter.addStepLog("Entering the value for input for manual list"+manualList);
		this.propertiesFile.SetProperty(key, String.valueOf(nodeManualList+1));
		return manualList;
	}
	
	@Then("^: Add Targeting group with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_group(String condition, String value)
			throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		targetingPage.clickAddTiggerGroup();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value);
		targetingPage.typeInputUnitTextBox(value);

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickProductAttributeButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : upc/gtin/plus");
		targetingPage.typeProductAttributeTextBox("upc/gtin/plus");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on list source");
		targetingPage.clickListSourceButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for list source : list manager");
		targetingPage.typeListSourceTextBox("list manager");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value for Input manager krishcoke");
		targetingPage.typeInputForManager("Krishcoke");

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clickTriggerScopeButton();

		Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");

	}

	@Then("^: Add Targeting group with User Identifier$")
	public void add_Targeting_group_With_UserIdentifer() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		targetingPage.clickAddTiggerGroup();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type audience segment");
		targetingPage.typeSelectTypeTextBox("Audience segment");

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Operation Button");
		targetingPage.clickOperationButton();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for operation in");
		targetingPage.typeSelectTypeTextBox("in");

		Thread.sleep(1 * 1000);

		targetingPage.typeAudienceSegmentInput("Krishwalgreensazure");

	}
	
	@Then("^: Add Payment Method \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Payment_Method(String payment, String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicked on Add Criteria Button");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicked on Select Type Button");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type Payment Method");
		targetingPage.typeSelectTypeTextBox("Payment Method");
		Log.info("Select type Payment Method ");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for text box ");
		targetingPage.typeInputTextBox(payment);
		Log.info("Enter value for text box" + payment);
		
		Reporter.addStepLog("Clicking on total order condition button");
		targetingPage.clickPaymentConditionButton();
		Log.info("Clicking on total order condition button");

		Reporter.addStepLog("Entering value for condition : between");
		targetingPage.typepaymentConditionTextBox(condition);
		Log.info("Entering value for condition : between");

		Reporter.addStepLog("Entering the number : 1");
		targetingPage.paymentInput(value);
		Log.info("Entering the number :" + value);

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Scope Button");
		targetingPage.clicksecondtriggerScopeButton();
		Log.info("Clicked on Scope Button");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger Scope : include");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("select Scope single session/trip");

		// Thread.sleep(3 * 1000);
	}
	
	
	@Then("^: Add Targeting group with Total Order first \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_group_With_TotalOrder_first(String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
			//Thread.sleep(3 * 1000);
			Log.info("Clicked on trigger button");
		}
		
		//Reporter.addStepLog("Clicking on Add Trigger Button");
		//targetingPage.clickAddTiggerGroup();

		Reporter.addStepLog("Clicking on add criteria button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicking on add criteria button");
		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Select Type button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicking on Select Type button");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering vlaue for select type : total order");
		targetingPage.typeSelectTypeTextBox("Total Order");
		Log.info("Entering vlaue for select type : total order");

		//Thread.sleep(3 * 1000);
//		Reporter.addStepLog("Clicking on total order measure button");
//		targetingPage.clickTotalOrderMeasureButton();
//		Log.info("Clicking on total order measure button");
//
//		//Thread.sleep(3 * 1000);
//		Reporter.addStepLog("Entering value for measure : total spend");
//		targetingPage.typeSelectTypeTextBox("total spend");
//		Log.info("Entering value for measure : total spend");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order condition button");
		targetingPage.clickTotalOrderConditionButton();
		Log.info("Clicking on total order condition button");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for condition : between");
		targetingPage.typeConditionTextBox(condition);
		Log.info("Entering value for condition : between");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering the number : 1");
		targetingPage.typeTotalOrderValue3(value);
		Log.info("Entering the number :"+value);

		//Thread.sleep(1 * 1000);
		//Reporter.addStepLog("Entering the second number : 10");
		//targetingPage.typeTotalOrderValue2("10");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on total order trigger scope");
		targetingPage.clickTotalOrderTriggerScope();
		Log.info("Clicking on total order trigger scope");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for trigger scope single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("Entering value for trigger scope single session/trip");

	}
	
	@Then("^: Add Targeting group with Total Order audiencesegment with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_group_With_TotalOrder_audiencesegment_with(String Conditionorder, String quantityorder) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
			Log.info("Clicked on trigger button");
			//Thread.sleep(3 * 1000);
		}
		
		

		Reporter.addStepLog("Clicking on add criteria button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicked on add criteria button");
		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Select Type button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicked on Select Type button");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering vlaue for select type : total order");
		targetingPage.typeSelectTypeTextBox("Total Order");
		Log.info("Entering vlaue for select type : total order");

		//Thread.sleep(3 * 1000);
//		Reporter.addStepLog("Clicking on total order measure button");
//		targetingPage.clicktotalOrderMeasureButtonaudiencesegment();
//		Log.info("Clicking on total order measure button");
//
//		//Thread.sleep(3 * 1000);
//		Reporter.addStepLog("Entering value for measure : total spend");
//		targetingPage.typeSelectTypeTextBox("total spend");
//		Log.info("Entered value for measure : total spend");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order condition button");
		targetingPage.clicktotalOrderConditionButtonaudiencesegment();
		Log.info("Clicking on total order condition button");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for condition : between");
		targetingPage.typeConditionTextBox(Conditionorder);
		Log.info("Entered value for condition : between");
		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering the number : 1");
		targetingPage.typeTotalOrderValue(quantityorder);
		Log.info("Entering the number : "+quantityorder);


		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on total order trigger scope");
		targetingPage.clicktotalOrderTriggerScopeaudienceseg();
		Log.info("Clicked on total order trigger scope");


		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for trigger scope single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("Entering value for trigger scope single session/trip");

	}

	@Then("^: Add Targeting group with Total Order with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_group_With_TotalOrder_with(String Conditionorder, String quantityorder) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
			Log.info("clicked on trigger group");
		//	Thread.sleep(3 * 1000);
		}
		
		

		Reporter.addStepLog("Clicking on add criteria button");
		targetingPage.clickAddCriteriaButton();
		Log.info("clicked on add criteria button");
		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Select Type button");
		targetingPage.clickSelectTypeButton();
		Log.info("clicked on Select Type button");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering vlaue for select type : total order");
		targetingPage.typeSelectTypeTextBox("Total Order");
		Log.info(" Select Type button Total Order");

		//Thread.sleep(3 * 1000);
//		Reporter.addStepLog("Clicking on total order measure button");
//		targetingPage.clickTotalOrderMeasureButton();
//		Log.info("clicked on Total Order measure button");
//
//		//Thread.sleep(3 * 1000);
//		Reporter.addStepLog("Entering value for measure : total spend");
//		targetingPage.typeSelectTypeTextBox("total spend");
//		Log.info("Entering value for measure : total spend");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order condition button");
		targetingPage.clickTotalOrderConditionButton();
		Log.info("Clicking on total order condition button");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for condition : between");
		targetingPage.typeConditionTextBox(Conditionorder);
		Log.info("Entering value for condition : "+Conditionorder);

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering the number : 1");
		targetingPage.typeTotalOrderValue(quantityorder);
		Log.info("Entered total order value  : "+quantityorder);

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on total order trigger scope");
		targetingPage.clickTotalOrderTriggerScope();
		Log.info("Clicked on total order trigger scope");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for trigger scope single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("Selected on total order trigger scope single session/trip");
		

	}
	@Then("^: Add Targeting group with Total Order third row$")
	public void add_Targeting_group_With_TotalOrder_third() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
			//Thread.sleep(3 * 1000);
		}
		
		

		Reporter.addStepLog("Clicking on add criteria button");
		targetingPage.clickAddCriteriaButton();
		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Select Type button");
		targetingPage.clickSelectTypeButton();

	//	Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering vlaue for select type : total order");
		targetingPage.typeSelectTypeTextBox("Total Order");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order measure button");
		targetingPage.clickTotalOrderMeasureButton();

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for measure : total spend");
		targetingPage.typeSelectTypeTextBox("total spend");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order condition button");
		targetingPage.clickTotalOrderConditionButton();

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for condition : between");
		targetingPage.typeConditionTextBox(">=");
		


		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering the number : 1");
		targetingPage.typetotalOrderValuethird("1");

		//Thread.sleep(1 * 1000);
		//Reporter.addStepLog("Entering the second number : 10");
		//targetingPage.typeTotalOrderValue2("10");

		
		
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order trigger scope");
		targetingPage.clicktotalOrderTriggerScopethird();
	
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for trigger scope single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");

	}

	@Then("^: Add Targeting group with Department codes$")
	public void add_Targeting_groupDepartmentCodes() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		Reporter.addStepLog("Clicking on Add Trigger Group");
		targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on measure button");
		targetingPage.clickMeasureButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for measure : total unitls");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Condiation Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for condiation : >=");
		targetingPage.typeConditionTextBox(">=");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering a number : 1");
		targetingPage.typeInputUnitTextBox("1");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute");
		targetingPage.clickProductAttributeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for product attiribute: department id");
		targetingPage.typeProductAttributeTextBox("department id");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering department codes:12345");
		targetingPage.typeDepartmentCodes("1");
		this.sharedResource.setdepartmentid(String.valueOf("1"));

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on department trigger scope");
		targetingPage.clickDepartmentTriggerScopeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering the value for single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");

	}

	@Then("^: Click on adgroup tab")
	public void Click_on_adgroup_tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		Reporter.addStepLog("scrolling to top");
		adPage.clickadgrouptabclick();
		Thread.sleep(2*1000);
		page.waitforLoading("ad group tab");
		
	}
	
	
	@Then("^: Click on campaign tab")
	public void Click_on_campaign_tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		Reporter.addStepLog("scrolling to top");
		adPage.clickcampaigntabclick();
		Thread.sleep(2*1000);
		page.waitforLoading("clickcampaigntab");
		
	}
	@Then("^: Publish \"([^\"]*)\"$")
	public void publish(String testCaseName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		Thread.sleep(2*1000);
		page.waitforLoading("Clicked save");
		
		Thread.sleep(2 * 1000);
		Reporter.addStepLog("scrolling to top");
		adPage.scrollToTop();
		Log.info("Scroll to Top");

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on setting tab");
		adPage.clickSettingButton();
		Log.info("clicked on setting tab");
		//Thread.sleep(2*1000);
		page.waitforLoading("clicked on setting tab");

		Thread.sleep(2* 1000);
		Reporter.addStepLog("clicking on save button");
		adPage.clickSaveButton();
		Log.info("clicked on save button");
		//Thread.sleep(2*1000);
		page.waitforLoading("clicked on save button");
		
		if((couponPage.alertOkClick())==true)
		{
			couponPage.clickalertAccept();
			Log.info("clicked on alert button");
		}

		Thread.sleep(2 * 1000);
		Reporter.addStepLog("scrolling to top");
		adPage.scrollToTop();
		Log.info("scroll to top");
		
		page.waitforLoading("Publish Button");

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("clicking on publishing button");
		adPage.clickPublishButton();
		Log.info("clicked on Publish");
		//Thread.sleep(2*1000);
		page.waitforLoading("clicking on publishing button");
		
		if((driver.findElements(By.xpath("//div[@role='danger']/ul/li")).size())>0)
		
		{
			String MandatoryErrorMess= driver.findElement(By.xpath("//div[@role='danger']/ul/li")).getText();
			Log.info("Mandatory error message :"+MandatoryErrorMess);
			String Country=this.sharedResource.GetCountry();
			String PromotionName=this.sharedResource.GetPromotionName();
			String BuildName=this.sharedResource.getBuildName();
			com.catalina.slack.Example.slackMessage(MandatoryErrorMess, Country, PromotionName,BuildName);
			throw new Exception();
		}
		
		
		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("clicking yes in dialog box");
		adPage.clickYesThisIsOkButton();
		Log.info("clicked on yes this is ok");
	//	Thread.sleep(2*1000);
		page.waitforLoading("clicked on yes this is ok");
		
		//Thread.sleep(2*1000);
		
		WebDriverWait wait= new WebDriverWait(driver,200);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("start-date-schedule")));
		//ScreenShot.addScreenshotToScreen(this.driver, "FinalImage");
		
		PropertiesFile pFile = new PropertiesFile("PromotionIDs");
		pFile.SetProperty(testCaseName, adPage.getPromotionId());
		Log.info("Promotion id created" +adPage.getPromotionId());

		this.sharedResource.setPromotionId(adPage.getPromotionId());
		
				
		Reporter.addStepLog("Promotion id is :" + adPage.getPromotionId());
		
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"Publish");
	}
	
	@Then("^: Publish Rank with different Bls \"([^\"]*)\"$")
	public void publish_Rank_with_different_Promotion(String testCaseName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		//Thread.sleep(2*1000);
		page.waitforLoading("click settings tab");
		
		Thread.sleep(2 * 1000);
		Reporter.addStepLog("scrolling to top");
		adPage.scrollToTop();
		
		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on setting tab");
		adPage.clickSettingButton();
		
		//Thread.sleep(2*1000);
		page.waitforLoading("click settings tab");

		Thread.sleep(2* 1000);
		Reporter.addStepLog("clicking on save button");
		adPage.clickSaveButton();
		
		//Thread.sleep(2*1000);
		page.waitforLoading("save button");
		
		if((couponPage.alertOkClick())==true)
		{
			couponPage.clickalertAccept();
		}

		Thread.sleep(2 * 1000);
		Reporter.addStepLog("scrolling to top");
		adPage.scrollToTop();
		
		page.waitforLoading("save button");

		Thread.sleep(3 * 1000);
		Reporter.addStepLog("clicking on publishing button");
		adPage.clickPublishButton();
		
		//Thread.sleep(2*1000);
		page.waitforLoading("save button");

		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("clicking yes in dialog box");
		adPage.clickYesThisIsOkButton();

	//	Thread.sleep(2*1000);
		page.waitforLoading("yes this ok button");
		
		//Thread.sleep(2*1000);
		
		WebDriverWait wait= new WebDriverWait(driver,200);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("start-date-schedule")));
		//ScreenShot.addScreenshotToScreen(this.driver, "FinalImage");

		PropertiesFile pFile = new PropertiesFile("PromotionIDs");
		pFile.SetProperty(testCaseName, adPage.getPromotionId());
		

		this.sharedResource.setPromotionId(adPage.getPromotionId()+":_:"+com.catalina.vault.steps.SharedResource.getPromotionId());
		System.out.println("BLS: "+adPage.getPromotionId()+":_:"+com.catalina.vault.steps.SharedResource.getPromotionId());
				
		Reporter.addStepLog("Promotion id is :" + adPage.getPromotionId());
		
		//NetworkUtils.getCall(PROPERTY_FILE_NAME)
	}
	
	@Then("^: Publish test \"([^\"]*)\" and \"([^\"]*)\"$")
	public void publish_test(String testCaseName, String URL) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		Thread.sleep(12 * 1000);
		Reporter.addStepLog("scrolling to top");
		adPage.scrollToTop();
		
		Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on setting tab");
		adPage.clickSettingButton();
		
		Thread.sleep(3 * 1000);
		Reporter.addStepLog("scrolling");
		adPage.scrollTodown();

		Thread.sleep(15 * 1000);
		Reporter.addStepLog("clicking on save button");
		adPage.clickSaveButton();

		Thread.sleep(10 * 1000);
		Reporter.addStepLog("scrolling to top");
		adPage.scrollToTop();

		Thread.sleep(9 * 1000);
		Reporter.addStepLog("clicking on publishing button");
		adPage.clickPublishButton();

		Thread.sleep(6 * 1000);
		Reporter.addStepLog("clicking yes in dialog box");
		adPage.clickYesThisIsOkButton();

		Thread.sleep(6 * 1000);

		//ScreenShot.addScreenshotToScreen(this.driver, "FinalImage");

		PropertiesFile pFile = new PropertiesFile("PromotionIDs");
		pFile.SetProperty(testCaseName, adPage.getPromotionId());

		this.sharedResource.setPromotionId(adPage.getPromotionId());

		Reporter.addStepLog("Promotion id is :" + adPage.getPromotionId());
		
		String country= "USA-BLIP-";
		String BLNumber=country+ adPage.getPromotionId();
		
		
		if( (scenario.getStatus()).equalsIgnoreCase("Passed"));
		{
			NetworkUtils.getCallWithQueryParameter(URL, BLNumber);
		}
	}
	@Then("^: Add department id with \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_department_id_with_and(String condition,String value, String value2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		////Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();

	//	Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value);
		targetingPage.typeInputTextBoxconditionselected(value);

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickproductAttributeButtondeptid();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : department id");
		targetingPage.typeProductAttributeTextBox("department id");

		//Thread.sleep(2 * 1000);
		
		Reporter.addStepLog("Entering the value for Input manager krishcoke");
		targetingPage.typedepartmentidTextBoxsecondrow(value2);
		this.sharedResource.setdepartmentid(String.valueOf(value2));

		//Thread.sleep(3 * 1000);

		
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clicktriggerScopeButtondeptid();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		//Thread.sleep(3*1000);
	}
	
	@Then("^: Add department id with secondrow \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_department_id_with_secondrow_and(String condition,String value, String value2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value);
		targetingPage.typeInputTextBoxconditionselected(value);

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickproductAttributeButtondeptid();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : department id");
		targetingPage.typeProductAttributeTextBox("department id");

		//Thread.sleep(2 * 1000);
		
		Reporter.addStepLog("Entering the value for Input manager krishcoke");
		targetingPage.typedepartmentidTextBoxsecondrowtxtbox(value2);
		this.sharedResource.setdepartmentid(String.valueOf(value2));

		//Thread.sleep(3 * 1000);

		
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clicktriggerScopeButtondeptsecondrow();

		//	Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		Thread.sleep(3*1000);
	}
	
	@Then("^: Add department id with audienceSegment \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_department_id_with_audienceSegment_and(String condition,
			String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

	//	Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

	//	Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickmeasureButtonaudienceSegment();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickconditionButtonaudienceSegment();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value);
		targetingPage.typeInputTextBoxconditionselected(value);

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickproductAttributeButtondeptid();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : department id");
		targetingPage.typeProductAttributeTextBox("department id");

		//Thread.sleep(2 * 1000);
		
		Reporter.addStepLog("Entering the value for Input manager krishcoke");
		targetingPage.typedepartmentidTextBox("1");
		this.sharedResource.setdepartmentid(String.valueOf("1"));

		//Thread.sleep(3 * 1000);

		
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clicktriggerScopeButtondeptid();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		//Thread.sleep(3*1000);
	}
	
	@Then("^: Add department id firstrow with \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_department_id_firstrow_with_and(String condition, String value, String value2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();

		//	Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();
		//
		//	Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value);
		targetingPage.typeInputUnitTextBox(value);

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickproductAttributeButtondeptidfirstrow();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : department id");
		targetingPage.typeProductAttributeTextBox("department id");

		//Thread.sleep(2 * 1000);
		
		Reporter.addStepLog("Entering the value for department id");
		targetingPage.typedepartmentidTextBox(value2);
		this.sharedResource.setdepartmentid(String.valueOf(value2));

		//Thread.sleep(3 * 1000);

		
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clicktriggerScopeButtondeptid();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		//Thread.sleep(3*1000);
	}
	
	@Then("^: Add department id totalspend with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_department_id_totalspend_with_and(String condition,
			String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickTotalSpendButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total spend");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value);
		targetingPage.typeInputUnitTextBox(value);

	//	Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickproductAttributeButtondeptidfirstrow();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : department id");
		targetingPage.typeProductAttributeTextBox("department id");

		//Thread.sleep(2 * 1000);
		
		Reporter.addStepLog("Entering the value for Input manager krishcoke");
		targetingPage.typedepartmentidTextBox("6");
		this.sharedResource.setdepartmentid(String.valueOf("6"));

		//Thread.sleep(3 * 1000);

		
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clicktriggerScopeButtondeptid();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		//Thread.sleep(3*1000);
	}
	
	@Then("^: Add department id firstrow between \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_department_id_firstrow_between(String condition, String value, String value2,String value3) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value);
		targetingPage.typeInputUnitTextBox(value);
		
		Reporter.addStepLog("Entering the value for Input units : " + value2);
		targetingPage.typeInputUnitTextBox2(value2);

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickproductAttributeButtondeptidfirstrow();

	//	Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : department id");
		targetingPage.typeProductAttributeTextBox("department id");

		//Thread.sleep(2 * 1000);
		
		Reporter.addStepLog("Entering the value for departmentid");
		targetingPage.typedepartmentidTextBox(value3);
		this.sharedResource.setdepartmentid(String.valueOf(value3));

		//Thread.sleep(3 * 1000);

		
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clicktriggerScopeButtondeptid();

	//	Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		//Thread.sleep(3*1000);
	}
	
	@Then("^: Add Targeting for itempurchase group with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_for_itempurchase_group_with_and (String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
			Log.info("Clciking on add trigger group");
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicking on Add Criteria Button");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicking on Add Criteria Button");
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");
		Log.info("Selected Item purchase");
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();
		Log.info("Clicked on measure");
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");
		Log.info("Selected total units");
		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();
		Log.info("Clicked on condition button");
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);
		Log.info("entered value for condition"+condition);
		//Thread.sleep(2 * 1000);
		
		if ((targetingPage.checktypeInputUnitTextBox())==true)
		{
			Reporter.addStepLog("Entering the value for Input units : " + value);
			targetingPage.typeInputUnitTextBox(value);
			Log.info("entered value for units "+value);
		}
		else{
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering the value for Input units  ");
		targetingPage.typeinputUnitTextBoxsecondrow(value);
		Log.info("entered value for units in second row "+value);
		}

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickProductAttributeButton();
		Log.info("clicked on attribute button");
		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : upc/gtin/plus");
		targetingPage.typeProductAttributeTextBox("upc/gtin/plus");
		Log.info("enter the value for attribute text box upc/gtin/plus");
		//Thread.sleep(2 * 1000);

		if ((targetingPage.checktypelistsourceTextBox())==true)
		{
			Reporter.addStepLog("Clicking on list source");
			targetingPage.clickListSourceButton();
			Log.info("Clicked on list source");
		}else{
		
		Reporter.addStepLog("Clicking on list source");
		targetingPage.clicklistSourceButtonsecondrow();
		Log.info("Clicked on list source button second row");
		}
		
		
		//Thread.sleep(3* 1000);

		Reporter.addStepLog("Entering value for list source : list manager");
		targetingPage.typeListSourceTextBox("from manual list");
		Log.info("entered value for list source : from manual list");
		//Thread.sleep(2 * 1000);

		//int manuallist = get_Manual_List();
		long manuallist = (endNumber ++);
		//System.out.println("list"+ manuallist);
	
		targetingPage.clickManualListeditbox(String.valueOf(manuallist));
		Log.info("entered value for list  : from manual list"+ String.valueOf(manuallist) );
		//upc is set to be written to excel
		this.sharedResource.setPromotionUpc(String.valueOf(manuallist));
		//Thread.sleep(3 * 1000);

		//Reporter.addStepLog("Select first item from the manager list");
		//targetingPage.clickManagerInputSelection();
		
		//Thread.sleep(3 * 1000);
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clickTriggerScopeButton();
		Log.info("clicked on trigger scope button" );
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("clicked on trigger scope button :single session/trip" );
		Thread.sleep(3*1000);
		
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"Targetting page");
	}
	
	@Then("^: Add Targeting for itempurchase group same upc \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_for_itempurchase_group_same_UPC (String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);
		
		if ((targetingPage.checktypeInputUnitTextBox())==true)
		{
			Reporter.addStepLog("Entering the value for Input units : " + value);
			targetingPage.typeInputUnitTextBox(value);
		}
		else{
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering the value for Input units  ");
		targetingPage.typeinputUnitTextBoxsecondrow(value);
		}

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickProductAttributeButton();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : upc/gtin/plus");
		targetingPage.typeProductAttributeTextBox("upc/gtin/plus");

		//Thread.sleep(2 * 1000);

		if ((targetingPage.checktypelistsourceTextBox())==true)
		{
			Reporter.addStepLog("Clicking on list source");
			targetingPage.clickListSourceButton();

		}else{
		
		Reporter.addStepLog("Clicking on list source");
		targetingPage.clicklistSourceButtonsecondrow();
		}
		
		
		//Thread.sleep(3* 1000);

		Reporter.addStepLog("Entering value for list source : list manager");
		targetingPage.typeListSourceTextBox("from manual list");

		//Thread.sleep(2 * 1000);

		//int manuallist = get_Manual_List();
		Reporter.addStepLog("upc");
		targetingPage.clickManualListeditbox(com.catalina.vault.steps.SharedResource.getPromotionUpc());
		
		//upc is set to be written to excel
		this.sharedResource.setPromotionUpc(com.catalina.vault.steps.SharedResource.getPromotionUpc());
		//Thread.sleep(3 * 1000);

		//Reporter.addStepLog("Select first item from the manager list");
		//targetingPage.clickManagerInputSelection();
		
		//Thread.sleep(3 * 1000);
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clickTriggerScopeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		Thread.sleep(3*1000);
	}
	
	@Then("^: Add Targeting for itempurchase group with PLU \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_for_itempurchase_group_with_PLU(String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);
		
		if ((targetingPage.checktypeInputUnitTextBox())==true)
		{
			Reporter.addStepLog("Entering the value for Input units : " + value);
			targetingPage.typeInputUnitTextBox(value);
		}
		else{
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering the value for Input units  ");
		targetingPage.typeinputUnitTextBoxsecondrow(value);
		}

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickProductAttributeButton();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : upc/gtin/plus");
		targetingPage.typeProductAttributeTextBox("upc/gtin/plus");

		//Thread.sleep(2 * 1000);

		if ((targetingPage.checktypelistsourceTextBox())==true)
		{
			Reporter.addStepLog("Clicking on list source");
			targetingPage.clickListSourceButton();

		}else{
		
		Reporter.addStepLog("Clicking on list source");
		targetingPage.clicklistSourceButtonsecondrow();
		}
		
		
		//Thread.sleep(3* 1000);

		Reporter.addStepLog("Entering value for list source : list manager");
		targetingPage.typeListSourceTextBox("from manual list");

		//Thread.sleep(2 * 1000);

		targetingPage.clickManualListeditbox("4011");
		//Thread.sleep(3 * 1000);

		//Reporter.addStepLog("Select first item from the manager list");
		//targetingPage.clickManagerInputSelection();
		
		//Thread.sleep(3 * 1000);
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clickTriggerScopeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		//Thread.sleep(3*1000);
	}
	
	@Then("^: Add Retail Triggered for$")
	public void add_Retail_Triggered_with() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type Retailer Triggered");
		targetingPage.typeSelectTypeTextBox("Retailer Triggered");
		

		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Entering value for select Type Retailer Triggered");
		targetingPage.clickGenerateMCLUIDButton();

		
	}
	
	@Then("^: Add Targeting for retailer group with total spend \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_for_retailer_group_with_total_spend (String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickTotalSpendButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total spend");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);
		
		if ((targetingPage.checktypeInputUnitTextBox())==true)
		{
			Reporter.addStepLog("Entering the value for Input units : " + value);
			targetingPage.typeInputUnitTextBox(value);
		}
		else{
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering the value for Input units  ");
		targetingPage.typeinputUnitTextBoxsecondrow(value);
		}

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickProductAttributeButton();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : upc/gtin/plus");
		targetingPage.typeProductAttributeTextBox("upc/gtin/plus");

		//Thread.sleep(2 * 1000);

		if ((targetingPage.checktypelistsourceTextBox())==true)
		{
			Reporter.addStepLog("Clicking on list source");
			targetingPage.clickListSourceButton();

		}else{
		
		Reporter.addStepLog("Clicking on list source");
		targetingPage.clicklistSourceButtonsecondrow();
		}
		
		
		//Thread.sleep(3* 1000);

		Reporter.addStepLog("Entering value for list source : list manager");
		targetingPage.typeListSourceTextBox("from manual list");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for input for manual list");
		//int manuallist = get_Manual_List();
		long manuallist=(endNumber++);
		targetingPage.clickManualListeditbox(String.valueOf(manuallist));
		//targetingPage.clickManualListeditbox(list);

		//Thread.sleep(3 * 1000);

		//Reporter.addStepLog("Select first item from the manager list");
		//targetingPage.clickManagerInputSelection();
		
		//Thread.sleep(3 * 1000);
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clickTriggerScopeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		//Thread.sleep(3*1000);
	}
	@Then("^: Add Targeting for retailer group with qty \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_for_retailer_group_with_qty (String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);
		
		
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering the value for Input units  ");
		targetingPage.typeinputUnitTextBoxsecondrow(value);
		

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickProductAttributeButton();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : upc/gtin/plus");
		targetingPage.typeProductAttributeTextBox("upc/gtin/plus");

		//Thread.sleep(2 * 1000);

		
		Reporter.addStepLog("Clicking on list source");
		targetingPage.clicklistSourceButtonsecondrow();
		//Thread.sleep(3* 1000);

		Reporter.addStepLog("Entering value for list source : list manager");
		targetingPage.typeListSourceTextBox("from manual list");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for input for manual list");
		//int manuallist = get_Manual_List();
		long manuallist=(endNumber++);
		targetingPage.clickManualListsecondeditbox(String.valueOf(manuallist));
		

		
		//Thread.sleep(3 * 1000);
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clicktriggerScopesecondButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		//Thread.sleep(3*1000);
	}
	
	@Then("^: Add Targeting for itempurchase group with lmc list \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_for_itempurchase_group_with_lmc_list_and(String condition, String value,String list) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");

		if ((targetingPage.checkkAddTiggerGroup()) == true) {
			// Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
			Log.info("Clciking on add trigger group");
		}

		// targetingPage.clickAddTiggerGroup();

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicking on Add Criteria Button");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicking on Add Criteria Button");
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");
		Log.info("Selected Item purchase");
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();
		Log.info("Clicked on measure");
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");
		Log.info("Selected total units");
		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();
		Log.info("Clicked on condition button");
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);
		Log.info("entered value for condition" + condition);
		// Thread.sleep(2 * 1000);

		if ((targetingPage.checktypeInputUnitTextBox()) == true) {
			Reporter.addStepLog("Entering the value for Input units : " + value);
			targetingPage.typeInputUnitTextBox(value);
			Log.info("entered value for units " + value);
		} else {
			// Thread.sleep(3 * 1000);
			Reporter.addStepLog("Entering the value for Input units  ");
			targetingPage.typeinputUnitTextBoxsecondrow(value);
			Log.info("entered value for units in second row " + value);
		}

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickProductAttributeButton();
		Log.info("clicked on attribute button");
		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : upc/gtin/plus");
		targetingPage.typeProductAttributeTextBox("upc/gtin/plus");
		Log.info("enter the value for attribute text box upc/gtin/plus");
		// Thread.sleep(2 * 1000);

		if ((targetingPage.checktypelistsourceTextBox()) == true) {
			Reporter.addStepLog("Clicking on list source");
			targetingPage.clickListSourceButton();
			Log.info("Clicked on list source");
		} else {

			Reporter.addStepLog("Clicking on list source");
			targetingPage.clicklistSourceButtonsecondrow();
			Log.info("Clicked on list source button second row");
		}

		// Thread.sleep(3* 1000);

		Reporter.addStepLog("Entering value for list source : list manager");
		targetingPage.typeListSourceTextBox("list manager");
		Log.info("entered value for list source : list manager");
		// Thread.sleep(2 * 1000);

//		int manuallist = get_Manual_List();
//		targetingPage.clickManualListeditbox(String.valueOf(manuallist));
//		Log.info("entered value for list  : from manual list" + String.valueOf(manuallist));
//		// upc is set to be written to excel
//		this.sharedResource.setPromotionUpc(String.valueOf(manuallist));
//		// Thread.sleep(3 * 1000);

		 Reporter.addStepLog("Select first item from the manager list");
		 targetingPage.typeInputForManager(list);

		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clickTriggerScopeButton();
		Log.info("clicked on trigger scope button");
		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("clicked on trigger scope button :single session/trip");
		Thread.sleep(3 * 1000);
	}
	
	
	@Then("^: Add Payment Method \"([^\"]*)\"$")
	public void add_Payment_Method(String payment) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicked on Add Criteria Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicked on Select Type Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type Payment Method");
		targetingPage.typeSelectTypeTextBox("Payment Method");
		Log.info("Select type Payment Method ");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for text box ");
		targetingPage.typeInputTextBox(payment);
		Log.info("Enter value for text box" +payment);
		
		
		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickpaymentmethodcondition();
		Log.info("Clicked on condition button");
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : ");
		targetingPage.typeinputconditiontext(">=");
		Log.info("entered value for condition");
		//Thread.sleep(2 * 1000);
		
		
		Reporter.addStepLog("Entering the value for Input units : ");
		targetingPage.typepaymentinputUnitTextBox("1");
		Log.info("entered value for units ");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Scope Button");
		targetingPage.clicksecondtriggerScopeButton();
		Log.info("Clicked on Scope Button");
		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger Scope : include");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("select Scope single session/trip");

		//Thread.sleep(3 * 1000);
	}
	
	@Then("^: Add Payment Method EFT$")
	public void add_Payment_Method_EFT() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("clicked on  Add Criteria Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("clicked on Select Type Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type Payment Method");
		targetingPage.typeSelectTypeTextBox("Payment Method");
		Log.info("select Payment Method");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for text box ");
		targetingPage.typeInputTextBox("EFT");
		Log.info("select EFT");
		
		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickpaymentmethodcondition();
		Log.info("Clicked on condition button");
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : ");
		targetingPage.typeinputconditiontext(">=");
		Log.info("entered value for condition");
		//Thread.sleep(2 * 1000);
		
		
		Reporter.addStepLog("Entering the value for Input units : ");
		targetingPage.typepaymentinputUnitTextBox("1");
		Log.info("entered value for units ");
		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Scope Button");
		targetingPage.clickpaymenttriggerScopeButton();
		Log.info("clicked on  scope Button");
		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger Scope : include");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("select single session/trip");

		//Thread.sleep(3 * 1000);
	}
	
	@Then("^: Add Payment Method EFT second row$")
	public void add_Payment_Method_EFT_second_row() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("clicked on  Add Criteria Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("clicked on Select Type Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type Payment Method");
		targetingPage.typeSelectTypeTextBox("Payment Method");
		Log.info("select Payment Method");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for text box ");
		targetingPage.typeInputTextBox("EFT");
		Log.info("select EFT");
		
		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickpaymentmethodconditionsecondrow();
		Log.info("Clicked on condition button");
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : ");
		targetingPage.typeinputconditiontext(">=");
		Log.info("entered value for condition");
		//Thread.sleep(2 * 1000);
		
		
		Reporter.addStepLog("Entering the value for Input units : ");
		targetingPage.typepaymentinputUnitTextBox("1");
		Log.info("entered value for units ");
		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Scope Button");
		targetingPage.clickpaymenttriggerScopeButton();
		Log.info("clicked on  scope Button");
		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger Scope : include");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("select single session/trip");

		//Thread.sleep(3 * 1000);
	}
	
	
	@Then("^: Add Payment Method with Foodstamp$")
	public void add_Payment_Method_with_Foodstamp() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicked on Add Criteria Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicked on Select Type Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type Payment Method");
		targetingPage.typeSelectTypeTextBox("Payment Method");
		Log.info("Select value Payment Method");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for text box ");
		targetingPage.typeInputTextBox("Foodstamp");
		Log.info("Enter value for Foodstamp ");
		
		
		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickpaymentmethodcondition();
		Log.info("Clicked on condition button");
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : ");
		targetingPage.typeinputconditiontext(">=");
		Log.info("entered value for condition");
		//Thread.sleep(2 * 1000);
		
		
		Reporter.addStepLog("Entering the value for Input units : ");
		targetingPage.typepaymentinputUnitTextBox("1");
		Log.info("entered value for units ");
		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Scope Button");
		targetingPage.clicksecondtriggerScopeButton();
		Log.info("Clicked on Scope Button");
		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger Scope : include");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("select Trigger Scope");

		//Thread.sleep(3 * 1000);
	}
	
	@Then("^: Add Audience Segment for retailer firstrow with \"([^\"]*)\"$")
	public void add_Audience_Segment_for_retailer_firstrow_with(String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
	//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
	if ((targetingPage.checkkAddTiggerGroup())==true)
	{
		//Reporter.addStepLog("Clicking on Add Trigger Button");
		targetingPage.clickAddTiggerGroup();
		Log.info("Clicked on trigger group");
	}
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicked on Add Criteria Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicked on Select Type Button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type audience segment");
		targetingPage.typeSelectTypeTextBox("Audience segment");
		Log.info("selected audience segment");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Operation Button");
		targetingPage.clickoperationbuttonfirstrow();
		Log.info("Clicked on Operation Button");

		//Thread.sleep(3 * 1000);		
		Reporter.addStepLog("Entering value for operation in" +value);
		targetingPage.typeinputForoperationbutton(value);
		Log.info("Enter value for operation: "+value);

		
		//Thread.sleep(3 * 1000);		
		Reporter.addStepLog("Clickin on search audience");
		targetingPage.clicksearchAudiencesfirstrow();
		Log.info("Clicked on search audience");
		
		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering the value for Input manager krishcoke");
		String audienceListNamePropFile="AUDIENCE_SEGMENT_"+com.catalina.vault.steps.SharedResource.GetCountry();
		System.out.println("sudience name"+audienceListNamePropFile);
		String audienceListName= propFile.getProperty(audienceListNamePropFile);
		targetingPage.typeinputForsearchAudiences(audienceListName);
		Log.info("Entered value for search Audiences:"+audienceListName);


	}
	
	@Then("^: Add Audience Segment for with$")
	public void add_Audience_Segment_for_with() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
	//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type audience segment");
		targetingPage.typeSelectTypeTextBox("Audience segment");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Operation Button");
		targetingPage.clickoperationbutton();

		//Thread.sleep(3 * 1000);		
		Reporter.addStepLog("Entering value for operation in");
		targetingPage.typeinputForoperationbutton("in");

		
		//Thread.sleep(3 * 1000);		
		Reporter.addStepLog("Clickin on search audience");
		targetingPage.clicksearchAudiences();
		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering the value for Input manager krishcoke");
		
		Reporter.addStepLog("Entering the value for Input manager krishcoke");
		String audienceListNamePropFile="AUDIENCE_SEGMENT_"+com.catalina.vault.steps.SharedResource.GetCountry();
		String audienceListName= propFile.getProperty(audienceListNamePropFile);
		targetingPage.typeinputForsearchAudiences(audienceListName);
		Log.info("Entered value for search Audiences:"+audienceListName);

		

	}

	@Then("^: Add Custom Targeting with external id \"([^\"]*)\"$")
	public void add_custom_targeting_with_external_id(String country) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
	//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type audience segment");
		targetingPage.typeSelectTypeTextBox("Custom Targeting");

		//Thread.sleep(1 * 1000);	
		Reporter.addStepLog("Clickin on external field");
		targetingPage.clickexternalidField(this.bls.getCellData(country, 1, 2));	
	}
	
	@Then("^: Add User Identifier")
	public void add_user_identifier() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
	//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type audience segment");
		targetingPage.typeSelectTypeTextBox("User identifier");
		
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Operation Button");
		targetingPage.clickuserIdentifieroperationbutton();

		//Thread.sleep(3 * 1000);		
		Reporter.addStepLog("Entering value for operation in");
		targetingPage.typeinputuserIdentifieroperation("loyalty card IS PRESENT in order/session");
		
		
	}
	
	@Then("^: Add Targeting for retailer group with between \"([^\"]*)\" , \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_for_retailer_group_with_between(String condition,String value, String value2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Item purchase");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on Measure Button");
		targetingPage.clickMeasureButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typeMeasureTextBox("total units");

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.clickConditionButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter value for condition : " + condition);
		targetingPage.typeConditionTextBox(condition);

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value);
		targetingPage.typeInputUnitTextBox(value);
		
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value2);
		targetingPage.typeInputUnitTextBox2(value2);

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute ");
		targetingPage.clickProductAttributeButton();

		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Entering the value for ProductAttribute : upc/gtin/plus");
		targetingPage.typeProductAttributeTextBox("upc/gtin/plus");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on list source");
		targetingPage.clickListSourceButton();

	//	Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for list source : list manager");
		targetingPage.typeListSourceTextBox("from manual list");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for input for manual list");
		
		//int manuallist = get_Manual_List();
		long manuallist = (endNumber ++);
		targetingPage.clickManualListeditbox(String.valueOf(manuallist));
	//	targetingPage.clickManualListeditbox(manualList);

		//Thread.sleep(1 * 1000);

		//Reporter.addStepLog("Select first item from the manager list");
		//targetingPage.clickManagerInputSelection();
		
		//Thread.sleep(1 * 1000);
		
		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clickTriggerScopeButton();

		//Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		//Thread.sleep(3*1000);
	}
	
	@Then("^: Clicking On Add Targeting Group")
	public void clicking_On_Add_Targeting_Group() throws Throwable {
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		targetingPage.clickAddTiggerGroup();

	}
	
	@Then("^: Add Total Spend between \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Total_Spend_for_advertisement_between(String Condition, String value, String value2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
			Log.info("Clicked on add trigger button");
			//Thread.sleep(3 * 1000);
		}
		
		//Reporter.addStepLog("Clicking on Add Trigger Button");
		//targetingPage.clickAddTiggerGroup();

		Reporter.addStepLog("Clicking on add criteria button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicked on add criteria button");
		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Select Type button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicked on Select Type button");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering vlaue for select type : total order");
		targetingPage.typeSelectTypeTextBox("Total Order");
		Log.info("Select value total Order");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order measure button");
		targetingPage.clickTotalOrderMeasureButton();
		Log.info("Clicked on total order measure button");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for measure : total spend");
		targetingPage.typeSelectTypeTextBox("total spend");
		Log.info("Enter value for measure button total spend");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order condition button");
		targetingPage.clickTotalOrderConditionButton();
		Log.info("Clicked on add trigger button");

		Reporter.addStepLog("Enter value for condition : " + Condition);
		targetingPage.typeConditionTextBox(Condition);
		Log.info("Clicked on add trigger button");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " + value);
		targetingPage.typeinputUnitTextBoxTotalOrder(value);
		Log.info("Clicked on add trigger button");
		
		Reporter.addStepLog("Entering the value for Input units : " + value2);
		targetingPage.typeinputinputUnitTextBoxTotalOrder2(value2);
		Log.info("Clicked on add trigger button");;

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on total order trigger scope");
		targetingPage.clickTotalOrderTriggerScope();
		Log.info("Clicked on add trigger button");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for trigger scope single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("Clicked on add trigger button");

		
	}

	
	@Then("^: Add Catalina Redemption \"([^\"]*)\"$")
	public void add_Catalina_Redemption_(String country) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		

		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");

		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		//targetingPage.clickAddTiggerGroup();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		targetingPage.typeSelectTypeTextBox("Catalina Redemption");
		
		Reporter.addStepLog("Clicking on scope Button");
		targetingPage.clickScopebuttoncatalinaredem();
		

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typescopeTextBox("ad(s)");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Condition Button");
		targetingPage.typeadsTextBoxcatalinaredem(this.bls.getCellData(country, 1, 2));
		
		//Thread.sleep(3 *1000);
		Reporter.addStepLog("Clicking on operation Button");
		targetingPage.clickoperationButtoncatalinaredem();
		
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on operation text box");
		targetingPage.typeInputTriggerScope("did redeem");
		
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on trigger scope Button");
		targetingPage.clickcatalinaredemTriggerScope();
		
		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Clicking on operation text box");
		targetingPage.typeInputTriggerScope("Current Session/Trip");
		
		//Thread.sleep(2 * 1000);
	}

	@Then("^: Add Payment Method EFT and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Payment_Method_EFT_and(String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("clicked on  Add Criteria Button");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("clicked on Select Type Button");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type Payment Method");
		targetingPage.typeSelectTypeTextBox("Payment Method");
		Log.info("select Payment Method");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for text box ");
		targetingPage.typeInputTextBox("EFT");
		Log.info("select EFT");
		
		Reporter.addStepLog("Clicking on total order condition button");
		targetingPage.clickPaymentConditionButton();
		Log.info("Clicking on total order condition button");

		Reporter.addStepLog("Entering value for condition : between");
		targetingPage.typepaymentConditionTextBox(condition);
		Log.info("Entering value for condition : between");

		Reporter.addStepLog("Entering the number : 1");
		targetingPage.paymentInput(value);
		Log.info("Entering the number :" + value);

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Scope Button");
		targetingPage.clicksecondtriggerScopeButton();
		// clickpaymenttriggerScopeButton();
		Log.info("clicked on  scope Button");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger Scope : include");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("select single session/trip");

		// Thread.sleep(3 * 1000);
	}
	
	@Then("^: Add Payment Method with Foodstamp and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Payment_Method_with_Foodstamp(String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicked on Add Criteria Button");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicked on Select Type Button");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type Payment Method");
		targetingPage.typeSelectTypeTextBox("Payment Method");
		Log.info("Select value Payment Method");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for text box ");
		targetingPage.typeInputTextBox("Foodstamp");
		Log.info("Enter value for Foodstamp ");
		
		Reporter.addStepLog("Clicking on total order condition button");
		targetingPage.clickPaymentConditionButton();
		Log.info("Clicking on total order condition button");

		Reporter.addStepLog("Entering value for condition : between");
		targetingPage.typepaymentConditionTextBox(condition);
		Log.info("Entering value for condition : between");

		Reporter.addStepLog("Entering the number : 1");
		targetingPage.paymentInput(value);
		Log.info("Entering the number :" + value);

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Scope Button");
		targetingPage.clicksecondtriggerScopeButton();
		Log.info("Clicked on Scope Button");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger Scope : include");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("select Trigger Scope");

		// Thread.sleep(3 * 1000);
	}
	
	@Then("^: Add Targeting group with Total Units Purchased first \"([^\"]*)\" and \"([^\"]*)\"$")
	public void add_Targeting_group_With_Total_Units_Purchased(String condition, String value) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		if ((targetingPage.checkkAddTiggerGroup()) == true) {
			Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
			// Thread.sleep(3 * 1000);
			Log.info("Clicked on trigger button");
		}

		// Reporter.addStepLog("Clicking on Add Trigger Button");
		// targetingPage.clickAddTiggerGroup();

		Reporter.addStepLog("Clicking on add criteria button");
		targetingPage.clickAddCriteriaButton();
		Log.info("Clicking on add criteria button");
		// Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on Select Type button");
		targetingPage.clickSelectTypeButton();
		Log.info("Clicking on Select Type button");

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering vlaue for select type : total order");
		targetingPage.typeSelectTypeTextBox("Total Order");
		Log.info("Entering vlaue for select type : total order");

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order measure button");
		targetingPage.clickTotalOrderMeasureButton();
		Log.info("Clicking on total order measure button");

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for measure : units purchased");
		targetingPage.typeSelectTypeTextBox("units purchased");
		Log.info("Entering value for measure : units purchased");

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on total order condition button");
		targetingPage.clickTotalOrderConditionButton();
		Log.info("Clicking on total order condition button");

		// Thread.sleep(3 * 1000);
		Reporter.addStepLog("Entering value for condition : between");
		targetingPage.typeConditionTextBox(condition);
		Log.info("Entering value for condition : between");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering the number : 1");
		targetingPage.typeTotalOrderValue3(value);
		Log.info("Entering the number :" + value);

		// Thread.sleep(1 * 1000);
		// Reporter.addStepLog("Entering the second number : 10");
		// targetingPage.typeTotalOrderValue2("10");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on total order trigger scope");
		targetingPage.clickTotalOrderTriggerScope();
		Log.info("Clicking on total order trigger scope");

		// Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for trigger scope single session/trip");
		targetingPage.typeInputTriggerScope("single session/trip");
		Log.info("Entering value for trigger scope single session/trip");

	}
	
	@Then("^: Add Item Purchase with \"([^\"]*)\"$")
	public void add_Item_Purchase_with(String value) throws Throwable {

		AdvertisementItemPurchasePage page = new AdvertisementItemPurchasePage(this.driver);

		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		page.clickOnAddCriteria();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		page.clickOnSelectTypeButton();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		page.typeSelectTypeInput("Item purchase");

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Measure Button");
		page.clickOnMeasureButton();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Enter measure Criteria : Total Units");
		page.typeMeasureInput("total units");

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Condition Button");
		page.clickOnConditionButton();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Condition : >=");
		page.typeConditionInput(">=");

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger Condition Input : 1");
		page.typeTriggerConditionInput("1");

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute Button");
		page.clickOnProductAttributeButton();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Product Attribute : upc/gtin/plus");
		page.typeProductAttributeInput("upc/gtin/plus");

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on List Source Button");
		page.clickOnListSourceButton();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for List Source : List Manager Groups");
		page.typeListSourceInput("list manager groups");

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for List Manager Groups : " + value);
		page.typeListManagerGroupsInput("Krishcoke");

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on trigger scope");
		page.clickOnTriggerScopeButton();

		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for trigger scope : single session/trip");
		page.typeTriggerScopeInput("single session/trip");

	}

	@Then("^: Add Targeting with Retargetting \"([^\"]*)\"$")
	public void add_Targeting_for_retailer_with_Retargetting(String country) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on select type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering value for Select Type : Retarget");
		targetingPage.typeSelectTypeTextBox("Retarget");

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on scope Button");
		targetingPage.clickScopebutton();
		

		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Enter meassure Criteria : Total Units");
		targetingPage.typescopeTextBox("ad(s)");

		//Thread.sleep(3 * 1000);

		
		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Clicking on Condition Button");
		System.out.println("excel value:"+this.bls.getCellData(country, 1, 2));
		targetingPage.typeadsTextBox(this.bls.getCellData(country, 1, 2));
		
		//Thread.sleep(2* 1000);
		Reporter.addStepLog("Click action button  " );
		targetingPage.clickactionbutton();

		Reporter.addStepLog("Enter value for action : " );
		targetingPage.typeactiontextbox("impression");

		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Click operation button  " );
		targetingPage.clickoperationButtonRetarget();

		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Entering the value for operation text box");
		targetingPage.typeoperationtextbox(">=");
		
		//Thread.sleep(2 * 1000);

		Reporter.addStepLog("Entering the value for Input units : " );
		targetingPage.typeretargetinputUnitTextBox("0");


		//Thread.sleep(3 * 1000);

		Reporter.addStepLog("Clicking on trigger scope");
		targetingPage.clickretargettriggerscopebutton();

		//Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for trigger scope : user history");
		targetingPage.typeInputTriggerScope("user history");
		//Thread.sleep(3*1000);
		
		Reporter.addStepLog("Clicking on time period button");
		targetingPage.clicktimeperiodbutton();
		
		//Thread.sleep(3*1000);
		Reporter.addStepLog("Entering value for time period : single session/trip");
		targetingPage.typetimeperiodtextbox("for all time");
	}

	@Then("^: Add Targeting with Item Purchase with fixed period and Manual List")
	public void add_Targeting_for_retailer_with_Item_Purchase_with_fixed_period_and_Manual_List()
			throws Throwable {

		RetailerManualListPage manualistPage = new RetailerManualListPage(this.driver);

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		manualistPage.clickOnAddCriteria();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		manualistPage.clickOnSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		manualistPage.typeSelectTypeInput("Item purchase");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Measure Button");
		manualistPage.clickOnMeasureButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Enter measure Criteria : Total Units");
		manualistPage.typeMeasureInput("total units");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Condition Button");
		manualistPage.clickOnConditionButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Condition : >=");
		manualistPage.typeConditionInput(">=");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger Condition Input : 1");
		manualistPage.typeTriggerConditionInput("1");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Product Attribute Button");
		manualistPage.clickOnProductAttributeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Product Attribute : upc/gtin/plus");
		manualistPage.typeProductAttributeInput("upc/gtin/plus");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on List Source Button");
		manualistPage.clickOnListSourceButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for List Source : from Manual List");
		manualistPage.typeListSourceInput("from manual list");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Target List : ");
		//int manuallist = get_Manual_List();
		long manuallist=(endNumber ++);
		manualistPage.typeTargetListInput(String.valueOf(manuallist));
		this.sharedResource.setPromotionUpc(String.valueOf(manuallist));
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Trigger Scope Button");
		manualistPage.clickOnTriggerScopeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger scope : user history");
		manualistPage.typeTriggerScopeInput("user history");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Time Period Input");
		manualistPage.clickTimePeriodButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Time period : for fixed period");
		manualistPage.typeTimePeriodInput("for fixed period");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering From Date");
		manualistPage.fromDateFormatter();
		page.waitforLoading("to date");
		Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering To Date");
		manualistPage.toDateFormatter();

	}
	
	@Then("^: Add Targeting for Advertisement with LaneType$")
	public void add_Targeting_for_Advertisement_with_LaneType() throws Throwable {

		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type audience segment");
		targetingPage.typeSelectTypeTextBox("Lane Type");
		

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Operation Button");
		targetingPage.clickoperationlanebutton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Operation : include");
		targetingPage.typeoperationtextbox("include");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Operation Input : Pharmacy");
		targetingPage.typeLaneTypeTextbox("Pharmacy");

		//Thread.sleep(1 * 1000);

	}

	
	
	@Then("^: Add Targeting for Loyalty Points$")
	public void add_Targeting_for_Loyalty_Points() throws Throwable {

		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type audience segment");
		targetingPage.typeSelectTypeTextBox("Loyalty points");
		
		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Action Button");
		targetingPage.clickactionbuttonLoyalty();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Operation : include");
		targetingPage.typeoperationtextbox("generic");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Action Button");
		targetingPage.clickactionbuttonLoyaltysecond();
		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Operation : include");
		targetingPage.typeoperationtextbox("current balance");

		
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Operation Button");
		targetingPage.clickoperationloyaltybutton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Operation : include");
		targetingPage.typeoperationtextbox(">=");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Input : 5000");
		targetingPage.clickinputforloyaltypoints("5000");

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Trigger Scope Button");
		targetingPage.clickloyaltytriggerScopeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Trigger SCope : Single session/trip");
		targetingPage.typeoperationtextbox("single session/trip");
	}
	
	
	@Then("^: Add Targeting for retailer with POS Redemption \"([^\"]*)\"$")
	public void add_Targeting_for_retailer_with_POS_Redemption(String country)
			throws Throwable {

		RetailerManualListPage manualistPage = new RetailerManualListPage(this.driver);
		
		
		//Thread.sleep(3 * 1000);
		//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
		Reporter.addStepLog("Clicking on Add Trigger Group");
		targetingPage.clickAddTiggerGroup();
		
		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		manualistPage.clickOnAddCriteria();

		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		manualistPage.clickOnSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		manualistPage.typeSelectTypeInput("P.O.S Redemption");

		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Clicking on coupon type Button");
		manualistPage.clickselectcoupontype();

		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Enter measure coupon type");
		manualistPage.typeInputcoupontype("Generic Coupon");


		//Thread.sleep(2 * 1000);
		Reporter.addStepLog("Entering type coupon text box ");
		manualistPage.typecoupontypetextbox(this.bls.getCellData(country, 1, 2));

		

	}
	
	@Then("^: Add Audience Segment for advertisement with$")
	public void add_Audience_Segment_for_advertisement_with() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
	//ScreenShot.addScreenshotToScreen(this.driver, "beforeAddTargetting");
	
		Reporter.addStepLog("Clicking on Add Trigger Group");
		
		if ((targetingPage.checkkAddTiggerGroup())==true)
		{
			//Reporter.addStepLog("Clicking on Add Trigger Button");
			targetingPage.clickAddTiggerGroup();
		}
		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Add Criteria Button");
		targetingPage.clickAddCriteriaButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Clicking on Select Type Button");
		targetingPage.clickSelectTypeButton();

		//Thread.sleep(1 * 1000);
		Reporter.addStepLog("Entering value for select Type audience segment");
		targetingPage.typeSelectTypeTextBox("Audience segment");

		//Thread.sleep(3 * 1000);
		Reporter.addStepLog("Clicking on Operation Button");
		targetingPage.clickoperationbuttonfirstrow();

		//Thread.sleep(3 * 1000);		
		Reporter.addStepLog("Entering value for operation in");
		targetingPage.typeinputForoperationbutton("in");

		
		//Thread.sleep(3 * 1000);		
		Reporter.addStepLog("Clickin on search audience");
		targetingPage.clicksearchAudiencesfirstrow();
		
		//Thread.sleep(1 * 1000);
		
		
		Reporter.addStepLog("Entering the value for Input manager ");
		String audienceListNamePropFile="AUDIENCE_SEGMENT_"+com.catalina.vault.steps.SharedResource.GetCountry();
		System.out.println("sudience name"+audienceListNamePropFile);
		String audienceListName= propFile.getProperty(audienceListNamePropFile);
		targetingPage.typeinputForsearchAudiences(audienceListName);
		Log.info("Entered value for search Audiences:"+audienceListName);
		

	}
	
	@Then("^: Fill ad form for digital redemption tab \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_ad_form_digital_Redemption_Tab(String barcode, String list) throws Throwable {

		// Thread.sleep(2*1000);
		page.waitforLoading("digital");
		
		Reporter.addStepLog("Changing the tab to Redemtion Clearing");
		couponPage.clickRedemtionClearingButton();

		Reporter.addStepLog("Clicking on Enable Expiration Date");
		couponPage.clickEnableExpireDateButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on date Format Button");
		couponPage.clickDateFormatButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering date formatter MM/DD/YY");
		couponPage.typeDateFormatTextBox("MM/DD/YY");

		// Thread.sleep(2 * 1000);

		Reporter.addStepLog("Clicking on date option");
		couponPage.expirationDate();

		Reporter.addStepLog("Clicking on Clearing Method");
		adPage.clickClearingMethodButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering the value to search box : Retail");
		couponPage.typeClearingMethodTypeBox("Manufacturer + Digital");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Clicking on company prefix");
		couponPage.typecompanyprefixfield();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering company prefix");
		couponPage.typecompanyprefixTextBox_Digital("0025000");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering clearing house");
		couponPage.typeclearinghouse("300259");

				
		Reporter.addStepLog("Clicking on BarCode Type");
		couponPage.clickBarCodeTypeButton();

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Entering value for barcode type : GS1 Databar North American (with text)");
		couponPage.typeBarCodeTypeTextBox("GS1 Databar North American (with text)");

		// Thread.sleep(1 * 1000);

		Reporter.addStepLog("Bar Code Value : 00000011111" + barcode);
		couponPage.typegs1barcodevalue(barcode);
		
		couponPage.scrollToDown();
		
		Reporter.addStepLog("Clicking on Add redemption requirement button");
		couponPage.clickaddRequirementButton();
		
		Reporter.addStepLog("Clicking on Add Criteria button");
		couponPage.clickAddCriteriaButton();
		
		Reporter.addStepLog("Clicking on select type Button");
		couponPage.clickSelectTypeButton();
		
		Reporter.addStepLog("Entering value for Select Type : Item Purchase");
		couponPage.typeSelectTypeTextBox("Item purchase");
		
		Reporter.addStepLog("Clicking on Measure Button");
		couponPage.clickMeasureButton();
		
		Reporter.addStepLog("Entering value for Measure Type : Total units");
		couponPage.typeMeasureTextBox("total units");
		
		Reporter.addStepLog("Clicking on Condition Button");
		couponPage.clickConditionButton();

		Reporter.addStepLog("Entering value for Condition: >=");
		couponPage.typeConditionTextBox(">=");
		
		Reporter.addStepLog("Entering value for Units: 1");
		couponPage.typeInputUnitTextBox("1");
		
		Reporter.addStepLog("Clicking on product Attribute Button");
		couponPage.clickProductAttributeButton();
				
		Reporter.addStepLog("Entering value for Product Attribute: upc/gtin/plus");
		couponPage.typeProductAttributeTextBox("upc/gtin/plus");
		
		Reporter.addStepLog("Clicking on List source Button");
		couponPage.clickListSourceButton();
				
		Reporter.addStepLog("Entering value for List Source: list manager");
		couponPage.typeListSourceTextBox("list manager");
		
		Reporter.addStepLog("Entering and Selecting the list:");
		couponPage.typeListManagerTextBox(list);
		
		Reporter.addStepLog("Clicking on Trigger Scope Button");
		couponPage.clickTriggerScopeButton();
	}
}




