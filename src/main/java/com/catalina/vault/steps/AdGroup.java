package com.catalina.vault.steps;

import java.nio.file.FileSystems;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.catalina.vault.entities.PerformanceTiming;
import com.catalina.vault.pagefactory.AdGroupListPage;
import com.catalina.vault.pagefactory.AdGroupPage;
import com.catalina.vault.pagefactory.MainPage;
import com.catalina.vault.pagefactory.RetailerCouponPage;
import com.catalina.vault.utils.ScreenShot;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.When;

public class AdGroup {

	WebDriver driver;
	AdGroupListPage adGroup;
	AdGroupPage form;
	MainPage page;
	RetailerCouponPage couponPage;
	SharedResource sharedResource;
	
	private static final Logger Log = LogManager.getLogger(AdGroup.class);
	public AdGroup(SharedResource sharedResource) {
		// TODO Auto-generated constructor stub
		this.driver = sharedResource.init();
		adGroup = new AdGroupListPage(this.driver);
		this.form = new AdGroupPage(this.driver);
		this.page = new MainPage(this.driver);
		this.couponPage = new RetailerCouponPage(this.driver);
		this.sharedResource= new SharedResource();
	}

	@When("^: Campagin created Then create Ad Group with \"([^\"]*)\"$")
	public void campagin_created_Then_create_Ad_Group(String testcasename) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Creating a new AdGroup");
		Reporter.addStepLog("Clicking on new Button");
		
		//Thread.sleep(2*1000);
		page.waitforLoading("Clicking on new Button");
		Thread.sleep(2*1000);
		if((couponPage.alertOkClick())==true)
		{
			couponPage.clickalertAccept();
			Log.debug("clickalertAccept");
			Log.info("clickalertAccept");
		}
		page.waitforLoading("clickalertAccept");
		Thread.sleep(3 * 1000);
		adGroup.clickNewButton();
		//Thread.sleep(2 * 1000);
		Log.info("Clicked on ad group button");
		Reporter.addStepLog("Clicking on new button");
		//Thread.sleep(2 * 1000);
		adGroup.clcikAdGroupButton();
		page.waitforLoading("clcikAdGroupButton");
		Log.info("Clicked on ad group button");
		Reporter.addStepLog("Entering Ad Group Name : Automation Test Group");
		form.typeName(testcasename);
		Log.info("enter test case name "+testcasename);
		this.sharedResource.setTestcaseName(testcasename);
		//Thread.sleep(2*1000);
		//form.typeBilling("Sun Light Advertising Services Srl");
		//form.clickChannel();
		form.clickSaveButton();
		Reporter.addStepLog("Clicked on save button");
		Log.info("Clicked on save button");
		page.waitforLoading("Clicked on save button");
		
		
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"AdGroup Page");
		
	}
	
	@When("^: Campagin created Then create Ad Group with Standard Delivery method \"([^\"]*)\"$")
	public void campagin_created_Then_create_Ad_Group_With_Standard_Delivery_Method(String testcasename) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Creating a new AdGroup");
		Reporter.addStepLog("Clicking on new Button");
		
		//Thread.sleep(2*1000);
		page.waitforLoading("Clicking on new Button");
		Thread.sleep(2*1000);
		if((couponPage.alertOkClick())==true)
		{
			couponPage.clickalertAccept();
			Log.debug("clickalertAccept");
			Log.info("clickalertAccept");
		}
		page.waitforLoading("clickalertAccept");
		Thread.sleep(3 * 1000);
		adGroup.clickNewButton();
		//Thread.sleep(2 * 1000);
		Log.info("Clicked on ad group button");
		Reporter.addStepLog("Clicking on new button");
		//Thread.sleep(2 * 1000);
		adGroup.clcikAdGroupButton();
		page.waitforLoading("clcikAdGroupButton");
		Log.info("Clicked on ad group button");
		Reporter.addStepLog("Entering Ad Group Name : Automation Test Group");
		form.typeName(testcasename);
		Log.info("enter test case name "+testcasename);
		this.sharedResource.setTestcaseName(testcasename);
		//Thread.sleep(2*1000);
		//form.typeBilling("Sun Light Advertising Services Srl");
		//form.clickChannel();
		
		Reporter.addStepLog("Clicking Delivery Method dropdown");
		form.clickDeliveryMethodDropdown();
		Log.info("Clicked on Delivery Method button");
		
		Reporter.addStepLog("Selecting Delivery Method: Standard");
		form.selectDeliveryMethod("Standard");
		Log.info("Selected the Delivery Method: Standard");
		
		form.clickSaveButton();
		Reporter.addStepLog("Clicked on save button");
		Log.info("Clicked on save button");
		page.waitforLoading("Clicked on save button");
		
		
		PerformanceTiming.writePerfMetricasJSON(this.driver,FileSystems.getDefault().getPath("vault.json").toAbsolutePath().toString(),"AdGroup Page");
		
	}
	
	@When("^: Campagin created Then create Ad Group with distribution cap \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void campagin_created_Then_create_Ad_Group_with_distribution_cap(String testcasename,String count, String level) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Creating a new AdGroup");
		Reporter.addStepLog("Clicking on new Button");

		//Thread.sleep(2*1000);
		page.waitforLoading("Clicking on new Button");
		Thread.sleep(2*1000);
		if((couponPage.alertOkClick())==true)
		{
			couponPage.clickalertAccept();
		}
		page.waitforLoading("clickalertAccept");
		Thread.sleep(3 * 1000);
		adGroup.clickNewButton();
		//Thread.sleep(2 * 1000);
		
		Reporter.addStepLog("Clicking on ad group button");
		//Thread.sleep(2 * 1000);
		adGroup.clcikAdGroupButton();
		page.waitforLoading("clcikAdGroupButton");
		Reporter.addStepLog("Entering Ad Group Name : Automation Test Group");
		form.typeName(testcasename);
		//Thread.sleep(2*1000);
		//form.typeBilling("Sun Light Advertising Services Srl");
		//form.clickChannel();
		Reporter.addStepLog("clicking on distribution capping");
		form.clickdistbutioncaplink();
		
		Reporter.addStepLog("Enter distribution cap count");
		form.typedistbutioncapcount(count);
		
		Reporter.addStepLog("select distribution cap level");
		form.typedistbutioncaplevel(level);
		
		form.clickSaveButton();
		Reporter.addStepLog("Clicked on save button");
		
		page.waitforLoading("Clicked on save button");
		

	}
	@When("^: Campagin created Then create Ad Group with distribution cap levels \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void campagin_created_Then_create_Ad_Group_with_distribution_cap_levels(String testcasename,String count, String level) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Creating a new AdGroup");
		Reporter.addStepLog("Clicking on new Button");

		//Thread.sleep(2*1000);
		page.waitforLoading("Clicking on new Button");
		Thread.sleep(2*1000);
		if((couponPage.alertOkClick())==true)
		{
			couponPage.clickalertAccept();
		}
		page.waitforLoading("clickalertAccept");
		Thread.sleep(3 * 1000);
		adGroup.clickNewButton();
		//Thread.sleep(2 * 1000);
		
		Reporter.addStepLog("Clicking on ad group button");
		//Thread.sleep(2 * 1000);
		adGroup.clcikAdGroupButton();
		page.waitforLoading("clcikAdGroupButton");
		Reporter.addStepLog("Entering Ad Group Name : Automation Test Group");
		form.typeName(testcasename);
		//Thread.sleep(2*1000);
		//form.typeBilling("Sun Light Advertising Services Srl");
		//form.clickChannel();
		Reporter.addStepLog("clicking on distribution capping");
		form.clickdistbutioncaplink();
		
		Reporter.addStepLog("Enter distribution cap count");
		form.typedistbutioncapcount(count);
		
		Reporter.addStepLog("select distribution cap level");
		form.typedistbutioncaplevel(level);
		
		
		Reporter.addStepLog("select distribution cap level");
		form.clickadddistributioncap();
		
		Reporter.addStepLog("Enter distribution cap count");
		form.typedistbutioncapcount2("4");
		
		Reporter.addStepLog("select distribution cap level");
		form.typeadgrouplevel2("at the adgroup level");
		
		form.clickSaveButton();
		Reporter.addStepLog("Clicked on save button");
		
		page.waitforLoading("Clicked on save button");
		

	}
	
	
	@When("^: Campagin created Then create Ad Group with frequency cap \"([^\"]*)\"$")
	public void campagin_created_Then_create_Ad_Group_with_frequency_cap(String TestCaseName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Creating a new AdGroup");
		Reporter.addStepLog("Clicking on new Button");

		//Thread.sleep(2*1000);
		page.waitforLoading("Clicking on new Button");
		
		if((couponPage.alertOkClick())==true)
		{
			couponPage.clickalertAccept();
		}
		page.waitforLoading("clickalertAccept");
		Thread.sleep(2 * 1000);
		adGroup.clickNewButton();
		//Thread.sleep(2 * 1000);
		
		Reporter.addStepLog("Clicking on ad group button");
		//Thread.sleep(2 * 1000);
		adGroup.clcikAdGroupButton();
		page.waitforLoading("clcikAdGroupButton");
		Reporter.addStepLog("Entering Ad Group Name : Automation Test Group");
		form.typeName(TestCaseName);
		Thread.sleep(2*1000);
		//form.typeBilling("Sun Light Advertising Services Srl");
		//form.clickChannel();
		Reporter.addStepLog("Entering Ad Group Name : Automation Test Group");
		form.selectFrequncyCappingDuration("per visit");
		
		Reporter.addStepLog("Entering Ad Group Name : Automation Test Group");
		form.clickfreqaddropdown("adgroup");
		
		
		form.clickSaveButton();
		Reporter.addStepLog("Clicked on save button");
		
		page.waitforLoading("Clicked on save button");
		
	}
}
