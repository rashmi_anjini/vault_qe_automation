package com.catalina.vault.steps;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.mortbay.log.Log;

import com.catalina.vault.jsoncomparisondemo.Main;
import com.catalina.vault.jsoncomparisondemo.utils.CompareUtil;
import com.catalina.vault.jsoncomparisondemo.utils.FileUtil;
import com.catalina.vault.utils.Country;
import com.catalina.vault.utils.FileUtils;
import com.catalina.vault.utils.JsonUtils;
import com.catalina.vault.utils.NetworkUtils;
import com.catalina.vault.utils.PropertiesFile;
import com.cucumber.listener.Reporter;
import com.catalina.vault.jsoncomparisondemo.utils.Colors;
import com.catalina.vault.jsoncomparisondemo.utils.CompareUtil;
//import com.catalina.vault.jsoncomparisondemo.utils.PropertiesFile;
import com.catalina.vault.jsoncomparisondemo.utils.FileUtil;

import com.catalina.vault.steps.Verification;
import com.cucumber.listener.Reporter;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import com.google.gson.JsonParser;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import cucumber.api.Scenario;
import cucumber.api.java.en.Then;

public class Verification2 {

	private static final Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static String DMP_PROPERTY = "DMP";
	private static String STORE_PROPERTY = "STORE_PAYLOAD";
	private static String TARGETING_PROPERTY = "TARGETING";
	private static String PROPERTY_FILE_NAME = "environment";
	private static String ACTUAL_STATUS = "PUBLISHED";
	private static String STATUS= "PROMOTION_NOT_FOUND";
	private static String COUNTRY_PARAMETER = "ctry";
	private static String COUNTRY_JSON_PROPERTY = "ctry";
	private static String CHAIN_JSON_PROPERTY = "chn";
	private static String STORE_JSON_PROPERTY = "str";
	private static String CHAIN_PARAMETER = "chain";
	private static String STORE_PARAMETER = "store";
	private static String POS_JSON_PROPERTY = "pos";
	private static String POS_DATA_FILE_NAME = "posdata.txt";
	private static String RESOURCE_PATH = "./src/resource/";
	private static String TIMESTAMP_TS_JSON_PROPERTY = "ts";
	private static String TIMESTAMP_JSON_PROPERTY = "timestamp";
	private static String MESSAGE_JSON_PROPERTY = "messages";
	private static String HEADER_JSON_PROPERTY = "hdr";
	private static String MOST_RECENT_MSG_JSON_PROPERTY = "most_recent_msg";
	private static PropertiesFile propertiesFile;
	private JsonElement targetingRequestData;
	private final SharedResource sharedResource;
	public static String jsonResponse ="";
	private static ExtentReports report = null;
	private static  com.catalina.vault.jsoncomparisondemo.utils.PropertiesFile colorpropertiesFile;
	private static String respublicationsprev =null;

	public Verification2(SharedResource sharedResource) {
		LOGGER.log(Level.INFO, "Initializing Resources");
		this.propertiesFile = new PropertiesFile(PROPERTY_FILE_NAME);
		this.colorpropertiesFile = new  com.catalina.vault.jsoncomparisondemo.utils.PropertiesFile();
		this.sharedResource = sharedResource;
		
	}

	@Then("^: Verify publication object \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public static boolean verify_DMP_Promotion_on_server(String promotionId, String country, String scenario)  {
		// Write code here that turns the phrase above into concrete actions
		// COMPLETED : create a url
		// TODO : Take a promotion id
		// COMPLETED : set in url
		// COMPLETED : Hit the get request
		// COMPLETED : Take status from response
		// COMPLETED : Check the status
		// COMPLETED : If its not matched then stop flow
		
		Colors colors = new Colors();
		colors.setValueNotMatched(colorpropertiesFile.getColorForValueNotMatch());
		colors.setElementNotFound(colorpropertiesFile.getColorForElementNotFound());
		colors.setIgnoredKeywords(colorpropertiesFile.getColorForIgnoredKeyWords());
		
		//create a url
		String url = "http://v02-production.edge-node.23.eastus.api.catalina.com/dmp/rest/promotions/" +country+"-"+"BLIP"+"-"+promotionId+"/history" ;
		System.out.println("Print url" +url);
		Log.info("Print url" +url);
		//Take a promotion id and set in URL and Hit get request 
		String response = NetworkUtils.getCall(url);
		Log.info("get call" +response);
		//convert response to Jsonelement
		JsonElement element = JsonUtils.convertToJsonElement(response);
		System.out.println("Print element" +element);
		
		JsonArray jp =new JsonParser().parse(response).getAsJsonArray();
		
					int range= IntStream.range(0, jp.size())
					
								.filter(i ->  jp.get(i).getAsJsonObject().getAsJsonObject("promotion").getAsJsonArray("publications").size()>0)
								
								.findFirst()
								.orElse(-1);
			System.out.println("range print :" +range);
			boolean compareStatus = false;
			if(range > 0){
				String respublications= jp.get(range).toString();
				System.out.println("Firstresponse: " +respublications);
				
				String respublicationsprev= jp.get(range-1).toString();
				System.out.println("second Response: " +respublicationsprev);
				
				CompareUtil compareUtil = new CompareUtil();
				try {
					compareStatus = compareUtil.doesMatch(respublications,
							respublicationsprev,colors, scenario,scenario);
				} catch (Exception e) {
					// Unable to convert File to String
					System.out.println("I am here"+e);
				}
			}
			
			return compareStatus;
			
	}
			
//			 
	// return -1 if target is not found 
	
}