package com.catalina.vault.steps;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assert;
import org.mortbay.log.Log;
import com.catalina.schemaComparison.FileUtil;
import com.catalina.schemaComparison.JsonComparision;
import com.catalina.schemaComparison.ValidationUtils;
import com.catalina.vault.jsoncomparisondemo.Main;
import com.catalina.vault.utils.CacheConnection;
import com.catalina.vault.utils.Country;
import com.catalina.vault.utils.FileUtils;
import com.catalina.vault.utils.JsonUtils;
import com.catalina.vault.utils.NetworkUtils;
import com.catalina.vault.utils.PropertiesFile;
import com.cucumber.listener.Reporter;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import cucumber.api.java.en.Then;


public class Verification {

	private static final Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static String DMP_PROPERTY = "DMP";
	private static String STORE_PROPERTY = "STORE_PAYLOAD";
	private static String TARGETING_PROPERTY = "TARGETING";
	private static String PROPERTY_FILE_NAME = "environment";
	private static String ACTUAL_STATUS = "PUBLISHED";
	private static String SIMULATION_STATUS="SIMULATION";
	private static String STATUS= "PROMOTION_NOT_FOUND";
	private static String COUNTRY_PARAMETER = "ctry";
	private static String COUNTRY_JSON_PROPERTY = "ctry";
	private static String CHAIN_JSON_PROPERTY = "chn";
	private static String STORE_JSON_PROPERTY = "str";
	private static String CHAIN_PARAMETER = "chain";
	private static String STORE_PARAMETER = "store";
	private static String POS_JSON_PROPERTY = "pos";
	private static String POS_DATA_FILE_NAME = "posdata.txt";
	private static String RESOURCE_PATH = "./src/resource/";
	private static String TIMESTAMP_TS_JSON_PROPERTY = "ts";
	private static String TIMESTAMP_JSON_PROPERTY = "timestamp";
	private static String MESSAGE_JSON_PROPERTY = "messages";
	private static String HEADER_JSON_PROPERTY = "hdr";
	private static String MOST_RECENT_MSG_JSON_PROPERTY = "most_recent_msg";
	private PropertiesFile propertiesFile;
	private JsonElement targetingRequestData;
	private final SharedResource sharedResource;
	public static String jsonResponse ="";
	private static JsonElement promotion_response;

	public Verification(SharedResource sharedResource) {
		LOGGER.log(Level.INFO, "Initializing Resources");
		this.propertiesFile = new PropertiesFile(PROPERTY_FILE_NAME);
		this.sharedResource = sharedResource;
		
	}
	
	@Then("^: Clear cache$")
	public void Clear_cache() throws IOException{
		String node = "03";
		String service = "dmp";
		CacheConnection.clearCache(node, "dmp");
		CacheConnection.clearCache(node, "targeting");
		CacheConnection.clearCache(node, "proxy");
		System.out.println("POST DONE");
		
		CacheConnection.setLoggingLevel(node, service, "DEBUG");
		
		CacheConnection.getLoggerLevelForService(node, service);
	}

	@Then("^: Verify DMP Promotion on server \"([^\"]*)\" and \"([^\"]*)\"$")
	public void verify_DMP_Promotion_on_server(String server, String country) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// COMPLETED : create a url
		// TODO : Take a promotion id
		// COMPLETED : set in url
		// COMPLETED : Hit the get request
		// COMPLETED : Take status from response
		// COMPLETED : Check the status
		// COMPLETED : If its not matched then stop flow
		
		//create a url
		String url = this.propertiesFile.getProperty(server.toUpperCase() + "_"	+ DMP_PROPERTY)+(country+"-"+"BLIP"+"-");
		System.out.println("Print url" +url);
		Log.info("Print url" +url);
		//Take a promotion id and set in URL and Hit get request 
		String response = NetworkUtils.getCall(url+sharedResource.getPromotionId());
		Log.info("get call" +response);
		//convert response to Jsonelement
		JsonElement element = JsonUtils.convertToJsonElement(response);
		

		
		//Json Comparision verificaiton code
//		Verification.jsonResponse =element.toString();
		//System.out.println("name:"+sharedResource.getTestcaseName());
//		FileUtil.writeTextFile(element.toString(), "textFile/"+sharedResource.getTestcaseName());
//		JsonComparision.jsonComparison();
		//ValidationUtils.jsonComparator();
//		try (FileWriter file = new FileWriter("textFile/jsonFile.txt")) {
//			System.out.println(file);
//			file.write(element.toString());
//			System.out.println("Successfully Copied JSON Object to File...");
//			System.out.println("\nJSON Object: " + element);
//		}
		//Take Status from response when promotion is published or in draft
		JsonElement statusElement = ((JsonObject) element).get("status");
		Log.info("get status" +statusElement);
		//Take Message from response when promotion is not found
		JsonElement messageElement = ((JsonObject) element).get("message");	
		Log.info("get message" +messageElement);
		//check whether status and message is  true or false for further verification 
		boolean isStatusPresent = statusElement != null? true:false;
		Log.info("isStatusPresent:" +isStatusPresent);
		boolean isMessagePresent = messageElement != null? true:false;
		Log.info("isMessagePresent:" +isMessagePresent);
		//when isMessagePresent is true and Promotion is not found in AZURE but present in AWS enter the below if condition to post the response body from AWS to Azure
		String statusnotfound = "PROMOTION_NOT_FOUND";
		if(isMessagePresent && statusnotfound.equalsIgnoreCase(messageElement.getAsString()))
		{
		//get message promotion not found from response 
		String promostatus = ((JsonObject) element).get("message").getAsString();
		System.out.println("Status in "+server+" is "+promostatus);
		Reporter.addStepLog("Getting Status for promotion "+ sharedResource.getPromotionId() + " is " + promostatus +" in " +server);
		Log.info("Getting Status for promotion "+ sharedResource.getPromotionId() + " is " + promostatus +" in " +server);
		//set server to AWS and create URL
		String verifyserver="AWS";
		String url2 = this.propertiesFile.getProperty(verifyserver.toUpperCase() + "_"+ DMP_PROPERTY)+(country+"-"+"BLIP"+"-");
		System.out.println("Print AWS  url :" +url2);
		
		
		Log.info("Print url :" +url2);
		//Take a promotion id and set in URL and Hit get request 
		String response2 = NetworkUtils.getCall(url2 +sharedResource.getPromotionId());
		
		//convert response to Jsonelement
		JsonElement element2 = JsonUtils.convertToJsonElement(response2);
		System.out.println("AWS json respe  is "+element2);
		//get status in AWS
		String status2 = ((JsonObject) element2).get("status").getAsString();
		System.out.println("Status in "+verifyserver+" is "+status2);
		Reporter.addStepLog("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + status2 +" in " +verifyserver);
		Log.info("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + status2 +" in " +verifyserver);
		//status in AWS is Published make a post call to AZURE
		
			if(ACTUAL_STATUS.equalsIgnoreCase(status2))
			{				
				
				//set server to AZURE and set url 
				String urlpost = this.propertiesFile.getProperty(server.toUpperCase() + "_"	+ DMP_PROPERTY);
				Reporter.addStepLog("Print post url" +urlpost);
				Log.info("Print post url" +urlpost);
				
				System.out.println("PrintAZURE  post url" +urlpost);
				//Hit a post request
				String responsepost = NetworkUtils.postCall(urlpost, response2); 
				
				//verify the response
				JsonElement elementpost = JsonUtils.convertToJsonElement(responsepost);
				
				System.out.println("PrintAZURE  post json responsel" +elementpost);
				String statuspost = ((JsonObject) elementpost).get("id").getAsString();
				
				Reporter.addStepLog("Status in "+verifyserver+" is "+statuspost);
				Log.info("Status in "+verifyserver+" is "+statuspost);
				System.out.println("Status in "+verifyserver+" is "+statuspost);
				//verify data is posted
				String url4 = this.propertiesFile.getProperty(server.toUpperCase() + "_"+ DMP_PROPERTY)+(country+"-"+"BLIP"+"-");
				Reporter.addStepLog("Print url to verify data posted" +url4);
				Log.info("Print url to verify data posted" +url4);
				
				System.out.println("Print url to verify data posted" +url4);
				String response4 = NetworkUtils.getCall(url +sharedResource.getPromotionId());
				JsonElement element4 = JsonUtils.convertToJsonElement(response4);
				System.out.println("posted response" +element4);
				String status4 = ((JsonObject) element4).get("status").getAsString();
				Reporter.addStepLog("Status in "+server+" is "+status4);
				Log.info("Status in "+server+" is "+status4);
				System.out.println("Status in last"+server+" is "+status4);
			}
			else
			{
			Assert.assertEquals(ACTUAL_STATUS,status2);
			}
		}
			
			
		
//Promotion found in AZURE
		if(isStatusPresent)
		{
		String promostatus2 = statusElement.getAsString();
		
		System.out.println("Status in "+server+" is "+promostatus2);
		Log.info("Status in "+server+" is "+promostatus2);
//		Reporter.addStepLog("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + promostatus2 +" in " +server);
//		Log.info("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + promostatus2 +" in " +server);
		Assert.assertEquals(ACTUAL_STATUS, promostatus2);
		}
		
	}
	
	@Then("^: Verify DMP Simulation Promotion on server \"([^\"]*)\" and \"([^\"]*)\"$")
	public void verify_DMP_Simulation_Promotion_on_server(String server, String country) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// COMPLETED : create a url
		// TODO : Take a promotion id
		// COMPLETED : set in url
		// COMPLETED : Hit the get request
		// COMPLETED : Take status from response
		// COMPLETED : Check the status
		// COMPLETED : If its not matched then stop flow
		
		//create a url
		String url = this.propertiesFile.getProperty(server.toUpperCase() + "_"	+ DMP_PROPERTY)+(country+"-"+"BLIP"+"-");
		System.out.println("Print url" +url);
		Log.info("Print url" +url);
		//Take a promotion id and set in URL and Hit get request 
		String response = NetworkUtils.getCall(url+sharedResource.getPromotionId());
		Log.info("get call" +response);
		//convert response to Jsonelement
		JsonElement element = JsonUtils.convertToJsonElement(response);
		

		
		//Json Comparision verificaiton code
	//Verification.jsonResponse =element.toString();
		
		
		//Take Status from response when promotion is published or in draft
		JsonElement statusElement = ((JsonObject) element).get("status");
		Log.info("get status" +statusElement);
		//Take Message from response when promotion is not found
		JsonElement messageElement = ((JsonObject) element).get("message");	
		Log.info("get message" +messageElement);
		//check whether status and message is  true or false for further verification 
		boolean isStatusPresent = statusElement != null? true:false;
		Log.info("isStatusPresent:" +isStatusPresent);
		boolean isMessagePresent = messageElement != null? true:false;
		Log.info("isMessagePresent:" +isMessagePresent);
		//when isMessagePresent is true and Promotion is not found in AZURE but present in AWS enter the below if condition to post the response body from AWS to Azure
		String statusnotfound = "PROMOTION_NOT_FOUND";
		if(isMessagePresent && statusnotfound.equalsIgnoreCase(messageElement.getAsString()))
		{
		//get message promotion not found from response 
		String promostatus = ((JsonObject) element).get("message").getAsString();
		System.out.println("Status in "+server+" is "+promostatus);
		Reporter.addStepLog("Getting Status for promotion "+ sharedResource.getPromotionId() + " is " + promostatus +" in " +server);
		Log.info("Getting Status for promotion "+ sharedResource.getPromotionId() + " is " + promostatus +" in " +server);
		//set server to AWS and create URL
		String verifyserver="AWS";
		String url2 = this.propertiesFile.getProperty(verifyserver.toUpperCase() + "_"+ DMP_PROPERTY)+(country+"-"+"BLIP"+"-");
		System.out.println("Print AWS  url :" +url2);
		
		
		Log.info("Print url :" +url2);
		//Take a promotion id and set in URL and Hit get request 
		String response2 = NetworkUtils.getCall(url2 +sharedResource.getPromotionId());
		
		//convert response to Jsonelement
		JsonElement element2 = JsonUtils.convertToJsonElement(response2);
		System.out.println("AWS json respe  is "+element2);
		//get status in AWS
		String status2 = ((JsonObject) element2).get("status").getAsString();
		System.out.println("Status in "+verifyserver+" is "+status2);
		Reporter.addStepLog("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + status2 +" in " +verifyserver);
		Log.info("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + status2 +" in " +verifyserver);
		//status in AWS is Published make a post call to AZURE
		
			if(SIMULATION_STATUS.equalsIgnoreCase(status2))
			{				
				
				//set server to AZURE and set url 
				String urlpost = this.propertiesFile.getProperty(server.toUpperCase() + "_"	+ DMP_PROPERTY);
				Reporter.addStepLog("Print post url" +urlpost);
				Log.info("Print post url" +urlpost);
				
				System.out.println("PrintAZURE  post url" +urlpost);
				//Hit a post request
				String responsepost = NetworkUtils.postCall(urlpost, response2); 
				
				//verify the response
				JsonElement elementpost = JsonUtils.convertToJsonElement(responsepost);
				
				System.out.println("PrintAZURE  post json responsel" +elementpost);
				String statuspost = ((JsonObject) elementpost).get("id").getAsString();
				
				Reporter.addStepLog("Status in "+verifyserver+" is "+statuspost);
				Log.info("Status in "+verifyserver+" is "+statuspost);
				System.out.println("Status in "+verifyserver+" is "+statuspost);
				//verify data is posted
				String url4 = this.propertiesFile.getProperty(server.toUpperCase() + "_"+ DMP_PROPERTY)+(country+"-"+"BLIP"+"-");
				Reporter.addStepLog("Print url to verify data posted" +url4);
				Log.info("Print url to verify data posted" +url4);
				
				System.out.println("Print url to verify data posted" +url4);
				String response4 = NetworkUtils.getCall(url +sharedResource.getPromotionId());
				JsonElement element4 = JsonUtils.convertToJsonElement(response4);
				System.out.println("posted response" +element4);
				String status4 = ((JsonObject) element4).get("status").getAsString();
				Reporter.addStepLog("Status in "+server+" is "+status4);
				Log.info("Status in "+server+" is "+status4);
				System.out.println("Status in last"+server+" is "+status4);
			}
			else
			{
			Assert.assertEquals(SIMULATION_STATUS,status2);
			}
		}
			
			
		
//Promotion found in AZURE
		if(isStatusPresent)
		{
		String promostatus2 = statusElement.getAsString();
		System.out.println("Status in "+server+" is "+promostatus2);
		Log.info("Status in "+server+" is "+promostatus2);
		Reporter.addStepLog("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + promostatus2 +" in " +server);
		Log.info("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + promostatus2 +" in " +server);
		Assert.assertEquals(SIMULATION_STATUS, promostatus2);
		}
	}
	
	@Then("^: Verify LMC List \"([^\"]*)\"$")
	public void Verify_LMC_List(String list) throws Throwable {
		String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
		System.out.println(timeStamp);

		JsonObject products = (JsonObject) ((JsonObject) promotion_response).get("products");
		// JsonObject inventory = (JsonObject)products.get("inventory");

		JsonArray inventorys = products.get("inventory").getAsJsonArray();

		// JsonArray lists = inventorys.getAsJsonArray();
		// System.out.println("get status1" +lists);

		//
		for (JsonElement invent : inventorys) {
			JsonArray listarr = invent.getAsJsonObject().get("lists").getAsJsonArray();
			for (JsonElement listarrays : listarr) {
				JsonArray rowsarray = listarrays.getAsJsonObject().get("rows").getAsJsonArray();
				System.out.println("get status row list" + rowsarray);

				//Assert.assertNotEquals(rowsarray.toString(), "[]");
//[]
				for (JsonElement rows : rowsarray) {
					String rowlist = rows.getAsString();
					System.out.println("get status row list2" + rowlist);

				}
			}
		}
		String lmcUrl = this.propertiesFile.getProperty("PRODUCTS_INVENTORY");
		String lmcResponse = NetworkUtils.getCall(lmcUrl);
		JsonElement lmcElement = JsonUtils.convertToJsonElement(lmcResponse);
		System.out.println(lmcElement.toString());
		String date = timeStamp.substring(6);
		int lmcDate = Integer.parseInt(timeStamp) - 1;
		String dateString = String.valueOf(lmcDate);
		JsonObject lmcObj = ((JsonObject) lmcElement).getAsJsonObject();
		JsonArray lmcarr = lmcObj.get(dateString).getAsJsonArray();
//		JsonArray lmcarr = (JsonArray) lmcObj.getAsJsonArray().get(lmcDate);

		for (JsonElement lmcArray : lmcarr) {
			System.out.println(lmcArray);

		}

	}
	
		
	@Then("^: Verify Json response \"([^\"]*)\" and \"([^\"]*)\"$")
	public void verify_JSON_response(String server, String country) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// COMPLETED : create a url
		// TODO : Take a promotion id
		// COMPLETED : set in url
		// COMPLETED : Hit the get request
		// COMPLETED : Take status from response
		// COMPLETED : Check the status
		// COMPLETED : If its not matched then stop flow
		String url = this.propertiesFile.getProperty(server.toUpperCase() + "_"
				+ DMP_PROPERTY)+(country+"-"+"BLIP"+"-");
		System.out.println("Print url" +url);
		String response = NetworkUtils.getCall(url 	+ sharedResource.getPromotionId());
		JsonElement element = JsonUtils.convertToJsonElement(response);
   //     Verification.jsonResponse =element.toString();
		
		/*String status = ((JsonObject) element).get("status").getAsString();
		//System.out.println(status);
		Reporter.addStepLog("Getting Status for promotion " 
				+ sharedResource.getPromotionId() + " is " + status);
		Assert.assertEquals(ACTUAL_STATUS, status);*/
		
		
		}
			
		
		
	

	@Then("^: Verify Store Payload on server \"([^\"]*)\" with parameters country \"([^\"]*)\", chain \"([^\"]*)\", store \"([^\"]*)\"$")
	public void verify_Store_Payload_on_server_with_parameters_country_chain_store(
			String server, String country, String chain, String store) {
		// Write code here that turns the phrase above into concrete actions
		// COMPLETED : create a url
		// COMPLETED : set in url
		// COMPLETED : Hit the get request
		// COMPLETED : Take pos from response
		// COMPLETED : Check the pos
		// COMPLETED : If its not matched then stop flow
		String url = this.propertiesFile.getProperty(server + "_"
				+ STORE_PROPERTY);
		String response = NetworkUtils.getCallWithQueryParameter(url,
				COUNTRY_PARAMETER + "=" + country, CHAIN_PARAMETER + "="
						+ chain, STORE_PARAMETER + "=" + store);

		JsonElement element = JsonUtils.convertToJsonElement(response);
		// Assert.assertEquals("null", ((JsonObject)
		// element).get(POS_JSON_PROPERTY));

		LOGGER.log(Level.INFO, "Getting Response" + element);

		targetingRequestData = createJsonObject(element,
				String.valueOf(Country.getCode(country)), chain, store);
		LOGGER.log(Level.INFO, "Generated data" + targetingRequestData);
	}

	@Then("^: Verify Targeting on server \"([^\"]*)\"$")
	public void verify_Targeting_on_server(String server) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// COMPLETED : create a url
		// COMPLETED : set in url
		// TODO : Create data
		// COMPLETED : Hit the post request
		// COMPLETED : Take response
		// TODO : Check the response
		// TODO : If its not matched then stop flow
		String url = this.propertiesFile.getProperty(server + "_"
				+ TARGETING_PROPERTY);
		String response = NetworkUtils.postCall(url,
				targetingRequestData.toString());
		JsonElement element = JsonUtils.convertToJsonElement(response);

		LOGGER.log(Level.INFO, "Getting Targeting Response :-\n" + element);

	}

	private JsonElement createJsonObject(JsonElement element, String country,
			String chain, String store) {
		// TODO Auto-generated method stub
		String dataToInsert = FileUtils.readFile(RESOURCE_PATH
				+ POS_DATA_FILE_NAME);
		JsonObject posData = (JsonObject) JsonUtils
				.convertToJsonElement(dataToInsert);
		long timestamp = getTime();

		LOGGER.log(Level.INFO, "Time Generated " + timestamp);
		posData.add(
				MESSAGE_JSON_PROPERTY,
				changeArrayData(posData.getAsJsonArray(MESSAGE_JSON_PROPERTY),
						country, chain, store, timestamp));
		posData.get(MOST_RECENT_MSG_JSON_PROPERTY).getAsJsonObject()
				.get(HEADER_JSON_PROPERTY).getAsJsonObject()
				.addProperty(COUNTRY_JSON_PROPERTY, country);
		posData.get(MOST_RECENT_MSG_JSON_PROPERTY).getAsJsonObject()
				.get(HEADER_JSON_PROPERTY).getAsJsonObject()
				.addProperty(CHAIN_JSON_PROPERTY, chain);
		posData.get(MOST_RECENT_MSG_JSON_PROPERTY).getAsJsonObject()
				.get(HEADER_JSON_PROPERTY).getAsJsonObject()
				.addProperty(STORE_JSON_PROPERTY, store);
		posData.get(MOST_RECENT_MSG_JSON_PROPERTY).getAsJsonObject()
				.get(HEADER_JSON_PROPERTY).getAsJsonObject()
				.addProperty(TIMESTAMP_TS_JSON_PROPERTY, timestamp);
		((JsonObject) element).add(POS_JSON_PROPERTY, posData);
		((JsonObject) element).addProperty(TIMESTAMP_JSON_PROPERTY, timestamp);
		return element;
	}

	private long getTime() {
		// TODO Auto-generated method stub
		return System.currentTimeMillis() / 1000;
	}

	private JsonArray changeArrayData(JsonArray array, String country,
			String chain, String store, long time) {
		// TODO Auto-generated method stub
		for (int i = 0; i < array.size(); i++) {
			((JsonObject) array.get(i)).get(HEADER_JSON_PROPERTY)
					.getAsJsonObject()
					.addProperty(COUNTRY_JSON_PROPERTY, country);
			((JsonObject) array.get(i)).get(HEADER_JSON_PROPERTY)
					.getAsJsonObject().addProperty(CHAIN_JSON_PROPERTY, chain);
			((JsonObject) array.get(i)).get(HEADER_JSON_PROPERTY)
					.getAsJsonObject().addProperty(STORE_JSON_PROPERTY, store);
			((JsonObject) array.get(i)).get(HEADER_JSON_PROPERTY)
					.getAsJsonObject()
					.addProperty(TIMESTAMP_TS_JSON_PROPERTY, time);

		}
		return array;
	}

	public void removeBadPromotions(String server) {

	}

	public void pausePromotion(String url) {
		NetworkUtils.putCall(url, url);
	}

	
	@Then("^: Verify DMP Promotion on servers \"([^\"]*)\" and \"([^\"]*)\"$")
	public void verify_DMP_Promotion_on_servers(String server, String country) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// COMPLETED : create a url
		// TODO : Take a promotion id
		// COMPLETED : set in url
		// COMPLETED : Hit the get request
		// COMPLETED : Take status from response
		// COMPLETED : Check the status
		// COMPLETED : If its not matched then stop flow
		
		//create a url
		String url = this.propertiesFile.getProperty(server.toUpperCase() + "_"	+ DMP_PROPERTY)+(country+"-"+"BLIP"+"-");
		System.out.println("Print url" +url);
		Log.info("Print url" +url);
		//Take a promotion id and set in URL and Hit get request 
		String response = NetworkUtils.getCall(url+sharedResource.getPromotionId());
		Log.info("get call" +response);
		//convert response to Jsonelement
		JsonElement element = JsonUtils.convertToJsonElement(response);
		

		
		//Json Comparision verificaiton code
	//Verification.jsonResponse =element.toString();
		
		
		//Take Status from response when promotion is published or in draft
		JsonElement statusElement = ((JsonObject) element).get("status");
		Log.info("get status" +statusElement);
		//Take Message from response when promotion is not found
		JsonElement messageElement = ((JsonObject) element).get("message");	
		Log.info("get message" +messageElement);
		//check whether status and message is  true or false for further verification 
		boolean isStatusPresent = statusElement != null? true:false;
		Log.info("isStatusPresent:" +isStatusPresent);
		boolean isMessagePresent = messageElement != null? true:false;
		Log.info("isMessagePresent:" +isMessagePresent);
		//when isMessagePresent is true and Promotion is not found in AZURE but present in AWS enter the below if condition to post the response body from AWS to Azure
		String statusnotfound = "PROMOTION_NOT_FOUND";
		
//Promotion found in server
		if(isStatusPresent)
		{
		String promostatus = statusElement.getAsString();
		System.out.println("Status in "+server+" is "+promostatus);
		Log.info("Status in "+server+" is "+promostatus);
		Reporter.addStepLog("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + promostatus +" in " +server);
		Log.info("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + promostatus +" in " +server);
		Assert.assertEquals(ACTUAL_STATUS, promostatus);
		}
		else{
			Assert.assertEquals(STATUS, statusnotfound);
			System.out.println("statusnotfound" +statusnotfound);
			Log.info("Getting Status for promotion "	+ sharedResource.getPromotionId() + " is " + statusnotfound +" in " +server);
		}
	}
}
