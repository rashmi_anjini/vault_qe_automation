package com.catalina.vault.steps;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.catalina.vault.entities.PerformanceTiming;
import com.catalina.vault.pagefactory.LoginPage;
import com.catalina.vault.pagefactory.MainPage;
import com.catalina.vault.utils.ScreenShot;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Login {

	// driver will help us to contain current session
	WebDriver driver;
	LoginPage lPage;
	String url = "https://sqa.dashboard.personalization.catalinamarketing.com/";
	String userName = "cs.auto4@catalina.com";
	String password = "Catalina123";
	private static final Logger Log = LogManager.getLogger(Login.class);
	
	public Login(SharedResource shareResource) {
		driver = shareResource.init();
		lPage = new LoginPage(driver);
	}

	@Given("^: Url of application Navigate to webpage$")
	public void url_of_application_Navigate_to_webpage() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("System Name : "+System.getenv().get("COMPUTERNAME"));
		Log.info("System Name : "+System.getenv().get("COMPUTERNAME"));
		Reporter.addStepLog("Opening url " + url);
		driver.navigate().to(url);
		Log.info("Opening url " + url);
		driver.manage().window().maximize();
		
	}

	@Then("^: Fill credentails$")
	public void fill_credentails() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Filling Credentials");
		lPage.enterUserName(userName);
		Log.info("Entered User Name " + userName);
		Reporter.addStepLog("Entered User Name " + userName);
		lPage.enterPassword(password);
		Log.info("Enter password " + password);
		Reporter.addStepLog("Enter password " + password);
System.out.println("entered password");
PerformanceTiming.writePerfMetricasJSON(this.driver,"C://Catalina//Vault//Demo3.json","Fill credentails");	

	}

	@Then("^: Click on Login$")
	public void click_on_Login() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		lPage.clickLogin();
		Reporter.addStepLog("clicked on login " + userName);
		//LOG.info("clicked on login " );
		Log.trace("clicked on login " );
		//ScreenShot.addScreenshotToScreen(this.driver, "After Clicked on Login");
		PerformanceTiming.writePerfMetricasJSON(this.driver,"C://Catalina//Vault//Demo3.json","Click on Login");
	}

	

}
