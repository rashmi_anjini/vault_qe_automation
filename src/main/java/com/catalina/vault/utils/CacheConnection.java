/**
 * 
 */
package com.catalina.vault.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

/**
 * @author csauto4
 *
 */
public class CacheConnection {

	private static String cacheUrl = "http://v02-sqa.edge-node.03.westeurope.api.catalina.com/targeting/jmx/servers/0/domains/com.catalina.platform.api.cache.management/mbeans/name%3DcacheInvalidationManager%2Ctype%3DCacheInvalidationManager/operations/invalidate()?skin=embedded";

	private static String getLoggerUrl = "http://v02-sqa.edge-node.<env>.westeurope.api.catalina.com/<service>/jmx/servers/0/domains/"
			+ "ch.qos.logback.classic/mbeans/Name%3Ddefault%2CType%3Dch.qos.logback.classic.jmx.JMXConfigurator/operations/"
			+ "getLoggerLevel(java.lang.String)?skin=embedded";

	private static String setLoggerUrl = "http://v02-sqa.edge-node.01.westeurope.api.catalina.com/dmp/jmx/servers/0/domains/"
			+ "ch.qos.logback.classic/mbeans/Name%3Ddefault%2CType%3Dch.qos.logback.classic.jmx.JMXConfigurator/operations/"
			+ "setLoggerLevel%28java.lang.String%2Cjava.lang.String%29?skin=embedded&ok=1";
	private static final String USER_AGENT = "Mozilla/5.0";

	public static void clearCache(String node, String service) throws IOException {
		final String postParams = "executed=true";

		String buildCacheUrl = cacheUrl.replace("<env>", node);
		buildCacheUrl = buildCacheUrl.replace("<service>", service);
		System.out.println("cacheUrl :: " + buildCacheUrl);
		URL obj = new URL(buildCacheUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("User-Agent", "application/x-www-form-urlencoded");
		con.setRequestProperty("Connection", "keep-alive");
		con.setRequestProperty("Accept-Encoding", "gzip, deflate");
		con.setRequestProperty("Cache-Control", "max-age=0");

		con.setRequestProperty("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
		con.setRequestProperty("Accept-Encoding", " gzip, deflate");
		con.setRequestProperty("Accept-Language", " en-US,en;q=0.9");
		con.setRequestProperty("Cache-Control", " max-age=0");
		con.setRequestProperty("Connection", " keep-alive");
		con.setRequestProperty("Content-Length", " 13");
		con.setRequestProperty("Content-Type", " application/x-www-form-urlencoded");
		con.setRequestProperty("Cookie",
				" _ga=GA1.2.339890881.1555444907; OptanonConsent=landingPath=https%3A%2F%2Fwww.catalina.com%2F&datestamp=Wed+Aug+28+2019+09%3A10%3A10+GMT-0400+(Eastern+Daylight+Time)&version=4.5.0&EU=false&groups=1%3A1%2C2%3A0%2C3%3A0%2C4%3A0%2C0_88592%3A0%2C0_88591%3A0%2C0_182541%3A0%2C0_182540%3A0%2C0_182543%3A0%2C0_182542%3A0%2C101%3A0%2C102%3A0%2C103%3A0%2C104%3A0%2C105%3A0%2C106%3A0%2C107%3A0");

		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(postParams.getBytes());
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println("Post response:" + response.toString());
		} else {
			System.out.println("POST request not worked");
		}
	}

	public static void setLoggingLevel(String node, String service, String loggerLevel) throws IOException {
		final String postParams = "executed=true&param=com.catalina.platform&param=" + loggerLevel;

		String buildLoggerUrl = setLoggerUrl.replace("<env>", node);
		buildLoggerUrl = buildLoggerUrl.replace("<service>", service);
		System.out.println("setLoggerUrl :: " + buildLoggerUrl);
		URL obj = new URL(buildLoggerUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("User-Agent", "application/x-www-form-urlencoded");
		con.setRequestProperty("Connection", "keep-alive");
		con.setRequestProperty("Accept-Encoding", "gzip, deflate");
		con.setRequestProperty("Cache-Control", "max-age=0");

		con.setRequestProperty("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
		con.setRequestProperty("Accept-Encoding", " gzip, deflate");
		con.setRequestProperty("Accept-Language", " en-US,en;q=0.9");
		con.setRequestProperty("Cache-Control", " max-age=0");
		con.setRequestProperty("Connection", " keep-alive");
		con.setRequestProperty("Content-Length", " 13");
		con.setRequestProperty("Content-Type", " application/x-www-form-urlencoded");
		con.setRequestProperty("Cookie",
				" _ga=GA1.2.339890881.1555444907; OptanonConsent=landingPath=https%3A%2F%2Fwww.catalina.com%2F&datestamp=Wed+Aug+28+2019+09%3A10%3A10+GMT-0400+(Eastern+Daylight+Time)&version=4.5.0&EU=false&groups=1%3A1%2C2%3A0%2C3%3A0%2C4%3A0%2C0_88592%3A0%2C0_88591%3A0%2C0_182541%3A0%2C0_182540%3A0%2C0_182543%3A0%2C0_182542%3A0%2C101%3A0%2C102%3A0%2C103%3A0%2C104%3A0%2C105%3A0%2C106%3A0%2C107%3A0");

		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(postParams.getBytes());
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println("Post response:" + response.toString());
		} else {
			System.out.println("POST request not worked");
		}
	}

	/**
	 * 
	 * @param node
	 * @param service
	 * @param loggerLevel
	 * @throws IOException
	 */
	public static void getLoggerLevelForService(String node, String service) throws IOException {
		final String postParams = "executed=true&param=com.catalina.platform";
		String buildLoggerUrl = getLoggerUrl.replace("<env>", node);
		buildLoggerUrl = buildLoggerUrl.replace("<service>", service);
		System.out.println("getLoggerUrl :: " + buildLoggerUrl);

		URL obj = new URL(buildLoggerUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("User-Agent", "application/x-www-form-urlencoded");
		con.setRequestProperty("Connection", "keep-alive");
		con.setRequestProperty("Accept-Encoding", "gzip, deflate");
		con.setRequestProperty("Cache-Control", "max-age=0");

		con.setRequestProperty("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
		// con.setRequestProperty("Accept-Encoding", " gzip, deflate");
		con.setRequestProperty("Accept-Language", " en-US,en;q=0.9");
		con.setRequestProperty("Cache-Control", " max-age=0");
		con.setRequestProperty("Connection", " keep-alive");
		con.setRequestProperty("Content-Length", " 13");
		con.setRequestProperty("Content-Type", " application/x-www-form-urlencoded");
		con.setRequestProperty("Cookie",
				" _ga=GA1.2.339890881.1555444907; OptanonConsent=landingPath=https%3A%2F%2Fwww.catalina.com%2F&datestamp=Wed+Aug+28+2019+09%3A10%3A10+GMT-0400+(Eastern+Daylight+Time)&version=4.5.0&EU=false&groups=1%3A1%2C2%3A0%2C3%3A0%2C4%3A0%2C0_88592%3A0%2C0_88591%3A0%2C0_182541%3A0%2C0_182540%3A0%2C0_182543%3A0%2C0_182542%3A0%2C101%3A0%2C102%3A0%2C103%3A0%2C104%3A0%2C105%3A0%2C106%3A0%2C107%3A0");
		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(postParams.getBytes());
		os.flush();
		os.close();

		int responseCode = con.getResponseCode();

		System.out.println("GET Response Code :: " + responseCode);
		System.out.println("GET Response message :: " + con.getResponseMessage());
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = null;
			String inputLine;
			StringBuffer response = new StringBuffer();

			if ("gzip".equals(con.getContentEncoding())) {
				in = new BufferedReader(new InputStreamReader(new GZIPInputStream(con.getInputStream())));
			} else {
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			}

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println("Logger Level get: " + response.toString());
		} else {
			System.out.println("GET request not worked");
		}

	}
}
