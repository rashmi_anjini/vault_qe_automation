package com.catalina.vault.utils;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class PropertiesFile {
	private Properties p;
	private String fileName;
	

	public PropertiesFile(String fileName) {
		
		
		FileReader reader = null;
		this.fileName = fileName;
		p = new Properties();
		try {
			String rootPath = System.getProperty("user.dir");
			System.out.println("Path of the file" + rootPath
					+ "//prp//" + this.fileName + ".properties");
			reader = new FileReader(rootPath+"//prp//" + this.fileName + ".properties");
			
			p.load(reader);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public PropertiesFile(String fileName, String userRelativefilePath) {
		
		
		FileReader reader = null;
		this.fileName = fileName;
		p = new Properties();
		try {
//			String rootPath = System.getProperty("user.dir");
			reader = new FileReader(userRelativefilePath + this.fileName + ".properties");
			System.out.println("Path of the new  file" + userRelativefilePath + this.fileName + ".properties");
			p.load(reader);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getProperty(String property) {
		return p.getProperty(property);
	}

	public void SetProperty(String propertyName, String PropertyValue) {
		p.setProperty(propertyName, PropertyValue);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(".//prp//" + this.fileName + ".properties");
			p.store(fos, "Runtime Decision of " + propertyName);
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void SetPropertyfolder(String propertyName, String PropertyValue ) {
		p.setProperty(propertyName, PropertyValue);
		FileOutputStream fos;
		try {
			String rootPath = System.getProperty("user.dir");
			fos = new FileOutputStream("//test_storage1/spliceshare/VaultPromotionPause//" + this.fileName + ".properties");
			System.out.println(fos);
			p.store(fos, "Runtime Decision of " + propertyName);
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
