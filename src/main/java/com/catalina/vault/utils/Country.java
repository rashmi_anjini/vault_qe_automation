package com.catalina.vault.utils;

public enum Country {

	USA(250);

	Country(int code) {
		this.code = code;
	}

	private int code;

	public static int getCode(String country) {
		switch (country) {
		case "USA":
			return USA.getCode();

		}
		return 0;
	}

	public int getCode() {
		return code;
	}
}
