package com.catalina.vault.utils;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;
import com.google.common.io.Files;

import cucumber.api.Scenario;

public class ScreenShot {

	public static Scenario scenario;

	public static void addScreenshotToScreen(WebDriver driver, String imageName)
			throws InterruptedException, IOException {
		Thread.sleep(5 * 1000);
		File sourcePath = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);

		File destinationPath = new File(System.getProperty("user.dir")
				+ "/target/cucumber-reports/"
				+ (scenario.getId() + scenario.getName() + imageName).replace(
						";", "").replace(":", "") + ".png");
		Files.copy(sourcePath, destinationPath);

		Reporter.addStepLog("<img src=\"./cucumber-reports/"
				+ (scenario.getId() + scenario.getName() + imageName).replace(
						";", "").replace(":", "") + ".png"
				+ "\" style=\"height:600px;width:100%\">");
	}
}
