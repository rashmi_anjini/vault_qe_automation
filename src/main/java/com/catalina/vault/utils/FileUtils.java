package com.catalina.vault.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import com.catalina.vault.pagefactory.BaseClass;
import com.catalina.vault.steps.AudienceSegmentSteps;
import com.catalina.vault.steps.SharedResource;

/**
 * 
 * @author automation
 *
 */
public class FileUtils {

	public static String audienceFileUSA;
	public static String audienceFileFRA;
	public static String audienceFileITA;
	public static String customFileUSA;
	public static String customFileFRA;
	public static String customFileITA;
	public static String pinsUSA;
	public static String pinsFRA;
	public static String pinsITA;
	public static String pins;
	public static String ab;
	public static String b;
	public static String a;
	public static String Itemunits;
	public static String ItemSpend;
	public static String ManualUpc;
	private static PropertiesFile propertiesFile;
	private static PropertiesFile propFile;
	static String environmentPropertiesFilePath = System.getProperty("user.dir") + "\\prp\\environment.properties";
	static SharedResource shareResource =new SharedResource();
	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */
	public static List<File> getFilesInFolder(String folderPath, String fileNamePattern) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);

		File files[] = folder.listFiles();
		if (files == null)
			return list;
		for (File file : files) {
			if (file.getName().contains(fileNamePattern))
				list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */
	public static List<File> getFilesInFolder(String folderPath, String fileNamePattern, String fileExtension) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return list;
		for (File file : files) {
			if (file.getName().contains(fileNamePattern) && file.getName().contains(fileExtension))
				list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */

	/**
	 * 
	 * @param folderPath
	 */
	public static List<File> getFilesInFolder(String folderPath) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return list;
		for (File file : files) {
			list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param sourceFolderPath
	 * @param targetFolderPath
	 */
	public static boolean moveFilesToFolder(String sourceFolderPath, String targetFolderPath) {
		File folder = new File(sourceFolderPath);
		boolean flag = true;
		File files[] = folder.listFiles();
		if (files == null)
			return false;
		for (File file : files) {
			if (file.renameTo(new File(targetFolderPath + file.getName()))) {
				file.delete();
			} else {
				flag = false;
			}
		}
		return flag;
	}

	/**
	 * 
	 * @param sourceFolderPath
	 * @param targetFolderPath
	 * @param fileName
	 */
	public static boolean moveFilesToFolder(String sourceFolderPath, String targetFolderPath, String fileName) {
		File folder = new File(sourceFolderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return false;
		for (File file : files) {
			if (file.getName().contains(fileName))
				if (file.renameTo(new File(targetFolderPath + file.getName()))) {
					file.delete();
					return true;
				}
		}
		return false;
	}

	/**
	 * 
	 * @param folderPath
	 * @return
	 */
	public static boolean deleteFilesInFolder(String folderPath) {
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return false;
		try {
			for (File file : files)
				file.delete();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static String fileTOString(File file) {
		StringBuilder str = new StringBuilder();
		try (FileInputStream fin = new FileInputStream(file)) {
			int i = 0;
			while ((i = fin.read()) != -1) {
				str.append((char) i);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str.toString();
	}

	/**
	 * This metjod returns the windows equivalent of the linux shared rive path
	 * 
	 * @param linuxPath
	 * @param windowsSharedDrive
	 * @return
	 */
	public static String getWindowsPath(String linuxPath, String windowsSharedDrive) {
		String tokens[] = linuxPath.split("/");

		StringBuffer buf = new StringBuffer();
		for (String token : tokens)
			if (!token.trim().isEmpty())
				buf.append("\\").append(token);

		return "\\\\" + windowsSharedDrive + buf;
	}

	public static String readFile(String fileName) {
		File file = new File(fileName);
		return fileTOString(file);
	}

	public static String getFileByPattern(String path, String pattern) {
		// TODO Auto-generated method stub
		Pattern pattern1 = Pattern.compile(pattern.replace("\\", "").replace("*", "[a-zA-Z0-9]*"));
		Matcher matcher = null;
		File folder = new File(path);
		String name = null;
		for (File file : folder.listFiles()) {
			matcher = pattern1.matcher(file.getName());
			if (matcher.lookingAt())
				name = file.getName().substring(0, file.getName().lastIndexOf("."));
		}
		return "\\" + name;
	}
	
	public static String getPINsFile(String country) {

		if (country.equalsIgnoreCase("USA")) {
			pins = pinsUSA;
			System.out.println("FileUtil " + pins);
		} else if (country.equalsIgnoreCase("FRA")) {
			pins = pinsFRA;
			System.out.println("FileUtil " + pins);
		} else if (country.equalsIgnoreCase("ITA")) {
			pins = pinsITA;
			System.out.println("FileUtil " + pins);
		}

		return pins;
	}
	public static void createPINTxtFiles(String array1, String FilePath1) throws IOException {
		String timeStamp = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());
		System.out.println(timeStamp);
		
			System.out.println("Creating Pin List txt file");

			FileWriter fileWriter = new FileWriter(FilePath1  +"_PINs_"+ array1 + "_" + timeStamp + ".txt");

		
				if (array1.equals("USA")) {
					pinsUSA = com.catalina.vault.steps.SharedResource.getBuildName() + "_PINS_" + array1 + "_" + timeStamp;
			}
//					else if (array[i].equals("ITA")) {
//					audienceFileITA = com.catalina.vault.steps.AudienceSegmentSteps.getValueFromPropertiesFile(
//							environmentPropertiesFilePath, "BUILD_NAME") + "_" + array[i] + "_" + timeStamp;
//				} else if (array[i].equals("FRA")) {
//					audienceFileFRA = com.catalina.vault.steps.AudienceSegmentSteps.getValueFromPropertiesFile(
//							environmentPropertiesFilePath, "BUILD_NAME") + "_" + array[i] + "_" + timeStamp;
//				}
			
		

}


	//
	static Random rand = new Random();
	static long endNumber = 1000000000L + ((long) rand.nextInt(800000000) * 10) + rand.nextInt(100);

	public static void createRegionsAudienceListFile() throws IOException {

		// To read the build name -- Environ Properties --
		String path = System.getProperty("user.dir") + "\\prp\\environment.properties";
		Properties properties = new Properties();
		FileReader reader = new FileReader(path);
		properties.load(reader);
		if (System.getProperty("BUILD_NAME") == null) {
			String BUILD_NAME_Jenkins=properties.getProperty("BUILD_NAME");
			shareResource.setBuildName(BUILD_NAME_Jenkins);
			} else {
			String Build_NAME_propFile=System.getProperty("BUILD_NAME");
			shareResource.setBuildName(Build_NAME_propFile);
			}
		String BUILD_NAME=SharedResource.getBuildName();
		
		
		String[] regions = properties.get("regions").toString().split(",");
		String folderPath = properties.get("sharedPath") + ""; // removed
		System.out.println("sharedPath:"+folderPath);

		File file = new File(folderPath);
		if (!file.exists()) {
			createTxtFiles(regions, properties.get("sharedPath") + "" + BUILD_NAME);
			createTestData(regions, properties.get("sharedPath") + "" + BUILD_NAME, endNumber);
		} else {

			createTxtFiles(regions, properties.get("sharedPath") + "" + BUILD_NAME);
			createTestData(regions, properties.get("sharedPath") + "" + BUILD_NAME, endNumber);
			System.out.println("Already directory exists");
		}

	}
	
	public static void createRegionsPINsListFile() throws IOException {

		// To read the build name -- Environ Properties --
		String path = System.getProperty("user.dir") + "\\prp\\environment.properties";
		Properties properties = new Properties();
		
		
		FileReader reader = new FileReader(path);
		properties.load(reader);

		
		if (System.getProperty("BUILD_NAME") == null) {
			String BUILD_NAME_propfile=properties.getProperty("BUILD_NAME");
			shareResource.setBuildName(BUILD_NAME_propfile);
			} else {
			String Build_NAME_jenkins=System.getProperty("BUILD_NAME");
			shareResource.setBuildName(Build_NAME_jenkins);
			}
		String BUILD_NAME=SharedResource.getBuildName();
		System.out.println("BuildName"+BUILD_NAME);
		//String[] regions = properties.get("regions").toString().split(",");
		String folderPath = properties.get("sharedPath") + ""; // removed
		System.out.println("sharedPath:"+folderPath);

		File file = new File(folderPath);
		if (!file.exists()) {
			createPINTxtFiles("USA", properties.get("sharedPath") + "" + BUILD_NAME);
			createPINTestData("USA", properties.get("sharedPath") + "" + BUILD_NAME, endNumber);
		} else {

			createPINTxtFiles("USA", properties.get("sharedPath") + "" + BUILD_NAME);
			createPINTestData("USA", properties.get("sharedPath") + "" + BUILD_NAME, endNumber);
			System.out.println("Already directory exists");
		}

	}
	public static void createPINTestData(String array, String Filepath, long n2) throws IOException {
		String timeStamp = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());
		
			FileWriter fileWriter = new FileWriter(Filepath  +"_PINs_"+ array + "_" + timeStamp + ".txt");
			System.out.println(Filepath  +"_PINs_"+ array + "_" + timeStamp + ".txt");
			BufferedWriter pinpath = new BufferedWriter(fileWriter);
			  
			for (int j = 1; j <= 100; j++) {			
					long number = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
					// System.out.println(number);					 
					 pinpath.write(String.valueOf(number));
					 pinpath.newLine();
				}
			pinpath.close();
	}


	public static void createTxtFiles(String[] array, String FilePath) throws IOException {
		String timeStamp = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());
		System.out.println(timeStamp);
		for (int i = 0; i < array.length; i++) {
			System.out.println("Enter");

			FileWriter fileWriter = new FileWriter(FilePath + "_" + array[i] + "_" + timeStamp + ".txt");

			
	for (int j = 1; j <= 3; j++) {
				if (array[i].equals("USA")) {
					
				 	audienceFileUSA = com.catalina.vault.steps.SharedResource.getBuildName() + "_" + array[i] + "_" + timeStamp;
					
				} else if (array[i].equals("ITA")) {
					audienceFileITA =  com.catalina.vault.steps.SharedResource.getBuildName()+ "_" + array[i] + "_" + timeStamp;
				} else if (array[i].equals("FRA")) {
					audienceFileFRA =  com.catalina.vault.steps.SharedResource.getBuildName()+ "_" + array[i] + "_" + timeStamp;
				}
			}
		}
	
}
	
	
	
	public static void createCustomTextFile(String[] array, String FilePath) throws IOException {
		String timeStamp = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());
		System.out.println(timeStamp);
		for (int i = 0; i < 1; i++) {
			System.out.println("Enter");

			FileWriter fileWriter = new FileWriter(FilePath + "Param_" + array[i] + "_" + timeStamp + ".txt");
			
			System.out.println(fileWriter);

			for (int j = 1; j <= 1; j++) {
				if (array[i].equals("USA")) {
					customFileUSA = "Param" + com.catalina.vault.steps.AudienceSegmentSteps.getValueFromPropertiesFile(
							environmentPropertiesFilePath, "BUILD_NAME") + "_" + array[i] + "_" + timeStamp;
//				} else if (array[i].equals("ITA")) {
//					customFileITA = "Param" + com.catalina.vault.steps.AudienceSegmentSteps.getValueFromPropertiesFile(
//							environmentPropertiesFilePath, "BUILD_NAME") + "_" + array[i] + "_" + timeStamp;
//				} else if (array[i].equals("FRA")) {
//					customFileFRA = "Param" + com.catalina.vault.steps.AudienceSegmentSteps.getValueFromPropertiesFile(
//							environmentPropertiesFilePath, "BUILD_NAME") + "_" + array[i] + "_" + timeStamp;
//				}
			}
			}}
	}
	


	public static String getAudienceFile(String country) {

		if (country.equalsIgnoreCase("USA")) {
			a = audienceFileUSA;
			System.out.println("FileUtil " + a);
		} else if (country.equalsIgnoreCase("FRA")) {
			a = audienceFileFRA;
			System.out.println("FileUtil " + a);
		} else if (country.equalsIgnoreCase("ITA")) {
			a = audienceFileITA;
			System.out.println("FileUtil " + a);
		}

		return a;
	}

	public static void createTestData(String[] array, String Filepath, long n2) throws IOException {
		String timeStamp = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());
		for (int i = 0; i < array.length; i++) {
			FileWriter fileWriter = new FileWriter(Filepath + "_" + array[i] + "_" + timeStamp + ".txt");
			BufferedWriter br = new BufferedWriter(fileWriter);

			for (int j = 1; j <= 10; j++) {
				if (array[i].equals("USA")) {
					String s = array[i] + "-" + 0 + 0 + 34 + "-" + (n2 + j);
					// System.out.println(s);
					br.write(s);
					br.newLine();
				} else if (array[i].equals("ITA")) {
					String s = array[i] + "-" + 0 + 0 + 29 + "-" + (n2 + j);
					br.write(s);
					br.newLine();
				} else if (array[i].equals("FRA")) {
					String s = array[i] + "-" + 0 + 0 + 0 + 4 + "-" + (n2 + j);
					br.write(s);
					br.newLine();
				}
			}
			br.close();
		}

	}
	
	public static void  customParameter() throws IOException {
		propertiesFile = new PropertiesFile("environment");
		propFile=new PropertiesFile("ReRunScenarioNames");
		long randNumber = 1000000000L + ((long) rand.nextInt(90000000) * 100) + rand.nextInt(100);
		String cidPath = propertiesFile.getProperty("sharedPath");
		
		String audienceListNamePropFile="AUDIENCE_SEGMENT_"+com.catalina.vault.steps.SharedResource.GetCountry();
		System.out.println("audience name"+audienceListNamePropFile);
		String audienceListName= propFile.getProperty(audienceListNamePropFile);
		
		System.out.println("get path for list: "+cidPath+audienceListName+".txt");
		
		List<String> lines = Files.readAllLines(Paths.get(cidPath+audienceListName+".txt"), StandardCharsets.UTF_8);
		
		int noOflines=lines.size();
		System.out.println("lines: "+noOflines);
		int i=0;
		FileWriter fileWriter = new FileWriter(cidPath + "CustomParameters_for_" + com.catalina.vault.pagefactory.RetailerCouponPage.bl +".txt");
		BufferedWriter br = new BufferedWriter(fileWriter);
		br.write("CID,ad,external_id,param1,param2");
		br.newLine();
		while(i<noOflines)
		{
		String customparameters=lines.get(i)+","+com.catalina.vault.pagefactory.RetailerCouponPage.bl+","+com.catalina.vault.pagefactory.RetailerCouponPage.bl+","+(randNumber+i)+","+(randNumber+i+10);
			System.out.println("customparameters:  "+customparameters);
		 br.write(lines.get(i)+","+com.catalina.vault.pagefactory.RetailerCouponPage.bl+","+com.catalina.vault.pagefactory.RetailerCouponPage.bl+","+(randNumber+i)+","+(randNumber+i+10));
		 br.newLine();
		 i++;
		}
		br.close();
	
		
	}
	
	public static void  AudienceListForLoyaltyRewards() throws IOException {
		String timeStamp = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss").format(new Date());
	
		propertiesFile = new PropertiesFile("environment");
		propFile=new PropertiesFile("ReRunScenarioNames");
		long randNumber = 2000000000L + ((long) rand.nextInt(60000000) * 100) + rand.nextInt(100);
		String path = propertiesFile.getProperty("sharedPath");
		if (System.getProperty("BUILD_NAME") == null) {
			String BUILD_NAME_propfile=propertiesFile.getProperty("BUILD_NAME");
			shareResource.setBuildName(BUILD_NAME_propfile);
			} else {
			String Build_NAME_jenkins=System.getProperty("BUILD_NAME");
			shareResource.setBuildName(Build_NAME_jenkins);
			}
		String BUILD_NAME=SharedResource.getBuildName();
			String testCasedata[]={"Itemunits","ItemSpend","ManualUpc"};
		
		
		String Country="USA";
		
		
		for(String LoyaltyRewardsdata:testCasedata){
		FileWriter fileWriter = new FileWriter(path+BUILD_NAME+"_"+ LoyaltyRewardsdata + "_" +timeStamp+".txt");
		BufferedWriter br = new BufferedWriter(fileWriter);
		for (int j = 1; j <= 10; j++) {
			String s = Country + "-" + 0 + 0 + 34 + "-" + (randNumber + j);
				 
				br.write(s);
				br.newLine();
		}
		br.close();
		String audienceFileLoyaltyRewards=BUILD_NAME+"_"+ LoyaltyRewardsdata + "_" +timeStamp;
		if (LoyaltyRewardsdata.equalsIgnoreCase("Itemunits")) {
			shareResource.setItemunits(audienceFileLoyaltyRewards);	
		}else if(LoyaltyRewardsdata.equalsIgnoreCase("ItemSpend")){
			shareResource.setItemSpend(audienceFileLoyaltyRewards);	
		}else if(LoyaltyRewardsdata.equalsIgnoreCase("ManualUpc")){
			shareResource.setManualUpc(audienceFileLoyaltyRewards);	
		}
		randNumber=randNumber+1000000000;
		
		}
		
		
				
			
	}
	
	
	
	public static String getCustomParameters() {

		b="CustomParameters_for_" + com.catalina.vault.pagefactory.RetailerCouponPage.bl;
		return b;
	}


}
