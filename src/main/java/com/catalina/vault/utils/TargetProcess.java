package com.catalina.vault.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.mortbay.log.Log;

import com.catalina.vault.entities.TestCase;
import com.catalina.vault.entities.TestCaseRun;
import com.catalina.vault.entities.TestPlan;
import com.catalina.vault.entities.TestPlanRun;
import com.catalina.vault.entities.TestPlans;
import com.catalina.vault.steps.SharedResource;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TargetProcess {

	public static final String TOKEN = "NzE0OkQvK1dPRmYxMXhEM2xEVjVvMjM2YlBGYmozZXRDMk5ZQktvQVFzRVUzS009";
	public static String SUNILTOKEN = "MjAwOmZwcFRvbFZtM29jYVRzOFppdmVLM2RGdldiVFIyc01rNHI2Rm52SjFtcWs9";
	public int testPlanRunId = 0;
	public int testPlanRunIdFrance = 0;
	public int testPlanRunIdItaly = 0;
	public int testPlanRunIdUSA = 0;
	PropertiesFile pFile;
	SharedResource shareResource = new SharedResource();
	public Response createTestCase(TestCase testCase) {

		Client client = ClientBuilder.newClient();
		return client.target("https://catalina.tpondemand.com/api/v1/TestCases?access_token=" + TOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testCase, MediaType.APPLICATION_JSON));
	}

	public Response addTestCaseToTestPlan(int testCaseId, int testPlanID) {

		TestCase testCase = new TestCase();
		TestPlans testPlans = new TestPlans();
		testPlans.getItems().add(new TestPlan(testPlanID));
		testCase.setTestPlans(testPlans);
		Client client = ClientBuilder.newClient();
		return client
				.target("https://catalina.tpondemand.com/api/v1/TestCases/" + testCaseId + "?access_token=" + TOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testCase, MediaType.APPLICATION_JSON));
	}

	public Response createTestPlan(String testPlan) {

		Client client = ClientBuilder.newClient();
		return client.target("https://catalina.tpondemand.com/api/v1/TestPlans?access_token=" + TOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testPlan, MediaType.APPLICATION_JSON));
	}

	// public Response uploadAttachment(Attachment attachment, int attchedWith)
	// {
	//
	// File file = new File(attachment.getFileName());
	// try {
	// FileOutputStream fos = new FileOutputStream(file);
	// fos.write(Base64.decodeBase64(attachment.getContent().getBytes()));
	// } catch (FileNotFoundException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// Client client = ClientBuilder.newBuilder()
	// .register(MultiPartFeature.class).build();
	// WebTarget webTarget = client
	// .target("https://catalina.tpondemand.com/UploadFile.ashx?access_token="
	// + TOKEN);
	//
	// FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("file", file,
	// MediaType.APPLICATION_OCTET_STREAM_TYPE);
	//
	// MultiPart multiPart = new FormDataMultiPart().field("generalId",
	// String.valueOf(attchedWith)).bodyPart(fileDataBodyPart);
	//
	// return webTarget.request(MediaType.APPLICATION_JSON)
	// .accept(MediaType.APPLICATION_JSON)
	// .post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA));
	// }

	public int createTestPlanRun(TestPlanRun testPlanRun) {
		System.out.println("createTestPlanRun" + testPlanRun.getName());
		Client client = ClientBuilder.newClient();
		Response response = client
				.target("https://catalina.tpondemand.com/api/v1/TestPlanRuns?access_token=" + SUNILTOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testPlanRun, MediaType.APPLICATION_JSON));
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject) parser.parse(data);
		System.out.println("Test Plan Run Created with id " + object.get("Id").getAsInt());
		return object.get("Id").getAsInt();
	}

	public int getTestCaseRunId(int testPlanRunId, int testCaseId) {
		String url = "https://catalina.tpondemand.com/api/v1/TestCaseRuns/?access_token=" + TOKEN
				+ "&where=(TestCase.ID eq " + testCaseId + ") and(TestPlanRun.ID eq " + testPlanRunId + ")";
		Client client = ClientBuilder.newClient();
		Response response = client.target(url.replace(" ", "%20")).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonObject object = ((JsonObject) parser.parse(data)).get("Items").getAsJsonArray().get(0).getAsJsonObject();
		return object.get("Id").getAsInt();
	}

	public int updateStatus(int testPlanRunId, int testCaseId, TestCaseRun testCaseRun) {
		int testCaseRunId = getTestCaseRunId(testPlanRunId, testCaseId);
		Client client = ClientBuilder.newClient();
		Response response = client
				.target("https://catalina.tpondemand.com/api/v1/TestCaseRuns/" + testCaseRunId + "?access_token="
						+ TOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testCaseRun, MediaType.APPLICATION_JSON));
		System.out.println("response posting comment"+response );
		String data = response.readEntity(String.class);
		System.out.println("response posting data"+data );
		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject) parser.parse(data);
		System.out.println("response posting object"+object );
		return object.get("Id").getAsInt();
	}

	public List<Integer> bulkUpdate(int testPlanRunId, Map<Integer, TestCaseRun> map) {
		List<Integer> list = new ArrayList<>();

		for (Map.Entry entry : map.entrySet()) {
			int id = (int) entry.getKey();
			TestCaseRun testCaseRun = (TestCaseRun) entry.getValue();
			list.add(updateStatus(testPlanRunId, id, testCaseRun));

		}
		return list;
	}

	public Response getTestCase(int testCaseId) {
		Client client = ClientBuilder.newClient();
		return client.target(
				"https://catalina.tpondemand.com/api/v1/TestCases/" + testCaseId + "?access_token=" + SUNILTOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
	}

	public void delete(int id) {
		Client client = ClientBuilder.newClient();
		System.out.println(
				(client.target("https://catalina.tpondemand.com/api/v1/TestCases/" + id + "?access_token=" + TOKEN)
						.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).delete())
								.readEntity(String.class));
	}

	public Response updateTestCase(int testCaseID, TestCase testCase) {
		// TODO Auto-generated method stub
		Client client = ClientBuilder.newClient();
		Gson gson = new Gson();
		String str = gson.toJson(testCase);
		str = str.replace("\u00C2", "");
		System.out.println(str);
		return client
				.target("https://catalina.tpondemand.com/api/v1/TestCases/" + testCaseID + "?access_token="
						+ SUNILTOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(str, MediaType.APPLICATION_JSON));

	}
	public int getBuild() {
		// TODO Auto-generated method stub

		try {

			Properties properties = new Properties();
			this.pFile= new PropertiesFile("environment");
			String rootPath = System.getProperty("user.dir");
			System.out.println("root" + rootPath + "//prp//TP.properties");
			File file = new File(".//prp//TP.properties");

			FileInputStream inStream = null;
			
			inStream = new FileInputStream(file);
			properties.load(inStream);

			if (properties.getProperty("RESULTS_UPDATE_EXISTING_BUILD_"+com.catalina.vault.steps.SharedResource.GetCountry()) != null) {
				int testPlanRunID = Integer.parseInt(properties.getProperty("RESULTS_UPDATE_EXISTING_BUILD_"+com.catalina.vault.steps.SharedResource.GetCountry()).trim());
				return testPlanRunID;
			}

			int testPlanId = 0;
			String Build_Name;// = Integer.parseInt(properties
			// .getProperty("Test_Plan_ID"));

			testPlanId = Integer.parseInt(properties.getProperty("Test_Plan_ID_"+com.catalina.vault.steps.SharedResource.GetCountry()));

			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy-hh_mm");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			String DateStamp=dateFormat.format(date);
			if (System.getProperty("BUILD_NAME") == null) {
				String BUILD_NAME_propfile=this.pFile.getProperty("BUILD_NAME");
				this.shareResource.setBuildName(BUILD_NAME_propfile);
				System.out.println("Jenkins build name"+BUILD_NAME_propfile);
				} else {
				String Build_NAME_jenkins=System.getProperty("BUILD_NAME");
				this.shareResource.setBuildName(Build_NAME_jenkins);
				System.out.println("Jenkins build name"+Build_NAME_jenkins);
				}
			String key="Build_Name";
			Build_Name = SharedResource.getBuildName()+"_" + DateStamp;
			properties.setProperty(key, "Vault_"+SharedResource.getBuildName()+"_" + DateStamp);
			System.out.println(" build name"+Build_Name);
			// buildName = properties.getProperty("Build_Name") + new
			// Date().toString();
			TestPlanRun testPlanRun = new TestPlanRun();
			testPlanRun.setName(Build_Name);
			testPlanRun.setTestPlan(new TestPlan(testPlanId));
			int testPlanRunID = createTestPlanRun(testPlanRun);
			System.out.println(" testPlanRunID name"+testPlanRunID);
			String buildid="RESULTS_UPDATE_EXISTING_BUILD_"+com.catalina.vault.steps.SharedResource.GetCountry();
			
			 properties.setProperty(buildid, String.valueOf(testPlanRunID));
			properties.store(new FileOutputStream(".//prp//TP.properties"), null);
			int testplanid= Integer.parseInt(properties.getProperty(buildid));
			
			SharedResource.TestPlanRunId = testplanid;
			Log.info("testplanid :"+testplanid);
			return testplanid;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return -1;
	}

	public int getTestPlanId() {
		// TODO Auto-generated method stub

		try {
				String country = com.catalina.vault.steps.SharedResource.GetCountry();
			Properties properties = new Properties();
			String rootPath = System.getProperty("user.dir");
			System.out.println("root" + rootPath + "//prp//TP.properties");
			File file = new File(".//prp//TP.properties");

			FileInputStream inStream = null;

			inStream = new FileInputStream(file);
			properties.load(inStream);

			

			int testPlanId = 0;
			System.out.println("Test Plan Id: "+country);
			testPlanId = Integer.parseInt(properties.getProperty("Test_Plan_ID_"+country));
			System.out.println("Test Plan Id: "+testPlanId);
			
			return testPlanId;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return -1;
	}

	public TargetProcess getBuild(int testplanid, String buildName) {
		// TODO Auto-generated method stub
		TargetProcess targetProcess = new TargetProcess();
		TestPlanRun testPlanRun = new TestPlanRun();
		testPlanRun.setName(buildName);
		testPlanRun.setTestPlan(new TestPlan(testplanid));

		if (getBuildId(buildName, testplanid) == -1) {
			testPlanRunId = createTestPlanRun(testPlanRun);
		}
		return targetProcess;

	}

	public int getBuildId(String buildName, int testplanid) {
		// TODO Auto-generated method stub
		String url = "https://catalina.tpondemand.com/api/v1/TestPlans/" + testplanid + "/TestPlanRuns?access_token="
				+ SUNILTOKEN + "&where=(name eq '" + buildName + "')";
		Client client = ClientBuilder.newClient();
		Response response = client.target(url.replace(" ", "%20")).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonArray array = ((JsonObject) parser.parse(data)).get("Items").getAsJsonArray();
		if (array.size() == 0)
			return -1;
		else
			this.testPlanRunId = array.getAsJsonArray().get(0).getAsJsonObject().get("Id").getAsInt();
		System.out.println(this.testPlanRunId);
		return this.testPlanRunId;
	}
//
//	public int getBuildId() {
//		// TODO Auto-generated method stub
//		return this.testPlanRunId;
//	}
}
