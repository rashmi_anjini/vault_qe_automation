
package com.catalina.vault.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class NetworkUtils {

	private final static Logger LOGGER = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);
	

	public static String getCall(String url) {

		LOGGER.log(Level.INFO, "Hitting a get call to url :- " + url);
		Client client = ClientBuilder.newClient();
		return client.target(url).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get()
				.readEntity(String.class);
	}

	public static String getCallWithQueryParameter(String URL, String... params) {

		LOGGER.log(Level.INFO, "Hitting a get call to url :- " + URL);
		Client client = ClientBuilder.newClient();
		System.out.println(URL);
		WebTarget target = client.target(URL);
		for (String param : params) {
			LOGGER.log(Level.INFO, "Adding query Parameter " + param);
			System.out.println(param);
			target = target
					.queryParam(param.split("=")[0], param.split("=")[1]);
			System.out.println(target);

		}

		LOGGER.log(Level.INFO, "Final request is :- " + target.toString());

		return target.request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get()
				.readEntity(String.class);
	}

	public static String postCall(String url, String data) {

		LOGGER.log(Level.INFO, "Hitting a post request to url :- " + url
				+ "\n Data is :- \n" + data);
		Client client = ClientBuilder.newClient();
		return client.target(url).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(data, MediaType.APPLICATION_JSON))
				.readEntity(String.class);
	}
	
	public static String putCall(String url, String data) {

		LOGGER.log(Level.INFO, "Hitting a put request to url :- " + url
				+ "\n Data is :- \n" + data);
		Client client = ClientBuilder.newClient();
		return client.target(url).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.put(Entity.entity(data, MediaType.APPLICATION_JSON))
				.readEntity(String.class);
	}
	
}
