package com.catalina.schemaComparison;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;

import com.fasterxml.jackson.databind.JsonNode;
import com.flipkart.zjsonpatch.JsonDiff;

public class JsonComparision {
	
	public static void jsonComparison() throws IOException, JSONException {
	//public static void main(String[] args) throws IOException{
		ValidationUtils validationUtils = new ValidationUtils();
		FileUtil fileUtils = new FileUtil();
//		JsonUtils jsonUtils=new JsonUtils();

//		ArrayList<String> ignoreList= new ArrayList<String>();
		
		String expectedJson = fileUtils.readTextFile(".//textFile//expectedJson");
		String actualJson=fileUtils.readTextFile(".//textFile//actualJson");
		JsonNode actualNode=	validationUtils.returnJsonNode(actualJson);
		JsonNode expectedNode=	validationUtils.returnJsonNode(expectedJson);
		JsonNode patch = JsonDiff.asJson(actualNode, expectedNode);
		String[] patchs=patch.toString().split("},");
		for(String a : patchs)
		System.out.println(a);
		
//		ignoreList=fileUtils.returnFileContentLineByLine(".//textFile//ignoreList.txt");
//		ArrayList<String> errorList	=jsonUtils.getErrorList(patch.toString());
//		ArrayList<String> pathList= jsonUtils.getPathList(patch.toString());
//
//
//		ArrayList<String> updatedErrorList=jsonUtils.returnFilteredErrorList(ignoreList, pathList, errorList);
//		
//		try{
//		jsonUtils.convertErrorJsonInEasyForm(updatedErrorList,expectedJson,actualJson);
//		
//		}catch(Exception e){
//			for(String error :updatedErrorList){
//				System.out.println(error);
//			}
//		}
//
//
//		System.out.println("Done ");
//		
	}
	
}
