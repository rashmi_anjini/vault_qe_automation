package com.catalina.schemaComparison;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;



public class ReportComputation {
	FileUtil fileUtils;
	public String  determineErrorType(String reportError) {
		String errorType="";
		//get error line
		String errorString =returnSubString("error:", "level:", reportError);

		System.out.println("////////////////////////////////////////////////");

		if(errorString.contains("properties which are not allowed")) {
			errorType= "extraElement"	;
		}else if(errorString.contains("missing required properties")) {
			errorType= "missingElement" ;
		}else if(errorString.contains("does not match any allowed primitive type")) {
			errorType= "typeError";
		}else {
			Assert.assertEquals("true", false);
		}
		return errorType;

	}

	public static String returnSubString(String startingPoint,String endpoint, String mainString) {
		String returnString="";

		String[] splitStartingPoint = mainString.split(startingPoint, 2); 

		String endSentence=splitStartingPoint[1].toString();
		String[] splitEndingPoint = endSentence.split(endpoint, 2); 


		return returnString=splitEndingPoint[0];
	}

	public ArrayList<String> determineNumberOfError(String errorType, String errorReport) {
		ArrayList<String> errorLogsList= new ArrayList<String>();
		int errorCount =countFreq("error:", errorReport);


		errorLogsList= getListOfErrors(errorReport, errorCount);


		return errorLogsList;
	}

	static int countFreq(String pat, String txt) {         
		int M = pat.length();         
		int N = txt.length();         
		int res = 0; 

		for (int i = 0; i <= N - M; i++) {
			int j;             
			for (j = 0; j < M; j++) { 
				if (txt.charAt(i + j) != pat.charAt(j)) { 
					break; 
				} 
			} 
			if (j == M) {                 
				res++;                 
				j = 0;                 
			}             
		}         
		return res;         
	} 

	public static String[] returnStringDivided(String startingPoint,String endpoint, String mainString) {
		String returnString="";

		String[] splitStartingPoint = mainString.split(startingPoint, 2); 

		String endSentence=splitStartingPoint[1].toString();
		String[] splitEndingPoint = endSentence.split(endpoint, 2); 


		return splitEndingPoint;
	}

	public ArrayList<String> getListOfErrors(String errorReport,int errorCount ) {
		ArrayList<String> errorLogsList = new ArrayList<String>();
		try {
			String endpoint="";
			if(errorCount==1) {
				endpoint="END MESSAGES";
			}else {
				endpoint="error:";
			}
			String mainString=errorReport;
			for(int i=0;i<errorCount;i++) {

				String[] errorLogDivided = returnStringDivided("error:", endpoint, mainString);
				errorLogsList.add(errorLogDivided[0]) ;
				mainString="error:"+errorLogDivided[1];

				if(i==errorCount-1) {
					endpoint="END MESSAGES";
				}

			}


		}catch (Exception e) {

		}


		return errorLogsList;
	}

	public void getErrorPointers(String errorType,ArrayList<String> errorLogsList,String json ) throws IOException {

		String newJson=json;
		ArrayList<String> locationList = new ArrayList<String>();
		String location="";
		switch (errorType) {
		case "extraElement":

			for(int i=0;i<errorLogsList.size();i++) {
				String [] unwantedElements= getUnwantedObject(errorLogsList.get(i));
				for(int j=0;j<unwantedElements.length;j++) {
					String unwantedElement=unwantedElements[j];
					String errorLog=errorLogsList.get(i);

					String arr[] = getInstanceArray(errorLog);
					if(arr==null) {
						String [] newArr= { unwantedElement };
						arr=newArr;
					}else {
						arr=addNewElementToArray(arr, unwantedElement);
					}
					String	element=locateLocationInJson(json, arr);
					locationList.add("\""+arr[arr.length-1]+"\""+":"+element);
				}
			}

			for(int j=0;j<locationList.size();j++) {

				newJson=newJson.replaceAll(locationList.get(j), locationList.get(j)+" <--");

			}
			fileUtils= new FileUtil();
			fileUtils.writeTextFile(newJson, ".//textFile//resultantFile");
			break;

		case "missingElement":
			String [] missingElements = null;
			Boolean flag=false;
			for(int i=0;i<errorLogsList.size();i++) {
				
				missingElements = getMissingObject(errorLogsList.get(i));
				
				String [] instanceElements = getInstanceArray(errorLogsList.get(i));
				if(instanceElements==null) {
					flag=true;
					
					
				}else {
				String element=locateLocationInJson(json, instanceElements)  ;
				location=instanceElements[instanceElements.length-1];
				}
				
			}
			String missingElement="";
			for(int j=0;j<missingElements.length;j++) {
				missingElement=missingElement+missingElements[j]+", ";
			}
			if(flag==true) {
				newJson=json.concat("  missing --->" )+missingElement	;
			}else {
			newJson=json.replaceAll("\""+location+"\"","missing "+ missingElement+"--> "+"\""+location+"\"");
			}
			fileUtils= new FileUtil();
			fileUtils.writeTextFile(newJson, ".//textFile//resultantFile");
			break;
		case "typeError":

			for(int i=0;i<errorLogsList.size();i++) {
				String element="";


				String errorLog=errorLogsList.get(i);
				String arr[] = getInstanceArray(errorLog);
				element=locateLocationInJson(json, arr);	
				
				locationList.add("\""+arr[arr.length-1]+"\""+":"+element);

			}

			for(int j=0;j<locationList.size();j++) {

				newJson=newJson.replaceAll(locationList.get(j), locationList.get(j)+" <--");

			}
			fileUtils= new FileUtil();
			fileUtils.writeTextFile(newJson, ".//textFile//resultantFile");
			break;
			
//		case "missingElements":
//			
//			String [] missingElements = null;
//			Boolean flag=false;
//			for(int i=0;i<errorLogsList.size();i++) {
//				
//				missingElements = getMissingObject(errorLogsList.get(i));
//				
//				String [] instanceElements = getInstanceArray(errorLogsList.get(i));
//				if(instanceElements==null) {
//					flag=true;
//					
//					
//				}else {
//				String element=locateLocationInJson(json, instanceElements)  ;
//				location=instanceElements[instanceElements.length-1];
//				}
//				
//			}
//			String missingElement="";
//			for(int j=0;j<missingElements.length;j++) {
//				missingElement=missingElement+missingElements[j]+", ";
//			}
//			if(flag==true) {
//				newJson=json.concat("  missing --->" )+missingElement	;
//			}else {
//			newJson=json.replaceAll("\""+location+"\"","missing "+ missingElement+"--> "+"\""+location+"\"");
//			}
//			fileUtils= new FileUtils();
//			fileUtils.writeTextFile(newJson, ".//textFile//resultantFile");
//			
//			break;
			
		default:
			break;
		}
	}

	public String locateLocationInJson(String json,String[] locationArr) {
		String element="";


		JSONObject jsonBody = null;
		JSONArray  jsonBodyArray=null;
		for(int i=0;i<locationArr.length+1;i++) {
			JSONObject jsonObject=null;
			if(i==0) {
				jsonBody = new JSONObject(json);

			}else {
				//System.out.println(jsonBody);

				try {
					jsonObject=  jsonBody.getJSONObject(locationArr[i-1]);
					element=jsonObject.toString();
					jsonBody=jsonObject;

				}catch (Exception e) {
					try {
						String endElement=jsonBody.getString(locationArr[i-1]);
						element="\""+endElement+"\"";
					}catch (Exception e2) {
						try {
							int endElement=jsonBody.getInt(locationArr[i-1]);
							element=""+endElement;
						}catch (Exception e3) {
							try {
							Boolean endElement=jsonBody.getBoolean(locationArr[i-1]);
							element=""+endElement;
							}catch (Exception e4) {
								try {
								element=locationArr[i-1];
								jsonBodyArray=jsonBody.getJSONArray(locationArr[i-1]);
								i++;
								jsonBody=jsonBodyArray.getJSONObject(Integer.parseInt(locationArr[i-1]));
								}catch (Exception e5) {
									element="null";
								}
							}
						}
					}
				}


			}
		}

		return element;
	}


	
	public String[] getUnwantedObject(String errorLog) {

		String missingObject =	returnSubString("unwanted:", "]---",errorLog);
		String[] missingLocation=missingObject.replaceAll("]", "").replaceAll("\"", "").replaceAll("---", "").trim().substring(1).split(",");

		return missingLocation;
	}
	
	public String[] getMissingObject(String errorLog) {

		String missingObject =	returnSubString("missing:", "]---",errorLog);
		String[] missingLocation=missingObject.replaceAll("]", "").replaceAll("\"", "").replaceAll("---", "").trim().substring(1).split(",");

		return missingLocation;
	}

	public String[] getInstanceArray(String errorLog) {
		String[] arr=null;

		try {
			String instance =	returnSubString("instance:", "domain",errorLog);
			String  pointer = returnSubString(":", "}",instance);
			String location=returnSubString("\"/", "\"",pointer);
			arr=location.split("/");
		}catch (Exception e) {

		}
		return arr;
	}
	//	 Map<String, Object> jsonMap=jsonBody.toMap();

	public String[] addNewElementToArray(String[] arr, String element) {
		String[] newArr= new String[arr.length+1];

		for(int i=0;i<arr.length;i++) {
			newArr[i]=arr[i];
		}
		newArr[arr.length]=element;

		return newArr;

	}

}
