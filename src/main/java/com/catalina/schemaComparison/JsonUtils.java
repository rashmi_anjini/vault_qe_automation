package com.catalina.schemaComparison;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtils {
	public ArrayList<String> getPathList(String json) throws JSONException {
		JSONArray errorLogArray=  new JSONArray(json);
		ArrayList<String> pathList= new ArrayList<String>();
		
		for(int i=0; i<errorLogArray.length();i++) {
			String path =errorLogArray.getJSONObject(i).getString("path");
			pathList.add(path);
		}
		
		return pathList;
	}
	
	public ArrayList<String> getErrorList(String json) throws JSONException {
		JSONArray errorLogArray=  new JSONArray(json);
		ArrayList<String> errorList= new ArrayList<String>();
		
		for(int i=0; i<errorLogArray.length();i++) {

			errorList.add(errorLogArray.getJSONObject(i).toString());
		}
		
		return errorList;
	}
	
	
	public ArrayList<String> returnFilteredErrorList(ArrayList<String> ignoreList, ArrayList<String> pathList,ArrayList<String> errorList){
		ArrayList<String> updatedErrorList = new ArrayList<String>();
		
		for(int i=0;i<errorList.size();i++) {
			String error=errorList.get(i);
			String path=pathList.get(i);
			Boolean flag = false;
			for(int j=0; j<ignoreList.size();j++) {
				flag=compareString(ignoreList.get(j), path);
				if(flag==true) {
					break;
				}
			}
		
			if(flag==false) {
				updatedErrorList.add(error);
			}
		}
		
		return updatedErrorList;
		
	}
	
	public Boolean compareString(String ignore, String path) {
		Boolean flag = null;
		
		if(path.length()<ignore.length()) {
			flag=false;
		}else {
			for(int j=0;j<ignore.length();j++) {
				char p=path.charAt(j);
				char i=ignore.charAt(j);
				
				if(p!=i) {
					flag=false;
					break;
				}else {
					flag=true;
				}
			}
		}
		return flag;
	}	
	
	public void convertErrorJsonInEasyForm(ArrayList<String> updatedErrorList, String expectedJson, String actualJson) throws JSONException {
		//		String operation="";
		for(int i=0;i<updatedErrorList.size();i++) {

			String operation=returnJsonElement(updatedErrorList.get(i),"op");
			String value="";
			String path ="";
			String wrongValue="";
			String correctValue="";
			String [] pathArray=null;
			String from="";
			switch (operation) {
			case "replace":
				value=returnJsonElement(updatedErrorList.get(i),"value");
				path = returnJsonElement(updatedErrorList.get(i),"path");
				pathArray=path.split("/");
				wrongValue=locateLocationInJson(actualJson, pathArray);

				System.out.println(" FOUND "+wrongValue+" INSTEAD OF \""+value+"\" AT PATH \""+path+"\"");

				break;

			case "remove":

				path = returnJsonElement(updatedErrorList.get(i),"path");
				pathArray=path.split("/");
				wrongValue=locateLocationInJson(actualJson, pathArray);

				System.out.println(" REMOVE ELEMENT @\""+pathArray[pathArray.length-1]+"\":"+" "+wrongValue+"@");

				break;

			case "copy":

				path=returnJsonElement(updatedErrorList.get(i),"path");
				pathArray=path.split("/");
				correctValue=locateLocationInJson(expectedJson, pathArray);
				 
				System.out.println(" ADD VALUE  @\""+pathArray[pathArray.length-1]+"\":"+" "+correctValue+"@ AT PATH \""+path+"\"");
				break;

			case "add":
				value=returnJsonElement(updatedErrorList.get(i),"value");
				path = returnJsonElement(updatedErrorList.get(i),"path");
				pathArray=path.split("/");
				System.out.println("@\""+pathArray[pathArray.length-1]+"\": "+"\""+value+"\"@ IS MISSING FROM PATH \""+path+"\"");
				break;

			default:
				break;
			}

		}
	}

	public String locateLocationInJson(String json,String[] locationArr) throws JSONException {
		String element="";


		JSONObject jsonBody = null;
		JSONArray  jsonBodyArray=null;
		for(int i=0;i<locationArr.length+1;i++) {
			JSONObject jsonObject=null;
			if(i==0) {
				jsonBody = new JSONObject(json);

			}else {
				//System.out.println(jsonBody);

				try {

				
					jsonObject=  jsonBody.getJSONObject(locationArr[i-1]);
					element=jsonObject.toString();
					jsonBody=jsonObject;

				}catch (Exception e) {
					try {
					
						element=locationArr[i-1];
						jsonBodyArray=jsonBody.getJSONArray(locationArr[i-1]);	
						element=jsonBodyArray.toString();
						i++;
						jsonBody=jsonBodyArray.getJSONObject(Integer.parseInt(locationArr[i-1]));
						element=jsonBody.toString();
					}catch (Exception e5) {
						try {

						
							String endElement=jsonBody.getString(locationArr[i-1]);
							element="\""+endElement+"\"";

						}catch (Exception e2) {
							try {
								
								int endElement=jsonBody.getInt(locationArr[i-1]);
								element=""+endElement;
							}catch (Exception e3) {
								try {
									
									Boolean endElement=jsonBody.getBoolean(locationArr[i-1]);
									element=""+endElement;
								}catch (Exception e4) {

								}
							}
						}
					}

				}
			}
		}

		return element;
	}
	
	public String returnJsonElement(String json, String key) throws JSONException {


		JSONObject object = new JSONObject(json);
		String element = object.getString(key);

		return element;
	}
}
