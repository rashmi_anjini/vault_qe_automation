package com.catalina.schemaComparison;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import com.catalina.vault.steps.Verification;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class ValidationUtils {
	public static final String JSON_V4_SCHEMA_IDENTIFIER = "http://json-schema.org/draft-04/schema#";
	public static final String JSON_SCHEMA_IDENTIFIER_ELEMENT = "$schema";
	static String completeErrorLog;

	public static void jsonComparator() throws IOException, ProcessingException{

		FileUtil fileUtils = new FileUtil();
		ReportComputation 	reportComputation = new ReportComputation();

		System.out.println("Inside json ");
		String schema=fileUtils.readTextFile(".//textFile//schemaFile");
		String json =fileUtils.readTextFile(".//textFile//jsonFile");
		//			 String json=Verification.jsonResponse;
		System.out.println("json response " +json);
		if(isJsonValid(schema, json)) {
			System.out.println("Valid!");
		}else{
			System.out.println("NOT valid!");
			String reportError=returnReportError();
			try {
				String errorType=reportComputation.determineErrorType(reportError);
				ArrayList<String> errorLogsList=	reportComputation.determineNumberOfError(errorType, reportError);
				reportComputation.getErrorPointers(errorType, errorLogsList, json);
			}catch (Exception e) {
				System.out.println(e);
			}
		}
	}

	public static JsonNode getJsonNode(String jsonText)  throws IOException
	{
		return JsonLoader.fromString(jsonText);
	} // getJsonNode(text) ends

	public static JsonNode getJsonNode(File jsonFile)  throws IOException 
	{
		return JsonLoader.fromFile(jsonFile);
	} // getJsonNode(File) ends

	public static JsonNode getJsonNode(URL url)  throws IOException 
	{
		return JsonLoader.fromURL(url);
	} // getJsonNode(URL) ends

	public static JsonNode getJsonNodeFromResource(String resource)   throws IOException
	{
		return JsonLoader.fromResource(resource);
	} // getJsonNode(Resource) ends

	public static JsonSchema getSchemaNode(String schemaText)throws IOException, ProcessingException 
	{
		final JsonNode schemaNode = getJsonNode(schemaText);
		return _getSchemaNode(schemaNode);
	} // getSchemaNode(text) ends

	public static JsonSchema getSchemaNode(File schemaFile) throws IOException, ProcessingException
	{
		final JsonNode schemaNode = getJsonNode(schemaFile);
		return _getSchemaNode(schemaNode);
	} // getSchemaNode(File) ends

	public static JsonSchema getSchemaNode(URL schemaFile) throws IOException, ProcessingException
	{
		final JsonNode schemaNode = getJsonNode(schemaFile);
		return _getSchemaNode(schemaNode);
	} // getSchemaNode(URL) ends

	public static JsonSchema getSchemaNodeFromResource(String resource) throws IOException, ProcessingException 
	{
		final JsonNode schemaNode = getJsonNodeFromResource(resource);
		return _getSchemaNode(schemaNode);
	} // getSchemaNode() ends


	public static void validateJson(JsonSchema jsonSchemaNode, JsonNode jsonNode) throws ProcessingException 
	{
		ProcessingReport report = jsonSchemaNode.validate(jsonNode);
		System.out.println("report "+report);

		if (!report.isSuccess()) {
			for (ProcessingMessage processingMessage : report) {
				throw new ProcessingException(processingMessage);
			}
		}
	} // validateJson(Node) ends

	public static boolean isJsonValid(JsonSchema jsonSchemaNode, JsonNode jsonNode) throws ProcessingException
	{
		ProcessingReport report = jsonSchemaNode.validate(jsonNode);
		completeErrorLog=report.toString();
		System.out.println(completeErrorLog);
		return report.isSuccess();
	} // validateJson(Node) ends

	public static boolean isJsonValid(String schemaText, String jsonText) throws ProcessingException, IOException
	{   
		final JsonSchema schemaNode = getSchemaNode(schemaText);
		final JsonNode jsonNode = getJsonNode(jsonText);
		return isJsonValid(schemaNode, jsonNode);
	} // validateJson(Node) ends

	public static boolean isJsonValid(File schemaFile, File jsonFile) throws ProcessingException, IOException
	{   
		final JsonSchema schemaNode = getSchemaNode(schemaFile);
		final JsonNode jsonNode = getJsonNode(jsonFile);     
		return isJsonValid(schemaNode, jsonNode);
	} // validateJson(Node) ends

	public static boolean isJsonValid(URL schemaURL, URL jsonURL) throws ProcessingException, IOException
	{   
		final JsonSchema schemaNode = getSchemaNode(schemaURL);
		final JsonNode jsonNode = getJsonNode(jsonURL);
		return isJsonValid(schemaNode, jsonNode);
	} // validateJson(Node) ends    

	//	    	    	    public static void validateJson(String schemaText, String jsonText) throws IOException, ProcessingException{
	//	    	    	        final JsonSchema schemaNode = getSchemaNode(schemaText);
	//	    	    	        final JsonNode jsonNode = getJsonNode(jsonText);
	//	    	    	        validateJson(schemaNode, jsonNode);
	//	    	    	    } // validateJson(text) ends

	public static void validateJson(String schemaText, String jsonText) throws IOException, ProcessingException{
		final JsonSchema schemaNode = getSchemaNode(schemaText);
		final JsonNode jsonNode = getJsonNode(jsonText);
		validateJson(schemaNode, jsonNode);

	}

	public static void validateJson(File schemaFile, File jsonFile) throws IOException, ProcessingException{
		final JsonSchema schemaNode = getSchemaNode(schemaFile);
		final JsonNode jsonNode = getJsonNode(jsonFile);
		validateJson(schemaNode, jsonNode);
	} // validateJson(File) ends

	public static void validateJson(URL schemaDocument, URL jsonDocument) throws IOException, ProcessingException{
		final JsonSchema schemaNode = getSchemaNode(schemaDocument);
		final JsonNode jsonNode = getJsonNode(jsonDocument);
		validateJson(schemaNode, jsonNode);
	} // validateJson(URL) ends

	public static void validateJsonResource(String schemaResource, String jsonResource) throws IOException, ProcessingException{
		final JsonSchema schemaNode = getSchemaNode(schemaResource);
		final JsonNode jsonNode = getJsonNodeFromResource(jsonResource);
		validateJson(schemaNode, jsonNode);
	} // validateJsonResource() ends

	private static JsonSchema _getSchemaNode(JsonNode jsonNode)
			throws ProcessingException
	{
		final JsonNode schemaIdentifier = jsonNode.get(JSON_SCHEMA_IDENTIFIER_ELEMENT);
		if (null == schemaIdentifier){
			((ObjectNode) jsonNode).put(JSON_SCHEMA_IDENTIFIER_ELEMENT, JSON_V4_SCHEMA_IDENTIFIER);
		}

		final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
		return factory.getJsonSchema(jsonNode);
	} // _getSchemaNode() ends



	public static String returnString(String startingPoint,String endpoint, String mainString) {
		String returnString="";

		String[] splitStartingPoint = mainString.split(startingPoint, 2); 

		String endSentence=splitStartingPoint[1].toString();
		String[] splitEndingPoint = endSentence.split(endpoint, 2); 


		return returnString=splitEndingPoint[0];
	}

	public static String returnReportError() {
		return completeErrorLog;
	}
	public static JsonNode returnJsonNode(String json) throws IOException {
		JsonNode jsonNode = getJsonNode(json);

		return jsonNode;
	}
}

