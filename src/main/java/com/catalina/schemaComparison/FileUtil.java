package com.catalina.schemaComparison;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class FileUtil {

	public String readTextFile(String path) throws IOException {
		String content="";
		
		 FileReader fr =  new FileReader(path+".txt"); 
			  
			    int i; 
			    while ((i=fr.read()) != -1) {
			     content=content+(char) i; 
			    }
		return content;
	}
	
	public static String writeTextFile(String content, String path) throws IOException {
		 FileWriter fw=new FileWriter(path+".txt");    
         fw.write(content);    
         fw.close();  
         return content;
	}
	
	public ArrayList<String> returnFileContentLineByLine(String path) throws IOException{
		ArrayList<String> fileContent= new ArrayList<String>();		
		BufferedReader reader;
		
			reader = new BufferedReader(new FileReader(path));
			String line = reader.readLine();
			while (line != null) {
				fileContent.add(line);
				// read next line
				line = reader.readLine();
			}
			reader.close();
		
		return fileContent;
	}
}
