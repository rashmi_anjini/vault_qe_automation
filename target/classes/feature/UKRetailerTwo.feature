Feature: Create Promotion for UKRetailer uses cases 2nd set
  Scenario Outline: Using Retailer and with  Total order  and  department purchase Targetting
  Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\ferrerorocher.jpg"
  Then : Add Targeting group with Total Order first
  Then : Add department id with "<Condition>", "<quantity>" and "<departmentid>"
  Then : Publish "<TestCaseName>"
  Then : Verify DMP Promotion on server "AWS" and "GBR"
  
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |	TestCaseName |	Barcode	|	departmentid	|
  		| >= | 1 |	Total order  and department purchase |	85214796301	|	1	|
  
  Scenario Outline: Using Retailer and with  Rolling day Validation
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "France"
  	When : Logged in create campaign
  	When : new tab open Switch to that tab
  	Then : Enter form details for new campaign with network "<TestCaseName>"
  	Then : Click on Save to create campagin
  	When : Campagin created Then create Ad Group
  	Then : Create ad using Ad Galary with retail
  	Then : Fill ad form for UKretailer with rolling expirationDate"<Barcode>" and "C:\Vault UI Template\fries.jpg"
  	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  	Then : Publish "<TestCaseName>"
  Then : Verify DMP Promotion on server "AWS" and "GBR"
  
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  		| >= | 5 |	Rolling day Validation |	85214796301	| 834560015	|
  
  
  Scenario Outline: Creating Retailer for Retargeting
	  Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\fruitjuice.jpg"
  	Then : Add Targeting for retailer with Retargetting
    Then : Add Targeting for retailer with Item Purchase with fixed period and Manual List "834560016"
  	Then : Publish "<TestCaseName>"
  Then : Verify DMP Promotion on server "AWS" and "GBR"
  
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |	TestCaseName |	Barcode	|	
  		| >= | 5 |	Creating Retailer for Retargeting  |	09841524701	| 
  
  Scenario Outline: Creating Retailer for POS Redemption
  	Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\gems.jpg"
  	Then : Add Targeting for retailer with POS Redemption
    Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AWS" and "GBR"
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  		| >= | 5 |	Creating Retailer for POS Redemption  |	85214796301	| 834560017	|
  
  
  Scenario Outline: Using Retailer and with item purchase qty and ==zero and Total order Targetting
  	Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\happysmiley.jpg"
  	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  	Then : Add Targeting for retailer group with qty "<Condition1>" , "<quantity1>" and "<manualList>"
  	Then : Add Targeting group with Total Order third row
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AWS" and "GBR"
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | manualList |	TestCaseName |	Barcode	|	 Condition1 | quantity1 |
  		| >= | 1 | 834560018	|	Item Purchase is >=1 and ==0 and total order |	85214796301	|	== | 0	|
  
  Scenario Outline: Using Retailer and with item purchase is equal to zero and qty
  	Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\icecreamcone images.jpg"
  	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  	Then : Add Targeting for retailer group with qty "<Condition1>" , "<quantity1>" and "<manualList>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AWS" and "GBR"
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | manualList |	TestCaseName |	Barcode	|	 Condition1 | quantity1 |
  		| == | 0| 834560019	|	Item Purchase is ==0 and >=1|	85214796301	|	>= | 1	|
  
  Scenario Outline: Using Retailer and with item purchase total spend
  	Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\italy.jpg"
  	Then : Add Targeting for retailer group with total spend "<Condition>" , "<quantity>" and "<manualList>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AWS" and "GBR"
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | manualList |	TestCaseName |	Barcode	|
  		| >= | 5 | 834560020	|	Item Purchase total spend	|	85214796301	|
  
  Scenario Outline: Using Retailer and with item purchase total spend and item purchase total units
  	Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\Italyimage.jpg"
  	Then : Add Targeting for retailer group with total spend "<Condition>" , "<quantity>" and "<manualList>"
  	Then : Add Targeting for retailer group with qty "<Condition1>" , "<quantity1>" and "<manualList>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AWS" and "GBR"
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | manualList |	TestCaseName |	Barcode	|	 Condition1	|	quantity1	|
  		| >= | 5 | 834560021	|	Item Purchase total spend and item purchase total units	|	85214796301	|	>=	|	1	|
  
  Scenario Outline: Using Retailer and with item purchase total spend and total order
  	Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\juice image.jpg"
  	Then : Add Targeting for retailer group with total spend "<Condition>" , "<quantity>" and "<manualList>"
  	Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AWS" and "GBR"
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | manualList |	TestCaseName |	Barcode	|	 Conditionorder	|	quantityorder	|
  		| >= | 3 | 834560022	|	Item purchase total spend and total order	|	85214796301	|	>=	|	5	|
  
  Scenario Outline: Using Retailer and with department purchase total units equal to one
  	Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\kinderjoy.jpg"
  	Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>"
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AWS" and "GBR"
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity | manualList |	TestCaseName |	Barcode	|	departmentid	|
  		| == | 1 | 834560023	|	Department purchase total units ==1	|	85214796301	|	2	|

    		
Scenario Outline: Using Retailer and with department purchase total units between 2 and 3
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\maggi.jpg"
    Then : Add department id firstrow between "<Condition>", "<quantity>", "<quantity2>" and "<departmentid>" 
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
      | Condition | quantity | quantity2 | manualList | TestCaseName                                          | Barcode     |	departmentid	|
      | between   |        2 |         3 |  834560026 | Department purchase total units between 2,00 and 3,00 | 85214796301 |	3	|
      
 Scenario Outline: Using Retailer and with department purchase total units between and total order
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\Mariegold.png"
    Then : Add department id firstrow between "<Condition>", "<quantity>", "<quantity2>" and "<departmentid>" 
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
      | Condition | quantity | quantity2 | manualList | TestCaseName                                          | Barcode     |	Conditionorder	|	quantityorder	|	departmentid	|
      | between   |        2 |         3 |  834560027 | Department purchase total units between 2 and 3 and total order-total spend | 85214796301 |	>=	|	1	|	6	|
      
 Scenario Outline: Using Retailer and with department purchase total units ==1 and total order spend
    Given : Url of application Navigate to webpage 
		Then : Fill credentails 
		Then : Click on Login 
		Then : Change region to "Great Britain"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
		Then : Click on Save to create campagin 
		When : Campagin created Then create Ad Group 
		Then : Create ad using Ad Galary with retail for UK 
		Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\oreo.jpg"
    Then : Add department id firstrow with "<Condition>", "<quantity>" and "<departmentid>" 
    Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AWS" and "GBR"
		
    #put in same add group provide rank as well
    Examples: 
      | Condition | quantity | manualList | TestCaseName                                          | Barcode     |	Conditionorder	|	quantityorder	|	departmentid	|
      | ==   |   1 |   834560028 | Department purchase total units ==1 and total order-total spend | 85214796301 |	>=	|	1	|	5	|
      
      
