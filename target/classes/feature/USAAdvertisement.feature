Feature: Advertisement

@SmokeTest @Regression
	Scenario Outline: Advertisement - Item purchase
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
   	Then : Enter form details for new campaign for USA with network "Walgreens" and "ADVERTISEMENT"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Advertisement
    Then : Fill ad form for advertisement with "Vault USA Template\29.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
		Then : Send data to Kafka Queue "<testCaseId>"
		
    Examples: 
      | TestCaseName  |	Condition	|	quantity	|	testCaseId	|
      | Item Purchase | >=	|	1	| 70900	|
     
  @Regression
  Scenario Outline: Advertisement - Catalina Redemption
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Advertisement
    Then : Fill ad form for advertisement with "Vault USA Template\27.jpg"
    Then : Add Catalina Redemption "USA"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue barcode "<testCaseId>"

    Examples: 
      | TestCaseName        | testCaseId	|
      | Catalina Redemption | 70896	|

 @Regression
  Scenario Outline: Advertisement - Payment method trigger
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Advertisement
    Then : Fill ad form for advertisement with "Vault USA Template\28.jpg"
    Then : Add Total Spend between "<Condition>", "<value>" and "<value2>"
    Then : Add Payment Method "<payment>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue betweenAmt "<testCaseId>" 

    Examples: 
      | TestCaseName   | Condition   | value	|value2	|	payment	|testCaseId	|
      | Payment method | between | 	25	|	45	|	WIC	|70899	|

 @Regression
  Scenario Outline: Advertisement - Retargeting
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Advertisement
    Then : Fill ad form for advertisement with "Vault USA Template\30.jpg"
    Then : Add Targeting with Retargetting "USA"
    Then : Add Targeting with Item Purchase with fixed period and Manual List 
    Then : Publish "<TestCaseName>"
     Then : Verify DMP Promotion on server "AZURE" and "USA"
     Then : Clear cache
     Then : Send data to Kafka Queue "<testCaseId>"

    Examples: 
      | TestCaseName | testCaseId	|
      | Retargeting  |70897	|

@Regression
  Scenario Outline: Advertisement - Lane type trigger
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Advertisement
    Then : Fill ad form for advertisement with "Vault USA Template\31.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Targeting for Advertisement with LaneType
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
     

    Examples: 
      | TestCaseName           | Condition | quantity |testCaseId	|
      | LaneType+Item Purchase |  >=        |        1 |70898	|
 
