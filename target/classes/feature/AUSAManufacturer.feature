Feature: Manufacturer Promotions 

	Scenario Outline: Coupon Item Purchase
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "MANUFACTURER"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\16.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName  |	Condition	|	quantity	|	Barcode	|
      | Item Purchase | >=	|	1	|    (8110)1007192113889131001203103181230	|
      
  Scenario Outline: Coupon Catalina Redemption
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\14.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Catalina Redemption "USA"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName        | Barcode	|
      | Catalina Redemption | (8110)1007192113889131001203103181230	|
      
  Scenario Outline: Coupon Retargteing
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\15.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting with Retargetting "USA"
    Then : Add Targeting with Item Purchase with fixed period and Manual List 
    Then : Publish "<TestCaseName>"
     Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName | Barcode	|
      | Retargeting  | (8110)1007192113889131001203103181230	|
      
  
 Scenario Outline: Coupon Lane type trigger
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\17.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Targeting for Advertisement with LaneType
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName           | Condition | quantity |Barcode	|
      | LaneType+Item Purchase |  >=        |        1 |	(8110)1007192113889131001203103181230	|
      
      
  #Scenario Outline: Coupon Payment method
    #Given : Url of application Navigate to webpage
    #Then : Fill credentails
    #Then : Click on Login
    #Then : Change region to "USA"
    #Then : Search for the Campaign name
  #	When : Campagin created Then create Ad Group with "<TestCaseName>"
    #Then : Create ad using Ad Galary with manufacturer
    #Then : Fill ad form for manufacturer with "Vault USA Template\18.jpg", "-1" and "5"
    #Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    #Then : Add Total Spend between "<Condition>", "<value>" and "<value2>"
    #Then : Add Payment Method "<payment>"
    #Then : Publish "<TestCaseName>"
    #Then : Verify DMP Promotion on server "AZURE" and "USA"
#
    #Examples: 
      #| TestCaseName   | Condition   | value	|value2	|	payment	|Barcode	|
      #| Payment method | between | 	25	|	45	|	WIC	|	(8110)1007192113889131001203103181230	|
 
  #Scenario Outline: Coupon Total Order
    #Given : Url of application Navigate to webpage
    #Then : Fill credentails
    #Then : Click on Login
    #Then : Change region to "USA"
    #Then : Search for the Campaign name
  #	When : Campagin created Then create Ad Group with "<TestCaseName>"
    #Then : Create ad using Ad Galary with manufacturer
    #Then : Fill ad form for manufacturer with "Vault USA Template\19.jpg", "-1" and "5"
    #Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    #Then : Add Targeting group with Total Order first "<Condition>" and "<value>"
    #Then : Publish "<TestCaseName>"
    #Then : Verify DMP Promotion on server "AZURE" and "USA"
#
    #Examples: 
      #| TestCaseName   |Condition   | value	|	Barcode|
      #| Targeting with total order |	>=	|	10	|		(8110)1007192113889131001203103181230	|

  Scenario Outline: Coupon Audience Segment
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\20.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Audience Segment for advertisement with "ODOULSUSA" 
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName     | Barcode|
      | Audience Segment in | 	(8110)1007192113889131001203103181230	|

  Scenario Outline: Coupon Department codes
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\21.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting group with Department codes
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName                    |Barcode|
      | Targeting with Department codes |	(8110)1007192113889131001203103181230	|

 Scenario Outline: Coupon Audience Segment +Item Purchase
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\22.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
		 Then : Add Audience Segment for with "ODOULSUSA"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName | Condition 	|	quantity 	|	Barcode|
      | Item Purchase and AudSeg    | >=	|	1	|	(8110)1007192113889131001203103181230	|
      
  Scenario Outline: Coupon PLU
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\23.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with PLU "<Condition>" and "<quantity>" 
    Then : Publish "<TestCaseName>"
     Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName  |	Condition	|	quantity	|	Barcode	|
      | Item Purchase | >=	|	1	|    (8110)1007192113889131001203103181230	|
      

 