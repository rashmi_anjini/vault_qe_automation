Feature: Create Promotion for Retailer UK set
Scenario Outline: Using Retailer and with item purchase and Total order Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\5star.jpg"
 	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>" 
	Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
	Then : Publish "<TestCaseName>"
	Then : Verify DMP Promotion on server "AWS" and "GBR" 
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | manualList |	TestCaseName |	Barcode	|	Conditionorder	|	quantityorder	|
		| >= | 1 | 834560001	|	Item Purchase and total order |	85214796301	|	>=	|	1	|
		
		
Scenario Outline: Using Retailer and with item purchase with Unlimited coupon condition Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with unlimited "<Barcode>" and "C:\Vault UI Template\Britannia.jpg"
	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>" 
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AWS" and "GBR"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	manualList |
		| == | 1 | Item purchase with Unlimited coupon condition |	09841524701	|	834560003	|
		
Scenario Outline: Using Retailer and with item purchase and Payment method  Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\Burgerimage.jpg"
	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
	Then : Add Payment Method 
	Then : Publish "<TestCaseName>"
	Then : Verify DMP Promotion on server "AWS" and "GBR" 
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	manualList |
		| = | 1 | Item purchase and Payment method check|	85214796301	|	834560004	|
		
Scenario Outline: Using Retailer and with item purchase and Audience Segment  Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\cakeimag.jpg"
	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>" 
	Then : Add Audience Segment for retailer with "Krishsainsbury" 
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AWS" and "GBR"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	manualList |
		| >= | 1 | Item purchase and Audience Segment |	85214796301	|	834560005 	|


	Scenario Outline: Using Retailer and with item purchase ==0 and Audience Segment and Custom Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\coffee.jpg"
	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>" 
	Then : Add Audience Segment for retailer with "Krishsainsbury"
	Then : Add Custom Targeting with external id "BL_1924_8584"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AWS" and "GBR"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	manualList |
		| == | 0 | Item purchase ==0 and Audience Segment and Custom |	85214796301	|	834560007	|	
		
Scenario Outline: Using Retailer and with item purchase and User identifier Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\colddoffe.jpg"
	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
	Then : Add User Identifier
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AWS" and "GBR"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	manualList |
		| >= | 1 | Item purchase and User identifier Loyalty card |	85214796301	|	834560008	|
		
Scenario Outline: Creating Retailer and with item purchase ==1 Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\cookies.jpg"
	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AWS" and "GBR"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	manualList |
		| == | 1 | Item purchase ==1 |	85214796301	|	834560009 |
		
Scenario Outline: Using Retailer and with item purchase between units 2.00 and 3.00  Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\crossents.jpg"
	Then : Add Targeting for retailer group with between "<Condition>" , "<quantity>", "<quantity1>" and "<manualList>"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AWS" and "GBR"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | quantity1	|	TestCaseName |	Barcode	|	manualList |
		| between | 2 | 3	|	item purchase between units 2.00 and 3.00 |	85214796301	|	834560010 	|

		
		
Scenario Outline: creating Retailer and with freq cap and item purchase and Audience seg Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with freqcap "<Barcode>" and "C:\Vault UI Template\dessert.png"
	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
	Then : Add Audience Segment for retailer with "Krishsainsbury"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AWS" and "GBR"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity |	manualList	| TestCaseName |	Barcode	|
		| >= | 1 |	834560014	|	Freq cap and item purchase and Audience seg |	85214796301	|	


Scenario Outline: Using Retailer and with  Total order  and payment method foodstamp Targetting 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "Great Britain"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for GreatBritain with  "<TestCaseName>"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group 
	Then : Create ad using Ad Galary with retail for UK 
	Then : Fill ad form for UKretailer with "<Barcode>" and "C:\Vault UI Template\donut image.jpg"
	Then : Add Targeting group with Total Order first
	Then : Add Payment Method with Foodstamp
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AWS" and "GBR"
	
	#put in same add group provide rank as well
	Examples: 
		 | TestCaseName |	Barcode	|
	 | Total order  and payment method foodstamp |	85214796301	|
		