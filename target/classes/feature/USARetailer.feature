Feature: Create Promotion for Retailer

@Regression
  Scenario Outline: CCM-TRANS-CLUSTER
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for USA with cluster "CCM-TRANS-CLUSTER" and "CCM-TRANS"
  	Then : Click on Save to create campagin
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
  	Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with Rank "<Barcode>", "Vault USA Template\1.jpg", "<ad1>" and "<Rank1>"
  	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Click on adgroup tab
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with Rank "<Barcode>", "Vault USA Template\2.jpg", "Default Rank 2" and "2"
    Then : Add Targeting for itempurchase group same upc "==" and "2"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Click on adgroup tab
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with Rank "<Barcode>", "Vault USA Template\3.jpg", "Default Rank 3" and "3"
    Then : Add Targeting for itempurchase group same upc "==" and "1"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | Condition | quantity | TestCaseName |   Rank1  |Barcode	| ad1 	|
      | >=        |        3 | CCM-TRANS-CLUSTER   |    1	| 85214796301	|	Default Rank 1	|

 @Regression 
Scenario Outline: Coupon - Dynamic Barcode
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "USA"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for USA with network "Walgreens" and "DYNAMIC BARCODE"
  	Then : Click on Save to create campagin
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
 	 	Then : Create US ad using Ad Galary with retail
  	Then : Fill ad form for retailer with Rolling Expirationdate dynamic barcode "<Barcode>" and "Vault USA Template\4.jpg"
  	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AZURE" and "USA" 
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |	TestCaseName |	Barcode	|	
  		| >= | 3 |Coupon-dynamic barcode |	85214796301	| 