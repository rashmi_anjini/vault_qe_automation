Feature: Create Promotions for USA EORD
#Scenario Outline: Using Retailer and with  Coupon-dynamic barcode
  #	Given : Url of application Navigate to webpage
  #	Then : Fill credentails
  #	Then : Click on Login
  #	Then : Change region to "USA (AWS)"
#		When : Logged in create campaign 
#		When : new tab open Switch to that tab 
#	Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  #	Then : Click on Save to create campagin
  #	When : Campagin created Then create Ad Group
  #Then : Create US ad using Ad Galary with retail
  #	Then : Fill ad form for retailer with Rolling Expirationdate dynamic barcode "<Barcode>" and "C:\Vault UI Template\fries.jpg"
  #	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  #	Then : Publish "<TestCaseName>"
  #	Then : Verify DMP Promotion on server "AWS" and "USA" 
  #	
  #	#put in same add group provide rank as well
  #	Examples:
  #		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  #		| >= | 3 |Coupon-dynamic barcode |	85214796301	| 834500015	|
  #		
#
#Scenario Outline: Using manufacturer and with FSC - Unlimited
  #	Given : Url of application Navigate to webpage
  #	Then : Fill credentails
  #	Then : Click on Login
  #	Then : Change region to "USA (AWS)"
#		When : Logged in create campaign 
#		When : new tab open Switch to that tab 
#		Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  #	Then : Click on Save to create campagin
  #	When : Campagin created Then create Ad Group
  #	Then : Create US ad using Ad Galary with retail
  #	Then : Fill ad form for manufacturer with unlimited "C:\Vault UI Template\Britannia.jpg"
  #	Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  #	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  #	Then : Add User Identifier
  #	Then : Publish "<TestCaseName>"
  #	Then : Verify DMP Promotion on server "AWS" and "USA" 
  #	
  #	#put in same add group provide rank as well
  #	Examples:
  #		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  #		| == | 1 |FSC - Unlimited |	(8110)1007192113889131001203103181230	| 834500015	|
  #		
#Scenario Outline: Using manufacturer and with FSC - limited
  #	Given : Url of application Navigate to webpage
  #	Then : Fill credentails
  #	Then : Click on Login
  #	Then : Change region to "USA (AWS)"
#		When : Logged in create campaign 
#		When : new tab open Switch to that tab 
#		Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  #	Then : Click on Save to create campagin
  #	When : Campagin created Then create Ad Group
  #	Then : Create US ad using Ad Galary with retail
  #	Then : Fill ad form for manufacturer with "C:\Vault UI Template\Britannia.jpg", "-1" and "5"
  #	Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  #	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  #	Then : Add User Identifier
  #	Then : Publish "<TestCaseName>"
  #	Then : Verify DMP Promotion on server "AWS" and "USA" 
  #	
  #	#put in same add group provide rank as well
  #	Examples:
  #		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  #		| == | 1 |FSC - limited |	(8110)1007192113889131001203103181230	| 834500015	|
  		
 #Scenario Outline: Using manufacturer and with Coupon - Start date as current date
  #	Given : Url of application Navigate to webpage
  #	Then : Fill credentails
  #	Then : Click on Login
  #	Then : Change region to "USA (AWS)"
#		When : Logged in create campaign 
#		When : new tab open Switch to that tab 
#		Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  #	Then : Click on Save to create campagin
  #	When : Campagin created Then create Ad Group
  #	Then : Create US ad using Ad Galary with retail
  #	Then : Fill ad form for manufacturer with "C:\Vault UI Template\Britannia.jpg", "0" and "5"
  #	Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  #	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  #	Then : Publish "<TestCaseName>"
  #	Then : Verify DMP Promotion on server "AWS" and "USA" 
  #	
  #	#put in same add group provide rank as well
  #	Examples:
  #		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  #		| >= | 1 |Coupon - Start date as current date |	(8110)1007192113889131001203103181230	| 834500015	|
  #		
Scenario Outline: Using manufacturer and with Coupon - Start date as current date-1
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "USA (AWS)"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  	Then : Click on Save to create campagin
  	When : Campagin created Then create Ad Group
  	Then : Create US ad using Ad Galary with retail
  	Then : Fill ad form for manufacturer with "C:\Vault UI Template\Britannia.jpg", "-1" and "5"
  	Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AWS" and "USA" 
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |	TestCaseName |	Barcode	|
  		| >= | 1 |Coupon - Start date as current date-1 |	(8110)1007192113889131001203103181230	| 
  		
 #Scenario Outline: Using manufacturer and with Coupon - Start date as current date+1
  #	Given : Url of application Navigate to webpage
  #	Then : Fill credentails
  #	Then : Click on Login
  #	Then : Change region to "USA (AWS)"
#		When : Logged in create campaign 
#		When : new tab open Switch to that tab 
#		Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  #	Then : Click on Save to create campagin
  #	When : Campagin created Then create Ad Group
  #	Then : Create US ad using Ad Galary with retail
  #	Then : Fill ad form for manufacturer with "C:\Vault UI Template\Britannia.jpg", "1" and "5"
  #	Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  #	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  #	Then : Publish "<TestCaseName>"
  #	Then : Verify DMP Promotion on server "AWS" and "USA" 
  #	
  #	#put in same add group provide rank as well
  #	Examples:
  #		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  #		| >= | 1 |Coupon - Start date as current date+1 |	(8110)1007192113889131001203103181230	| 834500015	|
  #		
 #Scenario Outline: Using manufacturer and with Coupon - Stop date as current date
  #	Given : Url of application Navigate to webpage
  #	Then : Fill credentails
  #	Then : Click on Login
  #	Then : Change region to "USA (AWS)"
#		When : Logged in create campaign 
#		When : new tab open Switch to that tab 
#		Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  #	Then : Click on Save to create campagin
  #	When : Campagin created Then create Ad Group
  #	Then : Create US ad using Ad Galary with retail
  #	Then : Fill ad form for manufacturer with "C:\Vault UI Template\Britannia.jpg", "-5" and "0"
  #	Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  #	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  #	Then : Publish "<TestCaseName>"
  #	Then : Verify DMP Promotion on server "AWS" and "USA" 
  #	
  #	#put in same add group provide rank as well
  #	Examples:
  #		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  #		| >= | 1 |Coupon - Stop date as current date |	(8110)1007192113889131001203103181230	| 834500015	|
  #		
  #Scenario Outline: Using manufacturer and with Coupon - Stop date as current date-1
  #	Given : Url of application Navigate to webpage
  #	Then : Fill credentails
  #	Then : Click on Login
  #	Then : Change region to "USA (AWS)"
#		When : Logged in create campaign 
#		When : new tab open Switch to that tab 
#		Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  #	Then : Click on Save to create campagin
  #	When : Campagin created Then create Ad Group
  #	Then : Create US ad using Ad Galary with retail
  #	Then : Fill ad form for manufacturer with "C:\Vault UI Template\Britannia.jpg", "-5" and "-1"
  #	Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  #	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  #	Then : Publish "<TestCaseName>"
  #	Then : Verify DMP Promotion on server "AWS" and "USA" 
  #	
  #	#put in same add group provide rank as well
  #	Examples:
  #		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  #		| >= | 1 |Coupon - Stop date as current date-1 |	(8110)1007192113889131001203103181230	| 834500015	|
  		
 #Scenario Outline: Using manufacturer and with Coupon - Stop date as current date+1
  #	Given : Url of application Navigate to webpage
  #	Then : Fill credentails
  #	Then : Click on Login
  #	Then : Change region to "USA (AWS)"
#		When : Logged in create campaign 
#		When : new tab open Switch to that tab 
#		Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  #	Then : Click on Save to create campagin
  #	When : Campagin created Then create Ad Group
  #	Then : Create US ad using Ad Galary with retail
  #	Then : Fill ad form for manufacturer with "C:\Vault UI Template\Britannia.jpg", "-5" and "1"
  #	Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  #	Then : Add Targeting for retailer group with "<Condition>" , "<quantity>" and "<manualList>"
  #	Then : Publish "<TestCaseName>"
  #	Then : Verify DMP Promotion on server "AWS" and "USA" 
  #	
  #	#put in same add group provide rank as well
  #	Examples:
  #		| Condition | quantity |	TestCaseName |	Barcode	|	manualList |
  #		| >= | 1 |Coupon - Stop date as current date+1 |	(8110)1007192113889131001203103181230	| 834500015	|
  
  Scenario Outline: Using manufacturer and with Coupon - Budget capping - Available
  	Given : Url of application Navigate to webpage
  	Then : Fill credentails
  	Then : Click on Login
  	Then : Change region to "USA (AWS)"
		When : Logged in create campaign 
		When : new tab open Switch to that tab 
		Then : Enter form details for new campaign for USA with "<TestCaseName>" and "Walgreens"
  	Then : Click on Save to create campagin
  	When : Campagin created Then create Ad Group
  	Then : Create US ad using Ad Galary with retail
  	Then : Fill ad form for manufacturer with distribution cap "C:\Vault UI Template\Britannia.jpg", "-1" and "5"
  	Then : Fill ad form for manufacturer redemption tab "<Barcode>"
  	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
  	Then : Publish "<TestCaseName>"
  	Then : Verify DMP Promotion on server "AWS" and "USA" 
  	
  	#put in same add group provide rank as well
  	Examples:
  		| Condition | quantity |	TestCaseName |	Barcode	|	
  		| >= | 1 |Budget capping - Available |	(8110)1007192113889131001203103181230	| 