Feature: Create Promotion for France Audience

  #Background: 
    #Given : Url of application Navigate to webpage
    #Then : Fill credentails
    #Then : Click on Login
    #Then : Change region to "France"
    #When : Click on Audience
    #Then : Search for AudienceList with
    #Then : Click on Static List
    #Then : Verify the Status of AudienceList

  Scenario Outline: FR: Coupon - Audience segment | in + Dept. purchase | total units -> qty
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\135.jpg"
    Then : Add Audience Segment for retailer firstrow with "in" and "QUAGLIA_FRA_05.09.2019_01.18.42"
    Then : Add department id with audienceSegment "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"

    Examples: 
      | Condition | quantity | TestCaseName                       | Barcode     |
      | >=        |        1 | Audience Segment and Dept purchase | 85214796301 |

  Scenario Outline: FR: Coupon - with Distribution and/or Frequency cap
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with frequency capping "<Barcode>" and "Vault France Template\115.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Audience Segment for with "QUAGLIA_FRA_05.09.2019_01.18.42"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"

    #put in same add group provide rank as well
    Examples: 
      | Condition | quantity | TestCaseName                                | Barcode     |
      | >=        |        1 | Freq cap and item purchase and Audience seg | 85214796301 |

  Scenario Outline: FR: Coupon - Audience segment | in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\136.jpg"
    Then : Add Audience Segment for retailer firstrow with "in" and "QUAGLIA_FRA_05.09.2019_01.18.42"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"

    Examples: 
      | TestCaseName        | Barcode     |
      | Audience Segment in | 85214796301 |

  Scenario Outline: FR: Coupon - Audience segment | not in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\119.jpg"
    Then : Add Audience Segment for retailer firstrow with "not in" and "QUAGLIA_FRA_05.09.2019_01.18.42"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"

    Examples: 
      | TestCaseName            | Barcode     |
      | Audience Segment Not in | 85214796301 |

  Scenario Outline: FR: Coupon - Item purchase |total units -> qty + Audience Segment -> in
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\105.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Audience Segment for retailer firstrow with "in" and "QUAGLIA_FRA_05.09.2019_01.18.42"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"

    #put in same add group provide rank as well
    Examples: 
      | Condition | quantity | TestCaseName                       | Barcode     |
      | >=        |        1 | Item purchase and Audience Segment | 85214796301 |

Scenario Outline: FR: Coupon - Audience segment | in + Total order | total spend
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\138.jpg"
    Then : Add Audience Segment for retailer firstrow with "in" and "QUAGLIA_FRA_05.09.2019_01.18.42"
    Then : Add Targeting group with Total Order audiencesegment with "<Conditionorder>" and "<quantityorder>"
    Then : Publish "<TestCaseName>"
		Then : Verify DMP Promotion on server "AZURE" and "FRA" 
		
    
    Examples: 
      | Conditionorder | quantityorder |  TestCaseName            | Barcode     |	
      | >=   |   3 |    Audience Segment and Total order | 85214796301 |	
 
 
  Scenario Outline: FRA: Coupon - Audience segment | in + Payment method | EFT
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with retail
    Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\120.jpg"
    Then : Add Audience Segment for retailer firstrow with "in" and "QUAGLIA_FRA_05.09.2019_01.18.42"
    Then : Add Payment Method EFT
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"

    Examples: 
      | TestCaseName                                | Barcode     |
      | Audience Segment and Add Payment Method EFT | 85214796301 |

  #Scenario Outline: FRA: Coupon - Audience segment | in + Custom parameters
    #Given : Url of application Navigate to webpage
    #Then : Fill credentails
    #Then : Click on Login
    #Then : Change region to "France"
    #Then : Search for the Campaign name
    #When : Campagin created Then create Ad Group with "<TestCaseName>"
    #Then : Create ad using Ad Galary with retail
    #Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\118.jpg"
    #Then : Add Audience Segment for retailer firstrow with "ShaanNordiconad" and "in"
    #Then : Add Custom Targeting with external id "FRA"
    #Then : Publish "<TestCaseName>"
    #Then : Verify DMP Promotion on server "AZURE" and "FRA"
#
    #Examples: 
      #| TestCaseName                           | Barcode     |
      #| Audience Segment and Custom Parameters | 85214796301 |

  Scenario Outline: FR: Coupon - Item purchase |total units -> qty + Audience Segment -> in
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\105.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
	Then : Add Audience Segment for with "QUAGLIA_FRA_05.09.2019_01.18.42" 
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	
		| >= | 1 | Item purchase and Audience Segment |	85214796301	|	
		
#	Scenario Outline: FR: Coupon - Item purchase |total units -> qty + Audience Segment -> in + Custom parameters
#	Given : Url of application Navigate to webpage 
#	Then : Fill credentails 
#	Then : Click on Login 
#	Then : Change region to "France"
#	Then : Search for the Campaign name
  #When : Campagin created Then create Ad Group with "<TestCaseName>" 	
#	Then : Create ad using Ad Galary with retail 
#	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\106.jpg"
#	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
#	Then : Add Audience Segment for with "Krishsystemu"
#	Then : Add Custom Targeting with external id "France"
#	Then : Publish "<TestCaseName>" 
#	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
#	
#	#put in same add group provide rank as well
#	Examples: 
#		| Condition | quantity | TestCaseName |	Barcode	|	
#		| >= | 1 | Item purchase >=1 and Audience Segment and Custom |	85214796301	|	
#		
#	
#	Scenario Outline: FR: Coupon - Item purchase |total units == 0 + Audience Segment -> in + Custom parameters
#	Given : Url of application Navigate to webpage 
#	Then : Fill credentails 
#	Then : Click on Login 
#	Then : Change region to "France"
#	Then : Search for the Campaign name
  #When : Campagin created Then create Ad Group with "<TestCaseName>" 
#	Then : Create ad using Ad Galary with retail 
#	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\107.jpg"
#	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
#	Then : Add Audience Segment for with "Krishsystemu"
#	Then : Add Custom Targeting with external id "France"
#	Then : Publish "<TestCaseName>" 
#	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
#	
#	#put in same add group provide rank as well
#	Examples: 
#		| Condition | quantity | TestCaseName |	Barcode	|	
#		| == | 0 | Item purchase ==0 and Audience Segment and Custom |	85214796301	|
#		
      