Feature: Product Carried

  Scenario Outline: Product carried upc is seen by the store in the specified product carried rolling days
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer for LMC Product Carried with "Vault USA Template\15.jpg","<List>", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName                                                                           | Condition | quantity | Barcode                               | List            |
      | Product carried upc is seen by the store in the specified product carried rolling days | >=        |        1 | (8110)1007192113889131001203103181230 | KrogerList2.csv |

  Scenario Outline: Product carried upc is not seen by the store in the specified product carried rolling days
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer for LMC Product Carried with "Vault USA Template\15.jpg","<List> "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    Examples: 
      | TestCaseName                                                                               | Condition | quantity | Barcode                               | List            |
      | Product carried upc is not seen by the store in the specified product carried rolling days | >=        |        1 | (8110)1007192113889131001203103181230 | KrogerList2.csv |
  #Scenario Outline: Use a manual list for the product carried upc�s instead of lmc list
    #Given : Url of application Navigate to webpage
    #Then : Fill credentails
    #Then : Click on Login
    #Then : Change region to "USA"
    #Then : Search for the Campaign name
    #When : Campagin created Then create Ad Group with "<TestCaseName>"
    #Then : Create ad using Ad Galary with manufacturer
    #Then : Fill ad form for manufacturer for Manual LMC Product Carried with "Vault USA Template\15.jpg", "-1" and "5"
    #Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    #Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    #Then : Publish "<TestCaseName>"
    #Then : Verify DMP Promotion on server "AZURE" and "USA"
#
    #Examples: 
      #| TestCaseName                                                        | Condition | quantity | Barcode                               |
      #| Use a manual list for the product carried upc�s instead of lmc list | >=        |        1 | (8110)1007192113889131001203103181230 |
