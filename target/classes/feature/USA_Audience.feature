Feature: Create Promotion for USA Audience

 @Regression   
 Scenario Outline: Coupon Audience Segment
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "MANUFACTURER_AUDIENCE"
    Then : Click on Save to create campagin
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\20.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Audience Segment for advertisement with  
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>" 

    Examples: 
      | TestCaseName     | Barcode|testCaseId 	| 
      | Audience Segment in | 	(8110)1007192113889131001203103201230	| 70886 | 
      
    @SmokeTest  @Regression
   Scenario Outline: Coupon Audience Segment +Item Purchase
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with manufacturer
    Then : Fill ad form for manufacturer with "Vault USA Template\22.jpg", "-1" and "5"
    Then : Fill ad form for manufacturer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
		 Then : Add Audience Segment for with
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>" 

    Examples: 
      | TestCaseName | Condition 	|	quantity 	|	Barcode|testCaseId 	|
      | Item Purchase and AudSeg    | >=	|	1	|	(8110)1007192113889131001203103201230	|12345|
   
   @Regression   
  Scenario Outline: EORD-Frequency capping - Available
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
  	When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form with frequency cap for USAretailer with "<Barcode>" and image "Vault USA Template\39.jpg"
    Then : Add Audience Segment for advertisement with 
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"

    Examples: 
      | TestCaseName | Condition | quantity | Barcode     |testCaseId 	| 
      | FrequencyCapping    |  ==        |        1 | 85214796301 |12345|

@Regression
  Scenario Outline: Advertisement - Audience Segment
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
   	Then : Enter form details for new campaign for USA with network "Walgreens" and "ADVERTISEMENT_AUDIENCE"
   	Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Advertisement
    Then : Fill ad form for advertisement with "Vault USA Template\24.jpg"
    Then : Add Audience Segment for advertisement with
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"

    Examples: 
      | TestCaseName        |testCaseId 	| 
      | Audience Segment in |12345|

@Regression
  Scenario Outline: Advertisement - Audience Segment + Item purchase
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Advertisement
    Then : Fill ad form for advertisement with "Vault USA Template\26.jpg"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Add Audience Segment for with
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Clear cache
    Then : Send data to Kafka Queue with cid "<testCaseId>"

    Examples: 
      | TestCaseName             | Condition | quantity |testCaseId 	| 
      | Item Purchase and AudSeg | >=        |        1 |12345|
      
         
  
