Feature: Distribution Capping

@SmokeTest @Regression 
Scenario Outline: Lifetime distribution capping at Ad Level
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "RETAILER"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with distribution cap at diffs level "Vault USA Template\47.jpg", "-1", "5", "4" and "<level>"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

   
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	 level	|
      | >=        |    1    | Lifetime distribution capping at Ad Level | 85214796301 |	at the ad level	|
     
 @SmokeTest @Regression      
  Scenario Outline: Lifetime distribution capping at Adgroup Level
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with distribution cap at diffs level "Vault USA Template\48.jpg", "-1", "5", "4" and "<level>"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

   
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	 level	|
      | >=        |        1 | Lifetime distribution capping at Adgroup Level | 85214796301	|	at the adgroup level	|
     
      
   
 @Regression     
Scenario Outline: Lifetime distribution capping at Ad and Ad Group Level
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with two distribution cap at diff level "Vault USA Template\50.jpg", "-1", "5", "4", "<level>", "3" and "<level2>"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

   
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	 level	|	level2	|
      | >=        |    1    | Lifetime distribution capping at Ad and Ad Group Level | 85214796301 |	at the ad level	|	at the adgroup level	|
     
      
  @Regression
  Scenario Outline:	 Daily and Lifetime distribution capping at Ad Level
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with distribution cap "Vault USA Template\52.jpg", "-1", "5", "2", "<level>", "3", "<level2>" and "per day"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

   
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	 level	|	level2	|
      | >=        |    1    | Daily and Lifetime distribution capping at Ad Level | 85214796301 |	at the ad level	|	at the ad level	|
  @Regression
   Scenario Outline:	 Hourly and Lifetime distribution capping at Ad Level
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with distribution cap "Vault USA Template\53.jpg", "-1", "5", "2", "<level>", "3", "<level2>" and "per hour"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

   
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	 level	|	level2	|
      | >=        |    1    | Hourly and Lifetime distribution capping at Ad Level | 85214796301 |	at the ad level	|	at the ad level	|
  
   @Regression   
  Scenario Outline:	 Weekly and Lifetime distribution capping at Ad Level
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with distribution cap weekly "Vault USA Template\54.jpg", "-1", "5", "2", "<level>", "3", "<level2>" and "Fri-Thu" 
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

   
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	 level	|	level2	|
      | >=        |    1    | Weekly and Lifetime distribution capping at Ad Level | 85214796301 |	at the ad level	|	at the ad level	|
      
   
              
 @Regression 
Scenario Outline: Lifetime distribution capping at Ad Group level for an Ad Group with more than one Ad 
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with distribution cap "<TestCaseName>", "<count>" and "<level>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with name "<Barcode>", "Vault USA Template\55.jpg" and "ad_one"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Click on adgroup tab
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with name "<Barcode>", "Vault USA Template\56.jpg" and "ad_two"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	count	| level	|
      | >=        |        1 | Lifetime distribution capping at Ad Group level for an Ad Group with more than one Ad | 85214796301	|3	|	at the adgroup level	|

@Regression
Scenario Outline: Lifetime distribution capping at Campaign Level
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "RETAILER"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with distribution cap at diffs level "Vault USA Template\49.jpg", "-1", "5", "4" and "<level>"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

   
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	 level	|
       | >=        |        1 | Lifetime distribution capping at campaign Level | 85214796301 |	at the campaign level	|
    
  @Regression  
 Scenario Outline: Lifetime distribution capping at Ad and Campaign Level
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "RETAILER"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with two distribution cap at diff level "Vault USA Template\51.jpg", "-1", "5", "4", "<level>", "3" and "<level2>"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

   
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	 level	|	level2	|
      | >=        |    1    | Lifetime distribution capping at Ad and Campaign Level | 85214796301 |	at the ad level	|	at the campaign level	|
   
 @Regression  
Scenario Outline: Lifetime distribution capping at Campaign level for a campaign with more than one Ad Group
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "RETAILER"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with distribution cap "<TestCaseName>", "<count>" and "<level>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with name "<Barcode>", "Vault USA Template\57.jpg" and "ad_one"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    Then : Click on campaign tab
    When : Campagin created Then create Ad Group with distribution cap "<TestCaseName>", "<count>" and "<level>"
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with name "<Barcode>", "Vault USA Template\58.jpg" and "ad"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"

    
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	count	| level	|
      | >=        |        1 | Lifetime distribution capping at Campaign level for a campaign with more than one Ad Group | 85214796301	|3	|	at the campaign level	|
 
 @Regression     
   Scenario Outline: Pacing - Standard
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "Pacing-Standard"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with Standard Delivery method "<TestCaseName>" 
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with distribution cap at diffs level "Vault USA Template\69.jpg", "0", "2", "30" and "<level>"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                               |	 level	|
      | >=        |    1    | Pacing - Standard | 85214796302 |	at the ad level	|

@Regression     
Scenario Outline: Pacing - Accelerated
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "USA"
    When : Logged in create campaign
    When : new tab open Switch to that tab
    Then : Enter form details for new campaign for USA with network "Walgreens" and "Pacing-Accelerated"
    Then : Click on Save to create campagin
    When : Campagin created Then create Ad Group with "<TestCaseName>" 
    Then : Create US ad using Ad Galary with retail
    Then : Fill ad form for retailer with distribution cap at diffs level "Vault USA Template\70.jpg", "0", "2", "30" and "<level>"
    Then : Fill ad form for retailer redemption tab "<Barcode>"
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "USA"
    
    Examples: 
      | Condition | quantity | TestCaseName               | Barcode                |	 level	|
      | >=        |    1    | Pacing - Accelerated | 85214796303 |	at the ad level	|
     

