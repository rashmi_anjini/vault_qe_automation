Feature: Create Promotion for France Retailer 1st set

@SmokeTest @Regression
Scenario Outline: FR: Coupon - Item purchase | total units = qty + Total order - total spend
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	When : Logged in create campaign 
	When : new tab open Switch to that tab 
	Then : Enter form details for new campaign for France "RETAILER"
	Then : Click on Save to create campagin 
	When : Campagin created Then create Ad Group with "<TestCaseName>"
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\101.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
	Then : Add Targeting group with Total Order with "<Conditionorder>" and "<quantityorder>"
	Then : Publish "<TestCaseName>"
	Then : Verify DMP Promotion on server "AZURE" and "FRA"
	Then : Clear cache
	Then : Send data to Kafka Queue "<testCaseId>" 
	 

	Examples: 
		| Condition | quantity | 	TestCaseName |	Barcode	|	Conditionorder	|	quantityorder	|testCaseId 	|
		| >= | 1| 	Item purchase and Total order |	85214796301	|	>=	|	1	|	70905		|	
		
@Regression
	Scenario Outline: FR: Coupon - Dynamic Offer Code
    Given : Url of application Navigate to webpage
    Then : Fill credentails
    Then : Click on Login
    Then : Change region to "France"
    Then : Search for the Campaign name
    When : Campagin created Then create Ad Group with "<TestCaseName>"
    Then : Create ad using Ad Galary with Dynamic Offer Code retail
    Then : Fill ad form for retailer with Dynamic offerCode
    Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
    Then : Publish "<TestCaseName>"
    Then : Verify DMP Promotion on server "AZURE" and "FRA"

    Examples: 
      | TestCaseName       | Barcode     | Condition | quantity |
      | Dynamic Offer Code | 85214796301 | ==        |        1 |
      
      
	@SmokeTest 	@Regression
Scenario Outline: FR: Coupon - Item purchase |total units -> qty with "Unlimited" coupon condition
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
	 When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with unlimited "<Barcode>" and "Vault France Template\103.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
	Then : Clear cache
	Then : Send data to Kafka Queue "<testCaseId>"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	testCaseId 	| 
		| == | 1 | Item purchase with Unlimited coupon condition |	09841524701	|	70906		|

@SmokeTest @Regression		
Scenario Outline: FR: Coupon - Item purchase |total units -> qty + Payment method -> Check
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\104.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
	Then : Add Payment Method "<payment>"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
	Then : Clear cache
	Then : Send data to Kafka Queue "<testCaseId>"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|		payment	|testCaseId 	| 
		| = | 1 | Item purchase and Payment method check|	85214796301	|		Check	|	70907		|	
		

 @Regression
Scenario Outline: FR: Coupon - Item purchase| total units -> qty + User Identifier -> Loyality card 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\108.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
	Then : Add User Identifier
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
	Then : Clear cache
	Then : Send data to Kafka Queue with cid "<testCaseId>"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	testCaseId 	| 
		| >= | 1 | Item purchase and User identifier Loyalty card |	85214796301	|	70910		|	

@Regression
Scenario Outline: FR: Coupon - Item purchase | total units == 1
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\109.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA"
	Then : Clear cache 
	Then : Send data to Kafka Queue "<testCaseId>"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | TestCaseName |	Barcode	|	testCaseId 	| 
		| == | 1 | Item purchase ==1 |	85214796301	|70911		|
	@Regression	
Scenario Outline: FR: Coupon - Item purchase | total units between 2,00->3,00
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\111.jpg"
	Then : Add Targeting for retailer group with between "<Condition>" , "<quantity>" and "<quantity1>"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
	Then : Clear cache
	Then : Send data to Kafka Queue betweenQty "<testCaseId>"  
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | quantity1	|	TestCaseName |	Barcode	|	testCaseId 	| 
		| between | 2 | 3	|	item purchase between units 2.00 and 3.00 |	85214796301	|	70913		|	

	@Regression	
Scenario Outline: FR: Coupon - Item purchase | total units between 15,00->20,00
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\112.jpg"
	Then : Add Targeting for retailer group with between "<Condition>" , "<quantity>" and "<quantity1>"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA"
	Then : Clear cache
	Then : Send data to Kafka Queue betweenQty "<testCaseId>" 
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity | quantity1	|	TestCaseName |	Barcode	|	testCaseId 	| 
		| between | 15 | 20	|	item purchase between units 15.00 and 20.00 |	85214796301	|	70916		|	
@Regression		
Scenario Outline: FR: Coupon - Item purchase | total units >= 4
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\113.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>" 
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
	Then : Clear cache
	Then : Send data to Kafka Queue "<testCaseId>"
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity |	 TestCaseName |	Barcode	|testCaseId 	| 
		| >= | 4 |		Item purchase total units >=4 |	85214796301	|	70914		|	
		
@Regression	
Scenario Outline: FR: Coupon - Item purchase | total units == 10 
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\114.jpg"
	Then : Add Targeting for itempurchase group with "<Condition>" and "<quantity>"
	Then : Publish "<TestCaseName>" 
	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
	Then : Clear cache
 Then : Send data to Kafka Queue "<testCaseId>" 
	
	#put in same add group provide rank as well
	Examples: 
		| Condition | quantity |	 TestCaseName |	Barcode	|testCaseId 	| 
		| == | 10 |		Item purchase total units ==10 |	85214796301	|	70915		|	
		
@Regression
Scenario Outline: FR: Coupon - Total order | total spend + Payment method | Foodstamp
	Given : Url of application Navigate to webpage 
	Then : Fill credentails 
	Then : Click on Login 
	Then : Change region to "France"
	Then : Search for the Campaign name
  When : Campagin created Then create Ad Group with "<TestCaseName>" 
	Then : Create ad using Ad Galary with retail 
	Then : Fill ad form for retailer with "<Barcode>" and "Vault France Template\116.jpg"
	Then : Add Targeting group with Total Order first "<Condition>" and "<value>"
	Then : Add Payment Method with Foodstamp
	Then : Publish "<TestCaseName>"
	Then : Verify DMP Promotion on server "AZURE" and "FRA" 
	Then : Clear cache
	Then : Send data to Kafka Queue "<testCaseId>"
	
	#put in same add group provide rank as well
	Examples: 
		 | TestCaseName |	Barcode	|Condition	|value	|testCaseId 	| 
	 | Total order  and payment method foodstamp |	85214796301	| >=	|	1	|70927		|	
	 
	 

